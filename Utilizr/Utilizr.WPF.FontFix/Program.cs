﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Utilizr.Windows;
using Utilizr.Logging;

namespace Utilizr.WPF.FontFix
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = null;
            string processArgs = null;

            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledException);
            }
            catch (Exception e)
            {
                Log.Exception(e);
                throw; 
            }

            Log.Info("FontFix Started");

            //Get passed in filename and process arugments of the application to start once complete.
            if (args.Length > 0)
                    filename = args[0];

                if (args.Length > 1)
                    processArgs = args[1];

                //Repair fongts
                RepairFonts fonts = new RepairFonts();
                fonts.OnUpdateStatus += (s) =>
                {
                    Log.Info(s);
                    Console.WriteLine(s);
                };

                fonts.Repair();

                //Start the passed in application
                if (filename != null && File.Exists(filename))
                {
                    try
                    {
                        Process.Start(filename, processArgs);
                    }
                    catch (Exception e)
                    {
                        Log.Info($"Failed to start application: {filename} {processArgs}");
                        Log.Info($"Excepyion: {e.Message}");
                        throw;
                    }
                }

                Log.Info("FontFix Finished");

                //Console.ReadLine();
        }

        static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject != null)
            {
                Log.Exception((e.ExceptionObject as Exception));
            }
        }
    }
}
