﻿using System;
using Utilizr.Info;

namespace Utilizr.TestOSBitness
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();

            if (Platform.Is64BitProcess)
                Console.Write("64 bit app ");
            else if (Platform.Is32BitProcess)
                Console.Write("32 bit app ");
            else
                Console.Write("Unknown bitness app ");
            
            if (Platform.Is64BitOS)
                Console.Write("running on a 64 bit computer");
            else
                Console.Write("running on a 32 bit computer");

            Console.ReadLine();
        }
    }
}
