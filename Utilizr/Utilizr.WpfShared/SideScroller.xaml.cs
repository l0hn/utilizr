﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilizr.Collections;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class SideScroller : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register(
                nameof(Items),
                typeof(ObservableCollection),
                typeof(SideScroller),
                new PropertyMetadata(null, OnItemsChanged)
            );

        private static void OnItemsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var scroller = sender as SideScroller;
            if (scroller == null)
                return;

            if (e.OldValue != null)
                ((ObservableCollection)e.OldValue).CollectionChanged -= scroller.Items_CollectionChanged;

            if (e.NewValue != null)
                ((ObservableCollection)e.NewValue).CollectionChanged += scroller.Items_CollectionChanged;

            scroller.SetCurrent();
        }

        public ObservableCollection Items
        {
            get { return (ObservableCollection)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }


        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register(
                nameof(SelectedIndex),
                typeof(int),
                typeof(SideScroller),
                new PropertyMetadata(-1)
            );

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }


        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register(
                nameof(SelectedItem),
                typeof(object),
                typeof(SideScroller),
                new PropertyMetadata(null)
            );

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }


        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register(
                nameof(ItemTemplate),
                typeof(DataTemplate),
                typeof(SideScroller),
                new PropertyMetadata(null)
            );

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }


        public static readonly DependencyProperty BackgroundSelectedProperty =
            DependencyProperty.Register(
                nameof(BackgroundSelected),
                typeof(Brush),
                typeof(SideScroller),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SideScroller), nameof(BackgroundSelected))
                )
            );

        public Brush BackgroundSelected
        {
            get { return (Brush)GetValue(BackgroundSelectedProperty); }
            set { SetValue(BackgroundSelectedProperty, value); }
        }


        public static readonly DependencyProperty BackgroundUnselectedProperty =
            DependencyProperty.Register(
                nameof(BackgroundUnselected),
                typeof(Brush),
                typeof(SideScroller),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SideScroller), nameof(BackgroundUnselected))
                )
            );

        public Brush BackgroundUnselected
        {
            get { return (Brush)GetValue(BackgroundUnselectedProperty); }
            set { SetValue(BackgroundUnselectedProperty, value); }
        }


        public static readonly DependencyProperty ArrowLeftProperty =
            DependencyProperty.Register(
                nameof(ArrowLeft),
                typeof(Geometry),
                typeof(SideScroller),
                new PropertyMetadata(null)
            );

        /// <summary>
        /// Geometry to set left arrow's Path.Data. Null if not arrow needed.
        /// </summary>
        public Geometry ArrowLeft
        {
            get { return (Geometry)GetValue(ArrowLeftProperty); }
            set { SetValue(ArrowLeftProperty, value); }
        }


        public static readonly DependencyProperty ArrowRightProperty =
            DependencyProperty.Register(
                nameof(ArrowRight),
                typeof(Geometry),
                typeof(SideScroller),
                new PropertyMetadata(null)
            );

        /// <summary>
        /// Geometry to set left arrow's Path.Data. Null if not arrow needed.
        /// </summary>
        public Geometry ArrowRight
        {
            get { return (Geometry)GetValue(ArrowRightProperty); }
            set { SetValue(ArrowRightProperty, value); }
        }


        public static readonly DependencyProperty ArrowBrushProperty =
            DependencyProperty.Register(
                nameof(ArrowBrush),
                typeof(Brush),
                typeof(SideScroller),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SideScroller), nameof(ArrowBrush))
                )
            );

        public Brush ArrowBrush
        {
            get { return (Brush)GetValue(ArrowBrushProperty); }
            set { SetValue(ArrowBrushProperty, value); }
        }


        public static readonly DependencyProperty ArrowThicknessProperty =
            DependencyProperty.Register(
                nameof(ArrowThickness),
                typeof(double),
                typeof(SideScroller),
                new PropertyMetadata(2.0)
            );

        public double ArrowThickness
        {
            get { return (double)GetValue(ArrowThicknessProperty); }
            set { SetValue(ArrowThicknessProperty, value); }
        }


        public static readonly DependencyProperty ArrowThicknessMouseOverProperty =
            DependencyProperty.Register(
                nameof(ArrowThicknessMouseOver),
                typeof(double),
                typeof(SideScroller),
                new PropertyMetadata(4.0)
            );

        public double ArrowThicknessMouseOver
        {
            get { return (double)GetValue(ArrowThicknessMouseOverProperty); }
            set { SetValue(ArrowThicknessMouseOverProperty, value); }
        }


        public static readonly DependencyProperty ArrowMarginProperty =
            DependencyProperty.Register(
                nameof(ArrowMargin),
                typeof(Thickness),
                typeof(SideScroller),
                new PropertyMetadata(default(Thickness))
            );

        public Thickness ArrowMargin
        {
            get { return (Thickness)GetValue(ArrowMarginProperty); }
            set { SetValue(ArrowMarginProperty, value); }
        }


        public static readonly DependencyProperty ArrowColumnWidthProperty =
            DependencyProperty.Register(
                nameof(ArrowColumnWidth),
                typeof(GridLength),
                typeof(SideScroller),
                new PropertyMetadata(GridLength.Auto)
            );

        /// <summary>
        /// If ArrowThicknessMouseOver is bigger than ArrowThickness, the column size will also
        /// increase during MouseOver, which may be undesirable. Explicitly set the column size
        /// to solve this issue.
        /// </summary>
        public GridLength ArrowColumnWidth
        {
            get { return (GridLength)GetValue(ArrowColumnWidthProperty); }
            set { SetValue(ArrowColumnWidthProperty, value); }
        }



        public SideScroller()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
            Items = new ObservableCollection();
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            SetCurrent();
        }

        private void SetCurrent()
        {
            // Set to first view if possible, and nothing else previously selected
            if (Items != null && Items.Count > 0 && SelectedIndex < 0)
                SelectedIndex = 0;
        }

        public void Next()
        {
            int potentialIndex = SelectedIndex + 1;

            if (Items != null && Items.Count > potentialIndex && potentialIndex > -1)
                SelectedIndex = potentialIndex;
        }

        public void Previous()
        {
            int potentialIndex = SelectedIndex - 1;

            if (Items != null && Items.Count > potentialIndex && potentialIndex > -1)
                SelectedIndex = potentialIndex;
        }

        private void LeftArrow_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Previous();
        }

        private void RightArrow_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Next();
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public Visibility PageIndicatorVisibility
        {
            get
            {
                if (Items.Count <= 1)
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }
        }
    }
}
