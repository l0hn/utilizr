﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Converters
{
    public class UriImageConverter: MarkupExtension, IValueConverter
    {
        private static UriImageConverter _converter;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ResourceHelper.GetImageSource((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null)
            {
                _converter = new UriImageConverter();
            }
            return _converter;
        }
    }
}
