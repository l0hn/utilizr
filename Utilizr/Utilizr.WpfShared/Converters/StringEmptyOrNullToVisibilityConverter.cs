﻿using GetText;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class StringEmptyOrNullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null) //use default
                parameter = StringEmptyOrNullVisibilityType.CollapsedOnNull;

            bool validString = !string.IsNullOrEmpty(value as string);
            bool validITranslatable = !string.IsNullOrEmpty((value as ITranslatable)?.Translation);

            if (validString || validITranslatable)
                return Visibility.Visible;

            return ((StringEmptyOrNullVisibilityType)parameter) == StringEmptyOrNullVisibilityType.HiddenOnNull
                ? Visibility.Hidden
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException($"Cannot convert {value} back to original object.");
        }
    }

    public enum StringEmptyOrNullVisibilityType
    {
        CollapsedOnNull,
        HiddenOnNull,
    }
}
