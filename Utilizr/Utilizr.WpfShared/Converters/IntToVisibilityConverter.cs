﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public enum IntToVisibilityConverterType
    {
        CollapsedOnZero,
        HiddenOnZero
    }

    public class IntToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int intValue = (int)value;

            if (intValue != 0)
                return Visibility.Visible;

            if (parameter == null)
                parameter = IntToVisibilityConverterType.CollapsedOnZero; // default

            return (IntToVisibilityConverterType)parameter == IntToVisibilityConverterType.CollapsedOnZero
                ? Visibility.Collapsed
                : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
