﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class DoubleAdditionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return System.Convert.ToDouble(value) + System.Convert.ToDouble(parameter);
            }
            catch
            {
                return value;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return System.Convert.ToDouble(value) - System.Convert.ToDouble(parameter);
            }
            catch
            {
                return value;
            }
        }
    }
}
