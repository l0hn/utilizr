﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class MultiplicationConverter: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var val = (double)values[0];
                for (int i = 1; i < values.Length; i++)
                {
                    val = val * (double)values[i];
                }
                return val;
            }
            catch (Exception)
            {
                
            }
            return 0.0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}