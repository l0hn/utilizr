﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class NullReferenceToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null) //use default
                parameter = NullReferenceVisibilityType.CollapsedOnNull;

            if (value != null)
                return Visibility.Visible;

            return ((NullReferenceVisibilityType)parameter) == NullReferenceVisibilityType.HiddenOnNull
                ? Visibility.Hidden
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException($"Cannot convert {value} back to original object.");
        }
    }

    public enum NullReferenceVisibilityType
    {
        CollapsedOnNull,
        HiddenOnNull,
    }

    public class InvertNullReferenceToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null) //use default
                parameter = InvertNullReferenceVisibilityType.CollapsedOnInstance;

            if (value == null)
                return Visibility.Visible;

            return ((InvertNullReferenceVisibilityType)parameter) == InvertNullReferenceVisibilityType.HiddenOnInstance
                ? Visibility.Hidden
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public enum InvertNullReferenceVisibilityType
    {
        CollapsedOnInstance,
        HiddenOnInstance,
    }
}
