﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class InvertBooleanToVisibilityConverter : IValueConverter
    {
        private BooleanToVisibilityConverter _converter;

        public InvertBooleanToVisibilityConverter()
        {
            _converter = new BooleanToVisibilityConverter();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return _converter.Convert(!((bool)value), targetType, parameter, culture);            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
           return _converter.ConvertBack(value, targetType, parameter, culture);
        }
    }
}
