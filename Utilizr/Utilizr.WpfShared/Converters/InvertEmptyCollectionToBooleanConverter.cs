﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class InvertEmptyCollectionToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colVal = value as ICollection;
            if (colVal == null)
                throw new ArgumentException($"Expected {nameof(value)} to derive from {nameof(ICollection)}");

            return colVal.Count != 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
