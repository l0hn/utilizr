﻿using System;
using System.Globalization;
using System.Windows.Data;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Converters
{
    public class UriVideoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ResourceHelper.GetVideoSource((string)value, parameter as string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class UriVideoLocalisedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var localised = ResourceHelper.GetLocalisedResourceKey((string)value, out bool notUsed, parameter as string);
            return ResourceHelper.GetVideoSource(localised, parameter as string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotSupportedException();
        }
    }
}