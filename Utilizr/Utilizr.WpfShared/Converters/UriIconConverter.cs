﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Converters
{
    public class UriIconConverter: MarkupExtension, IValueConverter
    {
        private static UriIconConverter _instance;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_instance == null)
            {
                _instance = new UriIconConverter();
            }
            return _instance;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ResourceHelper.GetIconSource((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
