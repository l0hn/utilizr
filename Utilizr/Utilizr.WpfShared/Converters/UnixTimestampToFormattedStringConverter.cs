﻿using System;
using System.Globalization;
using System.Windows.Data;
using Utilizr.Conversion;

namespace Utilzr.WPF.Converters
{
    public class UnixTimestampToFormattedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime? dt = null;

            if (value is int)
                dt = ((int)value).ToDateTime();
            else if (value is long)
                dt = ((long)value).ToDateTime();

            if (parameter == null || (!dt.HasValue))
                return dt;

            return dt.Value.ToString((string)parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}