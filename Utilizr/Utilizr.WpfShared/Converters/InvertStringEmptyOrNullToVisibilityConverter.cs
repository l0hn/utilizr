﻿using GetText;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public class InvertStringEmptyOrNullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null) //use default
                parameter = InvertStringEmptyOrNullVisibilityType.CollapsedOnValue;

            bool emptyOrNullString = string.IsNullOrEmpty(value as string);
            bool emptyOrNullITranslatable = string.IsNullOrEmpty((value as ITranslatable)?.Translation);

            if (emptyOrNullString && emptyOrNullITranslatable)
                return Visibility.Visible;

            return ((InvertStringEmptyOrNullVisibilityType)parameter) == InvertStringEmptyOrNullVisibilityType.HiddenOnValue
                ? Visibility.Hidden
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException($"Cannot convert {value} back to original object.");
        }
    }

    public enum InvertStringEmptyOrNullVisibilityType
    {
        CollapsedOnValue,
        HiddenOnValue,
    }
}
