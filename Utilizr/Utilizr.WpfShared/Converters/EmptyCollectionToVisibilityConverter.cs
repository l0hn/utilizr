﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public enum EmptyCollectionToVisibilityConverterType
    {
        CollapsedOnTrue,
        HiddenOnTrue,
    }

    public class EmptyCollectionToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colVal = value as ICollection;
            if (colVal == null)
                throw new ArgumentException($"Expected {nameof(value)} to derive from {nameof(ICollection)}");

            bool isEmpty = colVal.Count == 0;
            var converterType = parameter is EmptyCollectionToVisibilityConverterType
                ? (EmptyCollectionToVisibilityConverterType)parameter
                : EmptyCollectionToVisibilityConverterType.CollapsedOnTrue;


            if (converterType == EmptyCollectionToVisibilityConverterType.CollapsedOnTrue)
            {
                return isEmpty
                    ? Visibility.Collapsed
                    : Visibility.Visible;
            }

            return isEmpty
                ? Visibility.Hidden
                : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
