﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Converters
{
    public class ImageSourceAverageColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var bmp = value as BitmapSource;
                if (bmp == null)
                    return null;

                return new SolidColorBrush(bmp.GetAverageColor());
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
