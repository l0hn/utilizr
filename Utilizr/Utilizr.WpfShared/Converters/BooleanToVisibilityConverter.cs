﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Utilzr.WPF.Converters
{
    public enum BooleanToVisivilityConverterType
    {
        CollapsedOnFalse,
        HiddenOnFalse,
    }

    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibile = (bool)value;

            if (visibile)
                return Visibility.Visible;

            if (parameter is BooleanToVisivilityConverterType && ((BooleanToVisivilityConverterType)parameter) == BooleanToVisivilityConverterType.HiddenOnFalse)
                return Visibility.Hidden;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;

            if (visibility == Visibility.Visible)
                return true;

            return false;
        }
    }
}