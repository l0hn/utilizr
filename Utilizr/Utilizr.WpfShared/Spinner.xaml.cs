﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Utilzr.WPF
{
    /// <summary>
    /// Interaction logic for Spinner.xaml
    /// </summary>
    public partial class Spinner
    {
        public static readonly DependencyProperty AnimateProperty = DependencyProperty.Register(
            nameof(Animate), typeof (bool), typeof (Spinner), new PropertyMetadata(true));

        public bool Animate
        {
            get { return (bool) GetValue(AnimateProperty); }
            set { SetValue(AnimateProperty, value); }
        }

        public static readonly DependencyProperty SpinnerContentProperty = DependencyProperty.Register(
            nameof(SpinnerContent), typeof (FrameworkElement), typeof (Spinner), new PropertyMetadata(default(FrameworkElement)));

        public FrameworkElement SpinnerContent
        {
            get { return (FrameworkElement) GetValue(SpinnerContentProperty); }
            set { SetValue(SpinnerContentProperty, value); }
        }

        public Spinner()
        {
            DataContext = this;
            InitializeComponent();
        }
    }
}