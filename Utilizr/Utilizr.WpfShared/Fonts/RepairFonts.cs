﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media;
using Utilizr;
using Utilizr.Info;
using Utilizr.Logging;
using System.Threading;
using System.Windows;
using GetText;

namespace Utilizr.Windows
{
    public class RepairFonts
    {
        private const string REG_KEY_FONTS = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts";
        private const string FONTFIX_FILENAME = "Utilizr.WPF.FontFix.exe";
        private readonly static string FONTFIX_FILEPATH = $@"{AppInfo.AppDirectory}\{FONTFIX_FILENAME}";

        public delegate void UpdateStatus(string text);
        public UpdateStatus OnUpdateStatus;

        #region Private Properties


        #endregion

        #region Public Methods
        public void Repair()
        {
            RepairFontRegistry();

            DeleteFontCaches();

            //Notify other applications of font changes
            SystemFontsChanged();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Repairs font registry values, Invalid font entrys are either repaired or removed
        /// </summary>
        private void RepairFontRegistry()
        {
            OnUpdateStatus($"Checking registry");
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(REG_KEY_FONTS, true))
                {
                    var values = key.GetValueNames();
                    foreach (var subKey in values)
                    {
                        string fileName = key.GetValue(subKey).ToString();
                        string filePath = null;

                        //Check filename does not conatin illegal chars
                        if (fileName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                        {
                            //Invalid Key, Illegal chars in path
                            OnUpdateStatus($"Entry contains illegal characters {subKey}");

                            key.DeleteValue(subKey);

                            //Remove any invalid characters in the filename and see if it fixes it
                            string cleanName = RemoveInvalidCharacters(fileName);
                            if (cleanName != fileName)
                            {
                                filePath = Path.Combine(FontDirectory, cleanName);
                                if (File.Exists(filePath) && UpdateFontTimeStamp(filePath))
                                {
                                    //Update registry to cleaned name
                                    Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts", subKey, cleanName, RegistryValueKind.String);

                                    OnUpdateStatus($"Font entry repaired {subKey}");
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                filePath = Path.Combine(FontDirectory, fileName);
                            }
                            catch
                            {
                                Log.Info(fileName);
                                throw;
                            }

                            if (File.Exists(filePath))
                            {
                                //Check and fix timestamp 
                                if (!UpdateFontTimeStamp(filePath))
                                {
                                    //Could not fix timestamp
                                    key.DeleteValue(subKey);
                                }
                            }
                            else
                            {
                                //Font does not exist
                                OnUpdateStatus($"Font not found {subKey}");

                                key.DeleteValue(subKey);

                                //Try to remove any invalid characters in the filename
                                string cleanName = RemoveInvalidCharacters(fileName);
                                if (cleanName != fileName)
                                {
                                    filePath = Path.Combine(FontDirectory, cleanName);

                                    if (File.Exists(filePath) && UpdateFontTimeStamp(filePath))
                                    {
                                        //Update registry to cleaned name
                                        Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts", subKey, cleanName, RegistryValueKind.String);

                                        OnUpdateStatus($"Font entry repaired {subKey}");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
                throw;
            }
        }

        /// <summary>
        /// Fixies last write time of fonts that have an Invalid Win32 FileTime
        /// </summary>
        private bool UpdateFontTimeStamp(string filename)
        {
            try
            {
                File.GetLastWriteTimeUtc(filename);
                return true;
            }
            catch (ArgumentOutOfRangeException e)
            {
                OnUpdateStatus?.Invoke($"Invalid Last write time {e.Message}");
                //Set last write time
                try
                {
                    File.SetLastWriteTime(filename, DateTime.Now);
                    return true;
                }
                catch (Exception e1)
                {
                    OnUpdateStatus?.Invoke($"Set font last write time failed {e1.Message}");
                    return false;
                }
            }
            catch (Exception e2)
            {
                OnUpdateStatus?.Invoke($"UpdateFontTimeStamps error: {e2.Message}");
                return false;
            }
        }

        /// <summary>
        /// Deletes WPF font cache
        /// </summary>
        /// <see cref="https://support.microsoft.com/en-us/kb/937135"/>
        /// <param name="serviceName"></param>
        private void DeleteFontCache(string serviceName)
        {
            if (StringEx.IsNullOrWhitespace(serviceName))
            {
                Log.Info("Invalid service name");
                return;
            }

            OnUpdateStatus?.Invoke("Deleting font cache: " + serviceName);


            bool restartService = false;

            //Stop Font cache service            
            var timeoutMilliseconds = 5000; //5 seconds

            TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

            ServiceController service = new ServiceController(serviceName);
            try
            {
                if (service.Status == ServiceControllerStatus.Running)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                    restartService = true;
                }
            }
            catch (InvalidOperationException)
            {
                //Service does not exist       
            }

            try
            {
                //Delete font cache
                string fontCacheFilename;
                if (Platform.IsXPOrLower)
                {
                    //location %systemdrive%\Documents and Settings\LocalService\Local Settings\Application Data

                    //get system drive 
                    string systemDrive = Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));

                    fontCacheFilename = $@"{systemDrive}\\Documents and Settings\LocalService\Local Settings\Application Data\{serviceName}.dat";
                }
                else
                {
                    //location %windir%\ServiceProfiles\LocalService\AppData\Local folder 
                    fontCacheFilename = $@"{WindowsDirectory}\ServiceProfiles\LocalService\AppData\Local\{serviceName}.dat";
                }

                if (File.Exists(fontCacheFilename))
                {
                    File.Delete(fontCacheFilename);
                }
            }
            finally
            {
                //Re-start font cache service
                if (restartService && service.Status == ServiceControllerStatus.Stopped)
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
            }
        }

        /// <summary>
        /// Deletes WPF 3.0.0.0 & 4.0.0.0 font caches
        /// </summary>
        private void DeleteFontCaches()
        {
            DeleteFontCache("FontCache3.0.0.0");
            DeleteFontCache("FontCache4.0.0.0");
        }

        private void SystemFontsChanged()
        {
            //Send message to other applications
            OnUpdateStatus?.Invoke("Notify other applications");
            //SendMessage(new IntPtr(Win32.HWND_BROADCAST), WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
            Win32.PostMessage(new IntPtr(Win32.HWND_BROADCAST), Win32.WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
        }
        #endregion

        #region Static properties
        /// <summary>
        /// Windows font directory
        /// </summary>
        private static string FontDirectory
        {
            get
            {
                DirectoryInfo dirWindowsFolder = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.System));
                string fontDirectory = Path.Combine(dirWindowsFolder.FullName, "Fonts");

                if (!Directory.Exists(fontDirectory))
                {
                    throw new Exception("Font directory not found");
                }

                return fontDirectory;
            }
        }
        /// <summary>
        /// Get windows directory
        /// </summary>
        private static string WindowsDirectory
        {
            get
            {
                return Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.System)).FullName;
            }
        }
        /// <summary>
        /// Checks if passed exception obect is a TypeInitializationException
        /// </summary>
        /// <remarks>
        /// Foreign languages will all contain the string "System.Windows.Media.FontFamily"
        /// The rest of the message text is written in the local language.
        /// This should work for any lanuage
        /// </remarks>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool IsFontTypeIntializerException(Exception e)
        {
            do
            {
                if (e is TypeInitializationException && e.Message.Contains("System.Windows.Media.FontFamily"))
                {
                    return true;
                }
                e = e.InnerException;

            } while (e != null);

            return false;
        }
        /// <summary>
        /// Indicates if WPF fonts are broken
        /// </summary>
        public static bool FontsBroken
        {
            get
            {
                try
                {
                    var fonts = Fonts.GetFontFamilies(FontDirectory);
                    return false;
                }
                catch
                {
                    return true;
                }
            }
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Launchs Utilizr.WPF.FontFix.exe to fix fonts
        /// </summary>
        /// <param name="exePath"></param>
        /// <param name="processArgs"></param>
        public static void TryRepairSystemFonts(string exePath, string processArgs)
        {
            Process FontFixProcess = Process.Start(FONTFIX_FILEPATH, $@"{exePath} {processArgs}");
            Environment.Exit(0);
        }
        /// <summary>
        /// Restarts application as adminstrator to fix fonts
        /// </summary>
        /// <param name="appRunningMutex"></param>
        public static void TryRepairSystemFonts(Mutex appRunningMutex)
        {
            try
            {
                if (User.IsAdministrator())
                {
                    RepairSystemFonts(appRunningMutex);
                }
                else if (Environment.GetCommandLineArgs().Contains("--FontFix") == false)
                {
                    //Release mutex so the other instance can start
                    appRunningMutex.ReleaseMutex();

                    //Run application as admin so it can repair fonts
                    Log.Info($"Restarting application as admin to repair the fonts: {System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName}");
                    ProcessStartInfo processStartInfo = new ProcessStartInfo(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, "--FontFix");
                    processStartInfo.Verb = "runas";
                    Process.Start(processStartInfo);
                }

                Environment.Exit(0);
            }
            catch (Exception e)
            {
                Log.Exception("REPAIR FONTS", e);
            }
        }

        /// <summary>
        /// Fixes fonts and restarts the application
        /// </summary>
        /// <param name="appRunningMutex"></param>
        public static void RepairSystemFonts(Mutex appRunningMutex)
        {
            try
            {
                if (User.IsAdministrator())
                {
                    Log.Info("REPAIR FONTS", "Running as admin");

                    //Repair the font, write status to log
                    RepairFonts fonts = new RepairFonts();
                    fonts.OnUpdateStatus += (s) =>
                    {
                        Log.Info("REPAIR FONTS", s);
                    };

                    fonts.Repair();

                    Log.Info("REPAIR FONTS", $"Fonts repaired, restarting application: {System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName}");

                    //Restart the app - release mutex so the other instance can start
                    appRunningMutex.ReleaseMutex();
                    Process FontFixProcess = Process.Start(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, "--FontsFixed");
                }
                else
                {
                    Log.Info("REPAIR FONTS", "Failed repair, not an administrator");
                    MessageBox.Show(L._("Please run this program from the Administrator account"));
                }

                Environment.Exit(0);
            }
            catch (Exception e)
            {
                Log.Exception("REPAIR FONTS", e);
            }
        }

        /// <summary>
        /// Removes Path.GetInvalidFileNameChars() characters from a filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>string with invalid characters removed</returns>
        public static string RemoveInvalidCharacters(string filename)
        {
            // Replace invalid characters with an empty string.
            var InvalidChars = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex($"[{Regex.Escape(InvalidChars)}]");
            return r.Replace(filename, "");
        }
        #endregion
    }
}
