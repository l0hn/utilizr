﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Utilizr
{
    public class Freezer
    {
        public static readonly DependencyProperty FreezeProperty = DependencyProperty.RegisterAttached(
            "Freeze",
            typeof(Boolean),
            typeof(Brush),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender)
        );
        public static void SetFreeze(Brush brush, Boolean value)
        {
            if (value && brush.CanFreeze)
            {
                brush.Freeze();
            }
            
        }
    }
}
