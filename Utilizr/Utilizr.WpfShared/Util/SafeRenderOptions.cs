﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Utilzr.WPF
{
    public static class SafeRenderOptions
    {
        public static readonly DependencyProperty UnscaledProperty = DependencyProperty.RegisterAttached("Unscaled", typeof(bool), typeof(SafeRenderOptions), (PropertyMetadata)new UIPropertyMetadata(false, UnscaledPropertyChanged));

        private static void UnscaledPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var img = dependencyObject as Image;
            if (img == null)
                return;

            if ((bool)e.NewValue)
            {
                if (false && Enum.IsDefined(typeof (BitmapScalingMode), 3))
                {
                    RenderOptions.SetBitmapScalingMode(img, (BitmapScalingMode) 3);
                    RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);
                }
                else
                {
                    RenderOptions.SetBitmapScalingMode(img, BitmapScalingMode.HighQuality);
                }

                img.Stretch = Stretch.None;
                img.SnapsToDevicePixels = false;
            }
            else
            {
                RenderOptions.SetBitmapScalingMode(img, BitmapScalingMode.Unspecified);
                RenderOptions.SetEdgeMode(img, EdgeMode.Unspecified);
                img.Stretch = default(Stretch);
            }
        }

        [AttachedPropertyBrowsableForType(typeof(Image))]
        public static bool GetUnscaled(DependencyObject target)
        {
            if (target == null)
                throw new ArgumentNullException("target");
            return (bool)target.GetValue(SafeRenderOptions.UnscaledProperty);
        }


        public static void SetUnscaled(DependencyObject target, bool unscaled)
        {
            if (target == null)
                throw new ArgumentNullException("target");
            target.SetValue(UnscaledProperty, unscaled);
        }
    }
}
