﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Media.Animation;

namespace Utilzr.WPF
{
    public class AnimatedHeight
    {
        public static readonly DependencyProperty ExpandedHeightProperty = DependencyProperty.RegisterAttached(
            "TargetHeight",
            typeof(double),
            typeof(AnimatedHeight),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsMeasure, ContentElementPropertyChangedCallback));

        private static void ContentElementPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Debug.WriteLine(e.NewValue);
        }

        public static void SetTargetHeight(DependencyObject container, double value)
        {
            container.SetValue(ExpandedHeightProperty, value);
        }

        public static double GetTargetHeight(DependencyObject container)
        {
            return (double)container.GetValue(ExpandedHeightProperty);
        }



        public static readonly DependencyProperty ExpandProperty = DependencyProperty.RegisterAttached(
            "Expand",
            typeof(bool),
            typeof(AnimatedHeight),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsMeasure, PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (FrameworkElement) d;
            var expand = (bool) e.NewValue;
            var targetHeight = GetTargetHeight(d);
            var time = new Duration(TimeSpan.FromMilliseconds(Math.Min(targetHeight*3, 200)));
            var animation = new DoubleAnimation(expand ? targetHeight : 0, time);
            animation.AccelerationRatio = 1;
            element.BeginAnimation(FrameworkElement.HeightProperty, animation, HandoffBehavior.SnapshotAndReplace);
        }

        public static void SetExpand(DependencyObject container, bool value)
        {
            container.SetValue(ExpandProperty, value);
        }

        public static bool GetExpand(DependencyObject container)
        {
            return (bool)container.GetValue(ExpandProperty);
        }
    }
}
