﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Xml;
using Utilizr;
using Utilizr.FileSystem;
using Utilizr.Info;

namespace Utilzr.WPF.Util
{ 
    public class ThemeMaker
    {
        public static ResourceDictionary GenerateThemeResourceDictionary(Theme theme, string outputFile)
        {
            if (!UriParser.IsKnownScheme("pack"))
                new System.Windows.Application();

            var destinationDict = new ResourceDictionary();

            Console.WriteLine(AppInfo.AppDirectory);

            var sourceDir =
                Path.GetFullPath(PathHelper.Combine(AppInfo.AppDirectory, "..", "..", ".."));

            sourceDir = Path.Combine(sourceDir, "Utilizr.WpfShared");

            Console.WriteLine(sourceDir);

            var wait = new ManualResetEvent(false);

            var thread = new Thread(() =>
            {
                try
                {
                    var themeDict = GetResourceDictionary($"Themes/{theme}");
                    FlattenResourceDictionary(themeDict, destinationDict);
                    SaveResourceDictionary(destinationDict, outputFile);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                finally
                {
                    wait.Set();
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            wait.WaitOne();

            return destinationDict;
        }


        public static void FlattenResourceDictionary(ResourceDictionary source, ResourceDictionary dest)
        {
            foreach (DictionaryEntry kvp in source)
            {
                Console.WriteLine($"saving {kvp.Key}: {kvp.Value}");
                dest[kvp.Key] = kvp.Value;
            }

            foreach (var mergedDictionary in source.MergedDictionaries)
            {
                FlattenResourceDictionary(mergedDictionary, dest);
            }
        }

        public static ResourceDictionary GetResourceDictionary(string name)
        {
            ResourceDictionary rd = new ResourceDictionary();
            rd.Source = new Uri($"pack://application:,,,/Utilzr.WPF;component/{name}.xaml");
            return rd;
        }

        public static void SaveResourceDictionary(ResourceDictionary dict, string outFile)
        {
            var xmlSettings = new XmlWriterSettings()
            {
                Indent = true,
                NewLineOnAttributes = true,
            };
            using (var stream = File.Create(outFile))
            using (var writer = XmlTextWriter.Create(stream, xmlSettings))
            {
                XamlWriter.Save(dict, writer);
            }
        }

        static Uri GetThemeUri(Theme theme)
        {
            return new Uri($"pack://application:,,,/Utilzr.WPF;component/Themes/{theme}.xaml");
        }
    }

    /// <summary>
    /// Utilizr.Wpf.Theme
    /// </summary>
    public enum Theme
    {
        generic,
        v1,
        v2,
    }
}
