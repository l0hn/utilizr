﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Utilizr;
using Utilizr.FileSystem;
using Utilizr.Info;

namespace Utilzr.WPF.Util
{
    public class ResourceLoadable: Loadable<ResourceLoadable>
    {
        [JsonProperty("_resources")]
        private Dictionary<string, byte[]> _resources;

        public override string EmbeddedResourceName => "RESOURCES.URES";

        public override bool ReadOnly => true;

        public ResourceLoadable()
        {
            _resources = new Dictionary<string, byte[]>();
        }

        public void Add(string resourceKey, byte[] resourceData)
        {
            _resources[resourceKey.Replace("\\", "/").ToLowerInvariant()] = resourceData;
        }

        public byte[] Get(string resourceKey)
        {
            //resourceKey = $"{ResourceHelper.ResourceDir}/{resourceKey}".Trim('/');
            resourceKey = resourceKey.Replace("\\", "/").ToLowerInvariant();
            byte[] dat = null;
            _resources?.TryGetValue(resourceKey, out dat);
            return dat;
        }

        protected override string CustomDeserializeStep(string source)
        {
            return source;
        }

        protected override string CustomSerializeStep(string source)
        {
            return source;
        }

        protected override string getLoadPath()
        {
            return PathHelper.Combine(AppInfo.AppDirectory, "resources.ures");
        }
    }
}
