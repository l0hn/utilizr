﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Interop;
using Utilizr.Windows;
using Screen = System.Windows.Forms.Screen;

namespace Utilzr.WPF.Util
{
    public static class WindowHelper
    {
        public static void BringToFront(Window window)
        {
            IntPtr windowHandle = new WindowInteropHelper(window).Handle;
            Win32.SetForegroundWindow(windowHandle);
        }

        public static void DisableMaximiseButton(Window window)
        {
            var handle = new WindowInteropHelper(window).Handle;
            Win32.SetWindowLong(handle, Win32.GWL_STYLE, Win32.GetWindowLong(handle, Win32.GWL_STYLE) & ~Win32.WS_MAXIMIZEBOX);
        }

        public static Window GetActiveWindow()
        {
            IntPtr active = Win32.GetActiveWindow();

            if (active == IntPtr.Zero)
            {
                return Application.Current.MainWindow;
            }
            else
            {
                var ActiveWindow = Application.Current.Windows.OfType<Window>()
                    .SingleOrDefault(window => new WindowInteropHelper(window).Handle == active);

                return ActiveWindow;
            }
        }

        public static bool HasDetectedFullScreenApp()
        {
            try
            {
                Win32.SHQueryUserNotificationState(out var state);

                return state != Win32.UserNotificationState.AcceptsNotifications &&
                       state != Win32.UserNotificationState.QuietTime;
            }
            catch (Exception)
            {
                return IsForegroundWindowFullScreen(out _);
            }
        }

        /// <summary>
        /// Whether the foreground window on the primary screen is using all screen space.
        /// Any error assumes the screen is not occupying all screen space.
        /// </summary>
        /// <param name="processName"></param>
        public static bool IsForegroundWindowFullScreen(out string processName)
        {
            return IsForegroundWindowFullScreen(null, out processName);
        }

        /// <summary>
        /// Whether the foreground window on the specified screen is using all screen space.
        /// Any error assumes the screen is not occupying all screen space.
        /// </summary>
        /// <param name="screen"></param>
        /// <param name="processName"></param>
        public static bool IsForegroundWindowFullScreen(Screen screen, out string processName)
        {
            processName = string.Empty;
            if (screen == null)
                screen = Screen.PrimaryScreen;

            var hWndForeground = Win32.GetForegroundWindow();
            if (hWndForeground == IntPtr.Zero)
                return false;

            try
            {
                Win32.GetWindowThreadProcessId(hWndForeground, out uint processID);
                var proc = Process.GetProcessById((int)processID);
                processName = proc.ProcessName;

                if (processName.ToLowerInvariant() == "explorer")
                {
                    return false;
                }
            }
            catch
            {

            }

            if (!Win32.GetWindowRect(hWndForeground, out Win32.RECT rect))
                return false;


            if (screen.Bounds.Width <= (rect.right - rect.left) && screen.Bounds.Height <= (rect.bottom - rect.top))
                return true;
            
            return false;
        }

        public static bool ApplicationIsActive()
        {
            var activatedHandle = Win32.GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
                return false; // No window is currently activated

            var procId = Process.GetCurrentProcess().Id;
            Win32.GetWindowThreadProcessId(activatedHandle, out uint activeProcId);

            return activeProcId == procId;
        }
    }
}