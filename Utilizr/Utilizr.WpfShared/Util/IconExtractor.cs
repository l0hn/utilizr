﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using Utilizr.FileSystem;

namespace Utilzr.Wpf.Util
{
    public static class IconExtractor
    {
        /// <summary>
        /// Extracts an uninstaller's icon, where the path may or may not contain arguments.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="backupIconUriPath">Backup image URI to use if the icon cannot be extracted from the file</param>
        /// <returns></returns>
        public static byte[] GetInstallerIcon(string filePath, Uri backupIconUri)
        {
            if (string.IsNullOrEmpty(filePath))
                return GetIcon(null, backupIconUri);

            filePath = Environment.ExpandEnvironmentVariables(filePath);
            string pathNoArgs = ExecutableExtensionRemover.GetExecutablePathWithoutArguments(filePath, ".ico");

            //TODO: Temp hack, find better method
            if (pathNoArgs.Equals("msiexec.exe", StringComparison.OrdinalIgnoreCase))
            {
                string msiIconStr = GetMsiProductIconString(filePath);
                pathNoArgs = string.IsNullOrEmpty(msiIconStr)
                    ? Path.Combine(Environment.SystemDirectory, "MsiExec.exe")
                    : msiIconStr;
            }

            //Noticed some paths contain %APPDATA% etc...
            pathNoArgs = Environment.ExpandEnvironmentVariables(pathNoArgs);

            return GetIcon(pathNoArgs, backupIconUri);
        }

        public static byte[] GetIcon(string path, Uri backupIconUri = null)
        {
            try
            {
                var icon = Icon.ExtractAssociatedIcon(path);
                var bmp = icon.ToBitmap();

                byte[] imageData;
                using (var ms = new MemoryStream())
                {
                    bmp.Save(ms, ImageFormat.Png);
                    imageData = ms.ToArray();
                }

                return imageData;
            }
            catch (Exception)
            {
                try
                {
                    if (backupIconUri == null)
                        return new byte[0];

                    //Return default icon
                    var img = BitmapFrame.Create(backupIconUri, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);

                    byte[] data;
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(img);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        encoder.Save(ms);
                        data = ms.ToArray();
                    }
                    return data;
                }
                catch
                {
                    return new byte[0];
                }
            }
        }

        private static string GetMsiProductIconString(string uninstallString)
        {
            var results = new List<string>();
            string regexString = @".*(?<guid>{[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}})";
            var regex = new Regex(regexString, RegexOptions.IgnoreCase);
            var result = regex.Match(uninstallString);
            string guid = result.Groups[1].Value;
            int len = 512;
            var builder = new StringBuilder(len);

            try
            {
                MsiGetProductInfo(guid, "ProductIcon", builder, ref len); //"InstallLocation" "InstallProductName"
                return builder.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        [DllImport("msi.dll", CharSet = CharSet.Unicode)]
        private static extern Int32 MsiGetProductInfo(string product, string property, [Out] StringBuilder valueBuf, ref Int32 len);
    }
}
