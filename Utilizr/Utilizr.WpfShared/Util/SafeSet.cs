﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using Utilizr.Info;
using Utilizr.Logging;

namespace Utilzr.WPF.Util
{
    public static class SafeSetEx
    {
        private static DependencyPropertyDescriptor _effectDescriptor;
        private static Type _blurEffectType;
        private static Type _dropShadowEffectType;

        //blur
        private static PropertyInfo _radiusPropertyInfo;

        //drop shadow
        private static PropertyInfo _blurRadiusPropertyInfo;
        private static PropertyInfo _directionPropertyInfo;
        private static PropertyInfo _colorPropertyInfo;
        private static PropertyInfo _opacityPropertyInfo;
        private static PropertyInfo _shadowDepthPropertyInfo;

        private static Type _textFormattingModeType;
        private static Type _textRenderingModeType; 

        static SafeSetEx()
        {
            if (!Platform.IsNet3SP2OrHigher)
                return;

            _effectDescriptor = DependencyPropertyDescriptor.FromName("Effect", typeof(UIElement), typeof(UIElement));
            _blurEffectType = Type.GetType("System.Windows.Media.Effects.BlurEffect, PresentationCore, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
            _dropShadowEffectType = Type.GetType("System.Windows.Media.Effects.DropShadowEffect, PresentationCore, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");

            _textFormattingModeType = Type.GetType("System.Windows.Media.TextFormattingMode, PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
            _textRenderingModeType = Type.GetType("System.Windows.Media.TextRenderingMode, PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");

            if (_blurEffectType != null)
            {
                _radiusPropertyInfo = _blurEffectType.GetProperty("Radius");
            }

            if (_dropShadowEffectType != null)
            {
                _blurRadiusPropertyInfo = _dropShadowEffectType.GetProperty("BlurRadius");
                _directionPropertyInfo = _dropShadowEffectType.GetProperty("Direction");
                _colorPropertyInfo = _dropShadowEffectType.GetProperty("Color");
                _opacityPropertyInfo = _dropShadowEffectType.GetProperty("Opacity");
                _shadowDepthPropertyInfo = _dropShadowEffectType.GetProperty("ShadowDepth");
            }
        }

        /// <summary>
        /// This will set a blur effect on a UI element if the current framework supports the Effect property, otherwise will do nothing.
        /// </summary>
        public static void SetBlurEffect(this UIElement uiElement, double radius=5, bool enable=true)
        {
            try
            {
                if (!Platform.IsNet3SP2OrHigher)
                    return;

                if (!WpfSpec.IsHardwareRendering)
                    return;

                if (_blurEffectType == null)
                    return;

                var blurEffect = Activator.CreateInstance(_blurEffectType);
                if (blurEffect != null)
                {
                    _radiusPropertyInfo?.SetValue(blurEffect, radius, null);
                    _effectDescriptor?.SetValue(uiElement, enable ? blurEffect : null);
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }


        public static void SetDropShadowEffect(
            this UIElement uiElement, 
            double direction=315, 
            double blurRadius=5, 
            Color colour=new Color(), 
            double opacity=1, 
            double shadowDepth=5, 
            bool enable = true,
            bool hardwareRenderOnly = true)
        {

            try
            {
                if (!Platform.IsNet3SP2OrHigher)
                    return;

                if (hardwareRenderOnly && !WpfSpec.IsHardwareRendering)
                    return;

                if (!enable || _dropShadowEffectType == null)
                {
                    if (uiElement != null)
                        _effectDescriptor.SetValue(uiElement, null);
                }
                else
                {
                    var dropShadowEffect = _effectDescriptor.GetValue(uiElement);

                    if (dropShadowEffect == null)
                    {
                        dropShadowEffect = Activator.CreateInstance(_dropShadowEffectType);
                    }

                    if (dropShadowEffect != null)
                    {
                        _blurRadiusPropertyInfo?.SetValue(dropShadowEffect, blurRadius, null);
                        _directionPropertyInfo?.SetValue(dropShadowEffect, direction, null);
                        _colorPropertyInfo?.SetValue(dropShadowEffect, colour, null);
                        _opacityPropertyInfo?.SetValue(dropShadowEffect, opacity, null);
                        _shadowDepthPropertyInfo?.SetValue(dropShadowEffect, shadowDepth, null);
                        _effectDescriptor?.SetValue(uiElement, dropShadowEffect);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        public static void EnableHighQualityText(DependencyObject o)
        {
            var props = GetAttachedProperties(o).ToList();
            var formatProp = props.FirstOrDefault(i => i.Name == "TextFormattingMode");
            var renderProp = props.FirstOrDefault(i => i.Name == "TextRenderingMode");
            
            
            if (formatProp != null && _textFormattingModeType != null)
                o.SetValue(formatProp, Enum.ToObject(_textFormattingModeType, 0));

            if (renderProp != null && _textRenderingModeType != null)
                o.SetValue(renderProp, Enum.ToObject(_textRenderingModeType, 1));
        }

        public static void SetLayoutRounding(DependencyObject o, bool enable)
        {
            try
            {
                var props = GetAttachedProperties(o).ToList();
                var layoutRounding = props.FirstOrDefault(i => i.Name == "UseLayoutRounding");
                if (layoutRounding != null)
                    o.SetValue(layoutRounding, enable);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public static bool GetLayoutRounding(DependencyObject o)
        {
            try
            {
                var props = GetAttachedProperties(o).ToList();
                var layoutRounding = props.FirstOrDefault(i => i.Name == "UseLayoutRounding");
                if (layoutRounding != null)
                    return (bool)o.GetValue(layoutRounding);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return false;
        }

        static IEnumerable<DependencyProperty> GetAttachedProperties(DependencyObject obj)
        {
            List<DependencyProperty> attached = new List<DependencyProperty>();

            foreach (PropertyDescriptor pd in TypeDescriptor.GetProperties(obj,
                new Attribute[] { new PropertyFilterAttribute(PropertyFilterOptions.All) }))
            {
                DependencyPropertyDescriptor dpd =
                    DependencyPropertyDescriptor.FromProperty(pd);

                if (dpd != null)
                    yield return dpd.DependencyProperty;
            }
        }
    }
}