﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Utilzr.WPF.Util
{
    public static class TextUtil
    {
        public static IEnumerable<string> GetLines(this TextBlock source)
        {
            int offset = 0;
            var text = source.Text;
            var lineStart = source.ContentStart.GetPositionAtOffset(1, LogicalDirection.Forward);
            do
            {
                var lineEnd = lineStart != null 
                    ? lineStart.GetLineStartPosition(1)
                    : null;

                int length = lineEnd != null 
                    ? lineStart.GetOffsetToPosition(lineEnd) 
                    : text.Length - offset;
                
                yield return text.Substring(offset, length);
                offset += length;
                lineStart = lineEnd;
            }
            while (lineStart != null);
        }
    }
}
