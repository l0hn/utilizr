﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace System
{
    public static class Extensions
    {
        public static T GetVisualChild<T>(this DependencyObject parent) where T : Visual
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        public static Color ARGBToColor(this string hex)
        {
            return (Color)ColorConverter.ConvertFromString(hex);
        }

        public static void PositionAsTrayPopup(this Window window)
        {
            if (window.WindowStartupLocation != WindowStartupLocation.Manual)
                return;

            try
            {
                var taskBarHeight = Math.Abs(SystemParameters.WorkArea.Height - SystemParameters.PrimaryScreenHeight);
                window.Left = SystemParameters.PrimaryScreenWidth - window.ActualWidth;
                window.Top = SystemParameters.PrimaryScreenHeight - window.ActualHeight - taskBarHeight;
            }
            catch (Exception)
            {

            }
        }
    }
}
