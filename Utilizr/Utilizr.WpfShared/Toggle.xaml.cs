﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class Toggle : UserControl, INotifyPropertyChanged
    {
        Storyboard _onStoryBoard;
        Storyboard _offStoryBoard;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler SwitchedOn;
        public event EventHandler SwitchedOff;

        public static readonly DependencyProperty BackgroundOnProperty =
            DependencyProperty.Register(
                nameof(BackgroundOn),
                typeof(SolidColorBrush),
                typeof(Toggle),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<SolidColorBrush>(nameof(Toggle), nameof(BackgroundOn))
                )
            );

        public SolidColorBrush BackgroundOn
        {
            get { return (SolidColorBrush)GetValue(BackgroundOnProperty); }
            set { SetValue(BackgroundOnProperty, value.Clone()); }
        }


        public static readonly DependencyProperty BackgroundOffProperty =
            DependencyProperty.Register(
                nameof(BackgroundOff),
                typeof(SolidColorBrush),
                typeof(Toggle),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<SolidColorBrush>(nameof(Toggle), nameof(BackgroundOff))
                )
            );

        public SolidColorBrush BackgroundOff
        {
            get { return (SolidColorBrush)GetValue(BackgroundOffProperty); }
            set { SetValue(BackgroundOffProperty, value.Clone()); }
        }


        public static readonly DependencyProperty BackgroundHandleProperty =
            DependencyProperty.Register(
                nameof(BackgroundHandle),
                typeof(Brush),
                typeof(Toggle),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(Toggle), nameof(BackgroundHandle))
                    )
                );

        public Brush BackgroundHandle
        {
            get { return (Brush)GetValue(BackgroundHandleProperty); }
            set { SetValue(BackgroundHandleProperty, value); }
        }


        public static readonly DependencyProperty IsToggledProperty = 
            DependencyProperty.Register(
                nameof(IsToggled),
                typeof(bool),
                typeof(Toggle),
                new PropertyMetadata(default(bool), OnIsToggledChanged)
            );

        private static void OnIsToggledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var toggle = d as Toggle;
            if (toggle == null)
                return;

            bool newIsToggledValue = (bool)e.NewValue;
            toggle.SetToggleMargin(newIsToggledValue);
            toggle.StartStoryboard(newIsToggledValue);
        }

        public bool IsToggled
        {
            get { return (bool) GetValue(IsToggledProperty); }
            set
            {
                SetToggleMargin(value);
                SetValue(IsToggledProperty, value);
                OnPropertyChanged(nameof(IsToggled));
            }
        }

        public static readonly DependencyProperty ToggleMarginProperty = 
            DependencyProperty.Register(
                nameof(ToggleMargin),
                typeof(Thickness),
                typeof(Toggle),
                new PropertyMetadata(
                    default(Thickness),
                    OnToggleMarginChanged
                )
            );

        private static void OnToggleMarginChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Toggle)?.AnimateToggle();
        }

        public Thickness ToggleMargin
        {
            get { return (Thickness) GetValue(ToggleMarginProperty); }
            set
            {
                SetValue(ToggleMarginProperty, value);
                OnPropertyChanged(nameof(ToggleMargin));
                AnimateToggle();
            }
        }



        public static readonly DependencyProperty PreToggledCheckProperty =
            DependencyProperty.Register(
                nameof(PreToggledCheck),
                typeof(Func<bool>),
                typeof(Toggle),
                new PropertyMetadata(null)
            );

        /// <summary>
        /// Logic which will be invoked just before the toggle is switched.
        /// Return true to allow toggle position to be updated, false to deny.
        /// If null, no check is preformed, and toggle will always updated.
        /// </summary>
        public Func<bool> PreToggledCheck
        {
            get { return (Func<bool>)GetValue(PreToggledCheckProperty); }
            set { SetValue(PreToggledCheckProperty, value); }
        }


        private Storyboard _currentBackgroundStoryboard;

        public Toggle()
        {
            InitializeComponent();

#if DEBUG
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
#endif

            _onStoryBoard = LayoutRoot.TryFindResource("OnStoryBoard") as Storyboard;
            _offStoryBoard = LayoutRoot.TryFindResource("OffStoryBoard") as Storyboard;
            LayoutRoot.DataContext = this;
            SizeChanged += (sender, args) =>
            {
                SetToggleMargin(IsToggled);
                ToggleEllipse.Margin = ToggleMargin;
            };
            Loaded += (s, e) =>
            {
                Border.Background = IsToggled 
                    ? BackgroundOn.Clone()
                    : BackgroundOff.Clone();
            };
        }

        void SetToggleMargin(bool toggled)
        {
            ToggleMargin = new Thickness(
                toggled
                    ? ActualWidth - ToggleEllipse.ActualWidth - 4
                    : 4,
                0,
                0,
                0
            );
        }

        void AnimateToggle()
        {
            if (!IsLoaded)
            {
                // don't animate for inital gui setup
                ToggleEllipse.Margin = ToggleMargin;
            }
            else
            {
                var ta = new ThicknessAnimation()
                {
                    To = ToggleMargin,
                    Duration = TimeSpan.FromMilliseconds(200),
                    DecelerationRatio = 0.8,
                    FillBehavior = FillBehavior.Stop,
                };

                // Use FillBehaviour.Stop so we can manually update ToggleEllipse.Margin to
                // another value, e.g. when unloaded the assignment above will not work. But
                // this means the property won't be under the animation anymore and will revert
                // to the previous value, set corectly in the complete event.
                // https://learn.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/how-to-set-a-property-after-animating-it-with-a-storyboard
                void complete(object sender, EventArgs args)
                {
                    ToggleEllipse.Margin = ToggleMargin;
                    ta.Completed -= complete;
                }
                ta.Completed += complete;

                ToggleEllipse.BeginAnimation(Ellipse.MarginProperty, ta);
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            if(PreToggledCheck != null && !PreToggledCheck.Invoke())
            {
                return; // Not allowed to update toggle position
            }

            //IsToggled = !IsToggled;
            SetValue(IsToggledProperty, !IsToggled);

            // Above line really needs to be SetCurrentValue rather than SetValue. It won't overwrite any bindings. But
            // only available in .NET 4.0+. This control cannot support OneWay bindings on IsToggled unless .NET 4.0+
            //
            // From MSDN:
            //      This method is used by a component that programmatically sets the value of one of its own properties
            //      without disabling an application's declared use of the property. The SetCurrentValue method changes
            //      the effective value of the property, but existing triggers, data bindings, and styles will continue
            //      to work.

            StartStoryboard(IsToggled);

            if (IsToggled)
                OnSwitchedOn();
            else
                OnSwitchedOff();
        }

        private void StartStoryboard(bool newToggledState)
        {
            try
            {
                var startBoard = newToggledState ? _onStoryBoard : _offStoryBoard;
                startBoard.Begin(this, HandoffBehavior.SnapshotAndReplace, true);
            }
            catch (Exception)
            {
                Border.Background = newToggledState
                    ? BackgroundOn.Clone()
                    : BackgroundOff.Clone();
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnSwitchedOff()
        {
            SwitchedOff?.Invoke(this, new EventArgs());
        }

        protected virtual void OnSwitchedOn()
        {
            SwitchedOn?.Invoke(this, new EventArgs());
        }
    }
}
