﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilzr.WPF.Model;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class AppCheckBox : UserControl
    {
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register(
                nameof(IsChecked),
                typeof(bool?),
                typeof(AppCheckBox),
                new PropertyMetadata(false)
            );

        public bool? IsChecked
        {
            get { return (bool?)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(AppCheckBox),
                new PropertyMetadata(default(string))
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty TextSizeProperty =
            DependencyProperty.Register(
                nameof(TextSize),
                typeof(double),
                typeof(AppCheckBox),
                new PropertyMetadata(12.0)
            );

        public double TextSize
        {
            get { return (double)GetValue(TextSizeProperty); }
            set { SetValue(TextSizeProperty, value); }
        }


        public static readonly DependencyProperty TextWeightProperty =
            DependencyProperty.Register(
                nameof(TextWeight),
                typeof(FontWeight),
                typeof(AppCheckBox),
                new PropertyMetadata(FontWeights.Normal)
            );

        public FontWeight TextWeight
        {
            get { return (FontWeight)GetValue(TextWeightProperty); }
            set { SetValue(TextWeightProperty, value); }
        }


        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register(
                nameof(TextMargin),
                typeof(Thickness),
                typeof(AppCheckBox),
                new PropertyMetadata(new Thickness(4, 0, 0, 0))
            );

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }


        public static readonly DependencyProperty TextBrushProperty =
            DependencyProperty.Register(
                nameof(TextBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppCheckBox), nameof(TextBrush))
                )
            );

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }


        #region Normal brush (3 states)
        public static readonly DependencyProperty NormalUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalUncheckedBrush)
                    )
                )
            );

        public Brush NormalUncheckedBrush
        {
            get { return (Brush)GetValue(NormalUncheckedBrushProperty); }
            set { SetValue(NormalUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(NormalIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalIndeterminateBrush)
                    )
                )
            );

        public Brush NormalIndeterminateBrush
        {
            get { return (Brush)GetValue(NormalIndeterminateBrushProperty); }
            set { SetValue(NormalIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalCheckedBrush)
                    )
                )
            );

        public Brush NormalCheckedBrush
        {
            get { return (Brush)GetValue(NormalCheckedBrushProperty); }
            set { SetValue(NormalCheckedBrushProperty, value); }
        }
        #endregion

        #region Normal border brush (3 states)
        public static readonly DependencyProperty NormalBorderUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalBorderUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalBorderUncheckedBrush)
                    )
                )
            );

        public Brush NormalBorderUncheckedBrush
        {
            get { return (Brush)GetValue(NormalBorderUncheckedBrushProperty); }
            set { SetValue(NormalBorderUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalBorderIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(NormalBorderIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalBorderIndeterminateBrush)
                    )
                )
            );

        public Brush NormalBorderIndeterminateBrush
        {
            get { return (Brush)GetValue(NormalBorderIndeterminateBrushProperty); }
            set { SetValue(NormalBorderIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalBorderCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalBorderCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(NormalBorderCheckedBrush)
                    )
                )
            );

        public Brush NormalBorderCheckedBrush
        {
            get { return (Brush)GetValue(NormalBorderCheckedBrushProperty); }
            set { SetValue(NormalBorderCheckedBrushProperty, value); }
        }
        #endregion

        #region Lighter brush (3 states)
        public static readonly DependencyProperty LighterUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(LighterUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterUncheckedBrush)
                    )
                )
            );

        public Brush LighterUncheckedBrush
        {
            get { return (Brush)GetValue(LighterUncheckedBrushProperty); }
            set { SetValue(LighterUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty LighterIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(LighterIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterIndeterminateBrush)
                    )
                )
            );

        public Brush LighterIndeterminateBrush
        {
            get { return (Brush)GetValue(LighterIndeterminateBrushProperty); }
            set { SetValue(LighterIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty LighterCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(LighterCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterCheckedBrush)
                    )
                )
            );

        public Brush LighterCheckedBrush
        {
            get { return (Brush)GetValue(LighterCheckedBrushProperty); }
            set { SetValue(LighterCheckedBrushProperty, value); }
        }
        #endregion

        #region Lighter border brush (3 states)
        public static readonly DependencyProperty LighterBorderUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(LighterBorderUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterBorderUncheckedBrush)
                    )
                )
            );

        public Brush LighterBorderUncheckedBrush
        {
            get { return (Brush)GetValue(LighterBorderUncheckedBrushProperty); }
            set { SetValue(LighterBorderUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty LighterBorderIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(LighterBorderIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterBorderIndeterminateBrush)
                    )
                )
            );

        public Brush LighterBorderIndeterminateBrush
        {
            get { return (Brush)GetValue(LighterBorderIndeterminateBrushProperty); }
            set { SetValue(LighterBorderIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty LighterBorderCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(LighterBorderCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(LighterBorderCheckedBrush)
                    )
                )
            );

        public Brush LighterBorderCheckedBrush
        {
            get { return (Brush)GetValue(LighterBorderCheckedBrushProperty); }
            set { SetValue(LighterBorderCheckedBrushProperty, value); }
        }
        #endregion

        #region Disabled brush (3 states)
        public static readonly DependencyProperty DisabledUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledUncheckedBrush)
                    )
                )
            );

        public Brush DisabledUncheckedBrush
        {
            get { return (Brush)GetValue(DisabledUncheckedBrushProperty); }
            set { SetValue(DisabledUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty DisabledIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledIndeterminateBrush)
                    )
                )
            );

        public Brush DisabledIndeterminateBrush
        {
            get { return (Brush)GetValue(DisabledIndeterminateBrushProperty); }
            set { SetValue(DisabledIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty DisabledCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledCheckedBrush)
                    )
                )
            );

        public Brush DisabledCheckedBrush
        {
            get { return (Brush)GetValue(DisabledCheckedBrushProperty); }
            set { SetValue(DisabledCheckedBrushProperty, value); }
        }
        #endregion

        #region Disabled border brush (3 states)
        public static readonly DependencyProperty DisabledBorderUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledBorderUncheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledBorderUncheckedBrush)
                    )
                )
            );

        public Brush DisabledBorderUncheckedBrush
        {
            get { return (Brush)GetValue(DisabledBorderUncheckedBrushProperty); }
            set { SetValue(DisabledBorderUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty DisabledBorderIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledBorderIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledBorderCheckedBrush)
                    )
                )
            );

        public Brush DisabledBorderIndeterminateBrush
        {
            get { return (Brush)GetValue(DisabledBorderIndeterminateBrushProperty); }
            set { SetValue(DisabledBorderIndeterminateBrushProperty, value); }
        }


        public static readonly DependencyProperty DisabledBorderCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DisabledBorderCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(DisabledBorderCheckedBrush)
                    )
                )
            );

        public Brush DisabledBorderCheckedBrush
        {
            get { return (Brush)GetValue(DisabledBorderCheckedBrushProperty); }
            set { SetValue(DisabledBorderCheckedBrushProperty, value); }
        }
        #endregion

        #region Glyph brush (4 states)
        public static readonly DependencyProperty GlyphIndeterminateBrushProperty =
            DependencyProperty.Register(
                nameof(GlyphIndeterminateBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(GlyphIndeterminateBrush)
                    )
                )
            );

        public Brush GlyphIndeterminateBrush
        {
            get { return (Brush)GetValue(GlyphIndeterminateBrushProperty); }
            set { SetValue(GlyphIndeterminateBrushProperty, value); }
        }

        public static readonly DependencyProperty GlyphIndeterminateDisabledBrushProperty =
            DependencyProperty.Register(
                nameof(GlyphIndeterminateDisabledBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(GlyphIndeterminateDisabledBrush)
                    )
                )
            );

        public Brush GlyphIndeterminateDisabledBrush
        {
            get { return (Brush)GetValue(GlyphIndeterminateDisabledBrushProperty); }
            set { SetValue(GlyphIndeterminateDisabledBrushProperty, value); }
        }

        public static readonly DependencyProperty GlyphCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(GlyphCheckedBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(GlyphCheckedBrush)
                    )
                )
            );

        public Brush GlyphCheckedBrush
        {
            get { return (Brush)GetValue(GlyphCheckedBrushProperty); }
            set { SetValue(GlyphCheckedBrushProperty, value); }
        }

        public static readonly DependencyProperty GlyphCheckedDisabledBrushProperty =
            DependencyProperty.Register(
                nameof(GlyphCheckedDisabledBrush),
                typeof(Brush),
                typeof(AppCheckBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppCheckBox),
                        nameof(GlyphCheckedDisabledBrush)
                    )
                )
            );

        public Brush GlyphCheckedDisabledBrush
        {
            get { return (Brush)GetValue(GlyphCheckedDisabledBrushProperty); }
            set { SetValue(GlyphCheckedDisabledBrushProperty, value); }
        }
        #endregion


        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register(
                nameof(TextWrapping),
                typeof(TextWrapping),
                typeof(AppCheckBox),
                new PropertyMetadata(TextWrapping.NoWrap)
            );

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }


        public static readonly DependencyProperty CheckAlignmentProperty =
            DependencyProperty.Register(
                nameof(CheckAlignment),
                typeof(CheckAlignment),
                typeof(AppCheckBox),
                new PropertyMetadata(CheckAlignment.Left)
            );

        public CheckAlignment CheckAlignment
        {
            get { return (CheckAlignment)GetValue(CheckAlignmentProperty); }
            set { SetValue(CheckAlignmentProperty, value); }
        }


        public static readonly DependencyProperty ClickedCommandProperty =
            DependencyProperty.Register(
                nameof(ClickedCommand),
                typeof(ICommand),
                typeof(AppCheckBox),
                new PropertyMetadata(null)
            );

        public ICommand ClickedCommand
        {
            get { return (ICommand)GetValue(ClickedCommandProperty); }
            set { SetValue(ClickedCommandProperty, value); }
        }


        public static readonly DependencyProperty ClickedCommandParameterProperty =
            DependencyProperty.Register(
                nameof(ClickedCommandParameter),
                typeof(object),
                typeof(AppCheckBox),
                new PropertyMetadata(null)
            );

        public object ClickedCommandParameter
        {
            get { return GetValue(ClickedCommandParameterProperty); }
            set { SetValue(ClickedCommandParameterProperty, value); }
        }


        public AppCheckBox()
        {
            InitializeComponent();
            Check.DataContext = this;
        }

        void Check_Click(object sender, RoutedEventArgs e)
        {
            if (ClickedCommand?.CanExecute(ClickedCommandParameter) != true)
                return;

            ClickedCommand?.Execute(ClickedCommandParameter);
        }
    }
}