﻿namespace Utilzr.WPF
{
    public partial class NumberPicker : AbstractPicker<int>
    {
        public NumberPicker()
        {
            InitializeComponent();
            Setup(LayoutRoot, SelectionWheel, upperButton, lowerButton);
        }
    }
}