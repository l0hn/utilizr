﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class HorizontalRibbon : UserControl
    {
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(HorizontalRibbon),
                new PropertyMetadata(null)
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public new static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.Register(
                nameof(Foreground),
                typeof(Brush),
                typeof(HorizontalRibbon),
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb((byte)197, (byte)197, (byte)197))) // #FFC5C5C5
            );

        public new Brush Foreground
        {
            get { return (Brush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }


        public new static readonly DependencyProperty FontWeightProperty =
            DependencyProperty.Register(
                nameof(FontWeight),
                typeof(FontWeight),
                typeof(HorizontalRibbon),
                new PropertyMetadata(FontWeights.SemiBold)
            );

        public new FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }


        public new static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register(
                nameof(FontSize),
                typeof(int),
                typeof(HorizontalRibbon),
                new PropertyMetadata(18)
            );

        public new int FontSize
        {
            get { return (int)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }


        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register(
                nameof(TextWrapping),
                typeof(TextWrapping),
                typeof(HorizontalRibbon),
                new PropertyMetadata(TextWrapping.NoWrap)
            );

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }


        public static readonly DependencyProperty TextTrimmingProperty =
            DependencyProperty.Register(
                nameof(TextTrimming),
                typeof(TextTrimming),
                typeof(HorizontalRibbon),
                new PropertyMetadata(TextTrimming.None)
            );

        public TextTrimming TextTrimming
        {
            get { return (TextTrimming)GetValue(TextTrimmingProperty); }
            set { SetValue(TextTrimmingProperty, value); }
        }



        public static readonly DependencyProperty ShowDropTriangleProperty =
            DependencyProperty.Register(
                nameof(ShowDropTriangle),
                typeof(bool),
                typeof(HorizontalRibbon),
                new PropertyMetadata(true)
            );

        public bool ShowDropTriangle
        {
            get { return (bool)GetValue(ShowDropTriangleProperty); }
            set { SetValue(ShowDropTriangleProperty, value); }
        }


        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register(
                nameof(TextMargin),
                typeof(Thickness),
                typeof(HorizontalRibbon),
                new PropertyMetadata(new Thickness(0,3,0,3))
            );

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }



        public HorizontalRibbon()
        {
            InitializeComponent();
            Rectangle.SetDropShadowEffect(opacity: 0.3, blurRadius: 6);
            SideTriangle.SetDropShadowEffect(opacity: 0.3, blurRadius: 6);
            LayoutRoot.DataContext = this;

            LayoutRoot.SizeChanged += (s, e) => UpdateOffsets();
            Loaded += (s, e) => UpdateOffsets();
        }

        void UpdateOffsets()
        {
            var height = LayoutRoot.ActualHeight;
            var half = height / 2.0;

            // Keep drop triangle the same size regardless of height
            //FirstColumn.Width = new GridLength(half);
            LastColumn.Width = new GridLength(half);

            //DropTriangle.Margin = new Thickness(0, half, 0, 0 - half);
            //DropTriangle.Points = new PointCollection(new List<Point>
            //{
            //    new Point(0,0),
            //    new Point(half, half),
            //    new Point(half, 0),
            //});

            // Add slight rectangle on the left of the triangle. Can then set -1 margin
            // to avoid line showing on GUI where this side triangle meets middle rectangle.
            SideTriangle.Points = new PointCollection(new List<Point>
            {
                new Point(0,0),
                new Point(1, 0),
                new Point(half + 1, half),
                new Point(1, height),
                new Point(0, height)
            });
        }
    }
}
