﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class CircularProgressBar : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion INotifyPropertyChanged


        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register(
                nameof(Icon),
                typeof(ImageSource),
                typeof(CircularProgressBar),
                new PropertyMetadata(null)
            );

        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }


        public static readonly DependencyProperty ThumbTextProperty =
            DependencyProperty.Register(
                nameof(ThumbText),
                typeof(string),
                typeof(CircularProgressBar),
                new PropertyMetadata(string.Empty)
            );

        public string ThumbText
        {
            get { return (string)GetValue(ThumbTextProperty); }
            set { SetValue(ThumbTextProperty, value); }
        }


        public static readonly DependencyProperty ThumbTextSizeProperty =
            DependencyProperty.Register(
                nameof(ThumbTextSize),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(12.0)
            );

        public double ThumbTextSize
        {
            get { return (double)GetValue(ThumbTextSizeProperty); }
            set { SetValue(ThumbTextSizeProperty, value); }
        }


        public static readonly DependencyProperty HasThumbProperty =
            DependencyProperty.Register(
                nameof(HasThumb),
                typeof(bool),
                typeof(CircularProgressBar), 
                new PropertyMetadata(true)
            );

        public bool HasThumb
        {
            get { return (bool)GetValue(HasThumbProperty); }
            set { SetValue(HasThumbProperty, value); }
        }


        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register(
                nameof(Direction),
                typeof(SweepDirection),
                typeof(CircularProgressBar),
                new PropertyMetadata(SweepDirection.Clockwise)
            );

        public SweepDirection Direction
        {
            get { return (SweepDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }


        public static readonly DependencyProperty OriginRotationDegreesProperty =
            DependencyProperty.Register(
                nameof(OriginRotationDegrees),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(225.0)
            );

        public double OriginRotationDegrees
        {
            get { return (double)GetValue(OriginRotationDegreesProperty); }
            set { SetValue(OriginRotationDegreesProperty, value); }
        }


        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                nameof(Value),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(0.0)
            );

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
                nameof(Minimum),
                typeof(double),
                typeof(CircularProgressBar), 
                new PropertyMetadata(0.0)
            );

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }


        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
                nameof(Maximum),
                typeof(double),
                typeof(CircularProgressBar), 
                new PropertyMetadata(100.0)
            );

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }


        public static readonly DependencyProperty BarThicknessProperty =
            DependencyProperty.Register
            (nameof(BarThickness),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(14.0, OnBarThicknessChanged)
            );

        private static void OnBarThicknessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as CircularProgressBar)?.UpdateOpacityMaskOffets();
        }

        public double BarThickness
        {
            get { return (double)GetValue(BarThicknessProperty); }
            set { SetValue(BarThicknessProperty, value); }
        }


        public static readonly DependencyProperty BarBackgroundWholeProperty =
            DependencyProperty.Register(
                nameof(BarBackgroundWhole),
                typeof(Brush),
                typeof(CircularProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(CircularProgressBar), nameof(BarBackgroundWhole))
                )
            );

        public Brush BarBackgroundWhole
        {
            get { return (Brush)GetValue(BarBackgroundWholeProperty); }
            set { SetValue(BarBackgroundWholeProperty, value); }
        }


        public static readonly DependencyProperty BarBackgroundProgressProperty =
            DependencyProperty.Register(
                nameof(BarBackgroundProgress),
                typeof(Brush),
                typeof(CircularProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(CircularProgressBar), nameof(BarBackgroundProgress))
                )
            );

        public Brush BarBackgroundProgress
        {
            get { return (Brush)GetValue(BarBackgroundProgressProperty); }
            set { SetValue(BarBackgroundProgressProperty, value); }
        }


        public static readonly DependencyProperty ThumbTextForegroundProperty =
            DependencyProperty.Register(
                nameof(ThumbTextForeground),
                typeof(Brush),
                typeof(CircularProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(CircularProgressBar), nameof(ThumbTextForeground))
                )
            );

        public Brush ThumbTextForeground
        {
            get { return (Brush)GetValue(ThumbTextForegroundProperty); }
            set { SetValue(ThumbTextForegroundProperty, value); }
        }

        public static readonly DependencyProperty OriginShineDegreesProperty =
            DependencyProperty.Register(
                nameof(OriginShineDegrees),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(270.0)
            );

        public double OriginShineDegrees
        {
            get { return (double)GetValue(OriginShineDegreesProperty); }
            set { SetValue(OriginShineDegreesProperty, value); }
        }


        public static readonly DependencyProperty HasShineProperty =
            DependencyProperty.Register(
                nameof(HasShine),
                typeof(bool),
                typeof(CircularProgressBar),
                new PropertyMetadata(true)
            );

        public bool HasShine
        {
            get { return (bool)GetValue(HasShineProperty); }
            set { SetValue(HasShineProperty, value); }
        }


        public static readonly DependencyProperty ShineColourProperty =
            DependencyProperty.Register(
                nameof(ShineColour),
                typeof(Color),
                typeof(CircularProgressBar),
                new PropertyMetadata(Colors.White)
            );

        public Color ShineColour
        {
            get { return (Color)GetValue(ShineColourProperty); }
            set { SetValue(ShineColourProperty, value); }
        }


        public static readonly DependencyProperty ShineStrengthProperty =
            DependencyProperty.Register(
                nameof(ShineStrength),
                typeof(byte),
                typeof(CircularProgressBar),
                new PropertyMetadata((byte)120)
            );

        public byte ShineStrength
        {
            get { return (byte)GetValue(ShineStrengthProperty); }
            set { SetValue(ShineStrengthProperty, value); }
        }


        public static readonly DependencyProperty IsIndeterminateProgressProperty =
            DependencyProperty.Register(
                nameof(IsIndeterminate),
                typeof(bool),
                typeof(CircularProgressBar),
                new PropertyMetadata(false)
            );

        public bool IsIndeterminate
        {
            get { return (bool)GetValue(IsIndeterminateProgressProperty); }
            set { SetValue(IsIndeterminateProgressProperty, value); }
        }


        public static readonly DependencyProperty IndeterminateAngleProperty =
            DependencyProperty.Register(
                nameof(IndeterminateAngle),
                typeof(double),
                typeof(CircularProgressBar),
                new PropertyMetadata(0.0)
            );

        public double IndeterminateAngle
        {
            get { return (double)GetValue(IndeterminateAngleProperty); }
            set { SetValue(IndeterminateAngleProperty, value); }
        }


        public static readonly DependencyProperty OpacityMaskOffset1Property =
        DependencyProperty.Register(nameof(OpacityMaskOffset1), typeof(double), typeof(CircularProgressBar), new PropertyMetadata(0.77));

        public double OpacityMaskOffset1
        {
            get { return (double)GetValue(OpacityMaskOffset1Property); }
            set { SetValue(OpacityMaskOffset1Property, value); }
        }

        public static readonly DependencyProperty OpacityMaskOffset2Property =
        DependencyProperty.Register(nameof(OpacityMaskOffset2), typeof(double), typeof(CircularProgressBar), new PropertyMetadata(0.78));

        public double OpacityMaskOffset2
        {
            get { return (double)GetValue(OpacityMaskOffset2Property); }
            set { SetValue(OpacityMaskOffset2Property, value); }
        }




        public CircularProgressBar()
        {
            InitializeComponent();
            ProgressBar.DataContext = this;

            SizeChanged += (s, e) => UpdateOpacityMaskOffets();
        }

        private void UpdateOpacityMaskOffets()
        {
            OpacityMaskOffset1 = 1 - ((BarThickness / ActualHeight) * 2);
            OpacityMaskOffset2 = OpacityMaskOffset1 + 0.01;
        }
    }
}