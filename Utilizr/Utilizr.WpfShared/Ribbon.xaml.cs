﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace Utilzr.WPF
{
    /// <summary>
    /// Interaction logic for Ribbon.xaml
    /// </summary>
    public partial class Ribbon : UserControl
    {
        public Ribbon()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        public static readonly DependencyProperty RibbonColorProperty = DependencyProperty.Register(
            nameof(RibbonColor), typeof(Brush), typeof(Ribbon), new PropertyMetadata(new SolidColorBrush("#FF3799D6".ARGBToColor())));

        public Brush RibbonColor
        {
            get { return (Brush) GetValue(RibbonColorProperty); }
            set { SetValue(RibbonColorProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(Ribbon), new PropertyMetadata(default(string)));

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
    }
}
