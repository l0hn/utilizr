﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public enum ButtonIconAligmentType
    {
        Left,
        Right,
    }

    public partial class AppButton : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(params string[] properties)
        {
            foreach (var propName in properties)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion

        public event EventHandler ButtonClicked;


        public static readonly DependencyProperty ButtonTextProprety =
            DependencyProperty.Register(
                nameof(ButtonText),
                typeof(string),
                typeof(AppButton), 
                new PropertyMetadata(default(string))
            );

        public string ButtonText
        {
            get { return (string)GetValue(ButtonTextProprety); }
            set { SetValue(ButtonTextProprety, value); }
        }



        public static readonly DependencyProperty ButtonIconProprety =
            DependencyProperty.Register(
                nameof(ButtonIcon),
                typeof(ImageSource),
                typeof(AppButton),
                new PropertyMetadata(default(ImageSource), OnButtonIconChanged)
            );

        private static void OnButtonIconChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as AppButton)?.InvalidateIconProperties();
        }

        public ImageSource ButtonIcon
        {
            get { return (ImageSource)GetValue(ButtonIconProprety); }
            set { SetValue(ButtonIconProprety, value); }
        }


        public static readonly DependencyProperty ButtonIconContentTemplateProperty =
            DependencyProperty.Register(
                nameof(ButtonIconContentTemplate),
                typeof(DataTemplate),
                typeof(AppButton),
                new UIPropertyMetadata(default(DataTemplate), ButtonIconContentChanged)
            );

        private static void ButtonIconContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as AppButton)?.InvalidateIconProperties();
        }

        /// <summary>
        /// Custom content shown rather than an image specified within <see cref="ButtonIcon"/>.
        /// If not null, this will be shown rather than any image specified in <see cref="ButtonIcon"/>.
        /// </summary>
        public DataTemplate ButtonIconContentTemplate
        {
            get { return (DataTemplate)GetValue(ButtonIconContentTemplateProperty); }
            set { SetValue(ButtonIconContentTemplateProperty, value); }
        }


        public static readonly DependencyProperty ButtonIconHeightProperty =
            DependencyProperty.Register(
                nameof(ButtonIconHeight),
                typeof(double),
                typeof(AppButton), 
                new PropertyMetadata(30.0)
            );

        public double ButtonIconHeight
        {
            get { return (double)GetValue(ButtonIconHeightProperty); }
            set { SetValue(ButtonIconHeightProperty, value); }
        }

        public static readonly DependencyProperty ButtonIconStretchProperty = DependencyProperty.Register(
            "ButtonIconStretch", typeof (Stretch), typeof (AppButton), new PropertyMetadata(default(Stretch)));

        public Stretch ButtonIconStretch
        {
            get { return (Stretch) GetValue(ButtonIconStretchProperty); }
            set { SetValue(ButtonIconStretchProperty, value); }
        }


        public static readonly DependencyProperty ButtonIconWidthProperty =
            DependencyProperty.Register(
                nameof(ButtonIconWidth),
                typeof(double),
                typeof(AppButton),
                new PropertyMetadata(30.0)
            );

        public double ButtonIconWidth
        {
            get { return (double)GetValue(ButtonIconWidthProperty); }
            set { SetValue(ButtonIconWidthProperty, value); }
        }



        public static readonly DependencyProperty ButtonBackgroundProperty =
            DependencyProperty.Register(
                nameof(ButtonBackground),
                typeof(Brush), 
                typeof(AppButton), 
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppButton), nameof(ButtonBackground))
                )
            );

        public Brush ButtonBackground
        {
            get { return (Brush)GetValue(ButtonBackgroundProperty); }
            set { SetValue(ButtonBackgroundProperty, value); }
        }



        public static readonly DependencyProperty AnimateIconWithSpinProperty =
            DependencyProperty.Register(
                nameof(AnimateIconWithSpin),
                typeof(bool),
                typeof(AppButton),
                new PropertyMetadata(default(bool))
            );

        public bool AnimateIconWithSpin
        {
            get { return (bool)GetValue(AnimateIconWithSpinProperty); }
            set { SetValue(AnimateIconWithSpinProperty, value); }
        }



        public static readonly DependencyProperty ButtonIconAlignmentProperty =
            DependencyProperty.Register(
                nameof(ButtonIconAlignment),
                typeof(ButtonIconAligmentType),
                typeof(AppButton),
                new PropertyMetadata(ButtonIconAligmentType.Right, OnButtonIconAlignmentChanged)
            );

        static void OnButtonIconAlignmentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as AppButton)?.InvalidateIconProperties();
        }

        public ButtonIconAligmentType ButtonIconAlignment
        {
            get { return (ButtonIconAligmentType)GetValue(ButtonIconAlignmentProperty); }
            set { SetValue(ButtonIconAlignmentProperty, value); }
        }



        public static readonly DependencyProperty ButtonIconMarginProperty =
            DependencyProperty.Register(
                nameof(ButtonIconMargin),
                typeof(Thickness), 
                typeof(AppButton),
                new PropertyMetadata(default(Thickness))
            );

        public Thickness ButtonIconMargin
        {
            get { return (Thickness)GetValue(ButtonIconMarginProperty); }
            set { SetValue(ButtonIconMarginProperty, value); }
        }



        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register(
                nameof(CornerRadius),
                typeof(double),
                typeof(AppButton), 
                new PropertyMetadata(0.0)
            );

        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }



        public static readonly DependencyProperty HasCustomCornerRadiusProperty =
            DependencyProperty.Register(
                nameof(HasCustomCornerRadius),
                typeof(bool),
                typeof(AppButton),
                new PropertyMetadata(false, OnHasCustomCornerRadiusChanged)
            );

        private static void OnHasCustomCornerRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var appBtn = d as AppButton;
            if (appBtn == null || appBtn.HasCustomCornerRadius)
                return;

            // Set CornerRadius back to the default of half the height
            appBtn.CornerRadius = appBtn.ActualHeight > 0
                ? appBtn.ActualHeight / 2.0
                : appBtn.ButtonMinHeight / 2.0;
        }

        /// <summary>
        /// Corner radius property is recalculated on size changed to have perfect semi circle.
        /// The corner radius specified will be overwritten on SizeChanged, unless this property is true.
        /// </summary>
        public bool HasCustomCornerRadius
        {
            get { return (bool)GetValue(HasCustomCornerRadiusProperty); }
            set { SetValue(HasCustomCornerRadiusProperty, value); }
        }



        public static readonly DependencyProperty ButtonTextFontSizeProperty = 
            DependencyProperty.Register(
                nameof(ButtonTextFontSize),
                typeof (double),
                typeof (AppButton), 
                new PropertyMetadata(default(double))
            );

        public double ButtonTextFontSize
        {
            get { return (double) GetValue(ButtonTextFontSizeProperty); }
            set
            {
                SetValue(ButtonTextFontSizeProperty, value);
                OnPropertyChanged(nameof(ButtonTextFontSize));
            }
        }



        public static readonly DependencyProperty ButtonTextFontWeightProperty =
            DependencyProperty.Register(
                nameof(ButtonTextFontWeight), 
                typeof(FontWeight),
                typeof(AppButton),
                new PropertyMetadata(FontWeights.Normal)
            );

        public FontWeight ButtonTextFontWeight
        {
            get { return (FontWeight)GetValue(ButtonTextFontWeightProperty); }
            set { SetValue(ButtonTextFontWeightProperty, value); }
        }



        public static readonly DependencyProperty ButtonTextMarginProperty = 
            DependencyProperty.Register(
                nameof(ButtonTextMargin),
                typeof (Thickness),
                typeof (AppButton),
                new PropertyMetadata(default(Thickness))
            );

        public Thickness ButtonTextMargin
        {
            get { return (Thickness) GetValue(ButtonTextMarginProperty); }
            set
            {
                SetValue(ButtonTextMarginProperty, value);
                OnPropertyChanged(nameof(ButtonTextMargin));
            }
        }


        public static readonly DependencyProperty ButtonTextColourProperty =
            DependencyProperty.Register(
                nameof(ButtonTextColour),
                typeof(Brush),
                typeof(AppButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(AppButton),
                        nameof(ButtonTextColour)
                    )
                )
            );
        
        public Brush ButtonTextColour
        {
            get { return (Brush)GetValue(ButtonTextColourProperty); }
            set { SetValue(ButtonTextColourProperty, value); }
        }


        public static readonly DependencyProperty ButtonTextTextAlignmentProperty =
            DependencyProperty.Register(
                nameof(ButtonTextTextAlignment),
                typeof(TextAlignment),
                typeof(AppButton),
                new PropertyMetadata(TextAlignment.Center)
            );

        public TextAlignment ButtonTextTextAlignment
        {
            get { return (TextAlignment)GetValue(ButtonTextTextAlignmentProperty); }
            set { SetValue(ButtonTextTextAlignmentProperty, value); }
        }


        public static readonly DependencyProperty ButtonTextTextWrappingProperty =
            DependencyProperty.Register(
                nameof(ButtonTextTextWrapping),
                typeof(TextWrapping),
                typeof(AppButton),
                new PropertyMetadata(TextWrapping.NoWrap)
            );

        public TextWrapping ButtonTextTextWrapping
        {
            get { return (TextWrapping)GetValue(ButtonTextTextWrappingProperty); }
            set { SetValue(ButtonTextTextWrappingProperty, value); }
        }


        public static readonly DependencyProperty ButtonMinWidthProperty =
            DependencyProperty.Register(
                nameof(ButtonMinWidth),
                typeof(double),
                typeof(AppButton),
                new PropertyMetadata(75.0)
            );

        public double ButtonMinWidth
        {
            get { return (double)GetValue(ButtonMinWidthProperty); }
            set { SetValue(ButtonMinWidthProperty, value); }
        }



        public static readonly DependencyProperty ButtonMinHeightProperty =
            DependencyProperty.Register(
                nameof(ButtonMinHeight),
                typeof(double), 
                typeof(AppButton), 
                new PropertyMetadata(23.0)
            );

        public double ButtonMinHeight
        {
            get { return (double)GetValue(ButtonMinHeightProperty); }
            set { SetValue(ButtonMinHeightProperty, value); }
        }


        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                nameof(Command),
                typeof(ICommand),
                typeof(AppButton),
                new PropertyMetadata(default(ICommand))
            );

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register(
                nameof(CommandParameter),
                typeof(object),
                typeof(AppButton),
                new PropertyMetadata(default(object))
            );

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }


        public static readonly DependencyProperty ButtonTextDecorationsProperty =
            DependencyProperty.Register(
                nameof(ButtonTextDecorations),
                typeof(TextDecorationCollection),
                typeof(AppButton),
                new PropertyMetadata(default(TextDecorationCollection))
            );

        public TextDecorationCollection ButtonTextDecorations
        {
            get { return (TextDecorationCollection)GetValue(ButtonTextDecorationsProperty); }
            set { SetValue(ButtonTextDecorationsProperty, value); }
        }


        public static readonly DependencyProperty ButtonBorderThicknessProperty =
            DependencyProperty.Register(
                nameof(ButtonBorderThickness),
                typeof(Thickness),
                typeof(AppButton),
                new PropertyMetadata(new Thickness(0))
            );

        public Thickness ButtonBorderThickness
        {
            get { return (Thickness)GetValue(ButtonBorderThicknessProperty); }
            set { SetValue(ButtonBorderThicknessProperty, value); }
        }


        public static readonly DependencyProperty ButtonBorderBrushProperty =
            DependencyProperty.Register(
                nameof(ButtonBorderBrush),
                typeof(Brush),
                typeof(AppButton),
                new PropertyMetadata(new SolidColorBrush(Colors.Transparent))
            );

        public Brush ButtonBorderBrush
        {
            get { return (Brush)GetValue(ButtonBorderBrushProperty); }
            set { SetValue(ButtonBorderBrushProperty, value); }
        }


        // Can't use converter inside binding on multibinding, thus more properties...
        public bool ShowRightIcon => ButtonIconAlignment == ButtonIconAligmentType.Right && ButtonIcon != null && ButtonIconContentTemplate == null;
        public bool ShowLeftIcon => ButtonIconAlignment == ButtonIconAligmentType.Left && ButtonIcon != null && ButtonIconContentTemplate == null;
        // Any custom icon content will show instead of any image specified.
        public bool ShowRightIconContent => ButtonIconAlignment == ButtonIconAligmentType.Right && ButtonIconContentTemplate != null;
        public bool ShowLeftIconContent => ButtonIconAlignment == ButtonIconAligmentType.Left && ButtonIconContentTemplate != null;

        public static readonly DependencyProperty ButtonIconUnscaledProperty = DependencyProperty.Register(
            "ButtonIconUnscaled", typeof (bool), typeof (AppButton), new PropertyMetadata(default(bool)));

        public bool ButtonIconUnscaled
        {
            get { return (bool) GetValue(ButtonIconUnscaledProperty); }
            set { SetValue(ButtonIconUnscaledProperty, value); }
        }


        public static readonly DependencyProperty ForceAndScaleDownSingleLineProperty =
            DependencyProperty.Register(
                nameof(ForceAndScaleDownSingleLine),
                typeof(bool),
                typeof(AppButton),
                new PropertyMetadata(false)
            );

        /// <summary>
        /// If true, will ensure <see cref="ButtonText"/> will not clip if too long, and will reduce
        /// the size of the text to accommodate. Doesn't support multi-line text scaling, single line only.
        /// </summary>
        public bool ForceAndScaleDownSingleLine
        {
            get { return (bool)GetValue(ForceAndScaleDownSingleLineProperty); }
            set { SetValue(ForceAndScaleDownSingleLineProperty, value); }
        }



        public static readonly DependencyProperty EnforceIconColumnsWidthProperty =
            DependencyProperty.Register(
                nameof(EnforceIconColumnsWidth),
                typeof(bool),
                typeof(AppButton),
                new PropertyMetadata(false)
            );
  
        /// <summary>
        /// When true, ensures both the left and right icon ColumnDefinition's widths are equal.
        /// Useful when trying to keep text centred when dynamically showing an icon. Defaults 
        /// to false for backward compatibility.
        /// </summary>
        public bool EnforceIconColumnsWidth
        {
            get { return (bool)GetValue(EnforceIconColumnsWidthProperty); }
            set { SetValue(EnforceIconColumnsWidthProperty, value); }
        }


        public static readonly DependencyProperty SafeShadowEffectProperty = DependencyProperty.Register(
               "SafeShadowEffect", typeof(SafeShadowEffect), typeof(AppButton), new PropertyMetadata(default(SafeShadowEffect)));

        public SafeShadowEffect SafeShadowEffect
        {
            get { return (SafeShadowEffect)GetValue(SafeShadowEffectProperty); }
            set { SetValue(SafeShadowEffectProperty, value); }
        }



        public static readonly new DependencyProperty ContentProperty =
        DependencyProperty.Register(
            nameof(Content),
                typeof(object),
                typeof(AppButton),
                new PropertyMetadata(null)
            );

        public new object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }



        public static readonly DependencyProperty DisabledOpacityProperty =
            DependencyProperty.Register(
                nameof(DisabledOpacity),
                typeof(double),
                typeof(AppButton),
                new PropertyMetadata(0.5D)
            );

        public double DisabledOpacity
        {
            get { return (double)GetValue(DisabledOpacityProperty); }
            set { SetValue(DisabledOpacityProperty, value); }
        }




        public AppButton()
        {
            InitializeComponent();
            Button.DataContext = this;

            SizeChanged += (s, e) =>
            {
                if (!HasCustomCornerRadius)
                    CornerRadius = ActualHeight / 2.0;
            };
            Button.Click += (s, e) => OnButtonClicked();
        }

        void InvalidateIconProperties()
        {
            OnPropertyChanged(
                nameof(ShowLeftIcon),
                nameof(ShowRightIcon),
                nameof(ShowRightIconContent),
                nameof(ShowLeftIconContent)
            );
        }

        protected virtual void OnButtonClicked()
        {
            ButtonClicked?.Invoke(this, new EventArgs());
        }
    }
}