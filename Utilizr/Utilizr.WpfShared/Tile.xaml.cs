﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class Tile : UserControl, ICommandSource
    {
        public static readonly DependencyProperty TileContentProperty =
            DependencyProperty.Register(
                nameof(TileContent),
                typeof(UIElement),
                typeof(Tile),
                new PropertyMetadata(default(UIElement))
            );

        public UIElement TileContent
        {
            get { return (UIElement)GetValue(TileContentProperty); }
            set { SetValue(TileContentProperty, value); }
        }


        public static readonly DependencyProperty TileBackgroundProperty =
            DependencyProperty.Register(
                nameof(TileBackground), 
                typeof(Brush),
                typeof(Tile),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(Tile), nameof(TileBackground))
                )
            );

        public Brush TileBackground
        {
            get { return (Brush)GetValue(TileBackgroundProperty); }
            set { SetValue(TileBackgroundProperty, value); }
        }


        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                nameof(Command),
                typeof(ICommand),
                typeof(Tile),
                new PropertyMetadata(default(ICommand))
            );

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register(
                nameof(CommandParameter),
                typeof(object),
                typeof(Tile),
                new PropertyMetadata(default(object))
            );

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }


        public static readonly DependencyProperty CommandTargetProperty =
            DependencyProperty.Register(
                nameof(CommandTarget),
                typeof(IInputElement),
                typeof(Tile),
                new PropertyMetadata(default(IInputElement))
            );

        public IInputElement CommandTarget
        {
            get { return (IInputElement)GetValue(CommandTargetProperty); }
            set { SetValue(CommandTargetProperty, value); }
        }

        public static readonly DependencyProperty TileBorderThicknessProperty = DependencyProperty.Register(
            nameof(TileBorderThickness), typeof(Thickness), typeof(Tile), new PropertyMetadata(default(Thickness)));

        public Thickness TileBorderThickness
        {
            get { return (Thickness) GetValue(TileBorderThicknessProperty); }
            set { SetValue(TileBorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty TileBorderBrushProperty = DependencyProperty.Register(
            nameof(TileBorderBrush), typeof(Brush), typeof(Tile), new PropertyMetadata(default(Brush)));

        public Brush TileBorderBrush
        {
            get { return (Brush) GetValue(TileBorderBrushProperty); }
            set { SetValue(TileBorderBrushProperty, value); }
        }

        public Tile()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
            TileGrid.SetDropShadowEffect(blurRadius:4, shadowDepth:2, opacity:0.5);
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            var command = Command;
            var parameter = CommandParameter;
            var target = CommandTarget;

            var routedCmd = command as RoutedCommand;
            if (routedCmd != null && routedCmd.CanExecute(parameter, target))
            {
                routedCmd.Execute(parameter, target);
            }
            else if (command != null && command.CanExecute(parameter))
            {
                command.Execute(parameter);
            }
        }
    }
}
