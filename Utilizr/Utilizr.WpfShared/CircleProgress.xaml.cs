﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Shapes
{
    public partial class CircleProgress : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty BarThicknessProperty = DependencyProperty.Register(
            nameof(BarThickness), typeof (double), typeof (CircleProgress), new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender, BarThicknessChanged));

        private static void BarThicknessChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var self = dependencyObject as CircleProgress;
            if (self == null)
                return;

            self.BackgroundBarStrokeThickness = (double)e.NewValue + 2;
            self.PulseCircleStrokeThickness = self.BackgroundBarStrokeThickness + 2;
            self.UpdateThumbPosition();
        }

        public double BarThickness
        {
            get { return (double) GetValue(BarThicknessProperty); }
            set { SetValue(BarThicknessProperty, value); }
        }

        public static readonly DependencyProperty BarBrushProperty = DependencyProperty.Register(
            nameof(BarBrush), typeof (Brush), typeof (CircleProgress), new FrameworkPropertyMetadata(new SolidColorBrush(Colors.LimeGreen), FrameworkPropertyMetadataOptions.AffectsRender));

        public Brush BarBrush
        {
            get { return (Brush) GetValue(BarBrushProperty); }
            set { SetValue(BarBrushProperty, value); }
        }

        public static readonly DependencyProperty BarBackgroundBrushProperty = DependencyProperty.Register(
            nameof(BarBackgroundBrush), typeof (Brush), typeof (CircleProgress), new FrameworkPropertyMetadata(new SolidColorBrush(Colors.Black), FrameworkPropertyMetadataOptions.AffectsRender));

        private double _backgroundBarStrokeThickness = 12.0;

        public Brush BarBackgroundBrush
        {
            get { return (Brush) GetValue(BarBackgroundBrushProperty); }
            set { SetValue(BarBackgroundBrushProperty, value); }
        }

        public double BackgroundBarStrokeThickness
        {
            get { return _backgroundBarStrokeThickness; }
            set
            {
                _backgroundBarStrokeThickness = value;
                OnPropertyChanged(nameof(BackgroundBarStrokeThickness));
            }
        }

        private double _pulseCircleStrokeThickness = 14.0;
        public double PulseCircleStrokeThickness
        {
            get { return _pulseCircleStrokeThickness; }
            set
            {
                _pulseCircleStrokeThickness = value;
                OnPropertyChanged(nameof(PulseCircleStrokeThickness));
            }
        }


        public static readonly DependencyProperty AnimateProperty = DependencyProperty.Register(
            nameof(Animate), typeof (bool), typeof (CircleProgress), new PropertyMetadata(default(bool), AnimatePropertyChanged));

        private static void AnimatePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var self = dependencyObject as CircleProgress;
            if (self == null)
            {
                return;
            }
            
            Storyboard storyBoard = null;
            if (self.HardwareRendering)
            {
                storyBoard = self.LayoutRoot.TryFindResource("ShineAnimation") as Storyboard;
            }
            else
            {
                storyBoard = self.LayoutRoot.TryFindResource("PulseAnimation") as Storyboard;
            }

            if (storyBoard == null)
                return;

            if ((bool)e.NewValue)
                storyBoard.Begin(self, true); //must use Begin(self) not Begin() for .net 3.0 compatibility
            else
                storyBoard.Stop(self); //must use Stop(self) not Stop() for .net 3.0 compatibility
            
            self.OnPropertyChanged(nameof(ShowShineRing));
            self.OnPropertyChanged(nameof(ShowPulseRing));
        }

        public bool Animate
        {
            get { return (bool) GetValue(AnimateProperty); }
            set { SetValue(AnimateProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof (string), typeof (CircleProgress), new PropertyMetadata(default(string)));

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty PercentProperty = DependencyProperty.Register(
            nameof(Percent), typeof (double), typeof (CircleProgress), new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.AffectsRender));

        public double Percent
        {
            get { return (double) GetValue(PercentProperty); }
            set { SetValue(PercentProperty, value); }
        }

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
            nameof(Icon), typeof (ImageSource), typeof (CircleProgress), new PropertyMetadata(default(ImageSource)));

        public ImageSource Icon
        {
            get { return (ImageSource) GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty ThumbStrokeBrushProperty = DependencyProperty.Register(
            nameof(ThumbStrokeBrush), typeof (Brush), typeof (CircleProgress), new PropertyMetadata(new SolidColorBrush(Colors.Black)));

        public Brush ThumbStrokeBrush
        {
            get { return (Brush) GetValue(ThumbStrokeBrushProperty); }
            set { SetValue(ThumbStrokeBrushProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register(
            nameof(MaxValue), typeof (double), typeof (CircleProgress), new PropertyMetadata(100.0, ProgressValuesChanged));

        public double MaxValue
        {
            get { return (double) GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty = DependencyProperty.Register(
            nameof(MinValue), typeof (double), typeof (CircleProgress), new PropertyMetadata(default(double), ProgressValuesChanged));

        public double MinValue
        {
            get { return (double) GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty ProgressValueProperty = DependencyProperty.Register(
            nameof(ProgressValue), typeof (double), typeof (CircleProgress), new PropertyMetadata(default(double), ProgressValuesChanged));


        public static readonly DependencyProperty ShineBrushProperty = DependencyProperty.Register(
           nameof(ShineBrush), typeof (Brush), typeof (CircleProgress), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(0x99, 0xff, 0xff, 0xff))));

        public Brush ShineBrush
        {
            get { return (Brush) GetValue(ShineBrushProperty); }
            set { SetValue(ShineBrushProperty, value); }
        }

        public bool HardwareRendering => WpfSpec.IsHardwareRendering;

        public bool ShowPulseRing => Animate && !HardwareRendering;
        public bool ShowShineRing => Animate && HardwareRendering;


        private static void ProgressValuesChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var self = dependencyObject as CircleProgress;
            if (self == null)
                return;

            if (e.NewValue == e.OldValue)
                return;

            var percent = self.ProgressValue / (self.MaxValue - self.MinValue);

            self.Percent = percent > 1 ? 1 : percent;
        }

        public double ProgressValue
        {
            get { return (double) GetValue(ProgressValueProperty); }
            set { SetValue(ProgressValueProperty, value); }
        }

        public static readonly DependencyProperty MaxIlluminationOpacityProperty = DependencyProperty.Register(
            nameof(MaxIlluminationOpacity), typeof (double), typeof (CircleProgress), new PropertyMetadata(1.0));

        public double MaxIlluminationOpacity
        {
            get { return (double) GetValue(MaxIlluminationOpacityProperty); }
            set { SetValue(MaxIlluminationOpacityProperty, value); }
        }

        public CircleProgress()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            UpdateThumbPosition();
        }

        void UpdateThumbPosition()
        {
            ThumbGrid.Margin = new Thickness((ActualWidth/8)+(BarThickness/2)-(ThumbGrid.ActualWidth/2)+2);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
