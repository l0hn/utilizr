﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;
using Utilizr.Logging;

namespace Utilzr.WPF
{
    public static class SafeInvoker
    {
        public static void SafeInvoke(this Dispatcher d, Action action)
        {
            try
            {
                if (!d.CheckAccess())
                    d.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        try
                        {
                            action.Invoke();
                        }
                        catch (Exception e)
                        {
                            Log.Exception(e);
                        }
                    }));
                else
                    action.Invoke();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        public static void SafeBeginInvoke(this Dispatcher d, Action action)
        {
            try
            {
                if (!d.CheckAccess())
                {
                    d.BeginInvoke((Action)(() =>
                    {
                        try
                        {
                            action.Invoke();
                        }
                        catch (Exception e)
                        {
                            Log.Exception(e);
                        }
                    }), DispatcherPriority.Normal);
                }
                else
                {
                    action();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
    }
}