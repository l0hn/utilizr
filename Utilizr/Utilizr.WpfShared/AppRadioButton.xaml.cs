﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilzr.WPF.Model;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class AppRadioButton : UserControl
    {

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register(
                nameof(IsChecked),
                typeof(bool),
                typeof(AppRadioButton),
                new PropertyMetadata(false)
            );

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        [Obsolete("Known WPF bug fixed in .NET 4.0 prevents use of bindings radio buttons.", true)]
        public static readonly DependencyProperty GroupNameProperty =
            DependencyProperty.Register(
                "GroupName",
                typeof(string),
                typeof(AppRadioButton),
                new PropertyMetadata("DefaultAppRadioButtonGroup")
            );

        /// <summary>
        /// .NET 3.0 and 3.5 have a bug with the radio buttons where the binding is removed when
        /// a radio button is unchecked. .NET 4.0 is fine since the addition of the new .NET 4.0
        /// API and DependencyObject.SetCurrentValue(). We also cannot bind to <see cref="IsChecked"/>
        /// and manually set the radio button's IsChecked property in code here, as any changes from
        /// the UI and then updating <see cref="IsChecked"/> would overwrite the binding, too!
        /// 
        /// The workaround is to use a CheckBox rather than RadioButton, but this makes the GroupName
        /// obsolete, and you will need to manually update the other AppRadioButton instances to ensure
        /// only one radio button is checked at any one time. Mostly likely using an enum for the buttons
        /// anyway, a property for each button with the getter checking the current enum value, and firing
        /// all the properties for the PropertyChanged event within the setter.
        /// 
        /// https://stackoverflow.com/a/4235809/1229237
        /// </summary>
        [Obsolete("Known WPF bug fixed in .NET 4.0 prevents use of bindings radio buttons.", true)]
        public string GroupName
        {
            get { return (string)GetValue(GroupNameProperty); }
            set { SetValue(GroupNameProperty, value); }
        }


        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                nameof(Command),
                typeof(ICommand),
                typeof(AppRadioButton),
                new PropertyMetadata(default(ICommand))
            );

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(AppRadioButton),
                new PropertyMetadata(default(string))
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty TextSizeProperty =
            DependencyProperty.Register(
                nameof(TextSize),
                typeof(double),
                typeof(AppRadioButton),
                new PropertyMetadata(12.0)
            );

        public double TextSize
        {
            get { return (double)GetValue(TextSizeProperty); }
            set { SetValue(TextSizeProperty, value); }
        }


        public static readonly DependencyProperty TextWeightProperty =
            DependencyProperty.Register(
                nameof(TextWeight),
                typeof(FontWeight),
                typeof(AppRadioButton),
                new PropertyMetadata(FontWeights.Normal)
            );

        public FontWeight TextWeight
        {
            get { return (FontWeight)GetValue(TextWeightProperty); }
            set { SetValue(TextWeightProperty, value); }
        }


        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register(
                nameof(TextWrapping),
                typeof(TextWrapping),
                typeof(AppRadioButton),
                new PropertyMetadata(TextWrapping.NoWrap)
            );

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }


        public static readonly DependencyProperty TextBrushProperty =
            DependencyProperty.Register(
                nameof(TextBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(TextBrush))
                )
            );

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }


        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register(
                nameof(TextMargin),
                typeof(Thickness),
                typeof(AppRadioButton),
                new PropertyMetadata(new Thickness(4,0,0,0))
            );

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }


        public static readonly DependencyProperty GlyphBrushProperty =
            DependencyProperty.Register(
                nameof(GlyphBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(GlyphBrush))
                )
            );

        public Brush GlyphBrush
        {
            get { return (Brush)GetValue(GlyphBrushProperty); }
            set { SetValue(GlyphBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalUncheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(NormalUncheckedBrush))
                )
            );

        public Brush NormalUncheckedBrush
        {
            get { return (Brush)GetValue(NormalUncheckedBrushProperty); }
            set { SetValue(NormalUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalCheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(NormalCheckedBrush))
                )
            );

        public Brush NormalCheckedBrush
        {
            get { return (Brush)GetValue(NormalCheckedBrushProperty); }
            set { SetValue(NormalCheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalBorderUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalBorderUncheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(NormalBorderUncheckedBrush))
                )
            );

        public Brush NormalBorderUncheckedBrush
        {
            get { return (Brush)GetValue(NormalBorderUncheckedBrushProperty); }
            set { SetValue(NormalBorderUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty NormalBorderCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(NormalBorderCheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(NormalBorderCheckedBrush))
                )
            );

        public Brush NormalBorderCheckedBrush
        {
            get { return (Brush)GetValue(NormalBorderCheckedBrushProperty); }
            set { SetValue(NormalBorderCheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty DarkUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DarkUncheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(DarkUncheckedBrush))
                )
            );

        public Brush DarkUncheckedBrush
        {
            get { return (Brush)GetValue(DarkUncheckedBrushProperty); }
            set { SetValue(DarkUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty DarkCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(DarkCheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(DarkCheckedBrush))
                )
            );

        public Brush DarkCheckedBrush
        {
            get { return (Brush)GetValue(DarkCheckedBrushProperty); }
            set { SetValue(DarkCheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty PressedUncheckedBrushProperty =
            DependencyProperty.Register(
                nameof(PressedUncheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(PressedUncheckedBrush))
                )
            );

        public Brush PressedUncheckedBrush
        {
            get { return (Brush)GetValue(PressedUncheckedBrushProperty); }
            set { SetValue(PressedUncheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty PressedCheckedBrushProperty =
            DependencyProperty.Register(
                nameof(PressedCheckedBrush),
                typeof(Brush),
                typeof(AppRadioButton),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppRadioButton), nameof(PressedCheckedBrush))
                )
            );

        public Brush PressedCheckedBrush
        {
            get { return (Brush)GetValue(PressedCheckedBrushProperty); }
            set { SetValue(PressedCheckedBrushProperty, value); }
        }


        public static readonly DependencyProperty CheckAlignmentProperty =
            DependencyProperty.Register(
                nameof(CheckAlignment),
                typeof(CheckAlignment),
                typeof(AppRadioButton),
                new PropertyMetadata(CheckAlignment.Left)
            );

        public CheckAlignment CheckAlignment
        {
            get { return (CheckAlignment)GetValue(CheckAlignmentProperty); }
            set { SetValue(CheckAlignmentProperty, value); }
        }


        public AppRadioButton()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }
    }
}
