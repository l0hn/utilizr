﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace Utilzr.WPF
{
    /// <summary>
    /// Interaction logic for ShadowTextBlock.xaml
    /// </summary>
    [ContentProperty("Inlines")]
    public partial class EffectTextBlock : UserControl, IAddChild
    {
        public static readonly DependencyProperty InlinesProperty = DependencyProperty.Register(
            "Inlines", typeof(ObservableCollection<Inline>), typeof(EffectTextBlock), new PropertyMetadata(default(ObservableCollection<Inline>), InlinesChanged));

        private static void InlinesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var self = d as EffectTextBlock;
            self.UpdateInlines();
        }

        public ObservableCollection<Inline> Inlines
        {
            get { return (ObservableCollection<Inline>) GetValue(InlinesProperty); }
            set { SetValue(InlinesProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof(string), typeof(EffectTextBlock), new PropertyMetadata(default(string)));
        
        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty SubtextBrushProperty = DependencyProperty.Register(
            "SubtextBrush", typeof(Brush), typeof(EffectTextBlock), new PropertyMetadata(default(Brush)));

        public Brush SubtextBrush
        {
            get { return (Brush) GetValue(SubtextBrushProperty); }
            set { SetValue(SubtextBrushProperty, value); }
        }

        public static readonly DependencyProperty TextEffectProperty = DependencyProperty.Register(
            "TextEffect", typeof(SafeShadowEffect), typeof(EffectTextBlock), new FrameworkPropertyMetadata(default(SafeShadowEffect), FrameworkPropertyMetadataOptions.AffectsRender));

        public SafeShadowEffect TextEffect
        {
            get { return (SafeShadowEffect) GetValue(TextEffectProperty); }
            set { SetValue(TextEffectProperty, value); }
        }

        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register(
            "TextWrapping", typeof(TextWrapping), typeof(EffectTextBlock), new PropertyMetadata(default(TextWrapping)));

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping) GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }
        

        public static readonly DependencyProperty TextDecorationsProperty = DependencyProperty.Register(
            "TextDecorations", typeof(TextDecorationCollection), typeof(EffectTextBlock), new PropertyMetadata(default(TextDecorationCollection)));

        public TextDecorationCollection TextDecorations
        {
            get { return (TextDecorationCollection) GetValue(TextDecorationsProperty); }
            set { SetValue(TextDecorationsProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register(
            "TextAlignment", typeof(TextAlignment), typeof(EffectTextBlock), new PropertyMetadata(default(TextAlignment)));

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment) GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }


        public static readonly DependencyProperty TextTrimmingProperty =
            DependencyProperty.Register(
                nameof(TextTrimming),
                typeof(TextTrimming),
                typeof(EffectTextBlock),
                new PropertyMetadata(TextTrimming.None)
            );

        public TextTrimming TextTrimming
        {
            get { return (TextTrimming)GetValue(TextTrimmingProperty); }
            set { SetValue(TextTrimmingProperty, value); }
        }


        public EffectTextBlock()
        {
            Inlines = new ObservableCollection<Inline>();
            Inlines.CollectionChanged += (sender, args) => UpdateInlines();
            InitializeComponent();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            UpdateInlines();
        }

        void UpdateInlines()
        {
            if (Inlines == null)
                return;

            if (Inlines.Count == 0)
            {
                return;
            }

            ShadowBox.Inlines.Clear();
            TopBox.Inlines.Clear();

            foreach (var inline in Inlines)
            {
                TopBox.Inlines.Add(inline);
                ShadowBox.Inlines.Add(CopyInline(inline));
                
                if (Inlines.Count > 1)
                {
                    ShadowBox.Inlines.Add(" ");
                    TopBox.Inlines.Add(" ");
                }
            }

            if (Inlines.Count > 1)
            {
                ShadowBox.Inlines.Remove(ShadowBox.Inlines.Last());
                TopBox.Inlines.Remove(TopBox.Inlines.Last());
            }
        }

        Inline CopyInline(Inline inline)
        {
            var text = XamlWriter.Save(inline);
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(text)))
            {
                return (Inline)XamlReader.Load(stream);
            }
            
        }
    }
}
