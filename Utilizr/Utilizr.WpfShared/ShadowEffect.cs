﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public class SafeShadowEffect: DependencyObject
    {
        public static readonly DependencyProperty ShadowEffectProperty = DependencyProperty.RegisterAttached(
            "ShadowEffect", 
            typeof(SafeShadowEffect), 
            typeof(SafeShadowEffect), 
            new FrameworkPropertyMetadata(default(SafeShadowEffect), FrameworkPropertyMetadataOptions.AffectsRender, ShadowEffectPropertyChanged));

        private static void ShadowEffectPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = (UIElement) d;

            if (e.NewValue == null)
            {
                element.SetDropShadowEffect(enable:false, hardwareRenderOnly:false);
                return;
            }

            var effect = (SafeShadowEffect) e.NewValue;
            element.SetDropShadowEffect(effect.Direction,
                effect.BlurRadius,
                effect.Color,
                effect.Opacity,
                effect.ShadowDepth,
                true,
                false
            );
        }

        public static void SetShadowEffect(UIElement element, SafeShadowEffect value)
        {
            element.SetValue(ShadowEffectProperty, value);
        }

        public static SafeShadowEffect GetShadowEffect(UIElement element)
        {
            return (SafeShadowEffect)element.GetValue(ShadowEffectProperty);
        }

        public static readonly DependencyProperty ShadowDepthProperty = DependencyProperty.Register(
            "ShadowDepth", typeof(double), typeof(SafeShadowEffect), new PropertyMetadata(default(double)));
        
        public double ShadowDepth
        {
            get { return (double) GetValue(ShadowDepthProperty); }
            set { SetValue(ShadowDepthProperty, value); }
        }

        public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register(
            "Direction", typeof(double), typeof(SafeShadowEffect), new PropertyMetadata(default(double)));

        public double Direction
        {
            get { return (double) GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        public static readonly DependencyProperty BlurRadiusProperty = DependencyProperty.Register(
            "BlurRadius", typeof(double), typeof(SafeShadowEffect), new PropertyMetadata(default(double)));

        public double BlurRadius
        {
            get { return (double) GetValue(BlurRadiusProperty); }
            set { SetValue(BlurRadiusProperty, value); }
        }

        public static readonly DependencyProperty OpacityProperty = DependencyProperty.Register(
            "Opacity", typeof(double), typeof(SafeShadowEffect), new PropertyMetadata(default(double)));

        public double Opacity
        {
            get { return (double) GetValue(OpacityProperty); }
            set { SetValue(OpacityProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register(
            "Color", typeof(Color), typeof(SafeShadowEffect), new PropertyMetadata(default(Color)));

        public Color Color
        {
            get { return (Color) GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }
    }
}
