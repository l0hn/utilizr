﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using Utilizr.Logging;
using Utilizr.Windows;

namespace Utilzr.WPF.Extension
{
    public static class WindowEx
    {
        public static void OpenWindowAndBringIntoFocus(this Window window)
        {
            try
            {
                if (window.WindowState == WindowState.Minimized)
                    window.WindowState = WindowState.Normal;

                window.Show();

                var winInterop = new WindowInteropHelper(window);
                Win32.SetForegroundWindow(winInterop.Handle);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

            // Try old method, too. Win32 approach not always working, apparently
            try
            {
                window.Show();
                window.Activate();
                window.WindowState = WindowState.Normal;
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
            }
            catch (Exception innerEx)
            {
                Log.Exception(innerEx);
            }
        }
    }
}