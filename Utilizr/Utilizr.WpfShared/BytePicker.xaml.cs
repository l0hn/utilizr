﻿using GetText;
using System.Windows;

namespace Utilzr.WPF
{
    /// <summary>
    /// Base 2
    /// </summary>
    public enum ByteUnit
    {
        Byte = 0,
        Kilobyte = 1,
        Megabyte = 2,
        Gigabyte = 3,
        Terabyte = 4,
        Petabyte = 5,
    }

    public partial class BytePicker : AbstractPicker<ByteUnit>
    {
        public static readonly DependencyProperty RepeaterHeightProperty =
            DependencyProperty.Register(
                nameof(RepeaterHeight),
                typeof(double),
                typeof(BytePicker),
                new PropertyMetadata(12.0)
            );

        public double RepeaterHeight
        {
            get { return (double)GetValue(RepeaterHeightProperty); }
            set { SetValue(RepeaterHeightProperty, value); }
        }

        public BytePicker()
        {
            InitializeComponent();
            Setup(LayoutRoot, SelectionWheel, upperButton, lowerButton, this);
        }
    }

    public static class ByteUnitExtensions
    {
        public static string ToLocale(this ByteUnit byteUnit)
        {
            switch (byteUnit)
            {
                case ByteUnit.Byte:
                    return L._("B");
                case ByteUnit.Kilobyte:
                    return L._("KB");
                case ByteUnit.Megabyte:
                    return L._("MB");
                case ByteUnit.Gigabyte:
                    return L._("GB");
                case ByteUnit.Terabyte:
                    return L._("TB");
                case ByteUnit.Petabyte:
                    return L._("PB");
            }

            return string.Empty;
        }
    }
}