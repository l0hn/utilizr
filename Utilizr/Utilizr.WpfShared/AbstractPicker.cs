﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Utilizr.Extensions;
using Utilzr.WPF.Shapes;

namespace Utilzr.WPF
{
    public class AbstractPicker<T> : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event SelectionWheel<T>.MouseClickEvent WheelClick;

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register(
                nameof(Items),
                typeof(List<T>),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(null)
            );
        public List<T> Items
        {
            get { return (List<T>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }


        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register(
                nameof(Selected),
                typeof(T),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(default(T))
            );
        public T Selected
        {
            get { return (T)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }


        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(BackgroundBrush),
                typeof(Brush),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.White))
            );
        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty ForegroundBrushProperty =
            DependencyProperty.Register(
                nameof(ForegroundBrush),
                typeof(Brush),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.Black))
            );
        public Brush ForegroundBrush
        {
            get { return (Brush)GetValue(ForegroundBrushProperty); }
            set { SetValue(ForegroundBrushProperty, value); }
        }


        public static readonly DependencyProperty HighlightBrushProperty =
            DependencyProperty.Register(
                nameof(HighlightBrush),
                typeof(Brush),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.LightGray))
            );

        public Brush HighlightBrush
        {
            get { return (Brush)GetValue(HighlightBrushProperty); }
            set { SetValue(HighlightBrushProperty, value); }
        }


        public static readonly DependencyProperty FocusBrushProperty =
            DependencyProperty.Register(
                nameof(FocusBrush),
                typeof(Brush),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.Gray))
            );
        public Brush FocusBrush
        {
            get { return (Brush)GetValue(FocusBrushProperty); }
            set { SetValue(FocusBrushProperty, value); }
        }


        public static readonly DependencyProperty WheelHeightProperty =
            DependencyProperty.Register(
                nameof(WheelHeight),
                typeof(double),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(75.0)
            );
        public double WheelHeight
        {
            get { return (double)GetValue(WheelHeightProperty); }
            set { SetValue(WheelHeightProperty, value); }
        }


        public static readonly DependencyProperty WheelWidthProperty =
            DependencyProperty.Register(
                nameof(WheelWidth),
                typeof(double),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(80.0)
            );
        public double WheelWidth
        {
            get { return (double)GetValue(WheelWidthProperty); }
            set { SetValue(WheelWidthProperty, value); }
        }


        public static readonly DependencyProperty RowsToDisplayProperty =
            DependencyProperty.Register(
                nameof(RowsToDisplay),
                typeof(double),
                typeof(AbstractPicker<T>),
                new PropertyMetadata(5.0)
            );

        public double RowsToDisplay
        {
            get { return (double)GetValue(RowsToDisplayProperty); }
            set { SetValue(RowsToDisplayProperty, value); }
        }


        private bool _showRepeaterButtons;
        public bool ShowRepeaterButtons
        {
            get { return _showRepeaterButtons; }
            set
            {
                _showRepeaterButtons = value;
                OnPropertyChanged(nameof(ShowRepeaterButtons));
            }
        }

        public ICommand IncreaseIndexCommand { get; }
        public ICommand DecreaseIndexCommand { get; }


        public SelectionWheel<T> ContentWheel { private set; get; }

        private RepeatButton _upButton;
        private RepeatButton _downButton;

        public AbstractPicker()
        {
            MouseLeave += (s, e) => UpdateRepeaterButtons(false);

            IncreaseIndexCommand = new RelayCommand((obj) => UpdateSelected(1));
            DecreaseIndexCommand = new RelayCommand((obj) => UpdateSelected(-1));
        }

        void UpdateSelected(int indexDelta)
        {
            var newIndex = Items.IndexOf(Selected) + indexDelta;
            SetCurrentValue(SelectedProperty, Items[Items.WrappedIndex(newIndex)]);
        }
        
        public void Setup(FrameworkElement layoutRoot, SelectionWheel<T> selectionWheel, RepeatButton up, RepeatButton down, object datacontext = null)
        {
            layoutRoot.DataContext = datacontext ?? this;

            ContentWheel = selectionWheel;
            _upButton = up;
            _downButton = down;

            ContentWheel.MouseOverAndAbove += () => UpdateRepeaterButtons(true);
            ContentWheel.MouseOverAndBelow += () => UpdateRepeaterButtons(true);
            ContentWheel.MouseGone += () => UpdateRepeaterButtons(false);
            ContentWheel.MouseOverAndInSelection += () => UpdateRepeaterButtons(false);
            ContentWheel.Click += (location) => OnWheelClick(location);
        }

        void UpdateRepeaterButtons(bool isVisible)
        {
            ShowRepeaterButtons = _downButton?.IsMouseOver == true ||
                                  _upButton?.IsMouseOver == true ||
                                  isVisible;
        }

        protected void RepeatButton_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            ContentWheel?.HandleMouseWheel(e);
            e.Handled = true;
        }

        protected virtual void OnWheelClick(SelectionWheel<T>.Location location)
        {
            WheelClick?.Invoke(location);
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}