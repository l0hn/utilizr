﻿using System;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Utilzr.WPF.Animation
{
    public class SolidColorBrushAnimation : ColorAnimation
    {
        public SolidColorBrush Brush
        {
            get { return To.HasValue ? new SolidColorBrush(To.Value).Clone() : null; }
            set { To = value?.Color; }
        }
    }
}