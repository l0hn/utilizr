﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Animation;

namespace Utilzr.WPF.Animation
{
    public enum SafeEasingFunction
    {
        None,
        BackEase,
        BounceEase,
        CircleEase,
        CubicEase,
        ElasticEase,
        ExponentialEase,
        PowerEase,
        QuadraticEase,
        QuarticEase,
        QuinticEase,
        SineEase,
    }

    public enum SafeEasingMode
    {
        EaseIn = 0,
        EaseOut = 1,
        EaseInOut = 2,
    }

    /// <summary>
    /// Exposes some functionality on .NET 4.0+ which will gracefully fail.
    /// </summary>
    public static class SafeDoubleAnimation
    {
        #region EasingFunction Attached Property
        public static readonly DependencyProperty EasingFunctionProperty =
            DependencyProperty.RegisterAttached(
                "EasingFunction",
                typeof(SafeEasingFunction),
                typeof(SafeDoubleAnimation),
                new PropertyMetadata(SafeEasingFunction.None, OnEasingFunctionChanged)
            );

        static void OnEasingFunctionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is DoubleAnimationBase doubleAnimation))
                return;

            if (!(e.NewValue is SafeEasingFunction easingFunction))
                return;

            var easingMode = GetEasingMode(doubleAnimation);
            UpdateAnimation(doubleAnimation, easingFunction, easingMode);
        }

        public static SafeEasingFunction GetEasingFunction(DoubleAnimationBase obj)
        {
            return (SafeEasingFunction)obj.GetValue(EasingFunctionProperty);
        }

        public static void SetEasingFunction(DoubleAnimationBase obj, SafeEasingFunction val)
        {
            obj.SetValue(EasingFunctionProperty, val);
        }
        #endregion

        #region EasingMode Attached Property
        public static readonly DependencyProperty EasingModeProperty =
            DependencyProperty.RegisterAttached(
                "EasingMode",
                typeof(SafeEasingMode),
                typeof(SafeDoubleAnimation),
                new PropertyMetadata(SafeEasingMode.EaseIn, OnEasingModeChanged)
            );

        static void OnEasingModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is DoubleAnimationBase doubleAnimation))
                return;

            if (!(e.NewValue is SafeEasingMode easingMode))
                return;

            var easingFunction = GetEasingFunction(doubleAnimation);
            UpdateAnimation(doubleAnimation, easingFunction, easingMode);
        }

        public static SafeEasingMode GetEasingMode(DoubleAnimationBase obj)
        {
            return (SafeEasingMode)obj.GetValue(EasingModeProperty);
        }

        public static void SetEasingMode(DoubleAnimationBase obj, SafeEasingMode val)
        {
            obj.SetValue(EasingModeProperty, val);
        }
        #endregion


        static readonly Type _easingModeEnumType;
        static readonly Type _easingFunctionBaseClass;
        static readonly Dictionary<SafeEasingMode, object> _easingModeEnums;
        static readonly Dictionary<SafeEasingFunction, Type> _easingFunctionClasses;

        static SafeDoubleAnimation()
        {
            try
            {
                _easingModeEnumType = Type.GetType($"System.Windows.Media.Animation.EasingMode, PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
                _easingFunctionBaseClass = Type.GetType($"System.Windows.Media.Animation.EasingFunctionBase, PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");

                _easingFunctionClasses = new Dictionary<SafeEasingFunction, Type>();
                foreach (SafeEasingFunction easingFunctionEnum in Enum.GetValues(typeof(SafeEasingFunction)))
                {
                    _easingFunctionClasses.Add(
                        easingFunctionEnum,
                        Type.GetType($"System.Windows.Media.Animation.{easingFunctionEnum}, PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35")
                    );
                }

                _easingModeEnums = new Dictionary<SafeEasingMode, object>();
                foreach (SafeEasingMode easingModeEnum in Enum.GetValues(typeof(SafeEasingMode)))
                {
                    _easingModeEnums.Add(
                        easingModeEnum,
                        Enum.Parse(_easingModeEnumType, easingModeEnum.ToString())
                    );
                }
            }
            catch (Exception ex)
            {

            }
        }        


        static void UpdateAnimation(DoubleAnimationBase doubleAnimation, SafeEasingFunction easingFunction, SafeEasingMode easingMode)
        {
            // attempt to set with reflection
            try
            {
                var property = doubleAnimation.GetType().GetProperty("EasingFunction");
                if (property == null)
                    return;

                var easingType = _easingFunctionClasses[easingFunction];
                var easingInstance = Activator.CreateInstance(easingType);

                if (easingInstance == null)
                    return;

                var easingModeProperty = easingType.GetProperty("EasingMode");
                if (easingModeProperty != null)
                {
                    easingModeProperty.SetValue(easingInstance, easingMode, null);
                }

                property.SetValue(doubleAnimation, easingInstance, null);
            }
            catch (Exception ex)
            {

            }
        }

    }
}