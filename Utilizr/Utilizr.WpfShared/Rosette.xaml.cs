﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class Rosette : UserControl
    {
        public static readonly DependencyProperty RosetteThicknessProperty =
            DependencyProperty.Register(
                nameof(RosetteThickness),
                typeof(double),
                typeof(Rosette),
                new PropertyMetadata(6.0, UpdateRosetteTextMargin)
            );

        public double RosetteThickness
        {
            get { return (double)GetValue(RosetteThicknessProperty); }
            set { SetValue(RosetteThicknessProperty, value); }
        }


        public static readonly DependencyProperty RosetteBorderThicknessProperty =
            DependencyProperty.Register(
                    nameof(RosetteBorderThickness),
                    typeof(double),
                    typeof(Rosette),
                    new PropertyMetadata(3.0)//, UpdateRosetteTextMargin)
            );

        private static void UpdateRosetteTextMargin(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Rosette)?.UpdateRosetteTextMargin();
        }

        public double RosetteBorderThickness
        {
            get { return (double)GetValue(RosetteBorderThicknessProperty); }
            set { SetValue(RosetteBorderThicknessProperty, value); }
        }


        public static readonly DependencyProperty TextSizeProperty =
            DependencyProperty.Register(
                nameof(TextSize),
                typeof(double),
                typeof(Rosette),
                new PropertyMetadata(12.0)
            );

        public double TextSize
        {
            get { return (double)GetValue(TextSizeProperty); }
            set { SetValue(TextSizeProperty, value); }
        }


        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(Rosette),
                new PropertyMetadata(default(string))
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty TextWeightProperty =
            DependencyProperty.Register(
                nameof(TextWeight),
                typeof(FontWeight),
                typeof(Rosette),
                new PropertyMetadata(FontWeights.Normal)
            );

        public FontWeight TextWeight
        {
            get { return (FontWeight)GetValue(TextWeightProperty); }
            set { SetValue(TextWeightProperty, value); }
        }


        public static readonly DependencyProperty RosetteBackgroundProperty =
            DependencyProperty.Register(
                nameof(RosetteBackground),
                typeof(Brush),
                typeof(Rosette),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(Rosette),
                        nameof(RosetteBackground)
                    )
                )
            );

        public Brush RosetteBackground
        {
            get { return (Brush)GetValue(RosetteBackgroundProperty); }
            set { SetValue(RosetteBackgroundProperty, value); }
        }


        public static readonly DependencyProperty RosetteForegroundProperty =
            DependencyProperty.Register(
                nameof(RosetteForeground),
                typeof(Brush),
                typeof(Rosette),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(
                        nameof(Rosette),
                        nameof(RosetteForeground)
                    )
                )
            );

        public Brush RosetteForeground
        {
            get { return (Brush)GetValue(RosetteForegroundProperty); }
            set { SetValue(RosetteForegroundProperty, value); }
        }


        public static readonly DependencyProperty RosetteTextMarginProperty =
            DependencyProperty.Register(
                nameof(RosetteTextMargin),
                typeof(double),
                typeof(Rosette),
                new PropertyMetadata(6.0)
            );

        public double RosetteTextMargin
        {
            get { return (double)GetValue(RosetteTextMarginProperty); }
            set { SetValue(RosetteTextMarginProperty, value); }
        }



        public Rosette()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;

            UpdateRosetteTextMargin();
        }

        private void UpdateRosetteTextMargin()
        {
            RosetteTextMargin = RosetteThickness;
        }
    }
}
