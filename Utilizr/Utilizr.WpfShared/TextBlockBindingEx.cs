﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Utilzr.WPF
{
    /// <summary>
    /// Sub class of <see cref="TextBlock"/> which have dependency property versions of some original
    /// properties to support data binding.
    /// </summary>
    public class TextBlockBindingEx : TextBlock
    {

        public static readonly DependencyProperty InlinesBindableProperty =
            DependencyProperty.Register(
                nameof(InlinesBindable),
                typeof(ObservableCollection<Inline>),
                typeof(TextBlockBindingEx),
                new PropertyMetadata(null, OnInlinesBindableChanged)
            );

        private static void OnInlinesBindableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is ObservableCollection<Inline> oldValue)
                oldValue.CollectionChanged -= Inlines_CollectionChanged;

            if (e.NewValue is ObservableCollection<Inline> newValue)
                newValue.CollectionChanged -= Inlines_CollectionChanged;

            if (!(d is TextBlockBindingEx txtBlockEx))
                return;

            Inlines_CollectionChanged(txtBlockEx, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private static void Inlines_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!(sender is TextBlockBindingEx txtBlockEx))
                return;

            // TODO: Can switch on e.Action and add, remove, reset, move, etc, to improve performance
            txtBlockEx.Inlines.Clear();
            txtBlockEx.Inlines.AddRange(txtBlockEx.InlinesBindable);
        }

        /// <summary>
        /// A bindable equivalent of the <see cref="TextBlock.Inlines"/> property.
        /// </summary>
        public ObservableCollection<Inline> InlinesBindable
        {
            get { return (ObservableCollection<Inline>)GetValue(InlinesBindableProperty); }
            set { SetValue(InlinesBindableProperty, value); }
        }
    }
}