﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Utilizr;
using Utilizr.Logging;

namespace Utilzr.WPF
{
    public partial class FileSizeTextBox : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public static readonly DependencyProperty SizeInfoProperty =
            DependencyProperty.Register(
                nameof(SizeInfo),
                typeof(FileSizeTextBoxInfo),
                typeof(FileSizeTextBox),
                new PropertyMetadata(
                    null,
                    (d, e) =>
                    {
                        if (!(d is FileSizeTextBox fsTextBox))
                            return;

                        void propertyChanged(object sender, PropertyChangedEventArgs e) => fsTextBox.OnPropertyChanged(nameof(SizeInfo));
                        void sizeChanged(object sender, EventArgs e) => fsTextBox.OnPropertyChanged(nameof(ShowCustomUpperValue));

                        if (e.NewValue is FileSizeTextBoxInfo newSizeInfo && newSizeInfo != null)
                        {
                            newSizeInfo.PropertyChanged += propertyChanged;
                            newSizeInfo.SizeChanged += sizeChanged;
                        }

                        if (e.OldValue is FileSizeTextBoxInfo oldSizeInfo && oldSizeInfo != null)
                        {
                            oldSizeInfo.PropertyChanged -= propertyChanged;
                            oldSizeInfo.SizeChanged -= sizeChanged;
                        }
                    }
                )
            );

        public FileSizeTextBoxInfo SizeInfo
        {
            get { return (FileSizeTextBoxInfo)GetValue(SizeInfoProperty); }
            set { SetValue(SizeInfoProperty, value); }
        }


        public static readonly DependencyProperty IncrementDeltaProperty =
            DependencyProperty.Register(
                nameof(IncrementDelta),
                typeof(int),
                typeof(FileSizeTextBox),
                new PropertyMetadata(1)
            );

        /// <summary>
        /// Increment per selected unit
        /// </summary>
        public int IncrementDelta
        {
            get { return (int)GetValue(IncrementDeltaProperty); }
            set { SetValue(IncrementDeltaProperty, value); }
        }


        public static readonly DependencyProperty UnitsProperty =
            DependencyProperty.Register(
                nameof(Units),
                typeof(List<ByteUnit>),
                typeof(FileSizeTextBox),
                new PropertyMetadata(
                    new List<ByteUnit>
                    {
                        ByteUnit.Terabyte,
                        ByteUnit.Petabyte,
                        ByteUnit.Gigabyte,
                        ByteUnit.Megabyte,
                        ByteUnit.Kilobyte,
                        ByteUnit.Byte
                    }
                )
            );

        public List<ByteUnit> Units
        {
            get { return (List<ByteUnit>)GetValue(UnitsProperty); }
            set { SetValue(UnitsProperty, value); }
        }


        public static readonly DependencyProperty SizeAlignmentProperty =
            DependencyProperty.Register(
                nameof(SizeAlignment),
                typeof(TextAlignment),
                typeof(FileSizeTextBox),
                new PropertyMetadata(TextAlignment.Left)
            );

        public TextAlignment SizeAlignment
        {
            get { return (TextAlignment)GetValue(SizeAlignmentProperty); }
            set { SetValue(SizeAlignmentProperty, value); }
        }


        public static readonly DependencyProperty PrefixTextProperty =
            DependencyProperty.Register(
                nameof(PrefixText),
                typeof(string),
                typeof(FileSizeTextBox),
                new PropertyMetadata(string.Empty)
            );

        public string PrefixText
        {
            get { return (string)GetValue(PrefixTextProperty); }
            set { SetValue(PrefixTextProperty, value); }
        }


        public bool HasCustomUpperValue => !string.IsNullOrEmpty(CustomUpperValueText);

        public static readonly DependencyProperty CustomUpperValueTextProperty =
            DependencyProperty.Register(
                nameof(CustomUpperValueText),
                typeof(string),
                typeof(FileSizeTextBox),
                new PropertyMetadata(
                    null,
                    (d, e) => (d as FileSizeTextBox)?.OnPropertyChanged(nameof(HasCustomUpperValue))
                )
            );

        public string CustomUpperValueText
        {
            get { return (string)GetValue(CustomUpperValueTextProperty); }
            set { SetValue(CustomUpperValueTextProperty, value); }
        }

        public bool ShowCustomUpperValue => SizeInfo.SizeRawBytes == SizeInfo.MaximumBytes &&
                                            CustomUpperValueText.IsNotNullOrEmpty() &&
                                            !(IsFocused || IsKeyboardFocused || IsMouseOver); // allow editing


        public FileSizeTextBox()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;

            GotFocus += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));
            LostFocus += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));
            GotKeyboardFocus += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));
            LostKeyboardFocus += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));
            MouseEnter += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));
            MouseLeave += (s, e) => OnPropertyChanged(nameof(ShowCustomUpperValue));

            Loaded += (s, e) =>
            {
                DataObject.AddPastingHandler(TxtBox, OnPaste);
                TxtBox.PreviewTextInput += OnPreviewTextInput;
            };
        }

        void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            try
            {
                if (e.DataObject.GetDataPresent(DataFormats.Text))
                {
                    var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text));

                    if (!double.TryParse(text, out double number) || (SizeInfo?.IsWithinThresholds(number) != true))
                    {
                        e.CancelCommand();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(nameof(FileSizeTextBox), ex);
                e.CancelCommand();
            }
        }

        void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // Needs to be a preview event to stop non-numeric being added
            // But that also means we need to calculate what the entire text will be

            try
            {
                var txtBox = (TextBox)sender;
                var newText = e.Text;
                var currText = TxtBox.Text;

                string allText = e.Text;

                if (!string.IsNullOrEmpty(currText))
                {
                    var sb = new StringBuilder(currText);
                    sb.Insert(txtBox.CaretIndex, newText);
                    allText = sb.ToString();
                }

                if (!double.TryParse(allText, out double num) || (SizeInfo?.IsWithinThresholds(num) != true))
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                Log.Exception(nameof(FileSizeTextBox), ex);
                e.Handled = true;
            }
        }

        void Up_Click(object sender, RoutedEventArgs e)
        {
            SizeInfo.SizeUnitBytes += IncrementDelta;
        }

        void Down_Click(object sender, RoutedEventArgs e)
        {
            SizeInfo.SizeUnitBytes -= IncrementDelta;
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [DebuggerDisplay("SizeRawBytes={SizeRawBytes}, SizeUnitBytes={SizeUnitBytes}, SelectedUnit={SelectedUnit}")]
    public class FileSizeTextBoxInfo : INotifyPropertyChanged
    {
        public event EventHandler SizeChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private double _sizeRawBytes;
        public double SizeRawBytes
        {
            get { return _sizeRawBytes; }
            set
            {
                // validate changes, update gui size unit bytes values
                _sizeRawBytes = ValidateBounds(value);
                _sizeUnitBytes = Math.Round(_sizeRawBytes / OffsetForUnit(SelectedUnit), 0);

                OnPropertyChanged(
                    nameof(SizeRawBytes),
                    nameof(SizeUnitBytes)
                );

                OnSizeChanged();
            }
        }

        private double _sizeUnitBytes;
        public double SizeUnitBytes
        {
            get { return _sizeUnitBytes; }
            set
            {
                // user changed values from gui text, update underlying raw values
                _sizeRawBytes = ValidateBounds(value * OffsetForUnit(SelectedUnit));
                // recalculate for potentially different raw values adjusted for min/max boundary
                _sizeUnitBytes = Math.Round(_sizeRawBytes / OffsetForUnit(SelectedUnit), 0);

                OnPropertyChanged(
                    nameof(SizeRawBytes),
                    nameof(SizeUnitBytes)
                );

                OnSizeChanged();
            }
        }

        private ByteUnit _selectedUnit;
        public ByteUnit SelectedUnit
        {
            get { return _selectedUnit; }
            set
            {
                _selectedUnit = value;

                // gui has the same value, but raw will need to change
                // also check new unit doesn't go outside of bounds

                var offset = OffsetForUnit(value);
                var newRawBytes = ValidateBounds(SizeUnitBytes * offset);

                _sizeRawBytes = newRawBytes;
                // will reflect any min/max boundary change
                _sizeUnitBytes = Math.Round(newRawBytes / offset, 0);

                OnPropertyChanged(
                    nameof(SizeRawBytes),
                    nameof(SizeUnitBytes),
                    nameof(SelectedUnit)
                );

                OnSizeChanged();
            }
        }

        /// <summary>
        /// Raw bytes
        /// </summary>
        public double MinimumBytes { get; }
        /// <summary>
        /// Raw bytes
        /// </summary>
        public double MaximumBytes { get; }

        public FileSizeTextBoxInfo(double sizeRawBytes, ByteUnit unit, double minimumBytes, double maximumBytes)
        {
            // backing field not to invalidate;
            _selectedUnit = unit;
            MinimumBytes = minimumBytes;
            MaximumBytes = maximumBytes;

            // setter to propagate changes
            SizeRawBytes = sizeRawBytes;
        }

        public static double OffsetForUnit(ByteUnit unit)
        {
            var power = (int)unit;
            return Math.Pow(1024, power);
        }

        double ValidateBounds(double newRawBytes)
        {
            if (newRawBytes < MinimumBytes)
                return MinimumBytes;

            if (newRawBytes > MaximumBytes)
                return MaximumBytes;

            return newRawBytes;
        }

        public bool IsWithinThresholds(double sizeRawBytes)
        {
            if (sizeRawBytes > MaximumBytes)
                return false;

            if (sizeRawBytes < MinimumBytes)
                return false;

            return true;
        }

        protected virtual void OnSizeChanged()
        {
            SizeChanged?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}