﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class SeverityBar : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                nameof(Value),
                typeof(double),
                typeof(SeverityBar),
                new PropertyMetadata(0.0, UpdateIndicator)
            );

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }


        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
                nameof(Maximum),
                typeof(double),
                typeof(SeverityBar),
                new PropertyMetadata(100.0, UpdateIndicator)
            );

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }


        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
                nameof(Minimum),
                typeof(double),
                typeof(SeverityBar),
                new PropertyMetadata(0.0, UpdateIndicator)
            );

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        private static void UpdateIndicator(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var severitBar = (d as SeverityBar);
            if (severitBar == null)
                return;

            severitBar.UpdateIndicator(severitBar.Track.ActualWidth);
        }


        public static readonly DependencyProperty IndicatorFillProperty =
            DependencyProperty.Register(
                nameof(IndicatorFill),
                typeof(Brush),
                typeof(SeverityBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SeverityBar), nameof(IndicatorFill))
                )
            );

        public Brush IndicatorFill
        {
            get { return (Brush)GetValue(IndicatorFillProperty); }
            set { SetValue(IndicatorFillProperty, value); }
        }


        public static readonly DependencyProperty TrackBarBackgroundProperty =
            DependencyProperty.Register(
                nameof(TrackBarBackground),
                typeof(Brush),
                typeof(SeverityBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SeverityBar), nameof(TrackBarBackground))
                )
            );

        public Brush TrackBarBackground
        {
            get { return (Brush)GetValue(TrackBarBackgroundProperty); }
            set { SetValue(TrackBarBackgroundProperty, value); }
        }


        public static readonly DependencyProperty SeverityBarBackgroundProperty =
            DependencyProperty.Register(
                nameof(SeverityBarBackground),
                typeof(Brush),
                typeof(SeverityBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(SeverityBar), nameof(SeverityBarBackground))
                )
            );

        public Brush SeverityBarBackground
        {
            get { return (Brush)GetValue(SeverityBarBackgroundProperty); }
            set { SetValue(SeverityBarBackgroundProperty, value); }
        }


        public static readonly DependencyProperty SnapProperty =
            DependencyProperty.Register(
                nameof(Snap),
                typeof(bool),
                typeof(SeverityBar), 
                new PropertyMetadata(true)
            );

        public bool Snap
        {
            get { return (bool)GetValue(SnapProperty); }
            set { SetValue(SnapProperty, value); }
        }



        public double IndividualGradientBarRectangleWidth {  get { return DrawingBrush.Viewport.Width; } }

        public SeverityBar()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
            SizeChanged += (s, e) =>
            {
                if (Snap)
                {
                    var newWidth = SnapWidth(e.NewSize);
                    UpdateIndicator(newWidth - Track.Margin.Left - Track.Margin.Right);
                }
                else
                {
                    UpdateIndicator(Track.ActualWidth);
                }
            };
        }

        private double SnapWidth(Size newSize)
        {
            double toSnap = newSize.Width % IndividualGradientBarRectangleWidth;
            double newWidth = newSize.Width - toSnap;
            LayoutRoot.Width = newWidth;
            return newWidth;
        }

        private void UpdateIndicator(double width)
        {
            double percentage = (Value - Minimum) / (Maximum - Minimum);
            double totalSpace = Math.Max(0, width - Indicator.ActualWidth);
            double marginOffset = totalSpace * percentage;

            //if (Snap)
            //{
            //    var bars = width / IndividualGradientBarRectnagleWidth;
            //    var bar = Math.Round(bars * percentage, 0);
            //    marginOffset = bar * IndividualGradientBarRectnagleWidth;
            //}

            Indicator.Margin = new Thickness(
                marginOffset,
                Indicator.Margin.Top,
                Indicator.Margin.Right,
                Indicator.Margin.Bottom
            );
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
