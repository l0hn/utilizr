﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Utilizr;

namespace Utilzr.WPF
{
    public abstract class ParentViewModel : INotifyPropertyChanged
    {
        private string _logCat = "";
        public string LogCat
        {
            get
            {
                if (_logCat.IsNullOrEmpty())
                {
                    _logCat = this.GetType().Name;
                }
                return _logCat;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Support for Fody, has to be public and accept single string argument
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Original, keep for legacy / plus manual invocations
        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}