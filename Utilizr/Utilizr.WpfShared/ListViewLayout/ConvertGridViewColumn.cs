﻿// -- FILE ------------------------------------------------------------------
// name       : ConverterGridViewColumn.cs
// created    : Jani Giannoudis - 2008.03.27
// language   : c#
// environment: .NET 3.0
// copyright  : (c) 2008-2012 by Itenso GmbH, Switzerland
// --------------------------------------------------------------------------
using System;
using System.Windows.Data;
using System.Windows.Controls;
using System.Globalization;

namespace Itenso.Windows.Controls.ListViewLayout
{
    public abstract class ConverterGridViewColumn : GridViewColumn, IValueConverter
    {
        private readonly Type _bindingType;
        public Type BindingType { get { return _bindingType; } }

        protected abstract object ConvertValue(object value);

        protected ConverterGridViewColumn(Type bindingType)
        {
            if (bindingType == null)
                throw new ArgumentNullException(nameof(bindingType));

            _bindingType = bindingType;

            Binding binding = new Binding();
            binding.Mode = BindingMode.OneWay;
            binding.Converter = this;
            DisplayMemberBinding = binding;
        }

        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!_bindingType.IsInstanceOfType(value))
                throw new InvalidOperationException();

            return ConvertValue(value);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}