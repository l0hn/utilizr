﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Utilzr.WPF.Behaviours
{
    public static class ScrollViewerBehaviours
    {
        public static readonly DependencyProperty AutoScrollToTopProperty =
            DependencyProperty.RegisterAttached(
                "AutoScrollToTop",
                typeof(bool),
                typeof(ScrollViewerBehaviours),
                new PropertyMetadata(false, OnAutoScrollTopChanged)
            );

        private static void OnAutoScrollTopChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is ScrollViewer scrollViewer))
                return;

            if (!(e.NewValue is bool bNewValue))
                return; // binding not yet setup

            if (bNewValue)
            {
                scrollViewer.ScrollToTop();
                SetAutoScrollToTop(d, false);
            }
        }

        public static bool GetAutoSrollToTop(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoScrollToTopProperty);
        }

        public static void SetAutoScrollToTop(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoScrollToTopProperty, value);
        }

    }
}
