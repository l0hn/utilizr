﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilizr.Logging;

namespace Utilzr.WPF.Behaviours
{
    /// <summary>
    /// Exposes attached behaviours which can scroll the horizontal ScrollBar
    /// of a ScrollViewer to the left, or to the right.
    /// </summary>
    public static class ScrollViewerHorizontalScrollBehaviour
    {
        #region OffsetDeletaChangeMinElapsedMs DependencyProperty
        /// <summary>
        /// Minimum elapsed time, in milliseconds, before incrementing the scrollbar's horizontal offset.
        /// </summary>
        public static readonly DependencyProperty OffsetDeltaChangeMinElapsedMsProperty =
            DependencyProperty.RegisterAttached(
                "OffsetDeltaChangeMinElapsedMs",
                typeof(int),
                typeof(ScrollViewerHorizontalScrollBehaviour),
                new PropertyMetadata(0)
            );

        public static int GetOffsetDeltaChangeMinElapsedMs(DependencyObject obj)
        {
            return (int)obj.GetValue(OffsetDeltaChangeMinElapsedMsProperty);
        }

        public static void SetOffsetDeltaChangeMinElapsedMs(DependencyObject obj, int offsetDeltaChangeMinElapsedMs)
        {
            obj.SetValue(OffsetDeltaChangeMinElapsedMsProperty, offsetDeltaChangeMinElapsedMs);
        }
        #endregion

        #region DeltaPerUpdate DependencyProperty
        /// <summary>
        /// The amount to increase / decrease the horizontal offset
        /// </summary>
        public static readonly DependencyProperty DeltaPerUpdateProprety =
            DependencyProperty.RegisterAttached(
                "DeltaPerUpdate",
                typeof(int),
                typeof(ScrollViewerHorizontalScrollBehaviour),
                new PropertyMetadata(1)
            );

        public static int GetDeltaPerUpdate(DependencyObject obj)
        {
            return (int)obj.GetValue(DeltaPerUpdateProprety);
        }

        public static void SetDeltaPerUpdate(DependencyObject obj, int deltaPerUpdate)
        {
            obj.SetValue(DeltaPerUpdateProprety, deltaPerUpdate);
        }
        #endregion

        #region IsScrollingLeft DependencyProperty
        /// <summary>
        /// Whether the ScrollViewer is currently scrolling to the left
        /// </summary>
        public static readonly DependencyProperty IsScrollingLeftProperty =
            DependencyProperty.RegisterAttached(
                "IsScrollingLeft",
                typeof(bool),
                typeof(ScrollViewerHorizontalScrollBehaviour),
                new UIPropertyMetadata(
                    false,
                    OnIsScrollingChanged
                )
            );

        public static bool GetIsScrollingLeft(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsScrollingLeftProperty);
        }

        public static void SetIsScrollingLeft(DependencyObject obj, bool isScrollingLeft)
        {
            obj.SetValue(IsScrollingLeftProperty, isScrollingLeft);
        }
        #endregion

        #region IsScrollingRight DependencyProperty
        /// <summary>
        /// Whether the ScrollViewer is currently scrolling to the right.
        /// </summary>
        public static readonly DependencyProperty IsScrollingRightProperty =
            DependencyProperty.RegisterAttached(
                "IsScrollingRight",
                typeof(bool),
                typeof(ScrollViewerHorizontalScrollBehaviour),
                new UIPropertyMetadata(
                    false,
                    OnIsScrollingChanged
                )
            );

        public static bool GetIsScrollingRight(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsScrollingRightProperty);
        }

        public static void SetIsScrollingRight(DependencyObject obj, bool isScrollingRight)
        {
            obj.SetValue(IsScrollingRightProperty, isScrollingRight);
        }
        #endregion

        private static void OnIsScrollingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                StartScrolling((ScrollViewer)d);
            }
            else
            {
                EndScrolling();
            }
        }


        private static double _hOffset;
        private static DateTime _lastDrawn;
        private static ScrollViewer _scollViewer;

        private static bool _cachedIsScrollingLeft;
        private static bool _cachedIsScollingRight;
        private static int _cachedOffsetMinElapsedMs;
        private static int _cachedDeltaPerUpdate;
        private static TimeSpan _previousTime;

        private static void StartScrolling(ScrollViewer target)
        {
            _scollViewer = target;
            _hOffset = target.HorizontalOffset;

            _cachedIsScrollingLeft = GetIsScrollingLeft(target);
            _cachedIsScollingRight = GetIsScrollingRight(target);
            _cachedOffsetMinElapsedMs = GetOffsetDeltaChangeMinElapsedMs(target);
            _cachedDeltaPerUpdate = GetDeltaPerUpdate(target);

            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        private static void EndScrolling()
        {
            CompositionTarget.Rendering -= CompositionTarget_Rendering;
            _scollViewer = null;
            _hOffset = -1;
            _lastDrawn = DateTime.MinValue;
        }

        private static void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            try
            {
                TimeSpan newTime = ((RenderingEventArgs)e).RenderingTime;
                if (newTime == _previousTime)
                    return; // Can be called more than once per frame

                _previousTime = newTime;
                bool unRegAfterDraw = false;

                if (_hOffset > _scollViewer.ScrollableWidth)
                {
                    _hOffset = _scollViewer.ScrollableWidth;
                    unRegAfterDraw = true;
                }
                else if (_hOffset < 0)
                {
                    _hOffset = 0;
                    unRegAfterDraw = true;
                }

                if ((DateTime.UtcNow - _lastDrawn).TotalMilliseconds > _cachedOffsetMinElapsedMs)
                {
                    if (_cachedIsScrollingLeft)
                        _hOffset -= _cachedDeltaPerUpdate;
                    else if (_cachedIsScollingRight)
                        _hOffset += _cachedDeltaPerUpdate;

                    _scollViewer.ScrollToHorizontalOffset(_hOffset);
                    _lastDrawn = DateTime.UtcNow;
                }

                if(unRegAfterDraw)
                    CompositionTarget.Rendering -= CompositionTarget_Rendering;
            }
            catch (Exception ex)
	        {
                CompositionTarget.Rendering -= CompositionTarget_Rendering;
                Log.Exception(nameof(ScrollViewerHorizontalScrollBehaviour), ex);
            }
        }
    }
}