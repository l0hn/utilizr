﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Utilzr.WPF.Behaviours
{
    public static class TreeViewBehaviours
    {
        private static readonly DependencyProperty CollapsedBehaviourProperty =
            DependencyProperty.RegisterAttached(
                "CollapsedBehaviour",
                typeof(ICommand),
                typeof(TreeViewBehaviours),
                new PropertyMetadata(default(ICommand), OnCollapsedBehaviourChanged)
            );

        public static void SetCollapsedBehaviour(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CollapsedBehaviourProperty, value);
        }

        public static ICommand GetCollapsedBehaviour(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CollapsedBehaviourProperty);
        }

        static void OnCollapsedBehaviourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tvi = d as TreeViewItem;
            if (tvi == null)
                return;

            var ic = e.NewValue as ICommand;
            if (ic == null)
                return;

            tvi.Collapsed += (s, e1) =>
            {
                // Using tvi.DataContext (same as s.DataContext) will always use model obj behind the TreeViewItem
                if (ic.CanExecute(tvi.DataContext))
                {
                    ic.Execute(tvi.DataContext);
                }

                e1.Handled = true;
            };
        }


        public static readonly DependencyProperty ExpandedBehaviourProperty =
            DependencyProperty.RegisterAttached(
                "ExpandedBehaviour",
                typeof(ICommand),
                typeof(TreeViewBehaviours),
                new PropertyMetadata(default(ICommand), OnExpandedBehaviourChanged)
            );

        public static void SetExpandedBehaviour(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ExpandedBehaviourProperty, value);
        }

        public static ICommand GetExpandedBehaviour(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(ExpandedBehaviourProperty);
        }

        static void OnExpandedBehaviourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tvi = d as TreeViewItem;
            if (tvi == null)
                return;

            var ic = e.NewValue as ICommand;
            if (ic == null)
                return;

            tvi.Expanded += (s, e1) =>
            {
                // Using tvi.DataContext (same as s.DataContext) will always use model obj behind the TreeViewItem
                if (ic.CanExecute(tvi.DataContext))
                {
                    ic.Execute(tvi.DataContext);
                }

                e1.Handled = true;
            };
        }

        public static readonly DependencyProperty SelectedBehaviourProperty =
            DependencyProperty.RegisterAttached(
                "SelectedBehaviour",
                typeof(ICommand),
                typeof(TreeViewBehaviours),
                new PropertyMetadata(default(ICommand), OnSelectedBehaviourChanged)
            );

        public static void SetSelectedBehaviour(DependencyObject obj, ICommand value)
        {
            obj.SetValue(SelectedBehaviourProperty, value);
        }

        public static ICommand GetSelectedBehaviour(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(SelectedBehaviourProperty);
        }

        static void OnSelectedBehaviourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tvi = d as TreeViewItem;
            if (tvi == null)
                return;

            var ic = e.NewValue as ICommand;
            if (ic == null)
                return;

            tvi.Selected += (s, e1) =>
            {
                // Using tvi.DataContext (same as s.DataContext) will always use model obj behind the TreeViewItem
                if (ic.CanExecute(tvi.DataContext))
                {
                    ic.Execute(tvi.DataContext);
                }

                e1.Handled = true;
            };
        }

        public static readonly DependencyProperty UnselectedBehaviourProperty =
            DependencyProperty.RegisterAttached(
                "UnselectedBehaviour",
                typeof(ICommand),
                typeof(TreeViewBehaviours),
                new PropertyMetadata(default(ICommand), OnUnselectedBehaviourChanged)
            );

        public static void SetUnselectedBehaviour(DependencyObject obj, ICommand value)
        {
            obj.SetValue(UnselectedBehaviourProperty, value);
        }

        public static ICommand GetUnselectedBehaviour(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(UnselectedBehaviourProperty);
        }

        static void OnUnselectedBehaviourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tvi = d as TreeViewItem;
            if (tvi == null)
                return;

            var ic = e.NewValue as ICommand;
            if (ic == null)
                return;

            tvi.Unselected += (s, e1) =>
            {
                // Using tvi.DataContext (same as s.DataContext) will always use model obj behind the TreeViewItem
                if (ic.CanExecute(tvi.DataContext))
                {
                    ic.Execute(tvi.DataContext);
                }

                e1.Handled = true;
            };
        }
    }
}
