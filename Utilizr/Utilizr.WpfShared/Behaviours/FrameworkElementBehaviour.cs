﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Utilzr.WPF.Behaviours
{
    public static class FrameworkElementBehaviour
    {
        public static readonly DependencyProperty IgnoreScrollingProperty =
           DependencyProperty.RegisterAttached(
               "IgnoreScrolling",
               typeof(bool),
               typeof(FrameworkElementBehaviour),
               new PropertyMetadata(false, IgnoreScrollingPropertyChanged)
           );

        public static void IgnoreScrollingPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is FrameworkElement element))
                return;

            if ((bool)e.NewValue)
            {
                element.PreviewMouseWheel += new MouseWheelEventHandler(Element_PreviewMouseWheel);
            }
            else
            {
                element.PreviewMouseWheel -= new MouseWheelEventHandler(Element_PreviewMouseWheel);
            }
        }

        static void Element_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!(sender is FrameworkElement element))
                return;

            if (e.Handled)
                return;

            e.Handled = true;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
            {
                RoutedEvent = UIElement.MouseWheelEvent,
                Source = sender
            };

            (element.Parent as UIElement)?.RaiseEvent(eventArg);
        }

        public static bool GetIgnoreScrolling(FrameworkElement element)
        {
            return (bool)element.GetValue(IgnoreScrollingProperty);
        }

        public static void SetIgnoreScrolling(FrameworkElement element, bool val)
        {
            element.SetValue(IgnoreScrollingProperty, val);
        }




        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.RegisterAttached(
                "IsPressed",
                typeof(bool),
                typeof(FrameworkElementBehaviour),
                new PropertyMetadata(false)
            );


        public static readonly DependencyProperty AttachIsPressedProperty =
            DependencyProperty.RegisterAttached(
                "AttachIsPressed",
                typeof(bool),
                typeof(FrameworkElementBehaviour),
                new PropertyMetadata(false, AttachIsPressedPropertyChanged)
            );

        public static void AttachIsPressedPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is FrameworkElement element))
                return;

            if ((bool)e.NewValue)
            {
                element.MouseDown += new MouseButtonEventHandler(Element_MouseDown);
                element.MouseUp += new MouseButtonEventHandler(Element_MouseUp);
                element.MouseLeave += new MouseEventHandler(Element_MouseLeave);
            }
            else
            {
                element.MouseDown -= new MouseButtonEventHandler(Element_MouseDown);
                element.MouseUp -= new MouseButtonEventHandler(Element_MouseUp);
                element.MouseLeave -= new MouseEventHandler(Element_MouseLeave);
            }
        }

        static void Element_MouseLeave(object sender, MouseEventArgs e)
        {
            var element = sender as FrameworkElement;
            element?.SetValue(IsPressedProperty, false);
        }

        static void Element_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            element?.SetValue(IsPressedProperty, false);
        }

        static void Element_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            element?.SetValue(IsPressedProperty, true);
        }


        public static bool GetIsPressed(FrameworkElement element)
        {
            return (bool)element.GetValue(IsPressedProperty);
        }

        public static void SetIsPressed(FrameworkElement element, bool val)
        {
            element.SetValue(IsPressedProperty, val);
        }

        public static bool GetAttachIsPressed(FrameworkElement element)
        {
            return (bool)element.GetValue(AttachIsPressedProperty);
        }

        public static void SetAttachIsPressed(FrameworkElement element, bool val)
        {
            element.SetValue(AttachIsPressedProperty, val);
        }
    }
}