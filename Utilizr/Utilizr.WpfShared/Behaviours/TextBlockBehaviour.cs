﻿using GetText;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilizr.Logging;
using Utilzr.WPF.Extension;
using Utilzr.WPF.Util;

namespace Utilzr.WPF.Behaviours
{
    public static class TextBlockBehaviour
    {
        #region AutomaticTooltip Attached Property
        public static readonly DependencyProperty AutomaticTooltipProperty =
            DependencyProperty.RegisterAttached(
                "AutomaticTooltip",
                typeof(bool),
                typeof(TextBlockBehaviour),
                new UIPropertyMetadata(
                    false,
                    (d, e) =>
                    {
                        if (!(d is TextBlock textBlock))
                            return;

                        bool oldValue = (bool)e.OldValue;
                        bool newValue = (bool)e.NewValue;

                        // only unregister / register when switching state
                        // pointless to unregister/register if old and new value are both true

                        if (oldValue && (!newValue))
                        {
                            Unregister(textBlock);
                        }

                        if ((!oldValue) && newValue)
                        {
                            Register(textBlock);
                        }
                    }
                )
            );

        public static bool GetAutomaticTooltip(TextBlock obj)
        {
            return (bool)obj.GetValue(AutomaticTooltipProperty);
        }

        public static void SetAutomaticTooltip(TextBlock obj, bool val)
        {
            obj.SetValue(AutomaticTooltipProperty, val);
        }
        #endregion

        static void Register(TextBlock textBlock)
        {
            textBlock.SizeChanged += TextBlock_SizeChanged;
            textBlock.Loaded += TextBlock_Loaded;
        }

        static void Unregister(TextBlock textBlock)
        {
            textBlock.SizeChanged -= TextBlock_SizeChanged;
            textBlock.Loaded -= TextBlock_Loaded;
        }

        private static void TextBlock_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!(sender is TextBlock textBlock))
                return;

            if (!textBlock.IsLoaded)
                return;

            Calculate(textBlock);
        }

        private static void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextBlock textBlock))
                return;

            Calculate(textBlock);
        }

        static void Calculate(TextBlock textBlock)
        {
            try
            {
                if (double.IsNaN(textBlock.ActualWidth) || double.IsNaN(textBlock.ActualHeight))
                    return; // not shown yet

                bool autoShow = false;

                var heightSum = 0D;
                foreach (var line in textBlock.GetLines())
                {
                    var fmtTextLine = new FormattedText(
                        line,
                        CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface(
                           textBlock.FontFamily,
                           textBlock.FontStyle,
                           textBlock.FontWeight,
                           textBlock.FontStretch
                        ),
                        textBlock.FontSize,
                        textBlock.Foreground,
                        new NumberSubstitution(),
                        textBlock.GetDpi()
                    );

                    heightSum += fmtTextLine.Height;
                    if (Math.Floor(fmtTextLine.Width) > textBlock.ActualWidth)
                    {
                        // single word longer than width in multiline textblock, will but cut off
                        //System.Diagnostics.Debug.WriteLine($"***DEBUG**** detected long line ({fmtTextLine.Width} vs {textBlock.ActualWidth}) for '{line}' in '{textBlock.Text}'");
                        autoShow = true;
                        break;
                    }
                }

                // confirm combined height isn't clipping end of text
                if (Math.Floor(heightSum) > textBlock.ActualHeight)
                {
                    //System.Diagnostics.Debug.WriteLine($"***DEBUG**** total height {heightSum} bigger than {textBlock.ActualHeight} '{textBlock.Text}'");
                    autoShow = true;
                }

                textBlock.ToolTip = autoShow
                    ? textBlock.Text
                    : null;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, "Unable to calculate auto tooltip, clearning any tooltip.");
                textBlock.ToolTip = null;
            }
        }
    }
}
