﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Utilizr.Logging;
using Utilzr.WPF.Extension;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public class ArcWithThumb : Shape
    {
        private bool _isLoaded;

        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(BackgroundBrush),
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(ArcWithThumb), nameof(BackgroundBrush)),
                    UpdateArc
                )
            );

        private Typeface ThumbTypeFace => _thumbTypeFaceUnsafe ?? (_thumbTypeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name));
        private Typeface _thumbTypeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name);

        /// <summary>
        /// Background bar colour.
        /// </summary>
        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set
            {
                _backgroundPen.Brush = value;
                SetValue(BackgroundBrushProperty, value);
            }
        }

        private Pen _backgroundPen;

        public Pen BackgroundPen
        {
            get
            {
                if (_backgroundPen == null)
                {
                    _backgroundPen = new Pen(BackgroundBrush, BarThickness);
                }
                _backgroundPen.Brush = BackgroundBrush;
                return _backgroundPen; 
            }
        }



        public static readonly DependencyProperty ProgressBrushProperty =
            DependencyProperty.Register(
                nameof(ProgressBrush), 
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(ArcWithThumb), nameof(ProgressBrush)),
                    UpdateArc
                )
            );

        /// <summary>
        /// Progress bar colour.
        /// </summary>
        public Brush ProgressBrush
        {
            get { return (Brush)GetValue(ProgressBrushProperty); }
            set
            {
                _progressPen.Brush = value;
                SetValue(ProgressBrushProperty, value);
            }
        }

        private Pen _progressPen;
        public Pen ProgressPen
        {
            get
            {
                if (_progressPen == null)
                {
                    _progressPen = new Pen(ProgressBrush, BarThickness);
                }
                _progressPen.Brush = ProgressBrush;
                return _progressPen; 
            }
        }

        private Pen _shinePen;

        public Pen ShinePen
        {
            get
            {
                var brush = GetShineBrush();
                if (_shinePen == null)
                {
                    _shinePen = new Pen(brush, BarThickness);
                    return _shinePen;
                }
                _shinePen.Brush = brush;
                return _shinePen;
            }
        }

        private Pen _innerFillPen;

        public Pen InnerFillPen
        {
            get
            {
                if (_innerFillPen == null)
                {
                    _innerFillPen = new Pen(InnerFill, BarThickness);
                }
                return _innerFillPen;
            }
        }

        private Pen[] _animationPens;
        private double _animationPensThickness;

        void PopulateAnimationPens()
        {
            if (_animationPens != null)
            {
                return;
            }
            _animationPens = new Pen[720];
            for (int i = 0; i < 360; i++)
            {
                _animationPens[i] = new Pen(GetAnimationBrush(i, SweepDirection.Clockwise), BarThickness);
            }
            for (int i = 360; i < 720; i++)
            {
                _animationPens[i] = new Pen(GetAnimationBrush(i % 360, SweepDirection.Counterclockwise), BarThickness);
            }
        }

        public Pen AnimationPen
        {
            get
            {
                PopulateAnimationPens();
                int animationAngleThumbAdjusted = (int) (AnimationAngle + (AngleExcludedPercentage / 2.0));

                if (Direction == SweepDirection.Counterclockwise)
                {
                    animationAngleThumbAdjusted += 360;
                }
                return _animationPens[animationAngleThumbAdjusted];
            }
        }

        const int THUMB_STROKE_THICKNESS = 2;
        private Pen _thumbStrokePen;
        public Pen ThumbStrokePen
        {
            get
            {
                if (_thumbStrokePen == null)
                {
                    _thumbStrokePen = new Pen(ThumbStrokeBrush, THUMB_STROKE_THICKNESS);
                    return _thumbStrokePen;
                }
                return _thumbStrokePen; 
            }
        }

        
        public static readonly DependencyProperty ThumbBackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(ThumbBackgroundBrush),
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(ArcWithThumb), nameof(ThumbBackgroundBrush)),
                    UpdateArc
                )
            );

        /// <summary>
        /// Thumb background colour
        /// </summary>
        public Brush ThumbBackgroundBrush
        {
            get { return (Brush)GetValue(ThumbBackgroundBrushProperty); }
            set { SetValue(ThumbBackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty ThumbStrokeBrushProperty =
            DependencyProperty.Register(
                nameof(ThumbStrokeBrush),
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(ArcWithThumb), nameof(ThumbStrokeBrush)),
                    UpdateArc
                )
            );

        /// <summary>
        /// The brush for the border of the thumb
        /// </summary>
        public Brush ThumbStrokeBrush
        {
            get { return (Brush)GetValue(ThumbStrokeBrushProperty); }
            set { SetValue(ThumbStrokeBrushProperty, value); }
        }


        public static readonly DependencyProperty StartAngleProperty =
            DependencyProperty.Register(
                nameof(StartAngle),
                typeof(double),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(0.0, UpdateArc)
            );

        /// <summary>
        /// Start angle, in degrees, from which the progress bar will be drawn.
        /// </summary>
        public double StartAngle
        {
            get { return (double)GetValue(StartAngleProperty); }
            set
            {
                if (value == (double)GetValue(StartAngleProperty))
                {
                    return;
                }
                SetValue(StartAngleProperty, value);
                
            }
        }


        public static readonly DependencyProperty EndAngleProperty =
            DependencyProperty.Register(
                nameof(EndAngle),
                typeof(double), 
                typeof(ArcWithThumb),
                new UIPropertyMetadata(90.0, UpdateArc)
            );

        /// <summary>
        /// End angle, in degrees, from which the progress bar will stop being drawn.
        /// </summary>
        public double EndAngle
        {
            get { return (double)GetValue(EndAngleProperty); }
            set
            {
                if (value == (double)GetValue(EndAngleProperty))
                {
                    return;
                }
                SetValue(EndAngleProperty, value); 
                
            }
        }


        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register(
                nameof(Direction), 
                typeof(SweepDirection),
                typeof(ArcWithThumb), 
                new UIPropertyMetadata(SweepDirection.Clockwise, UpdateArc)
            );

        /// <summary>
        /// Whether the bar is drawn Clockwise of Counterclockwise.
        /// </summary>
        public SweepDirection Direction
        {
            get { return (SweepDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }


        public static readonly DependencyProperty OriginRotationDegreesProperty =
            DependencyProperty.Register(
                nameof(OriginRotationDegrees),
                typeof(double), 
                typeof(ArcWithThumb),
                new UIPropertyMetadata(270.0, UpdateArc)
            );

        /// <summary>
        /// Start rotating the bar from the given angle, in degrees. If you wanted it to start
        /// from 12.00 that would be 270 Clockwise or 90 Counterclockwise.
        /// </summary>
        public double OriginRotationDegrees
        {
            get { return (double)GetValue(OriginRotationDegreesProperty); }
            set
            {
                if (value == (double)GetValue(OriginRotationDegreesProperty))
                {
                    return;
                }
                SetValue(OriginRotationDegreesProperty, value);
            }
        }


        public static readonly DependencyProperty OriginShineDegreesProperty =
            DependencyProperty.Register(
                nameof(OriginShineDegrees),
                typeof(double),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(270.0, UpdateArc)
            );

        /// <summary>
        /// The angle, in degrees, at which the shine will be added, if <see cref="HasShine"/> is true.
        /// </summary>
        public double OriginShineDegrees
        {
            get { return (double)GetValue(OriginShineDegreesProperty); }
            set
            {
                if (value == (double)GetValue(OriginShineDegreesProperty))
                {
                    return;
                }
                SetValue(OriginShineDegreesProperty, value);
                
            }
        }


        public static readonly DependencyProperty ThumbTextProperty =
            DependencyProperty.Register(
                nameof(ThumbText),
                typeof(string), 
                typeof(ArcWithThumb), 
                new UIPropertyMetadata(string.Empty, UpdateArc)
            );

        /// <summary>
        /// Any text which will de drawn inside the thumb, if <see cref="HasThumb"/> is true.
        /// </summary>
        public string ThumbText
        {
            get { return (string)GetValue(ThumbTextProperty); }
            set
            {
                if (value == (string)GetValue(ThumbTextProperty))
                {
                    return;
                }
                SetValue(ThumbTextProperty, value);   
            }
        }


        public static readonly DependencyProperty HasThumbProperty =
            DependencyProperty.Register(
                nameof(HasThumb), 
                typeof(bool), 
                typeof(ArcWithThumb),
                new UIPropertyMetadata(true, UpdateArc)
            );

        /// <summary>
        /// Sets whether a thumb is drawn on the bar.
        /// </summary>
        public bool HasThumb
        {
            get { return (bool)GetValue(HasThumbProperty); }
            set
            {
                if (value == (bool)GetValue(HasThumbProperty))
                {
                    return;
                }
                SetValue(HasThumbProperty, value);
                
            }
        }


        public static readonly DependencyProperty ThumbTextSizeProperty =
            DependencyProperty.Register(
                nameof(ThumbTextSize), 
                typeof(double), 
                typeof(ArcWithThumb), 
                new UIPropertyMetadata(12.0, UpdateArc)
            );

        /// <summary>
        /// Size of the thumb text.
        /// </summary>
        public double ThumbTextSize
        {
            get { return (double)GetValue(ThumbTextSizeProperty); }
            set
            {
                if (value == (double)GetValue(ThumbTextSizeProperty))
                {
                    return;
                }
                SetValue(ThumbTextSizeProperty, value);
                
            }
        }


        public static readonly DependencyProperty ThumbTextBrushProperty =
            DependencyProperty.Register(
                nameof(ThumbTextBrush),
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(ArcWithThumb), nameof(ThumbTextBrush)),
                    UpdateArc
                )
            );

        /// <summary>
        /// Brush for the text drawn inside the thumb.
        /// </summary>
        public Brush ThumbTextBrush
        {
            get { return (Brush)GetValue(ThumbTextBrushProperty); }
            set { SetValue(ThumbTextBrushProperty, value); }
        }


        public static readonly DependencyProperty ArcHeightProperty =
            DependencyProperty.Register(
                nameof(ArcHeight), 
                typeof(double),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(100.0, UpdateArc)
            );

        /// <summary>
        /// Height of the arc (diameter).
        /// </summary>
        public double ArcHeight
        {
            get { return (double)GetValue(ArcHeightProperty); }
            set
            {
                if (value == (double)GetValue(ArcHeightProperty))
                {
                    return;
                }
                SetValue(ArcHeightProperty, value);
            }
        }


        public static readonly DependencyProperty ArcWidthProperty =
            DependencyProperty.Register(
                nameof(ArcWidth), 
                typeof(double), 
                typeof(ArcWithThumb), 
                new UIPropertyMetadata(100.0, UpdateArc)
            );

        /// <summary>
        /// Width of the arc (diameter).
        /// </summary>
        public double ArcWidth
        {
            get { return (double)GetValue(ArcWidthProperty); }
            set
            {
                if (value == (double)GetValue(ArcWidthProperty))
                {
                    return;
                }
                SetValue(ArcWidthProperty, value);
            }
        }


        public static readonly DependencyProperty AngleExcludedPercentageProperty =
            DependencyProperty.Register(
                nameof(AngleExcludedPercentage),
                typeof(double), 
                typeof(ArcWithThumb),
                new UIPropertyMetadata(0.0)
            );

        /// <summary>
        /// The thumb takes up space on the progress bar. This needs to be taken into consideration when showing
        /// the current progress percentage. Less than ~ 7% progress bar is drawn behind thumb. Likewise when the 
        /// percentage is more than ~ 93%. If thumb is shown, exclude the size of the progress bar's arc covered 
        /// by the thumb. This arc length can then be used to calculate the angle, in degrees, which shouldn't be
        /// used when calculating the start and end angles for the current progress percentage. This property's
        /// value will be calculated automatically.
        /// </summary>
        public double AngleExcludedPercentage
        {
            get { return (double)GetValue(AngleExcludedPercentageProperty); }
            private set
            {
                if (value == (double)GetValue(AngleExcludedPercentageProperty))
                {
                    return;
                }
                SetValue(AngleExcludedPercentageProperty, value);
            }
        }


        public static readonly DependencyProperty HasShineProperty =
            DependencyProperty.Register(
                nameof(HasShine),
                typeof(bool),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(true, UpdateArc)
            );

        /// <summary>
        /// Sets whether a shine is shown on the bar.
        /// </summary>
        public bool HasShine
        {
            get { return (bool)GetValue(HasShineProperty); }
            set
            {
                if (value == (bool)GetValue(HasShineProperty))
                {
                    return;
                }
                SetValue(HasShineProperty, value);
            }
        }


        public static readonly DependencyProperty ShineColourProperty =
            DependencyProperty.Register(
                nameof(ShineColour),
                typeof(Color),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(Colors.White, UpdateArc));

        /// <summary>
        /// Only RGB values will be used, if A also set.
        /// </summary>
        public Color ShineColour
        {
            get { return (Color)GetValue(ShineColourProperty); }
            set { SetValue(ShineColourProperty, value); }
        }


        public static readonly DependencyProperty ShineStrengthProperty =
            DependencyProperty.Register(
                nameof(ShineStrength),
                typeof(byte),
                typeof(ArcWithThumb),
                new UIPropertyMetadata((byte)120, UpdateArc)
            );

        /// <summary>
        /// Alpha byte used in the gradient to determine the strengths of the shine.
        /// </summary>
        public byte ShineStrength
        {
            get { return (byte)GetValue(ShineStrengthProperty); }
            set
            {
                if (value == (byte)GetValue(ShineStrengthProperty))
                {
                    return;
                }
                SetValue(ShineStrengthProperty, value);
            }
        }


        public static readonly DependencyProperty IsDashedProgressBarProperty =
            DependencyProperty.Register(
                nameof(IsDashedProgressBar),
                typeof(bool),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(false, UpdateArc)
            );

        /// <summary>
        /// Draw dashed rather than solid progress bar
        /// </summary>
        public bool IsDashedProgressBar
        {
            get { return (bool)GetValue(IsDashedProgressBarProperty); }
            set
            {
                if (value == (bool)GetValue(IsDashedProgressBarProperty))
                {
                    return;
                }
                SetValue(IsDashedProgressBarProperty, value);
            }
        }


        public static readonly DependencyProperty DashedSegmentCountProperty =
            DependencyProperty.Register(
                nameof(DashedSegmentCount),
                typeof(int),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(24, UpdateArc)
            );

        /// <summary>
        /// The number of segments the dashed bar should contain
        /// </summary>
        public int DashedSegmentCount
        {
            get { return (int)GetValue(DashedSegmentCountProperty); }
            set
            {
                if (value == (int)GetValue(DashedSegmentCountProperty))
                {
                    return;
                }
                SetValue(DashedSegmentCountProperty, value);
            }
        }


        public static readonly DependencyProperty InnerFillProperty =
            DependencyProperty.Register(
                nameof(InnerFill),
                typeof(Brush),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(null, UpdateArc)
            );

        /// <summary>
        /// A brush to use if the hollow centre of the arc is to be filled. 
        /// If null, nothing will be drawn
        /// </summary>
        public Brush InnerFill
        {
            get { return (Brush)GetValue(InnerFillProperty); }
            set
            {
                _innerFillPen.Brush = value;
                SetValue(InnerFillProperty, value);
            }
        }

        public static readonly DependencyProperty BarThicknessProperty =
            DependencyProperty.Register(
                nameof(BarThickness),
                typeof(double),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(14.0, UpdateArc)
            );

        public double BarThickness
        {
            get { return (double)GetValue(BarThicknessProperty); }
            set
            {
                _progressPen.Thickness = value;
                _backgroundPen.Thickness = value;
                _shinePen.Thickness = value;
                _innerFillPen.Thickness = value;
                PopulateAnimationPens();
                foreach (var animationPen in _animationPens)
                {
                    animationPen.Thickness = BarThickness;
                }
                if (value == (double)GetValue(BarThicknessProperty))
                {
                    return;
                }
                SetValue(BarThicknessProperty, value);
            }
        }


        public static readonly DependencyProperty AnimationAngleProperty =
            DependencyProperty.Register(
                nameof(AnimationAngle),
                typeof(double),
                typeof(ArcWithThumb),
                new PropertyMetadata(0.0, UpdateArc)
            );

        /// <summary>
        /// The angle, in degrees, at which the current AnimationPath should be drawn.
        /// This is the property a StoryBoard should target.
        /// </summary>
        public double AnimationAngle
        {
            get { return (double)GetValue(AnimationAngleProperty); }
            set
            {
                if (value == (double)GetValue(AnimationAngleProperty))
                {
                    return;
                }
                SetValue(AnimationAngleProperty, value);
            }
        }


        public static readonly DependencyProperty IsAnimatingProperty =
            DependencyProperty.Register(
                nameof(IsAnimating),
                typeof(bool),
                typeof(ArcWithThumb),
                new PropertyMetadata(false, UpdateArc)
            );

        public bool IsAnimating
        {
            get { return (bool)GetValue(IsAnimatingProperty); }
            set { SetValue(IsAnimatingProperty, value); }
        }


        public static readonly DependencyProperty DashedSeparationGapAngleProperty =
            DependencyProperty.Register(
                nameof(DashedSeparationGapAngle),
                typeof(int?),
                typeof(ArcWithThumb),
                new UIPropertyMetadata(null, UpdateArc));

        /// <summary>
        /// Angle, in degrees, at which to space the bar's <see cref="ProgressBrush"/>. If null,
        /// the separation between dashed will be the same length of an individual dash.
        /// </summary>
        public int? DashedSeparationGapAngle
        {
            get { return (int?)GetValue(DashedSeparationGapAngleProperty); }
            set { SetValue(DashedSeparationGapAngleProperty, value); }
        }


        protected static void UpdateArc(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ArcWithThumb arc = d as ArcWithThumb;
            arc?.InvalidateVisual();
        }

        public const double END_ANGLE = 359.999;        

        private Geometry _fullArc;
        protected override Geometry DefiningGeometry
        {
            get
            {
                if (_fullArc == null || !_isLoaded)
                {
                    _fullArc = GetArcGeometry(0, END_ANGLE);
                }

                return _fullArc;
            }
        }

        private double _previousProgressBarStartAngleThumbAdjusted;
        private double _previousProgressBarEndAngleThumbAdjusted;
        private Geometry _currentProgressBarGeomerty;

        public ArcWithThumb()
        {
            StrokeThickness = 0;
            Loaded += (sender, args) => _isLoaded = true;
#if NETCOREAPP
            this.RegisterDpiChanged(() => InvalidateVisual());
#endif
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            // Arc is a length on a circle circumference.
            // Arc length = 2 * Pi * radius * (centreAngleDegrees / 360)
            // Thus centreAngleDegrees = (180 * arc length) / Pi * radius 
            // Arc length of progress bar covered by thumb circle can be obtained using law of cosines.
            // See forum post for detailed steps: http://mymathforum.com/algebra/15531-two-intersecting-circles.html
            
            double thumbRadius = BarThickness*1.25;
            if(HasThumb && ArcHeight != 0)
            {
                //2R x arccos( (2R^2 - r^2) / 2R^2 ) where R is bigger circle radius, r is smaller circle radius
                double twoRSquared = 2 * Math.Pow(ArcHeight / 2.0, 2); //2R^2
                double arcLengthCoveredByThumbCircle = ArcHeight * Math.Acos((twoRSquared - Math.Pow(thumbRadius, 2)) / twoRSquared);

                AngleExcludedPercentage = (180 * arcLengthCoveredByThumbCircle) / ((ArcHeight / 2.0) * Math.PI);
            }
            else
            {
                AngleExcludedPercentage = 0;
            }

            // Offset start / finish angle so it starts at the thumb. Angle 0 if no thumb shown
            double startAngleThumbAdjusted = StartAngle + (AngleExcludedPercentage / 2.0);
            double endAngleThumbAdjusted = EndAngle + (AngleExcludedPercentage / 2.0);


            // Background
            DrawSolidBar(drawingContext, BackgroundPen, DefiningGeometry);

            // Progress
            if (IsDashedProgressBar)
            {
                DrawDashedBar(drawingContext, startAngleThumbAdjusted, endAngleThumbAdjusted, true, ProgressPen);
            }
            else
            {
                if(startAngleThumbAdjusted != _previousProgressBarStartAngleThumbAdjusted ||
                   endAngleThumbAdjusted != _previousProgressBarEndAngleThumbAdjusted ||
                   _currentProgressBarGeomerty == null ||
                   !_isLoaded)
                {
                    _currentProgressBarGeomerty = GetArcGeometry(startAngleThumbAdjusted, endAngleThumbAdjusted);
                }

                DrawSolidBar(drawingContext, ProgressPen, _currentProgressBarGeomerty);
            }

            // Shine
            DrawShineInBar(drawingContext);

            // Fill centre with specific colour
            DrawInnerFill(drawingContext, startAngleThumbAdjusted, endAngleThumbAdjusted);

            // Animation
            DrawAnimationGeometry(drawingContext, startAngleThumbAdjusted, endAngleThumbAdjusted);

            // Thumb
            DrawThumb(drawingContext, thumbRadius);

            _previousProgressBarStartAngleThumbAdjusted = startAngleThumbAdjusted;
            _previousProgressBarEndAngleThumbAdjusted = endAngleThumbAdjusted;
        }

        private void DrawDashedBar(DrawingContext drawingContext, double startAngle, double endAngle, bool isEven, Pen barPen)
        {
            double dashProgressAngle = 0;
            double dashBackgroundAngle = 0;
            double dashProgressAndBackgroundAngle = 360.0 / DashedSegmentCount;

            if (DashedSeparationGapAngle.HasValue && (dashProgressAndBackgroundAngle - DashedSeparationGapAngle.Value) > 0)
            {
                dashProgressAngle = dashProgressAndBackgroundAngle - DashedSeparationGapAngle.Value;
                dashBackgroundAngle = DashedSeparationGapAngle.Value;
            }
            else
            {
                // Default to equal size if no gap given, or if gap too big to accommodate
                dashProgressAngle = dashProgressAndBackgroundAngle / 2.0;
                dashBackgroundAngle = dashProgressAngle;
            }

            for (int i = 0; i < DashedSegmentCount; i++)
            {
                // p = progress angle, b = background angle

                // Even: i*p + i*b
                // Odd: (i+1)*p + i*b
                double segmentStartAngle = startAngle
                    + ((i + (isEven ? 0 : 1)) * dashProgressAngle)
                    + (i * dashBackgroundAngle);

                // Even: (i+1)*p + i*b
                // Odd: (i+1)*p + (i+1)*b
                double segmentEndAngle = startAngle
                    + ((i + 1) * dashProgressAngle)
                    + ((i + (isEven ? 0 : 1)) * dashBackgroundAngle);

                if (segmentStartAngle >= endAngle)
                    break;

                if (segmentEndAngle > endAngle)
                    segmentEndAngle = endAngle;

                drawingContext.DrawGeometry(
                    null,
                    barPen,
                    GetArcGeometry(segmentStartAngle, segmentEndAngle)
                );
            }
        }

        private void DrawSolidBar(DrawingContext drawingContext, Pen pen, Geometry arcGeometry)
        {
            drawingContext.DrawGeometry(
                null,
                pen,
                arcGeometry
            );
        }

        private void DrawShineInBar(DrawingContext drawingContext)
        {
            if (!HasShine)
                return;

            drawingContext.DrawGeometry(
                null,
                ShinePen,
                DefiningGeometry // 0, 359.999
            );
        }

        private Pen _nullPen = new Pen(null, 0);
        private void DrawInnerFill(DrawingContext drawingContext, double startAngle, double endAngle)
        {
            // inner fill
            if (InnerFill == null)
                return;
            
            drawingContext.DrawEllipse(
                InnerFill,
                _nullPen,
                GetCenterPoint(),
                ArcWidth / 2.0 - BarThickness,
                ArcHeight / 2.0 - BarThickness
            );

            if (!HasShine)
                return;

            // Need to draw dashed bar again, to cover shine on background bar. Segments offset
            // by 1, so only the shine on the background bar, not progress bar, will be hidden.
            DrawDashedBar(drawingContext, startAngle, endAngle, false, BackgroundPen);

            // When the end angle is less than 360, a shine can still be visible. Draw slice of 
            // circle to ensure this bit of the shine is also removed.
            drawingContext.DrawGeometry(
                InnerFill,
                InnerFillPen,
                GetArcGeometry(endAngle, END_ANGLE)
            );
        }

        private Geometry _animationGeometry;
        private SweepDirection _previousAnimationDirection;
        private void DrawAnimationGeometry(DrawingContext drawingContext, double progressStartAngle, double progressEndAngle)
        {
            if (!IsAnimating)
                return;

           

            if(_animationGeometry == null || _previousAnimationDirection != Direction || !_isLoaded)
            {
                _animationGeometry = GetArcGeometry(0, END_ANGLE, Direction);
            }

            // Outer x2 speed of inner
            drawingContext.DrawGeometry(
                null,
                AnimationPen,
                _animationGeometry
            );

            _previousAnimationDirection = Direction;
        }

        private void DrawThumb(DrawingContext drawingContext, double thumbRadius)
        {
            if (!HasThumb)
                return;

            // thumb circle
            
            Point thumbStartPoint = PointAtAngle(Math.Min(StartAngle, EndAngle), Direction);
            drawingContext.DrawEllipse(ThumbBackgroundBrush, ThumbStrokePen, thumbStartPoint, thumbRadius, thumbRadius);

            // thumb shine
            if (HasShine)
                drawingContext.DrawEllipse(GetShineBrush(), null, thumbStartPoint, thumbRadius, thumbRadius);

            try
            {
                // thumb text
#if NETCOREAPP
                var formattedText = new FormattedText(
                    ThumbText ?? string.Empty,
                    CultureInfo.CurrentCulture,
                    CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                        ? FlowDirection.RightToLeft
                        : FlowDirection.LeftToRight,
                    ThumbTypeFace,
                    ThumbTextSize,
                    ThumbTextBrush,
                    this.GetDpi()
                );
#else
                var formattedText = new FormattedText(
                    ThumbText ?? string.Empty,
                    CultureInfo.CurrentCulture,
                    CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                        ? FlowDirection.RightToLeft
                        : FlowDirection.LeftToRight,
                    ThumbTypeFace,
                    ThumbTextSize,
                    ThumbTextBrush
                );
#endif

                //TODO: Possibly improve performance, find a way to get from given size, rather than keep trying one size smaller...
                double fontSize = ThumbTextSize;
                bool shouldBreak = false;
                var wAvailable = (2 * thumbRadius) - THUMB_STROKE_THICKNESS;
                while (formattedText.Width > wAvailable)
                {
                    fontSize--;

                    if (fontSize < 1)
                    {
                        fontSize = ThumbTextSize;
                        shouldBreak = true;
                    }

                    formattedText.SetFontSize(fontSize);

                    if (shouldBreak)
                        break;
                }

                thumbStartPoint.Offset(-formattedText.Width / 2.0, -formattedText.Height / 2.0);
                drawingContext.DrawText(formattedText, thumbStartPoint);
            }
            catch (Exception ex)
            {
                // TODO: Investigate further:
                // Something wrong here if cannot access default font.

                // UriFormatException: Invalid URI: The format of the URI could not be determined.
                // System.Uri.CreateThis(String uri, Boolean dontEscape, UriKind uriKind):0
                // System.Uri..ctor(String uriString, UriKind uriKind):0
                // MS.Internal.FontCache.Util..cctor():0

                // Potentially caused by long path env var: http://stackoverflow.com/a/33423879/1229237
                // Either way, if fonts messed up on their machine, lots of stuff probably wrong
                // But at least it can now create the object and not crash the app, just no thumb text
                // Only log on debug since will flood the logs

                Log.Exception(LoggingLevel.DEBUG, nameof(ArcWithThumb), ex);
            }
        }

        
        private double _previousShineRotationDegrees;
        private CachedBrush _shineBrush;
        private RadialGradientBrush GetShineBrush()
        {
            if (_shineBrush.Brush == null)
            {
                _shineBrush.Brush = new RadialGradientBrush
                {
                    GradientStops = new GradientStopCollection(2)
                {
                    new GradientStop(Color.FromArgb(ShineStrength, ShineColour.R, ShineColour.G, ShineColour.B), 0),
                    new GradientStop(Color.FromArgb(0, ShineColour.R, ShineColour.G, ShineColour.B), 1),
                },
                    Center = new Point(0.5, 0.5),
                };
            }
            
            if(_previousShineRotationDegrees != OriginShineDegrees)
            {
                // To position shine, use relative transform of values up to plus/minus 0.5 from origin.
                // This clips the brush to half of the arc. Shine is always over starting point. If X=0
                // and Y = -0.5 shine would be on north half of circle. Relative transform point can be
                // calculated using a circle of radius 0.5. Any point on the circumference of this circle
                // maps directly to shine's relative transform. The point can be given by:
                // x = cx + r * cos(a) where cx = origin, r = radius, a = angle in radians
                // y = cy + r * sin(a) where cy = origin, r = radius, a = angle in radians
                var originShineVal = Math.Round(OriginShineDegrees*Math.PI/180.0, 2);

                if (_shineBrush.KeyVal == originShineVal)
                {
                    return _shineBrush.Brush;
                }

                _shineBrush.Brush.RelativeTransform = new TranslateTransform()
                {
                    X = 0.5 * Math.Cos(originShineVal),
                    Y = 0.5 * Math.Sin(originShineVal),
                };

                _shineBrush.KeyVal = originShineVal;
            }

            _previousShineRotationDegrees = OriginShineDegrees;

            return _shineBrush.Brush;
        }

        private Geometry GetArcGeometry()
        {
            return GetArcGeometry(StartAngle, EndAngle);
        }

        private Geometry GetArcGeometry(double startAngle, double endAngle)
        {
            return GetArcGeometry(startAngle, endAngle, Direction);
        }

        private Geometry GetArcGeometry(double startAngle, double endAngle, SweepDirection direction)
        {
            Point startPoint = PointAtAngle(Math.Min(startAngle, endAngle), direction);
            Point endPoint = PointAtAngle(Math.Max(startAngle, endAngle), direction);

            Size arcSize = new Size(
                Math.Max(0, (ArcWidth - BarThickness) / 2),
                Math.Max(0, (ArcHeight - BarThickness) / 2)
            );

            bool isLargeArc = Math.Abs(endAngle - startAngle) > 180;
            var arcGeometry = new StreamGeometry();
            using (StreamGeometryContext context = arcGeometry.Open())
            {
                context.BeginFigure(startPoint, false, false);
                context.ArcTo(endPoint, arcSize, 0, isLargeArc, direction, true, false);
            }

            if(arcGeometry.CanFreeze)
                arcGeometry.Freeze();

            return arcGeometry;
        }

        private Point PointAtAngle(double angle, SweepDirection sweep)
        {
            double translatedAngle = angle + OriginRotationDegrees;
            double radAngle = translatedAngle * (Math.PI / 180);
            var centre = GetCenterPoint();

            double x = ((ArcWidth - BarThickness) / 2.0) * Math.Cos(radAngle);
            double y = ((ArcHeight - BarThickness) / 2.0) * Math.Sin(radAngle);

            return new Point
            {
                X = x + centre.X,
                Y = Direction == SweepDirection.Clockwise
                    ? centre.Y + y
                    : centre.Y - y
            };
        }

        private Point GetCenterPoint()
        {
            double xCentre = ArcWidth / 2;
            double yCentre = ArcHeight / 2;
            return new Point(xCentre, yCentre);
        }

//        private RadialGradientBrush _animationBrush;

        private static Brush[] _animationBrushes;

        private static void PopulateAnimationBrushCache()
        {
            if (_animationBrushes != null)
                return;

            _animationBrushes = new Brush[360];
            for (int i = 0; i < 360; i++)
            {
                _animationBrushes[i] = new RadialGradientBrush
                {
                    GradientOrigin = new Point(0.5, 0.5),
                    Center = new Point(0.5, 0.5),
                    RadiusX = 1,
                    RadiusY = 1,
                    GradientStops = new GradientStopCollection(2)
                    {
                        new GradientStop(Color.FromArgb(0, 255, 255, 255), 1),
                        new GradientStop(Color.FromArgb(0, 255, 255, 255), 0.4),
                        new GradientStop(Color.FromArgb(255, 255, 255, 255), 0),
                    }
                };
                double angle = i;
                var rads = angle * Math.PI / 180.0;
                _animationBrushes[i].RelativeTransform = new TranslateTransform()
                {
                    X = 0.5 * Math.Cos(rads),
                    Y = 0.5 * Math.Sin(rads),
                };
            }
        }

        private Brush GetAnimationBrush(double angle, SweepDirection direction)
        {
            PopulateAnimationBrushCache();

            angle = direction == SweepDirection.Clockwise
                ? angle
                : END_ANGLE - angle;


            int intAngle = Math.Max(0, ((int) angle) % 360);

            return _animationBrushes[intAngle];
        }

    }

    struct CachedBrush
    {
        public RadialGradientBrush Brush;
        public double KeyVal;
    }
}