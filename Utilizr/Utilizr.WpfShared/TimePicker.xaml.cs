﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Utilizr.Logging;

namespace Utilzr.WPF
{
    public partial class TimePicker: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// New and old values 
        /// </summary>
        /// <returns>Whether the new time is valid, will revert if not</returns>
        public delegate bool TimePickerDelegate(int newHour, int newMinute, int oldHour, int oldMin);


        public static readonly RoutedEvent PopupOpenedEvent =
            EventManager.RegisterRoutedEvent(
                nameof(PopupOpened),
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(TimePicker)
            );
        public event RoutedEventHandler PopupOpened
        {
            add { AddHandler(PopupOpenedEvent, value); }
            remove { RemoveHandler(PopupOpenedEvent, value); }
        }
            

        public static readonly DependencyProperty TimePickedProperty =
            DependencyProperty.Register(
                nameof(TimePicked),
                typeof(TimePickerDelegate),
                typeof(TimePicker),
                new PropertyMetadata(null)
            );
        public TimePickerDelegate TimePicked
        {
            get { return (TimePickerDelegate)GetValue(TimePickedProperty); }
            set { SetValue(TimePickedProperty, value); }
        }


        public static readonly DependencyProperty HourProperty =
            DependencyProperty.Register(
                nameof(Hour),
                typeof(int),
                typeof(TimePicker),
                new PropertyMetadata(0)
            );

        public int Hour
        {
            get { return (int)GetValue(HourProperty); }
            set { SetValue(HourProperty, value); }
        }


        public static readonly DependencyProperty MinuteProperty =
            DependencyProperty.Register(
                nameof(Minute),
                typeof(int),
                typeof(TimePicker),
                new PropertyMetadata(0)
            );
        public int Minute
        {
            get { return (int)GetValue(MinuteProperty); }
            set { SetValue(MinuteProperty, value); }
        }



        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                if (value)
                {
                    // save original values
                    _isValidChange = false;
                    _lastSelectedHour = Hour;
                    _lastSelectedMinute = Minute;
                }
                else if (!_isValidChange)
                {
                    SetCurrentValue(HourProperty, _lastSelectedHour);
                    SetCurrentValue(MinuteProperty, _lastSelectedMinute);
                }

                _isPopupOpen = value;
                OnPropertyChanged(nameof(IsPopupOpen));

                if (!value)
                    return;

                OnPopupOpened();
            }
        }

        public List<int> Hours { get; set; } = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
        public List<int> Minutes { get; } = new List<int> { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 };


        public ICommand SelectCommand { get; }
        public ICommand CancelCommand { get; }

        int _lastSelectedHour;
        int _lastSelectedMinute;
        bool _isValidChange;
        Window _lastLoadedWindow;

        public TimePicker()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;

            ToggleButton.Click += (s, e) => IsPopupOpen = true;

            CancelCommand = new RelayCommand(Cancel);
            SelectCommand = new RelayCommand(Select);

            Loaded += (s, e) =>
            {
                _lastLoadedWindow = Window.GetWindow(ToggleButton);
                if (_lastLoadedWindow == null)
                    return;

                HourPicker.WheelClick += Picker_WheelClick;
                MinutePicker.WheelClick += Picker_WheelClick;

                _lastLoadedWindow.LocationChanged += Window_LocationChanged;
                _lastLoadedWindow.PreviewMouseWheel += Window_PreviewMouseWheel;
            };

            Unloaded += (s, e) =>
            {
                HourPicker.WheelClick -= Picker_WheelClick;
                MinutePicker.WheelClick -= Picker_WheelClick;

                if (_lastLoadedWindow == null)
                    return;

                _lastLoadedWindow.LocationChanged -= Window_LocationChanged;
                _lastLoadedWindow.PreviewMouseWheel -= Window_PreviewMouseWheel;
            };
        }

        void Window_LocationChanged(object sender, EventArgs e)
        {
            // hacky: workaround to reposition popup
            var offset = Popup.HorizontalOffset;
            Popup.HorizontalOffset = offset + 1;
            Popup.HorizontalOffset = offset;
        }

        void Window_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!IsPopupOpen)
                    return;

                // Close when scrolled outside to prevent parent ScrollViewer updating position
                // Hit test as mouse captured when settings StaysOpen on the popup
                if (Popup.Child.InputHitTest(e.GetPosition(Popup.Child)) == null)
                {
                    IsPopupOpen = false;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(nameof(TimePicker), ex);
            }
        }

        void Picker_WheelClick(Shapes.SelectionWheel<int>.Location location)
        {
            if (location != Shapes.SelectionWheel<int>.Location.InSelectionBox)
                return;

            // treat a click on the highlight selection box as a confirmation
            Select();
        }

        void Cancel(object parameter = null)
        {
            IsPopupOpen = false;
        }

        void Select(object parameter = null)
        {
            _isValidChange = TimePicked?.Invoke(Hour, Minute, _lastSelectedHour, _lastSelectedMinute) == true;
            IsPopupOpen = false;
        }

        protected virtual void OnPopupOpened()
        {
            RaiseEvent(new RoutedEventArgs(PopupOpenedEvent, this));
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        void Popup_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            // Don't allow parent to scroll when over things like confirm / cancel buttons
            e.Handled = true;
        }
    }
}