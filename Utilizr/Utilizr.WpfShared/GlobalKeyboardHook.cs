﻿using System;
using System.Runtime.InteropServices;
using Utilizr.Logging;

namespace Utilzr.WPF
{
    public class GlobalKeyboardHook : IDisposable
    {
        public delegate void GlobalKeyEventHandler(int vkCode);
        public event GlobalKeyEventHandler GlobalKeyDown;
        public event GlobalKeyEventHandler GlobalKeyUp;

        public delegate int CallbackDelegate(int code, IntPtr w, IntPtr l);

        [DllImport("user32", CallingConvention = CallingConvention.StdCall)]
        private static extern int SetWindowsHookEx(HookType idHook, CallbackDelegate lpfn, int hInstance, int threadId);

        [DllImport("user32", CallingConvention = CallingConvention.StdCall)]
        private static extern bool UnhookWindowsHookEx(int idHook);

        [DllImport("user32", CallingConvention = CallingConvention.StdCall)]
        private static extern int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);


        public enum HookType : int
        {
            WH_JOURNALRECORD = 0,
            WH_JOURNALPLAYBACK = 1,
            WH_KEYBOARD = 2,
            WH_GETMESSAGE = 3,
            WH_CALLWNDPROC = 4,
            WH_CBT = 5,
            WH_SYSMSGFILTER = 6,
            WH_MOUSE = 7,
            WH_HARDWARE = 8,
            WH_DEBUG = 9,
            WH_SHELL = 10,
            WH_FOREGROUNDIDLE = 11,
            WH_CALLWNDPROCRET = 12,
            WH_KEYBOARD_LL = 13,
            WH_MOUSE_LL = 14
        }

        public enum KeyEvents
        {
            KeyDown = 0x0100,
            KeyUp = 0x0101,
            SKeyDown = 0x0104,
            SKeyUp = 0x0105
        }

        readonly int _key;
        int _hookID = 0;
        readonly CallbackDelegate _hookCallback = null;

        // https://stackoverflow.com/a/34290332
        /// <summary>
        /// </summary>
        /// <param name="key">Enum int value from System.Windows.Forms.Key</param>
        public GlobalKeyboardHook(int key)
        {
            _key = key;
            _hookCallback = new CallbackDelegate(KeybHookProc);            
        }

        public void Register()
        {
            _hookID = SetWindowsHookEx(HookType.WH_KEYBOARD_LL, _hookCallback, 0, 0);
        }

        public void Unregister()
        {
            if (_hookID == 0)
                return;
            
            UnhookWindowsHookEx(_hookID);
        }
        
        public void Dispose()
        {
            Unregister();
        }

        int KeybHookProc(int code, IntPtr w, IntPtr l)
        {
            if (code < 0)
                return CallNextHookEx(_hookID, code, w, l);

            try
            {
                var kEvent = (KeyEvents)w;
                var vkCode = Marshal.ReadInt32((IntPtr)l);

                if (vkCode == _key)
                {
                    if (kEvent == KeyEvents.KeyDown || kEvent == KeyEvents.SKeyDown)
                    {
#if DEBUG
                        Log.Info(nameof(GlobalKeyboardHook), $"KeyDown {vkCode}");
#endif
                        GlobalKeyDown?.Invoke(vkCode);
                    }

                    if (kEvent == KeyEvents.KeyUp || kEvent == KeyEvents.SKeyUp)
                    {
#if DEBUG
                        Log.Info(nameof(GlobalKeyboardHook), $"KeyUp {vkCode}");
#endif
                        GlobalKeyUp?.Invoke(vkCode);
                    }
                }
            }
            catch (Exception)
            {
                //Ignore all errors...
            }

            return CallNextHookEx(_hookID, code, w, l);
        }
    }
}