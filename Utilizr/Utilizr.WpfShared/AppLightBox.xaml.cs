﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilizr.Async;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class AppLightBox : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler LightBoxClosing;
        public event EventHandler LightBoxOpening;


        public static readonly DependencyProperty ContentItemProperty =
            DependencyProperty.Register(
                nameof(ContentItem),
                typeof(UIElement), 
                typeof(AppLightBox),
                new PropertyMetadata(null)
            );        

        public UIElement ContentItem
        {
            get { return (UIElement)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }
        

        public static readonly DependencyProperty OverlayBackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(OverlayBackgroundBrush),
                typeof(Brush),
                typeof(AppLightBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppLightBox), nameof(OverlayBackgroundBrush))
                )
            );

        public Brush OverlayBackgroundBrush
        {
            get { return (Brush)GetValue(OverlayBackgroundBrushProperty); }
            set { SetValue(OverlayBackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty OverlayBackgroundBorderBrushProperty = 
            DependencyProperty.Register(
                nameof(OverlayBackgroundBorderBrush),
                typeof(Brush), 
                typeof(AppLightBox), 
                new PropertyMetadata(default(Brush))
            );

        public Brush OverlayBackgroundBorderBrush
        {
            get { return (Brush) GetValue(OverlayBackgroundBorderBrushProperty); }
            set { SetValue(OverlayBackgroundBorderBrushProperty, value); }
        }


        public static readonly DependencyProperty OverlayBackgroundBorderThicknessProperty = 
            DependencyProperty.Register(
                nameof(OverlayBackgroundBorderThickness),
                typeof(Thickness),
                typeof(AppLightBox),
                new PropertyMetadata(default(Thickness))
            );

        public Thickness OverlayBackgroundBorderThickness
        {
            get { return (Thickness) GetValue(OverlayBackgroundBorderThicknessProperty); }
            set { SetValue(OverlayBackgroundBorderThicknessProperty, value); }
        }


        public static readonly DependencyProperty LightBoxBackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(LightBoxBackgroundBrush),
                typeof(Brush),
                typeof(AppLightBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppLightBox), nameof(LightBoxBackgroundBrush))
                )
            );

        public Brush LightBoxBackgroundBrush
        {
            get { return (Brush)GetValue(LightBoxBackgroundBrushProperty); }
            set { SetValue(LightBoxBackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty HasCloseButtonProperty =
            DependencyProperty.Register(
                nameof(HasCloseButton), 
                typeof(bool),
                typeof(AppLightBox), 
                new PropertyMetadata(true)
            );

        public bool HasCloseButton
        {
            get { return (bool)GetValue(HasCloseButtonProperty); }
            set { SetValue(HasCloseButtonProperty, value); }
        }


        public static readonly DependencyProperty ShowLightBoxProperty =
            DependencyProperty.Register(
                nameof(ShowLightBox),
                typeof(bool),
                typeof(AppLightBox),
                new PropertyMetadata(false, OnShowLightBoxChanged)
            );

        static void OnShowLightBoxChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is AppLightBox appLightBox))
                return;

            if ((bool)e.NewValue)
            {
                appLightBox.ClosedAndAnimationComplete = false;
                appLightBox.OnLightBoxOpening();
                return;
            }

            appLightBox.OnLightBoxClosing();

            // fail safe if something wrong with lightbox, otherwise will never be removed from gui
            AsyncHelper.BeginExecute(() =>
            {
                Sleeper.Sleep(300); // animation currently takes 200 ms
                appLightBox.ClosedAndAnimationComplete = true;
            });
        }

        public bool ShowLightBox
        {
            get { return (bool)GetValue(ShowLightBoxProperty); }
            set { SetValue(ShowLightBoxProperty, value); }
        }


        public static readonly DependencyProperty ShadowBorderBrushProperty =
            DependencyProperty.Register(
                nameof(ShadowBorderBrush),
                typeof(Brush),
                typeof(AppLightBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppLightBox), nameof(ShadowBorderBrush))
                )
            );

        public Brush ShadowBorderBrush
        {
            get { return (Brush)GetValue(ShadowBorderBrushProperty); }
            set { SetValue(ShadowBorderBrushProperty, value); }
        }


        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register(
                nameof(CornerRadius),
                typeof(CornerRadius),
                typeof(AppLightBox),
                new PropertyMetadata(new CornerRadius(5))
            );

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }


        public static readonly DependencyProperty ShadowBorderThicknessProperty =
            DependencyProperty.Register(
                nameof(ShadowBorderThickness),
                typeof(Thickness),
                typeof(AppLightBox),
                new PropertyMetadata(new Thickness(0))

            );

        public Thickness ShadowBorderThickness
        {
            get { return (Thickness)GetValue(ShadowBorderThicknessProperty); }
            set { SetValue(ShadowBorderThicknessProperty, value); }
        }


        public static readonly DependencyProperty LightBoxBorderBrushProperty =
            DependencyProperty.Register(
                nameof(LightBoxBorderBrush),
                typeof(Brush),
                typeof(AppLightBox),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppLightBox), nameof(LightBoxBorderBrush))
                )
            );

        public Brush LightBoxBorderBrush
        {
            get { return (Brush)GetValue(LightBoxBorderBrushProperty); }
            set { SetValue(LightBoxBorderBrushProperty, value); }
        }



        public static readonly DependencyProperty CloseButtonBrushProperty =
            DependencyProperty.Register(
                nameof(CloseButtonBrush),
                typeof(Brush),
                typeof(AppLightBox),
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb(188, 188, 188)))
            );

        public Brush CloseButtonBrush
        {
            get { return (Brush)GetValue(CloseButtonBrushProperty); }
            set { SetValue(CloseButtonBrushProperty, value); }
        }



        public static readonly DependencyProperty EnableShadowProperty =
            DependencyProperty.Register(
                nameof(EnableShadow), 
                typeof(bool),
                typeof(AppLightBox)
            );

        public bool EnableShadow
        {
            get { return (bool)GetValue(EnableShadowProperty); }
            set { SetValue(EnableShadowProperty, value); }
        }


        private HorizontalAlignment _contentHorizontalAlignment = HorizontalAlignment.Center;
        public HorizontalAlignment ContentHorizontalAlignment
        {
            get { return _contentHorizontalAlignment; }
            set
            {
                _contentHorizontalAlignment = value;
                OnPropertyChanged(nameof(ContentHorizontalAlignment));
            }
        }

        private VerticalAlignment _contentVerticalAlignment = VerticalAlignment.Center;
        public VerticalAlignment ContentVerticalAlignment
        {
            get { return _contentVerticalAlignment; }
            set
            {
                _contentVerticalAlignment = value;
                OnPropertyChanged(nameof(ContentVerticalAlignment));
            }
        }

        private Thickness _contentMargin;
        public Thickness ContentMargin
        {
            get { return _contentMargin; }
            set
            {
                _contentMargin = value;
                OnPropertyChanged(nameof(ContentMargin));
            }
        }

        private bool _closedAndAnimationComplete = true;
        public bool ClosedAndAnimationComplete
        {
            get { return _closedAndAnimationComplete; }
            private set
            {
                _closedAndAnimationComplete = value;
                OnPropertyChanged(nameof(ClosedAndAnimationComplete));
            }
        }

        public AppLightBox()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        void AppButton_ButtonClicked(object sender, EventArgs e)
        {
            ShowLightBox = false;
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnLightBoxClosing()
        {
            LightBoxClosing?.Invoke(this, new EventArgs());
        }

        protected virtual void OnLightBoxOpening()
        {
            LightBoxOpening?.Invoke(this, new EventArgs());
        }

        void ClosingAnimation_Completed(object sender, EventArgs e)
        {
            ClosedAndAnimationComplete = true;
        }

        void OpeningAnimation_Completed(object sender, EventArgs e)
        {
            InvalidateVisual();
        }
    }
}