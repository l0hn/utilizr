﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Extension;

namespace Utilzr.WPF
{
    public partial class TextBlockHorizontalScale : UserControl
    {
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(null, OnInvalidate)
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        private static readonly DependencyProperty LineCountProperty =
            DependencyProperty.Register(
                nameof(LineCount),
                typeof(int),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(1, OnInvalidate)
            );        

        /// <summary>
        /// Maximum line count before the text increases width
        /// </summary>
        public int LineCount
        {
            get { return (int)GetValue(LineCountProperty); }
            set { SetValue(LineCountProperty, value); }
        }


        public static readonly DependencyProperty TextTrimmingProperty =
            DependencyProperty.Register(
                nameof(TextTrimming),
                typeof(TextTrimming),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(TextTrimming.None, OnInvalidate)
            );

        public TextTrimming TextTrimming
        {
            get { return (TextTrimming)GetValue(TextTrimmingProperty); }
            set { SetValue(TextTrimmingProperty, value); }
        }


        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register(
                nameof(TextWrapping),
                typeof(TextWrapping),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(TextWrapping.NoWrap, OnInvalidate)
            );

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }


        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register(
                nameof(TextAlignment),
                typeof(TextAlignment),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(TextAlignment.Center, OnInvalidate)
            );

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }


        public static readonly DependencyProperty StepProperty =
            DependencyProperty.Register(
                nameof(Step),
                typeof(int),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(5)
            );

        /// <summary>
        /// Not all values of <see cref="Text"/> will be split perfectly between the number of
        /// lines specified on <see cref="LineCount"/>. The width will keeping increasing by this
        /// specified value in a loop until the text does now fit on the specified <see cref="LineCount"/>
        /// Smaller values will be more fine tuned, but also incur greater iterations.
        /// </summary>
        public int Step
        {
            get { return (int)GetValue(StepProperty); }
            set { SetValue(StepProperty, value); }
        }



        public static readonly DependencyProperty FailSafeMarginProperty =
            DependencyProperty.Register(
                nameof(FailSafeMargin),
                typeof(int),
                typeof(TextBlockHorizontalScale),
                new PropertyMetadata(20, OnInvalidate)
            );

        /// <summary>
        /// The <see cref="Text"/> will stretch to all available width if unable to preform
        /// calculations. This value will be subtracted from the available width. Note: no
        /// margin will be applied if subtracting this value produces a negative number.
        /// </summary>
        public int FailSafeMargin
        {
            get { return (int)GetValue(FailSafeMarginProperty); }
            set { SetValue(FailSafeMarginProperty, value); }
        }


        public TextBlockHorizontalScale()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;

            Loaded += (s, e) => Invalidate();
            SizeChanged += (s, e) => Invalidate();

#if NETCOREAPP
            this.RegisterDpiChanged(() => Invalidate());
#endif
        }

        static void OnInvalidate(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is TextBlockHorizontalScale tb))
                return;

            tb.Invalidate();
        }

        void Invalidate()
        {
            var text = Text;
            var width = ActualWidth;

            void failSafe()
            {
                textBlock.Text = text;
                var failSafeWidth = width - FailSafeMargin;
                textBlock.MaxWidth = failSafeWidth > 0
                    ? failSafeWidth
                    : width;
            }

            try
            {
                if (width < 1 || double.IsInfinity(width) || double.IsNaN(width) || string.IsNullOrEmpty(text))
                    return; // not yet loaded

                var fmtText = GenerateFormattedText(text);
                var fmtTextSingleLineHeight = fmtText.Height;

                if (fmtTextSingleLineHeight < 1)
                {
                    failSafe();
                    return;
                }

                var textWidth = fmtText.Width;
                var textLines = Math.Max(LineCount, 1);

                // set width so height will now show the total number of lines
                fmtText.MaxTextWidth = (int)(textWidth / textLines) + 1;
                while (fmtText.Height / fmtTextSingleLineHeight > textLines)
                {
                    // may still be more than desired count as unable to perfectly split string, long words, etc
                    fmtText.MaxTextWidth += Step;
                }

                textBlock.MaxWidth = fmtText.MaxTextWidth;
                textBlock.Text = text;
            }
            catch (Exception)
            {
                failSafe();
            }
        }

        private FormattedText GenerateFormattedText(string text)
        {
#if NETCOREAPP
            return new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(FontFamily, FontStyle, FontWeight, FontStretch),
                FontSize,
                Foreground,
                new NumberSubstitution(),
                this.GetDpi()
            );
#else
            return new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(FontFamily, FontStyle, FontWeight, FontStretch),
                FontSize,
                Foreground,
                new NumberSubstitution()
            );
#endif
        }
    }
}