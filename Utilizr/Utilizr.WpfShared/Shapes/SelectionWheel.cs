﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Utilizr.Extensions;
using Utilzr.WPF.Extension;

namespace Utilzr.WPF.Shapes
{
    public class DefaultWheel : SelectionWheel<object> { }

    public class NumberWheel : SelectionWheel<int>
    {
        protected override string ItemDisplayFormat(int item)
        {
            return item.ToString("00");
        }
    }

    public class ByteWheel : SelectionWheel<ByteUnit>
    {
        protected override string ItemDisplayFormat(ByteUnit item)
        {
            return item.ToLocale();
        }
    }

    public class SelectionWheel<T> : Shape
    {
        public enum Location
        {
            Unknown,
            AboveSelectionBox,
            InSelectionBox,
            BelowSelectionBox,
        }

        public delegate void MouseMovementEvent();
        public delegate void MouseClickEvent(Location location);

        public event MouseMovementEvent MouseGone;
        public event MouseMovementEvent MouseOverAndAbove;
        public event MouseMovementEvent MouseOverAndInSelection;
        public event MouseMovementEvent MouseOverAndBelow;

        public event MouseClickEvent Click;

        protected override Geometry DefiningGeometry => new RectangleGeometry(new Rect(0, 0, WheelWidth, WheelHeight));


        public static readonly DependencyProperty WheelHeightProperty =
            DependencyProperty.Register(
                nameof(WheelHeight),
                typeof(double),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(180.0, InvalidateSelectionWheel)
            );
        public double WheelHeight
        {
            get { return (double)GetValue(WheelHeightProperty); }
            set { SetValue(WheelHeightProperty, value); }
        }


        public static readonly DependencyProperty WheelWidthProperty =
            DependencyProperty.Register(
                nameof(WheelWidth),
                typeof(double),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(80.0, InvalidateSelectionWheel)
            );
        public double WheelWidth
        {
            get { return (double)GetValue(WheelWidthProperty); }
            set { SetValue(WheelWidthProperty, value); }
        }


        public static readonly DependencyProperty RowsToDisplayProperty =
            DependencyProperty.Register(
                nameof(RowsToDisplay),
                typeof(double),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(5.0, InvalidateSelectionWheel)
            );

        public double RowsToDisplay
        {
            get { return (double)GetValue(RowsToDisplayProperty); }
            set { SetValue(RowsToDisplayProperty, value); }
        }


        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(BackgroundBrush),
                typeof(Brush),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.White), InvalidateSelectionWheel)
            );
        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.Register(
                nameof(BorderBrush),
                typeof(Brush),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.Black), InvalidateSelectionWheel)
            );
        public Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }


        public static readonly DependencyProperty HighlightBrushProperty =
            DependencyProperty.Register(
                nameof(HighlightBrush),
                typeof(Brush),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.LightGray), InvalidateSelectionWheel)
            );
        public Brush HighlightBrush
        {
            get { return (Brush)GetValue(HighlightBrushProperty); }
            set { SetValue(HighlightBrushProperty, value); }
        }


        public static readonly DependencyProperty BorderThicknessProperty =
            DependencyProperty.Register(
                nameof(BorderThickness),
                typeof(int),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(1, InvalidateSelectionWheel)
            );
        public int BorderThickness
        {
            get { return (int)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }


        public static readonly DependencyProperty ForegroundBrushProperty =
            DependencyProperty.Register(
                nameof(ForegroundBrush),
                typeof(Brush),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(new SolidColorBrush(Colors.Black), InvalidateSelectionWheel)
            );
        /// <summary>
        /// Text colour
        /// </summary>
        public Brush ForegroundBrush
        {
            get { return (Brush)GetValue(ForegroundBrushProperty); }
            set { SetValue(ForegroundBrushProperty, value); }
        }



        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register(
                nameof(FontSize),
                typeof(double),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(14.0)
            );
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }


        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register(
                nameof(Items),
                typeof(List<T>),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(new List<T>(), InvalidateSelectionWheel)
            );
        public List<T> Items
        {
            get { return (List<T>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }


        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register(
                nameof(Selected),
                typeof(T),
                typeof(SelectionWheel<T>),
                new PropertyMetadata(default(T), InvalidateSelectionWheel)
            );

        public T Selected
        {
            get { return (T)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }


        static void InvalidateSelectionWheel(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            InvalidateSelectionWheel(d as SelectionWheel<T>);
        }

        static void InvalidateSelectionWheel(SelectionWheel<T> selectionWheel)
        {
            selectionWheel?.InvalidateMeasure();
            selectionWheel?.InvalidateVisual();
        }

        private Typeface _textTypeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name);
        private Typeface TextTypeFace => _textTypeFaceUnsafe ?? (_textTypeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name));

        int _lastMouseWheelEventMs = 0;

        public SelectionWheel()
        {
            MouseWheel += SelectionWheel_MouseWheel;
            MouseMove += SelectionWheel_MouseUpdate;
            MouseLeave += SelectionWheel_MouseUpdate;

            MouseLeftButtonUp += SelectionWheel_MouseLeftButtonUp;

#if NETCOREAPP
            this.RegisterDpiChanged(() => InvalidateSelectionWheel(this));
#endif
        }

        void SelectionWheel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var mp = e.GetPosition(this);
            var location = GetLocationForPoint(mp);

            var index = Items.IndexOf(Selected);
            if (location == Location.AboveSelectionBox)
                index--;
            else if (location == Location.BelowSelectionBox)
                index++;

            SetCurrentValue(SelectedProperty, Items[Items.WrappedIndex(index)]);
            OnClick(location);
        }

        void SelectionWheel_MouseUpdate(object sender, MouseEventArgs e)
        {
            if (!IsMouseOver)
            {
                OnMouseGone();
                return;
            }

            var mp = Mouse.GetPosition(this);
            var location = GetLocationForPoint(mp);

            switch (location)
            {
                case Location.AboveSelectionBox:
                    OnMouseOverAndAbove();
                    break;
                case Location.InSelectionBox:
                    OnMouseOverAndInSelection();
                    break;
                case Location.BelowSelectionBox:
                    OnMouseOverAndBelow();
                    break;
            }
        }

        Location GetLocationForPoint(Point mousePoint)
        {
            if (HighlightBoxBounds.Contains(mousePoint))
                return Location.InSelectionBox;

            if (mousePoint.Y >= 0 && mousePoint.Y < HighlightBoxBounds.Top)
                return Location.AboveSelectionBox;

            if (mousePoint.Y > HighlightBoxBounds.Bottom && mousePoint.Y <= WheelHeight)
                return Location.BelowSelectionBox;

            return Location.Unknown;
        }

        void SelectionWheel_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            HandleMouseWheel(e);
            e.Handled = true;
        }

        public void HandleMouseWheel(MouseWheelEventArgs e)
        {
            // todo: fine tune scrolling on selection wheel
            var time = e.Timestamp - _lastMouseWheelEventMs;
            if (time > 5)
            {
                var index = Items.IndexOf(Selected);
                if (e.Delta > 10)
                    index--;
                else if (e.Delta < -10)
                    index++;

                SetCurrentValue(SelectedProperty, Items[Items.WrappedIndex(index)]);
            }

            _lastMouseWheelEventMs = e.Timestamp;
        }

        public double RowHeight => WheelHeight / RowsToDisplay; // Split up like Grid.RowDefintions
        private Rect HighlightBoxBounds => new Rect(0, (WheelHeight / 2) - (RowHeight / 2), WheelWidth, RowHeight);

        protected override void OnRender(DrawingContext drawingContext)
        {
            var borderPen = BorderThickness > 0
                ? new Pen(BorderBrush, BorderThickness)
                : null;

            drawingContext.DrawRectangle(BackgroundBrush, borderPen, new Rect(0, 0, WheelWidth, WheelHeight));
            drawingContext.DrawRectangle(HighlightBrush, borderPen, HighlightBoxBounds);

            if (Items?.Count > 2)
            {
                var rows = (int)RowsToDisplay;
                var toDraw = new T[rows];
                var offset = rows / 2; // intentional integer division
                var selectedIndex = Items.IndexOf(Selected);
                for (int i = 0; i < rows; i++)
                {
                    toDraw[i] = Items[Items.WrappedIndex(selectedIndex + (i - offset))];

                }

                for (int i = 0; i < toDraw.Length; i++)
                {
                    var item = toDraw[i];

#if NETCOREAPP
                    var formattedText = new FormattedText(
                        ItemDisplayFormat(item),
                        CultureInfo.CurrentCulture,
                        CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                            ? FlowDirection.RightToLeft
                            : FlowDirection.LeftToRight,
                        TextTypeFace,
                        FontSize,
                        ForegroundBrush,
                        this.GetDpi()
                    );
#else
                    var formattedText = new FormattedText(
                        ItemDisplayFormat(item),
                        CultureInfo.CurrentCulture,
                        CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                            ? FlowDirection.RightToLeft
                            : FlowDirection.LeftToRight,
                        TextTypeFace,
                        FontSize,
                        ForegroundBrush
                    );
#endif

                    var textPoint = new Point(
                        WheelWidth / 2.0,
                        (RowHeight * i) + (RowHeight / 2.0)
                    );
                    textPoint.Offset(-formattedText.Width / 2.0, -formattedText.Height / 2.0);
                    drawingContext.DrawText(formattedText, textPoint);
                }
            }
        }

        /// <summary>
        /// How an instance of T will be displayed on the GUI. Default is simply object.ToString().
        /// </summary>
        protected virtual string ItemDisplayFormat(T item)
        {
            return item.ToString();
        }

        protected virtual void OnMouseGone()
        {
            MouseGone?.Invoke();
        }

        protected virtual void OnMouseOverAndAbove()
        {
            MouseOverAndAbove?.Invoke();
        }

        protected virtual void OnMouseOverAndInSelection()
        {
            MouseOverAndInSelection?.Invoke();
        }

        protected virtual void OnMouseOverAndBelow()
        {
            MouseOverAndBelow?.Invoke();
        }

        protected virtual void OnClick(Location location)
        {
            Click?.Invoke(location);
        }
    }
}