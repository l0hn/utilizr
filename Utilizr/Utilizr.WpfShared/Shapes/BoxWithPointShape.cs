﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Utilzr.WPF.Shapes
{
    public class BoxWithPointShape : Shape
    {
        private readonly double _minHeight = 25;
        private readonly double _minWidth = 50;

        private Geometry _definingGeometry;
        protected override Geometry DefiningGeometry
        {
            get
            {
                if (_definingGeometry == null)
                {
                    _definingGeometry = new RectangleGeometry(
                        new Rect(
                            0,
                            0,
                            ActualWidth < _minWidth
                                ? _minWidth
                                : ActualWidth,
                            ActualHeight < _minHeight
                                ? _minHeight
                                : ActualHeight
                        )
                    );
                }

                return _definingGeometry;
            }
        }

        public double PointWidthProportion { get; set; } = 0.1;
        public double PointHeight => 10;

        protected override void OnRender(DrawingContext drawingContext)
        {
            double pointWidth = ActualWidth * PointWidthProportion;

            var points = new Point[]
            {
                new Point(ActualWidth, 0 + PointHeight), // start top line right
                new Point(0 + (2 * pointWidth), 0 + PointHeight), // begin point
                new Point(0 + (1.5 * pointWidth), 0), // tip
                new Point(0 + pointWidth, 0 + PointHeight), // end point
                new Point(0, 0 + PointHeight), // end line top left
                new Point(0, ActualHeight), // line left
                new Point(ActualWidth, ActualHeight), // line bottom
            };

            var streamGeometry = new StreamGeometry();
            using (var ctx = streamGeometry.Open())
            {
                ctx.BeginFigure(points[0], true, true);
                for (int i = 1; i < points.Length; i++)
                {
                    ctx.LineTo(points[i], true, true);
                }
            }

            streamGeometry.Freeze();
            drawingContext.DrawGeometry(Fill, new Pen(Stroke, StrokeThickness), streamGeometry);
        }
    }
}