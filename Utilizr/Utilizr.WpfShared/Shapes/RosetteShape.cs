﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Utilzr.WPF.Shapes
{
    public class RosetteShape : Shape
    {

        public static readonly DependencyProperty RosetteSizeProperty =
            DependencyProperty.Register(
                nameof(RosetteSize),
                typeof(double),
                typeof(RosetteShape),
                new PropertyMetadata(100.0, UpdateRosette)
            );

        public double RosetteSize
        {
            get { return (double)GetValue(RosetteSizeProperty); }
            set { SetValue(RosetteSizeProperty, value); }
        }


        public static readonly DependencyProperty SpikeCountProperty =
            DependencyProperty.Register(
                nameof(SpikeCount),
                typeof(int),
                typeof(RosetteShape),
                new PropertyMetadata(16, UpdateRosette)
            );

        public int SpikeCount
        {
            get { return (int)GetValue(SpikeCountProperty); }
            set { SetValue(SpikeCountProperty, value); }
        }


        public static readonly DependencyProperty SpikeHeightProperty =
            DependencyProperty.Register(
                nameof(SpikeHeight),
                typeof(double),
                typeof(RosetteShape),
                new UIPropertyMetadata(10.0, UpdateRosette)
            );

        public double SpikeHeight
        {
            get { return (double)GetValue(SpikeHeightProperty); }
            set { SetValue(SpikeHeightProperty, value); }
        }

        protected static void UpdateRosette(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var rs = d as RosetteShape;
            rs?.InvalidateVisual();
        }


        private Geometry _definingGeometry;
        protected override Geometry DefiningGeometry
        {
            get
            {
                return _definingGeometry ?? (_definingGeometry = new EllipseGeometry(new Rect(new Size(RosetteSize, RosetteSize))));
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            var points = new Point[SpikeCount * 2];
            double anglePerPoint = 360.0 / SpikeCount;
            Point pCenter = GetCenterPoint();
            for (int i = 0; i < SpikeCount; i++)
            {
                points[i*2] = PointAtAngle(anglePerPoint * i, pCenter, true);
                points[(i*2)+1] = PointAtAngle((anglePerPoint * i) + (anglePerPoint / 2), pCenter, false);
            }

            var streamGeometry = new StreamGeometry();
            using (var ctx = streamGeometry.Open())
            {
                ctx.BeginFigure(points[0], true, true);
                for (int i = 1; i < points.Length; i++)
                {
                    ctx.LineTo(points[i], true, true);
                }
            }

            streamGeometry.Freeze();
            drawingContext.DrawGeometry(Fill, new Pen(Stroke, StrokeThickness), streamGeometry);
        }

        /// <summary>
        /// Get the point on the circle circumference
        /// </summary>
        /// <param name="angle">Angle of the point to get</param>
        /// <param name="center">Center point of the circle</param>
        /// <param name="isInnerPoint">If true, substracts <see cref="SpikeHeight"/> from radius. 
        /// Allows option to get zigzag points on the two circumferences. Think of one circle for
        /// chord points, and another for triangular tip points</param>
        /// <returns></returns>
        protected Point PointAtAngle(double angle, Point center, bool isInnerPoint)
        {
            double radAngle = angle * (Math.PI / 180);;
            double x = ((RosetteSize - (isInnerPoint ? 0 : SpikeHeight)) / 2.0) * Math.Cos(radAngle);
            double y = ((RosetteSize - (isInnerPoint ? 0 : SpikeHeight)) / 2.0) * Math.Sin(radAngle);

            return new Point
            {
                X = x + center.X,
                Y = center.Y + y,
            };
        }

        protected Point GetCenterPoint()
        {
            double xCentre = RosetteSize / 2;
            double yCentre = RosetteSize / 2;
            return new Point(xCentre, yCentre);
        }
    }
}