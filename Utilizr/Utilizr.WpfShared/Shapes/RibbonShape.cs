﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Utilzr.WPF.Extension;

namespace Utilzr.WPF.Shapes
{
    public class RibbonShape : Shape
    {

        public static readonly DependencyProperty RosetteSizeProperty =
            DependencyProperty.Register(
                nameof(RibbonSize),
                typeof(double),
                typeof(RibbonShape),
                new UIPropertyMetadata(100.0, UpdateRibbon)
            );

        public double RibbonSize
        {
            get { return (double)GetValue(RosetteSizeProperty); }
            set { SetValue(RosetteSizeProperty, value); }
        }


        public static readonly DependencyProperty RibbonBarHeightProperty =
            DependencyProperty.Register(
                nameof(RibbonBarHeight),
                typeof(double),
                typeof(RibbonShape),
                new UIPropertyMetadata(25.0, UpdateRibbon)
            );

        /// <summary>
        /// Bar height relative to x,y sides of parent, not of 45 degree angle.
        /// </summary>
        public double RibbonBarHeight
        {
            get { return (double)GetValue(RibbonBarHeightProperty); }
            set { SetValue(RibbonBarHeightProperty, value); }
        }


        public static readonly DependencyProperty GripSizeProperty =
            DependencyProperty.Register(
                nameof(GripSize),
                typeof(double),
                typeof(RibbonShape),
                new UIPropertyMetadata(10.0, UpdateRibbon)
            );

        public double GripSize
        {
            get { return (double)GetValue(GripSizeProperty); }
            set { SetValue(GripSizeProperty, value); }
        }


        public static readonly DependencyProperty GripShadowBrushProperty =
            DependencyProperty.Register(
                nameof(GripShadowBrush),
                typeof(Brush),
                typeof(RibbonShape),
                new UIPropertyMetadata(new SolidColorBrush(Color.FromArgb(128, 0, 0, 0)), UpdateRibbon)
            );

        public Brush GripShadowBrush
        {
            get { return (Brush)GetValue(GripShadowBrushProperty); }
            set { SetValue(GripShadowBrushProperty, value); }
        }


        public static readonly DependencyProperty TextBrushProperty =
            DependencyProperty.Register(
                nameof(TextBrush),
                typeof(Brush),
                typeof(RibbonShape),
                new UIPropertyMetadata(new SolidColorBrush(Colors.Black), UpdateRibbon)
            );

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }


        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(RibbonShape),
                new UIPropertyMetadata(null, UpdateRibbon)
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register(
                nameof(FontSize),
                typeof(double),
                typeof(RibbonShape),
                new UIPropertyMetadata(12.0, UpdateRibbon)
            );



        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register(
                nameof(TextMargin),
                typeof(Thickness),
                typeof(RibbonShape),
                new UIPropertyMetadata(new Thickness(10,5,10,5), UpdateRibbon)
            );

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }


        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }


        public static readonly DependencyProperty FontWeightProperty =
            DependencyProperty.Register(
                nameof(FontWeight),
                typeof(FontWeight),
                typeof(RibbonShape),
                new UIPropertyMetadata(FontWeights.Normal, UpdateRibbon)
            );

        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }


        protected static void UpdateRibbon(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var rs = d as RibbonShape;
            rs?.InvalidateVisual();
        }


        //private Geometry _definingGeometry;
        protected override Geometry DefiningGeometry
        {
            get
            {
                //if (_definingGeometry == null)
                //{
                //    // always takes up square on gui
                //    var squareSide = RibbonSize + RibbonBarHeight + GripSize;
                //    _definingGeometry = new RectangleGeometry(new Rect(new Size(squareSide, squareSide)));
                //}
                //return _definingGeometry;

                if (double.IsInfinity(ActualRibbonBarHeight))
                    return new RectangleGeometry(new Rect(new Size(RibbonSize + RibbonBarHeight + GripSize, RibbonSize + RibbonBarHeight + GripSize)));

                return new RectangleGeometry(new Rect(new Size(RibbonSize + ActualRibbonBarHeight + GripSize, RibbonSize + ActualRibbonBarHeight + GripSize)));
            }
        }

        public double ActualRibbonBarHeight { get; private set; } = double.PositiveInfinity;

        Typeface _typeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name);
        Typeface TypeFace => _typeFaceUnsafe ?? (_typeFaceUnsafe = new Typeface(System.Drawing.SystemFonts.DefaultFont.FontFamily.Name));

        public RibbonShape()
        {
#if NETCOREAPP
            this.RegisterDpiChanged(() => InvalidateVisual());
#endif
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            // Ribbon dimensions:
            // --------x-----y
            // |      /    /
            // |    /    /          
            // |  /  t /        x = first hypotenuse (iscolesesSide) which is ribbon length
            // x/    /          y  = x + ribbon height
            // |   /            t = text midpoint
            // | /
            // y

            // Ribbon text dimensions / layout / logic:
            //     --------------z
            //     |       |   /        z = midpoint between x and y above to create text centre line hypotenuse
            //     |      h| /          l = hypotenuse length
            //     |   h   |/           m = hypotenuse midpoint (l/2)
            //     -------- m           h = height relative to x (and y since right angle triangle)
            //     |     /     l
            // z/2 |   /     
            //     | /
            //     z

            const int angle = -45;

            var iscolesesSide = (RibbonSize / 2.0) * Math.Sqrt(2);
            var pen = new Pen(Stroke, StrokeThickness);

            Point textOrigin = new Point();
            Point textMidpoint = new Point();
            FormattedText formattedText = null;
            if (!string.IsNullOrEmpty(Text))
            {
                // Need to check now if text will fit.
                // If not, increase ribbon bar height to accommodate now before we draw anything
                formattedText = CalculateTextAndRibbonHeight(iscolesesSide, RibbonBarHeight, out textMidpoint, out textOrigin);
            }

            // trapezium segment
            var trapeziumPoints = new List<Point>()
            {
                new Point(0, iscolesesSide), //top left
                new Point(0, iscolesesSide + ActualRibbonBarHeight), //bottom left
                new Point(iscolesesSide + ActualRibbonBarHeight, 0), //bottom right
                new Point(iscolesesSide, 0), // top right
            };
            DrawShape(drawingContext, trapeziumPoints, Fill, pen);

            // left grip
            // rotation grip triangle at top left corner
            var leftGripRt = new RotateTransform(angle, 0, iscolesesSide + ActualRibbonBarHeight);
            drawingContext.PushTransform(leftGripRt);
            var leftGripPoints = new List<Point>()
            {
                new Point(0, iscolesesSide + ActualRibbonBarHeight), //bottom left trapezium (top left grip)
                new Point(0, iscolesesSide + ActualRibbonBarHeight + GripSize), //bottom left
                new Point(GripSize, iscolesesSide + ActualRibbonBarHeight), //top right
            };
            DrawShape(drawingContext, leftGripPoints, Fill, pen);
            DrawShape(drawingContext, leftGripPoints, GripShadowBrush, pen); // shadow
            drawingContext.Pop();


            // right grip
            var rightGripRt = new RotateTransform(angle, iscolesesSide + ActualRibbonBarHeight, 0);
            drawingContext.PushTransform(rightGripRt);
            var rightGripPoints = new List<Point>()
            {
                new Point(iscolesesSide + ActualRibbonBarHeight, 0), //bottom right trapezium (top right grip)
                new Point(iscolesesSide + ActualRibbonBarHeight, GripSize), //bottom right
                new Point(iscolesesSide + ActualRibbonBarHeight - GripSize, 0), //top left
            };
            DrawShape(drawingContext, rightGripPoints, Fill, pen);
            DrawShape(drawingContext, rightGripPoints, GripShadowBrush, pen); // shadow
            drawingContext.Pop();

            var gripOffset = (GripSize / 2.0) * Math.Sqrt(2);
            Margin = new Thickness(-gripOffset, -gripOffset, 0, 0);

            if (formattedText == null)
                return;

            var rt = new RotateTransform(-45, textMidpoint.X, textMidpoint.Y);
            drawingContext.PushTransform(rt);
            drawingContext.DrawText(formattedText, textOrigin);
            drawingContext.Pop();                        
        }

        void DrawShape(DrawingContext dc, List<Point> points, Brush fill, Pen pen)
        {
            var streamGeometry = new StreamGeometry();
            using (var ctx = streamGeometry.Open())
            {
                ctx.BeginFigure(points[0], true, true);
                foreach (var point in points)
                {
                    ctx.LineTo(point, true, true);
                }
            }
            streamGeometry.Freeze();
            dc.DrawGeometry(fill, pen, streamGeometry);
        }

        FormattedText CalculateTextAndRibbonHeight(double iscolesesSide, double ribbonHeight, out Point textMidpoint, out Point textOrigin)
        {
            var sideWithRibbonMidpoint = iscolesesSide + (ribbonHeight / 2.0); // z on diagram
            var textHypontenuse = sideWithRibbonMidpoint * Math.Sqrt(2.0); // l on diagram
            var aSq = Math.Pow((sideWithRibbonMidpoint / 2.0), 2);
            var cSq = Math.Pow((textHypontenuse / 2.0), 2);
            var textOffset = Math.Sqrt(cSq - aSq);

            textMidpoint = new Point(textOffset, textOffset);
            var marginOffsetX = TextMargin.Left - TextMargin.Right;
            var marginOffsetY = TextMargin.Top - TextMargin.Bottom;
            textMidpoint.Offset(marginOffsetX, marginOffsetY);

#if NETCOREAPP
            var formattedText = new FormattedText(
                Text,
                CultureInfo.CurrentCulture,
                CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                    ? FlowDirection.RightToLeft
                    : FlowDirection.LeftToRight,
                TypeFace,
                FontSize,
                TextBrush,
                this.GetDpi()
            );
#else
            var formattedText = new FormattedText(
                Text,
                CultureInfo.CurrentCulture,
                CultureInfo.CurrentCulture.TextInfo.IsRightToLeft
                    ? FlowDirection.RightToLeft
                    : FlowDirection.LeftToRight,
                TypeFace,
                FontSize,
                TextBrush
            );
#endif

            formattedText.TextAlignment = TextAlignment.Center;
            formattedText.Trimming = TextTrimming.CharacterEllipsis;
            formattedText.SetFontWeight(FontWeight);
            formattedText.MaxTextWidth = textHypontenuse - TextMargin.Left - TextMargin.Right;
            //formattedText.MaxTextWidth = RibbonSize;

            textOrigin = new Point(textMidpoint.X, textMidpoint.Y);

            // Here be dragons: setting MaxTextWidth affects the location of origin when using DrawingContext.DrawText
            // Instead of offsetting by something sensible to get top left corner, you need to half the max width value
            // when centre aligned, or full if right aligned. Good job MS documents this and hours aren't wasted...
            var offsetX = formattedText.MaxTextWidth / 2.0;
            var offsetY = formattedText.Height / 2.0;
            textOrigin.Offset(-offsetX, -offsetY);

            var realRibbonHeight = Math.Sqrt(Math.Pow(ribbonHeight, 2) / 2.0); // height relative to 45 degree angle, not x,y parent border
            var ribbonBarHeightDelta = realRibbonHeight - (formattedText.Height + TextMargin.Bottom + TextMargin.Top);
            if (ribbonBarHeightDelta < 0)
            {
                // recalculate
                // convert height difference back to original
                var newRealRibbonHeight = realRibbonHeight + (Math.Abs((int)ribbonBarHeightDelta)) + 1;
                //var newRibbonHeight = ribbonHeight + (Math.Abs((int)ribbonBarHeightDelta)) + 1;
                var newRibbonHeight = Math.Sqrt(2 * Math.Pow(newRealRibbonHeight, 2));
                return CalculateTextAndRibbonHeight(iscolesesSide, newRibbonHeight, out textMidpoint, out textOrigin);
            }

            ActualRibbonBarHeight = ribbonHeight;
            //formattedText.MaxTextHeight = ActualRibbonBarHeight - TextMargin.Bottom - TextMargin.Top;
            return formattedText;
        }
    }
}