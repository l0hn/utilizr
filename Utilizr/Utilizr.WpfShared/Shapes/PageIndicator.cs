﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Utilzr.WPF.Shapes
{
    public class PageIndicator : Shape
    {

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                nameof(Command),
                typeof(ICommand),
                typeof(PageIndicator),
                new PropertyMetadata(null)
            );

        /// <summary>
        /// Invoked on click with the clicked index
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly DependencyProperty PageCountProperty =
            DependencyProperty.Register(
                nameof(PageCount),
                typeof(int),
                typeof(PageIndicator),
                new PropertyMetadata(1, UpdateIndicatorAndLayout)
            );

        public int PageCount
        {
            get { return (int)GetValue(PageCountProperty); }
            set { SetValue(PageCountProperty, value); }
        }


        public static readonly DependencyProperty SelectedPageIndexProperty =
            DependencyProperty.Register(
                nameof(SelectedPageIndex),
                typeof(int),
                typeof(PageIndicator),
                new UIPropertyMetadata(0, UpdateIndicatorIndex)
            );

        public int SelectedPageIndex
        {
            get { return (int)GetValue(SelectedPageIndexProperty); }
            set { SetValue(SelectedPageIndexProperty, value); }
        }


        public static readonly DependencyProperty IndicatorSizeProperty =
            DependencyProperty.Register(
                nameof(IndicatorSize),
                typeof(int),
                typeof(PageIndicator),
                new PropertyMetadata(16, UpdateIndicatorAndLayout)
            );

        /// <summary>
        /// Note: indicator total size is this plus <see cref="IndicatorBorderSize"/>
        /// </summary>
        public int IndicatorSize
        {
            get { return (int)GetValue(IndicatorSizeProperty); }
            set { SetValue(IndicatorSizeProperty, value); }
        }


        public static readonly DependencyProperty IndicatorBorderSizeProperty =
            DependencyProperty.Register(
                nameof(IndicatorBorderSize),
                typeof(int),
                typeof(PageIndicator),
                new PropertyMetadata(0, UpdateIndicatorAndLayout)
            );

        /// <summary>
        /// Note: indicator total size is this plus <see cref="IndicatorSize"/>
        /// </summary>
        public int IndicatorBorderSize
        {
            get { return (int)GetValue(IndicatorBorderSizeProperty); }
            set { SetValue(IndicatorBorderSizeProperty, value); }
        }


        public static readonly DependencyProperty IndicatorPaddingProperty =
            DependencyProperty.Register(
                nameof(IndicatorPadding),
                typeof(int),
                typeof(PageIndicator),
                new PropertyMetadata(8, UpdateIndicatorAndLayout)
            );

        /// <summary>
        /// Padding per indicator within the control, not on the control itself.
        /// </summary>
        public int IndicatorPadding
        {
            get { return (int)GetValue(IndicatorPaddingProperty); }
            set { SetValue(IndicatorPaddingProperty, value); }
        }


        public static readonly DependencyProperty IndicatorBackgroundProperty =
            DependencyProperty.Register(
                nameof(IndicatorBackground),
                typeof(Brush),
                typeof(PageIndicator),
                new PropertyMetadata(new SolidColorBrush(Colors.DarkGray), UpdateIndicator)
            );

        public Brush IndicatorBackground
        {
            get { return (Brush)GetValue(IndicatorBackgroundProperty); }
            set { SetValue(IndicatorBackgroundProperty, value); }
        }


        public static readonly DependencyProperty IndicatorBorderBackgroundProperty =
            DependencyProperty.Register(
                nameof(IndicatorBorderBackground),
                typeof(Brush),
                typeof(PageIndicator),
                new PropertyMetadata(new SolidColorBrush(Colors.DarkGray), UpdateIndicator)
            );

        public Brush IndicatorBorderBackground
        {
            get { return (Brush)GetValue(IndicatorBorderBackgroundProperty); }
            set { SetValue(IndicatorBorderBackgroundProperty, value); }
        }


        public static readonly DependencyProperty IndicatorSelectedBackgroundProperty =
            DependencyProperty.Register(
                nameof(IndicatorSelectedBackground),
                typeof(Brush),
                typeof(PageIndicator),
                new PropertyMetadata(new SolidColorBrush(Colors.Gray), UpdateIndicator)
            );

        public Brush IndicatorSelectedBackground
        {
            get { return (Brush)GetValue(IndicatorSelectedBackgroundProperty); }
            set { SetValue(IndicatorSelectedBackgroundProperty, value); }
        }



        public static readonly DependencyProperty AnimationDurationMsProperty =
            DependencyProperty.Register(
                nameof(AnimationDurationMs),
                typeof(int),
                typeof(PageIndicator),
                new PropertyMetadata(300)
            );

        /// <summary>
        /// A value of less than 1 will disable animation when the selected index updates.
        /// </summary>
        public int AnimationDurationMs
        {
            get { return (int)GetValue(AnimationDurationMsProperty); }
            set { SetValue(AnimationDurationMsProperty, value); }
        }


        protected static void UpdateIndicatorIndex(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is PageIndicator pi))
                return;

            pi._lastSelectedIndex = (int)e.OldValue;

            if (pi.AnimationDurationMs > 0)
            {
                var duration = new Duration(TimeSpan.FromMilliseconds(pi.AnimationDurationMs));
                var reduceDa = new DoubleAnimation(pi.IndicatorRadius, 0, duration);
                pi._reduceAnimationClock = reduceDa.CreateClock();
                var increaseDa = new DoubleAnimation(0, pi.IndicatorRadius, duration);
                pi._increaseAnimationClock = increaseDa.CreateClock();
            }

            pi.InvalidateMeasure();
            pi.InvalidateVisual();
        }

        protected static void UpdateIndicatorAndLayout(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pi = d as PageIndicator;
            pi?.InvalidateMeasure();
            pi?.InvalidateVisual();
        }

        protected static void UpdateIndicator(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pi = d as PageIndicator;
            pi?.InvalidateVisual();
        }

        protected double IndicatorRadius => IndicatorSize / 2.0;
        protected int SpacePerIndicator => IndicatorSize + IndicatorBorderSize + IndicatorPadding;
        protected override Geometry DefiningGeometry => new EllipseGeometry(new Rect(new Size(SpacePerIndicator * PageCount, SpacePerIndicator)));

        readonly Brush _transparentBrush;

        private int _lastSelectedIndex = -1;
        private AnimationClock _reduceAnimationClock;
        private AnimationClock _increaseAnimationClock;

        public PageIndicator()
        {
            MouseLeftButtonUp += PageIndicator_MouseLeftButtonUp;
            _transparentBrush = new SolidColorBrush(Colors.Transparent);
        }

        void PageIndicator_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Command == null)
                return;            

            var point = e.GetPosition(this);
            var index = (int)point.X / SpacePerIndicator; // intentional integer division

            if (index < 0 || index >= PageCount)
                return; // should never happen, better for no click rather than crash

            if (Command?.CanExecute(index) == true)
                Command?.Execute(index);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            var borderPen = IndicatorBorderSize > 0
                    ? new Pen(IndicatorBorderBackground, IndicatorBorderSize)
                    : null;

            var transparentBorderPen = new Pen(_transparentBrush, IndicatorBorderSize);

            for (int i = 0; i < PageCount; i++)
            {
                var centrePoint = new Point(
                    (i * SpacePerIndicator) + SpacePerIndicator / 2.0,
                    SpacePerIndicator / 2.0
                );

                // make it easier to click indicators with transparent background
                var paddingRadius = IndicatorRadius + IndicatorPadding;
                drawingContext.DrawEllipse(_transparentBrush, null, centrePoint, paddingRadius, paddingRadius);

                // always draw border + unselected fill so we can only animate the inner selected background
                drawingContext.DrawEllipse(
                    IndicatorBackground,
                    borderPen,
                    centrePoint,
                    IndicatorRadius,
                    IndicatorRadius
                );

                if (!(i == SelectedPageIndex || i == _lastSelectedIndex))
                    continue;

                if (i == _lastSelectedIndex && AnimationDurationMs < 1)
                    continue; // only draw selected background for previous when animating removal

                var radiusAnimationClock = i == SelectedPageIndex
                    ? _increaseAnimationClock
                    : _reduceAnimationClock;

                // selected indicator fill
                drawingContext.DrawEllipse(
                    IndicatorSelectedBackground,
                    transparentBorderPen, // don't draw over border but ensure same size
                    centrePoint,
                    null,
                    IndicatorRadius,
                    radiusAnimationClock,
                    IndicatorRadius,
                    radiusAnimationClock
                );                
            }
        }
    }
}