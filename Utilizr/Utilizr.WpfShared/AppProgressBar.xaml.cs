﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Utilzr.WPF.Util;

namespace Utilzr.WPF
{
    public partial class AppProgressBar : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                nameof(Value),
                typeof(double),
                typeof(AppProgressBar),
                new PropertyMetadata(0.0, UpdateProgress)
            );

        private static void UpdateProgress(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as AppProgressBar)?.UpdatePixelWidth();
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }


        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register(
                nameof(Minimum),
                typeof(double),
                typeof(AppProgressBar),
                new PropertyMetadata(0.0, UpdateProgress)
            );

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }


        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register(
                nameof(Maximum),
                typeof(double),
                typeof(AppProgressBar),
                new PropertyMetadata(100.0, UpdateProgress)
            );

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }


        public static readonly DependencyProperty IsAnimatingProperty =
            DependencyProperty.Register(
                nameof(IsAnimating),
                typeof(bool),
                typeof(AppProgressBar),
                new PropertyMetadata(false)
            );

        public bool IsAnimating
        {
            get { return (bool)GetValue(IsAnimatingProperty); }
            set { SetValue(IsAnimatingProperty, value); }
        }



        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(BackgroundBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BackgroundBrush))
                )
            );

        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }


        public new static readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.Register(
                nameof(BorderBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BorderBrush))
                )
            );

        public new Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }



        public static readonly DependencyProperty BarBorderBrushProperty =
            DependencyProperty.Register(
                nameof(BarBorderBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BarBorderBrush))
                )
            );

        public Brush BarBorderBrush
        {
            get { return (Brush)GetValue(BarBorderBrushProperty); }
            set { SetValue(BarBorderBrushProperty, value); }
        }


        public static readonly DependencyProperty BarBackgroundBrushProperty =
            DependencyProperty.Register(
                nameof(BarBackgroundBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BarBackgroundBrush))
                )
            );

        public Brush BarBackgroundBrush
        {
            get { return (Brush)GetValue(BarBackgroundBrushProperty); }
            set { SetValue(BarBackgroundBrushProperty, value); }
        }


        public static readonly DependencyProperty BarChevronBrushProperty =
            DependencyProperty.Register(
                nameof(BarChevronBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BarChevronBrush))
                )
            );

        public Brush BarChevronBrush
        {
            get { return (Brush)GetValue(BarChevronBrushProperty); }
            set { SetValue(BarChevronBrushProperty, value); }
        }


        public static readonly DependencyProperty BarShineBrushProperty =
            DependencyProperty.Register(
                nameof(BarShineBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(BarShineBrush))
                )
            );

        public Brush BarShineBrush
        {
            get { return (Brush)GetValue(BarShineBrushProperty); }
            set { SetValue(BarShineBrushProperty, value); }
        }


        public static readonly DependencyProperty TextBrushProperty =
            DependencyProperty.Register(
                nameof(TextBrush),
                typeof(Brush),
                typeof(AppProgressBar),
                new PropertyMetadata(
                    ResourceHelper.GetDictionaryDefined<Brush>(nameof(AppProgressBar), nameof(TextBrush))
                )
            );

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }


        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(AppProgressBar),
                new PropertyMetadata(default(string))
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty TextSizeProperty =
            DependencyProperty.Register(
                nameof(TextSize),
                typeof(double),
                typeof(AppProgressBar),
                new PropertyMetadata(12.0)
            );

        public double TextSize
        {
            get { return (double)GetValue(TextSizeProperty); }
            set { SetValue(TextSizeProperty, value); }
        }


        public static readonly DependencyProperty TextWeightProperty =
            DependencyProperty.Register(
                nameof(TextWeight),
                typeof(FontWeight),
                typeof(AppProgressBar),
                new PropertyMetadata(FontWeights.Medium)
            );

        public FontWeight TextWeight
        {
            get { return (FontWeight)GetValue(TextWeightProperty); }
            set { SetValue(TextWeightProperty, value); }
        }


        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register(
                nameof(TextMargin),
                typeof(Thickness),
                typeof(AppProgressBar),
                new PropertyMetadata(default(Thickness))
            );

        public Thickness TextMargin
        {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }




        private double _progressPixelWidth;
        public double ProgressPixelWidth
        {
            get { return _progressPixelWidth; }
            set
            {
                _progressPixelWidth = value;
                OnNotifyPropertyChanged(nameof(ProgressPixelWidth));
            }
        }

        private void UpdatePixelWidth()
        {
            if (double.IsInfinity(Value) || double.IsNaN(Value))
            {
                ProgressPixelWidth = 0;
                return;
            }

            double percent = (Value - Minimum) / (Maximum - Minimum);

            if (percent >= 1)
                ProgressPixelWidth = ActualWidth;

            try
            {
                double w = ActualWidth * percent;
                if (w < 0)
                {
                    w = 0;
                }
                ProgressPixelWidth = w;
            }
            catch
            { }
        }

        public AppProgressBar()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;

            Loaded += (s, e) => UpdatePixelWidth();
            SizeChanged += (s, e) => UpdatePixelWidth();
        }

        protected virtual void OnNotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
