﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace Utilzr.WPF
{
    public class BindableRun : Run
    {
        public static readonly DependencyProperty TextProperty = 
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(BindableRun),
                new PropertyMetadata(
                    default(string),
                    (s, e) => (s as BindableRun)?.SetBase()
                )
            );

        public new string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                nameof(Command),
                typeof(ICommand),
                typeof(BindableRun),
                new PropertyMetadata(default(ICommand))
            );

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register(
                nameof(CommandParameter),
                typeof(object),
                typeof(BindableRun),
                new PropertyMetadata(default(object))
            );

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public BindableRun()
        {
            MouseLeftButtonUp += (s, e) =>
            {
                if (Command?.CanExecute(CommandParameter) == true)
                {
                    // Null check, as unlikely, but possibly changed can execute check
                    Command?.Execute(CommandParameter);
                }
            };
        }

        void SetBase()
        {
            base.Text = Text;
        }
    }
}
