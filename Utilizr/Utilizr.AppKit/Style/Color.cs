﻿
using CoreGraphics;
using AppKit;
using CoreImage;
using System;

namespace Style
{
	public class Color
	{

		public float R, G, B, A;

		public static Color Red = new Color(1, 0, 0);
		public static Color White = new Color(1, 1, 1);
		public static Color Black = new Color(0, 0, 0);

		public Color(float r, float g, float b, float a = 1)
		{
			R = r;
			G = g;
			B = b;
			A = a;
		}

		public static Color FromRgba(int r, int g, int b, int a)
		{
			return new Color((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, (float)a / 255.0f);
		}

		public static Color ParseHex(string hexString)
		{
			if (hexString.StartsWith("#"))
			{
				hexString = hexString.Substring(1);
			}

			if (hexString.Length == 6)
			{
				return Color.FromRgba(
					int.Parse(hexString.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
					int.Parse(hexString.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
					int.Parse(hexString.Substring(4, 2), System.Globalization.NumberStyles.HexNumber),
					255);
			}
			else if (hexString.Length == 8)
			{
				return Color.FromRgba(
					int.Parse(hexString.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
					int.Parse(hexString.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
					int.Parse(hexString.Substring(4, 2), System.Globalization.NumberStyles.HexNumber),
					int.Parse(hexString.Substring(6, 2), System.Globalization.NumberStyles.HexNumber));
			}
			return new Color(0, 0, 0);

		}

		public CGColor ToCGColor()
		{
			return new CGColor(
				R,
				G,
				B,
				A
			);
		}

		public static Color FromNSColor(NSColor color)
		{
			CIColor c = new CIColor(color);
			return new Color((float)c.Red, (float)c.Green, (float)c.Blue, (float)c.Alpha);
		}

		public static implicit operator NSColor(Color c)
		{
			return c.ToNSColor();
		}

		public static implicit operator Color(NSColor c)
		{
			return Color.FromNSColor(c);
		}

		public NSColor ToNSColor()
		{
			return NSColor.FromDeviceRgba(R, G, B, A);
		}
	}
}

