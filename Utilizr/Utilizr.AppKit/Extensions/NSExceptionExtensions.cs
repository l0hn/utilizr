﻿using System;
using Foundation;

namespace Utilizr.MacKit
{
    public static class NSExceptionExtensions
    {
        public static Exception ToException(this NSException nsEx)
        {
            if (nsEx == null)
                return null;

            var stackTrace = string.Join(Environment.NewLine, nsEx.CallStackSymbols);
            var returnAddress = string.Join<NSNumber>(Environment.NewLine, nsEx.CallStackReturnAddresses);
            var name = nsEx.Name;
            var message = nsEx.Reason;


            string exContants = $"{name}: {message}{Environment.NewLine}{stackTrace}";
            return new Exception(exContants);
        }
    }
}