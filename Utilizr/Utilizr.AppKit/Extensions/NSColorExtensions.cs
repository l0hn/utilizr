﻿using System;
using AppKit;
using Foundation;
using Utilizr.Logging;

namespace Util
{
	public static class NSColorExtensions
	{

		public static string ToHex(this NSColor c)
		{
			var hex = string.Format("#{0:X2}{1:X2}{2:X2}", (int)(c.RedComponent * 0xFF), (int)(c.GreenComponent * 0xFF),
			                        (int)(c.BlueComponent * 0xFF));
			return hex;
		}

		public static NSColor FromHex (this NSColor color, string hexValue, float alpha = 1.0f)
		{

			if (hexValue == "White") {
				return NSColor.White;
			} else if (hexValue == "Black") {
				return NSColor.Black;
			} else if (hexValue == "Blue") {
				return NSColor.Blue;
			} else if (hexValue == "Green") {
				return NSColor.Green;
			} else if (hexValue == "Red") {
				return NSColor.Red;
			}

			var colorString = hexValue.Replace ("#", "");
			if (alpha > 1.0f) {
				alpha = 1.0f;
			} else if (alpha < 0.0f) {
				alpha = 0.0f;
			}

			float red, green, blue;

			switch (colorString.Length) {
			case 3: // #RGB
				{
					red = Convert.ToInt32 (string.Format ("{0}{0}", colorString.Substring (0, 1)), 16) / 255f;
					green = Convert.ToInt32 (string.Format ("{0}{0}", colorString.Substring (1, 1)), 16) / 255f;
					blue = Convert.ToInt32 (string.Format ("{0}{0}", colorString.Substring (2, 1)), 16) / 255f;
					return NSColor.FromDeviceRgba (red, green, blue, alpha);
				}
			case 6: // #RRGGBB
				{
					red = Convert.ToInt32 (colorString.Substring (0, 2), 16) / 255f;
					green = Convert.ToInt32 (colorString.Substring (2, 2), 16) / 255f;
					blue = Convert.ToInt32 (colorString.Substring (4, 2), 16) / 255f;
					return NSColor.FromDeviceRgba (red, green, blue, alpha);
				} 
			case 8: // #AARRGGBB
				{
					alpha = Convert.ToInt32 (colorString.Substring (0, 2), 16) / 255f;
					red = Convert.ToInt32 (colorString.Substring (2, 2), 16) / 255f;
					green = Convert.ToInt32 (colorString.Substring (4, 2), 16) / 255f;
					blue = Convert.ToInt32 (colorString.Substring (6, 2), 16) / 255f;
					return NSColor.FromDeviceRgba (red, green, blue, alpha);
				}   

			default :
				return NSColor.Black;	
			}

		}
	}
}

