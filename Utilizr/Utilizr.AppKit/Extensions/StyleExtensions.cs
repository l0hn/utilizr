﻿using System;
using AppKit;
using CoreGraphics;
using Style;

namespace Extensions
{
	public static class StyleExtensions
	{
		public static NSColor ToNSColor(this Color color) {
			return NSColor.FromDeviceRgba (color.R, color.G, color.B, color.A);
		}

		public static CGColor ToCGColor(this Color color) {
			return NSColor.FromDeviceRgba (color.R, color.G, color.B, color.A).CGColor;
		}

	}
}

