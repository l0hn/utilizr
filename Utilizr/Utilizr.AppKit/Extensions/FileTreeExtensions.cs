﻿using System;
using AppKit;
using Utilizr.FileSystem.IncludeExclude;
using Foundation;

namespace Extensions
{
	public static class FileTreeExtensions
	{
		public static NSCellStateValue ToCellState(this CheckState state)
		{
			switch (state)
			{
				case CheckState.CHECKED:
					return NSCellStateValue.On;
				case CheckState.INTERMEDIATE:
					return NSCellStateValue.Mixed;
				default:
					return NSCellStateValue.Off;
			}
		}


		public static CheckState ToCheckState(this NSCellStateValue state)
		{
			switch (state)
			{
				case NSCellStateValue.On:
					return CheckState.CHECKED;
				case NSCellStateValue.Mixed:
					return CheckState.INTERMEDIATE;
				default:
					return CheckState.UNCHECKED;
			}
		}

		public static string GetVolumeName(string path)
		{
			var url = NSUrl.CreateFileUrl(new[] { path });
			NSError e = null;
			NSObject name = null;
			url.TryGetResource(NSUrl.VolumeNameKey, out name, out e);

			return name?.ToString();
		}

	}
}
