﻿using System;
using CoreFoundation;

namespace Extensions
{
	public static class GCDExtensions
	{

		public static void DispatchAfter(this DispatchQueue queue, TimeSpan delay, Action action)
		{
			queue.DispatchAfter(new DispatchTime(DispatchTime.Now, delay), action);
		}

	}
}
