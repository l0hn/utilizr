﻿using System;
using Foundation;

namespace Extensions
{
    public static class NSDateExtensions
    {
        public static DateTime ToDateTime(this NSDate date, bool utc = true)
        {
            DateTime reference = new DateTime(2001, 1, 1, 0, 0, 0);
            DateTime currentDate = reference.AddSeconds(date.SecondsSinceReferenceDate);

            return utc
                ? currentDate.ToUniversalTime()
                : currentDate.ToLocalTime();
        }
    }
}