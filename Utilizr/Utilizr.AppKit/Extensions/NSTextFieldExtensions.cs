﻿using System;
using AppKit;
using Foundation;
using GetText;
using Util;

namespace Extensions
{
	public static class NSTextFieldExtensions
	{
		public static NSTextField MakeLabel()
		{
			return new NSTextField { Bordered = false, Selectable = false, Editable = false };
		}

		public static void SetTextSpacing(this NSTextField l, float spacing)
		{
			var attr = new NSMutableAttributedString(l.StringValue);
			attr.AddAttribute(NSStringAttributeKey.KerningAdjustment, NSNumber.FromFloat(spacing), new NSRange(0, attr.Length));
			l.AttributedStringValue = attr;
		}


		
	}

}
