﻿using System;
using AppKit;
using Foundation;
using NaturalLanguage;

namespace Util
{
	public static class VersionHelper
	{
		private static bool? _mountainLion;
		private static bool? _mavericks;
		private static bool? _yosemite;
        private static bool? _elcap;
        private static bool? _sierra;
        private static bool? _highsierra;
        private static bool? _mojave;
        private static bool? _catalina;


        /// <summary>
        /// Is running 10.8 or later
        /// </summary>
        /// <returns><c>true</c>, if mountain lion or higher was ised, <c>false</c> otherwise.</returns>
        public static bool IsMountainLionOrHigher()
		{
			if (_mountainLion.HasValue)
			{
				return _mountainLion.Value;
			}
			try
			{
				var x = new NSPageController();
				if (x == null)
				{
					_mountainLion = false;
					return IsMountainLionOrHigher();
				}
			}
			catch (Exception)
			{
				_mountainLion = false;
				return IsMountainLionOrHigher();
			}

			_mountainLion = true;
			return IsMountainLionOrHigher();
		}

		/// <summary>
		/// Is running 10.9 or later
		/// </summary>
		/// <returns><c>true</c>, if mavericks or higher was ised, <c>false</c> otherwise.</returns>
		public static bool IsMavericksOrHigher()
		{
			if (_mavericks.HasValue)
			{
				return _mavericks.Value;
			}
			try
			{
				var x = new NSStackView();
				if (x == null)
				{
					_mavericks = false;
					return IsMavericksOrHigher();
				}
			}
			catch (Exception)
			{
				_mavericks = false;
				return IsMavericksOrHigher();
			}

			_mavericks = true;
			return IsMavericksOrHigher();
		}

		/// <summary>
		/// Is running 10.10 or later
		/// </summary>
		/// <returns><c>true</c>, if yosemite or higher was ised, <c>false</c> otherwise.</returns>
		public static bool IsYosemiteOrHigher()
		{
			if (_yosemite.HasValue) {
				return _yosemite.Value;
			}
			try
			{
				var x = new NSVisualEffectView();
				if (x == null) 
				{
					_yosemite = false;
					return IsYosemiteOrHigher();
				}
			}
			catch(Exception)
			{
				_yosemite = false;
				return IsYosemiteOrHigher();
			}

			_yosemite = true;
			return IsYosemiteOrHigher();
		}

		/// <summary>
		/// Is running 10.11 or later
		/// </summary>
		/// <returns><c>true</c>, if el capitan or higher was ised, <c>false</c> otherwise.</returns>
		public static bool IsElCapitanOrHigher()
		{
			if (_elcap.HasValue) {
				return _elcap.Value;
			}
			try
			{
				var x = new NSAlignmentFeedbackFilter();
				if (x == null) 
				{
					_elcap = false;
					return IsElCapitanOrHigher();
				}
			}
			catch(Exception)
			{
				_elcap = false;
				return IsElCapitanOrHigher();
			}

			_elcap = true;
			return IsElCapitanOrHigher();
		}

        /// <summary>
        /// Is running 10.12 or later
        /// </summary>
        /// <returns><c>true</c>, if sierra or higher was ised, <c>false</c> otherwise.</returns>
        public static bool IsSierraOrHigher()
        {
            if (_sierra.HasValue)
            {
                return _sierra.Value;
            }
            try
            {
                var x = new NSUnitConverter();
                if (x == null)
                {
                    _sierra = false;
                    return IsSierraOrHigher();
                }
            }
            catch (Exception)
            {
                _sierra = false;
                return IsSierraOrHigher();
            }

            _sierra = true;
            return IsSierraOrHigher();
        }

        /// <summary>
        /// Is running 10.13 or later
        /// </summary>
        /// <returns><c>true</c>, if high sierra or higher was ised, <c>false</c> otherwise.</returns>
        public static bool IsHighSierraOrHigher()
        {
            if (_highsierra.HasValue)
            {
                return _highsierra.Value;
            }
            try
            {
                var x = new NSUserInterfaceCompressionOptions();
                if (x == null)
                {
                    _highsierra = false;
                    return IsHighSierraOrHigher();
                }
            }
            catch (Exception)
            {
                _highsierra = false;
                return IsHighSierraOrHigher();
            }

            _highsierra = true;
            return IsHighSierraOrHigher();
        }

        /// <summary>
        /// Is running 10.14 or later
        /// </summary>
        /// <returns><c>true</c>, if high sierra or higher was ised, <c>false</c> otherwise.</returns>
        public static bool IsMojaveOrHigher()
        {
            if (_mojave.HasValue)
            {
                return _mojave.Value;
            }
            try
            {
                var x = new NLLanguageRecognizer();
                if (x == null)
                {
                    _mojave = false;
                    return IsMojaveOrHigher();
                }
            }
            catch (Exception)
            {
                _mojave = false;
                return IsMojaveOrHigher();
            }

            _mojave = true;
            return IsMojaveOrHigher();
        }

        /// <summary>
        /// Is running 10.15 or later
        /// </summary>
        /// <returns><c>true</c>, if high sierra or higher was ised, <c>false</c> otherwise.</returns>
        public static bool IsCatalinaOrHigher()
        {
            if (_catalina.HasValue)
            {
                return _catalina.Value;
            }
            if (NSProcessInfo.ProcessInfo.OperatingSystemVersion.Major == 10
                     && NSProcessInfo.ProcessInfo.OperatingSystemVersion.Minor == 15)
            {
                _catalina = true;
                return IsCatalinaOrHigher();
            }

            _catalina = false;
            return IsCatalinaOrHigher();
        }
    }
}

