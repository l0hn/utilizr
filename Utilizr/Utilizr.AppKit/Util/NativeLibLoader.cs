﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Utilizr.MacKit
{
    public static class NativeLibLoader
    {
        /// <summary>
        /// Loads the libs.
        /// </summary>
        /// <param name="libPaths">Lib files to load. e.g. "/lib/libSomeStuff.dylib"</param>
        public static void LoadLibs(params string[] libPaths)
        {
            var paths = new List<string>();
            var curDir = GetCurrentExecutingDirectory();
            paths.AddRange(libPaths);
            foreach (var path in paths)
            {
                var v = ObjCRuntime.Dlfcn.dlopen(path, 0);
                if (v == IntPtr.Zero)
                {
                    throw new DllNotFoundException($"Whoaa, could not load dylib \"{path}\"");
                }
            }
        }

        public static string GetCurrentExecutingDirectory()
        {
            string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            return Path.GetDirectoryName(filePath);
        }

        //public static string GetResourceDir()
        //{
        //    return "../Resources";
        //}
    }
}