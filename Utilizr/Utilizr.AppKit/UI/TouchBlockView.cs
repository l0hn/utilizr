﻿using System;
using Foundation;
using AppKit;
using CoreGraphics;

namespace UI
{
    [Register("TouchBlockView")]
    public partial class TouchBlockView : AppKit.NSView
    {
        #region Constructors

        // Called when created from unmanaged code
        public TouchBlockView(IntPtr handle) : base(handle)
        {
            Initialize();
        }


        public TouchBlockView(CGRect frameRect) 
            : base(frameRect)
        {
            Initialize();
        }

        [Export("initWithCoder:")]
        public TouchBlockView(NSCoder coder) : base(coder)
        {
            Initialize();
        }

        // Shared initialization code
        void Initialize()
        {
        }

        public override void MouseDown(NSEvent theEvent)
        {

        }

        public override void MouseUp(NSEvent theEvent)
        {

        }

        public override void MouseEntered(NSEvent theEvent)
        {
            
        }

        public override void MouseExited(NSEvent theEvent)
        {
            
        }

        public override bool MouseDownCanMoveWindow { get { return false; } }

        #endregion
    }
}
