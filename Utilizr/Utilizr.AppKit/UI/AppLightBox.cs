﻿using System;
using AppKit;
using Foundation;
using CoreGraphics;
using System.Linq;
using Extensions;
using Utilizr.Logging;

namespace UI
{
	[Register(nameof(AppLightBox))]
	public class AppLightBox : NSView
	{
		private static NSViewResizingMask FillMask
		{
			get
			{
				return NSViewResizingMask.MinXMargin
					| NSViewResizingMask.MaxXMargin
					| NSViewResizingMask.MaxYMargin
					| NSViewResizingMask.MinYMargin;
			}
		}

		private NSColor _overlayColour = NSColor.FromDeviceWhite(0.0f, 0.5f);
		public NSColor OverlayColour
		{
			get { return _overlayColour; }
			set
			{
				_overlayColour = value;
				NeedsDisplay = true;
			}
		}

		private NSColor _lightBoxColour = NSColor.White;
		public NSColor LightBoxColour
		{
			get { return _lightBoxColour; }
			set
			{
				_lightBoxColour = value;
				NeedsDisplay = true;
			}
		}

		private bool _autoLayout = false;
		public bool AutoLayout
		{
			get { return _autoLayout; }
			set
			{
				_autoLayout = value;
				//Add contraints for autolayout
				if (_autoLayout)
				{
					//Overlay to cover full width and height
					_overlay.TranslatesAutoresizingMaskIntoConstraints = false;
					AddConstraint(NSLayoutConstraint.Create(_overlay, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0));
					AddConstraint(NSLayoutConstraint.Create(_overlay, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0));
					AddConstraint(NSLayoutConstraint.Create(_overlay, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0));
					AddConstraint(NSLayoutConstraint.Create(_overlay, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0));

					////Center Content over overlay, will need height and width contraint set on _contentContainer later on
					//_contentContainer.TranslatesAutoresizingMaskIntoConstraints = false;
					//AddConstraint(NSLayoutConstraint.Create(_contentContainer, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlay, NSLayoutAttribute.CenterX, 1, 0));
					//AddConstraint(NSLayoutConstraint.Create(_contentContainer, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0));

					//               _contentContainerBackgroud.TranslatesAutoresizingMaskIntoConstraints = false;
					//               AddConstraint(NSLayoutConstraint.Create(_contentContainerBackgroud, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlay, NSLayoutAttribute.CenterX, 1, 0));
					//               AddConstraint(NSLayoutConstraint.Create(_contentContainerBackgroud, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0));
				}
			}
		}

		private TouchBlockView _overlay;
		/// <summary>
		/// The lightbox background with rounded corners. Having the background in its own NSView
		/// with the colour and rounded corners set solved the weird non-rounded corners issue.
		/// </summary>
		private NSView _contentContainerBackgroud;
		/// <summary>
		/// The content shown within the lightbox. Having the content in its own NSView without
		/// a background and rounded corners set solved the weird non-rounded corners issue.
		/// </summary>
		private NSView _contentContainer;
		private NSView _currentContent;
		private NSView _parentView;
		private const int _cornerRadius = 6;

		#region Ctors
		public AppLightBox()
			: base()
		{
			Initialize();
		}

		public AppLightBox(CGRect frameRect)
			: base(frameRect)
		{
			Initialize();
		}

		public AppLightBox(NSCoder coder)
			: base(coder)
		{
			Initialize();
		}

		public AppLightBox(NSObjectFlag t)
			: base(t)
		{
			Initialize();
		}

		public AppLightBox(IntPtr handle)
			: base(handle)
		{
			Initialize();
		}
		#endregion

		private void Initialize()
		{

		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			Hidden = true;

			_overlay = new TouchBlockView(Bounds);
			_overlay.WantsLayer = true;
			_overlay.AutoresizingMask = FillMask;
			_overlay.Layer.BackgroundColor = OverlayColour.CGColor;
			AddSubview(_overlay, NSWindowOrderingMode.Above, null);

			_contentContainerBackgroud = new NSView(Bounds);
			_contentContainerBackgroud.AutoresizingMask = NSViewResizingMask.NotSizable;
			_contentContainerBackgroud.WantsLayer = true;
			_contentContainerBackgroud.Layer.MasksToBounds = true;
			_contentContainerBackgroud.Layer.BackgroundColor = LightBoxColour.CGColor;
			_contentContainerBackgroud.Layer.CornerRadius = _cornerRadius;
			AddSubview(_contentContainerBackgroud, NSWindowOrderingMode.Above, _overlay);

			_contentContainer = new NSView(Bounds);
			_contentContainer.AutoresizingMask = NSViewResizingMask.NotSizable;
			_contentContainer.WantsLayer = true;
			_contentContainer.Layer.MasksToBounds = true;
			_contentContainerBackgroud.AddSubview(_contentContainer);
			//AddSubview(_contentContainer, NSWindowOrderingMode.Above, _contentContainerBackgroud);

			_parentView = this.Superview;
		}

		public void SetContent(NSView content, float contentPadding = 10, bool contentUsesAutoLayout = false, int verticalPadding = 0)
		{
			//update overlay frame size to pickup when window size has changed
			if (!_autoLayout)
				_overlay.Frame = this.Frame;

			RemoveCurrentContent();

			_currentContent = content;

			if (contentUsesAutoLayout)
			{
				AddSubview(content);
			}
			else
			{
				_contentContainer.AddSubview(content);
			}


			if (_autoLayout)
			{
				_contentContainerBackgroud.RemoveConstraints(_contentContainerBackgroud.Constraints);

				if (contentUsesAutoLayout)
				{
					_contentContainerBackgroud.AlignEdges(content, contentPadding);

					content.BuildConstraint()
						.H().Width((float)content.Bounds.Width).Center()
						.V().Center().Build();

                    if (verticalPadding != 0)
                    {
                        //only need top as the vertical center constraint will balance out the bottom spacing
                        AddConstraint(NSLayoutConstraint.Create(content, NSLayoutAttribute.Top, NSLayoutRelation.GreaterThanOrEqual, this, NSLayoutAttribute.Top, 1, verticalPadding));
                    }

				}
				else
				{
					_contentContainerBackgroud.BuildConstraint()
					 .H().Width((float)content.Bounds.Width + contentPadding).Center()
					 .V().Height((float)content.Bounds.Height + contentPadding).Center().Build();
					content.BuildConstraint()
					   .H().Width((float)content.Bounds.Width).Center()
					   .V().Height((float)content.Bounds.Height).Center().Build();

				}

				_contentContainer.BuildConstraint().FillParentAndBuild();


			}
			else
			{
				var offsetX = (_overlay.Frame.Width - content.Bounds.Width) / 2;
				var offsetY = (_overlay.Frame.Height - content.Bounds.Height) / 2;
				var contentWidth = Math.Round(content.Bounds.Width);
				var contentHeight = Math.Round(content.Bounds.Height);
				_contentContainerBackgroud.Frame = new CGRect(Math.Round(offsetX), Math.Round(offsetY), contentWidth, contentHeight);
				_contentContainer.Frame = _contentContainerBackgroud.Frame;
				//_contentContainer.Frame = new CGRect(Math.Round(offsetX), Math.Round(offsetY), contentWidth, contentHeight);
				content.Frame = new CGRect(0, 0, contentWidth, contentHeight);
			}
			BringToFront();

			Hidden = false;
			NeedsDisplay = true;
		}

		public void CloseCurrentContent()
		{
			Hidden = true;
			RemoveCurrentContent();
			NeedsDisplay = true;
		}

		private void RemoveCurrentContent()
		{
			_currentContent?.RemoveFromSuperviewWithoutNeedingDisplay();
			foreach (var view in _contentContainer.Subviews)
			{
				view.RemoveFromSuperviewWithoutNeedingDisplay();
			}
		}

		private void BringToFront()
		{
			//Save contraints they get removed when RemoveFromSuperview is called and need to added back
			var contraints = _parentView.Constraints.Where(x => x.FirstItem == this
																 || x.SecondItem == this);

			//Bring to front
			RemoveFromSuperview();
			_parentView.AddSubview(this);

			//Restore contraints
			_parentView.AddConstraints(contraints.ToArray());
		}
	}
}

