﻿using System;
using AppKit;
using CoreGraphics;
using Foundation;

namespace Utilizr.MacKit
{
    public class TooltipView : NSView
    {
        NSTextField label;
        const float PADDING = 5;
        const float WINDOW_PADDING = 10;
        const float TRIANGLE_HEIGHT = 12;
        float triangleXOffset = 0;

        #region Constructors
        public TooltipView(string title) : base(CGRect.Empty)
        {
            WantsLayer = true;

            label = new NSTextField(CGRect.Empty);
            label.StringValue = title;
            label.Editable = false;
            label.DrawsBackground = false;
            label.TextColor = NSColor.White;
            label.Font = NSFont.SystemFontOfSize(14);
            label.Cell.UsesSingleLineMode = true;
            label.Alignment = NSTextAlignment.Center;
            label.Bordered = false;
            label.SizeToFit();

            RecalcSize();

            triangleXOffset = (float)Frame.Width / 2;

            try
            {
                var s = Animator as NSView;
            }
            catch (Exception)
            {

            }

            Initialize();
        }

        // Called when created from unmanaged code
        public TooltipView(IntPtr handle) : base(handle)
        {
            Initialize();
        }

        // Called when created directly from a XIB file
        [Export("initWithCoder:")]
        public TooltipView(NSCoder coder) : base(coder)
        {
            Initialize();
        }

        // Shared initialization code
        void Initialize()
        {
            if (label != null)
            {
                AddSubview(label);
            }
        }

        #endregion

        private void RecalcSize()
        {
            Frame = new CGRect(CGPoint.Empty, new CGSize(label.Frame.Width + PADDING * 2, label.Frame.Height + PADDING * 2 + TRIANGLE_HEIGHT));
            label.Frame = new CGRect(new CGPoint(PADDING, TRIANGLE_HEIGHT + PADDING), label.Frame.Size);
        }

        public void ShowAtPoint(CGPoint p, NSView inView)
        {
            while (Frame.Width > inView.Frame.Width && label.Font.PointSize > 5)
            {
                label.Font = NSFont.SystemFontOfSize(label.Font.PointSize - 1);
                label.SizeToFit();
                RecalcSize();
            }

            inView.AddSubview(this);

            if (p.X + Frame.Width / 2 > inView.Frame.Width - WINDOW_PADDING)
            {
                Frame = new CGRect(inView.Frame.Width - WINDOW_PADDING - Frame.Width, p.Y, Frame.Width, Frame.Height);
                triangleXOffset = (float)p.X - (float)Frame.X;
            }
            else {
                Frame = new CGRect(p.X - Frame.Width / 2, p.Y, Frame.Width, Frame.Height);
            }

            AlphaValue = 0;

            try
            {
                NSAnimationContext.BeginGrouping();
                NSAnimationContext.CurrentContext.Duration = 0.3;

                var anim = this.Animator as NSView;
                anim.AlphaValue = 1;

                NSAnimationContext.EndGrouping();
            }
            catch
            {
                AlphaValue = 1;
            }
        }

        public void Hide()
        {
            NSAnimationContext.BeginGrouping();
            NSAnimationContext.CurrentContext.Duration = 0.3;

            var anim = this.Animator as NSView;
            anim.AlphaValue = 0;
            NSAnimationContext.CurrentContext.CompletionHandler += () =>
            {
                RemoveFromSuperview();
            };
            NSAnimationContext.EndGrouping();
        }

        public override void DrawRect(CoreGraphics.CGRect dirtyRect)
        {
            base.DrawRect(dirtyRect);
            // Color Declarations
            NSColor color = Styles.WarningColor.ToNSColor();

            // Rounded Rectangle Drawing

            var roundedRectanglePath = NSBezierPath.FromRoundedRect(new CGRect(0, TRIANGLE_HEIGHT, dirtyRect.Size.Width, dirtyRect.Size.Height - TRIANGLE_HEIGHT), 8, 8);
            color.SetFill();
            roundedRectanglePath.Fill();

            // Polygon Drawing
            NSBezierPath polygonPath = new NSBezierPath();
            polygonPath.MoveTo(new CGPoint(triangleXOffset + TRIANGLE_HEIGHT, TRIANGLE_HEIGHT + 1));
            polygonPath.LineTo(new CGPoint(triangleXOffset, 0));
            polygonPath.LineTo(new CGPoint(triangleXOffset - TRIANGLE_HEIGHT, TRIANGLE_HEIGHT + 1));
            polygonPath.ClosePath();
            color.SetFill();
            polygonPath.Fill();
        }
    }
}
