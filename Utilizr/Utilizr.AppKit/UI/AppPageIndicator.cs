﻿using System;
using AppKit;
using CoreGraphics;
using Foundation;
using Style;

namespace UI
{
    [Register("AppPageIndicator")]
    public class AppPageIndicator : NSView
    {
        public event EventHandler<AppPageIndicatorSelectionEventArgs> SelectionChanged;

        private nint _indicatorCount;
        public nint IndicatorCount
        {
            get { return _indicatorCount; }
            set
            {
                _indicatorCount = value;
                NeedsDisplay = true;
            }
        }

        private nint _selectedIndex = -1;
        public nint SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if(ValidIndex(value))
                {
                    var tempOldValue = _selectedIndex;
                    _selectedIndex = value;
                    NeedsDisplay = true;
                    OnSelectionChanged(tempOldValue, _selectedIndex);
                }
            }
        }

        private NSColor _indicatorColourSelected = Color.ParseHex("#369AF5").ToNSColor();
        public NSColor IndicatorColourSelected
        {
            get { return _indicatorColourSelected; }
            set
            {
                _indicatorColourSelected = value;
                NeedsDisplay = true;
            }
        }

        private NSColor _indicatorColourUnselected = Color.ParseHex("#CFCFCF").ToNSColor();
        public NSColor IndicatorColourUnselected
        {
            get { return _indicatorColourUnselected; }
            set
            {
                _indicatorColourUnselected = value;
                NeedsDisplay = true;
            }
        }

        public nint MinHeight { get { return _indicatorDiameter + (2 * _indicatorOffsetY); } }
        public nint MinWidth { get { return (IndicatorCount * _indicatorDiameter) + ((nint)Math.Max(0, IndicatorCount - 1) * _indicatorSpacing); } }

        private const int _indicatorDiameter = 16;
        private const int _indicatorSpacing = 8;
        private const int _indicatorOffsetY = 2;

        #region Ctors
        public AppPageIndicator()
            : base()
        {
            Initialize();
        }

        public AppPageIndicator(CGRect frameRect) 
            : base(frameRect)
        {
            Initialize();
        }

        [Export("initWithCoder:")]
        public AppPageIndicator(NSCoder coder)
            : base(coder)
        {
            Initialize();
        }

        public AppPageIndicator(NSObjectFlag t)
            : base(t)
        {
            Initialize();
        }

        public AppPageIndicator(IntPtr handle)
            : base(handle)
        {
            Initialize();
        }
        #endregion

        // Shared Initialize code from various ctors
        private void Initialize()
        {
            
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
        }

        public override void DrawRect(CGRect dirtyRect)
        {
            base.DrawRect(dirtyRect);
            var indicatorRects = GetIndicatorLocations(dirtyRect);

            for(int i = 0; i < indicatorRects.Length; i++)
            {
                if(i == SelectedIndex)
                    IndicatorColourSelected.Set();
                else
                    IndicatorColourUnselected.Set();

                var indicatorPath = new NSBezierPath();
                indicatorPath.AppendPathWithOvalInRect(indicatorRects[i]);
                indicatorPath.Stroke();
                indicatorPath.Fill();
            }
        }

        private CGRect[] GetIndicatorLocations()
        {
            return GetIndicatorLocations(Bounds);
        }

        private CGRect[] GetIndicatorLocations(CGRect bounds)
        {
            var results = new CGRect[IndicatorCount];

            nfloat indicatorsOffsetX = (bounds.Width - MinWidth) / 2.0f;
            nfloat indicatorOffsetY = (bounds.Height - _indicatorDiameter) / 2.0f;
            nfloat offsetXPerIndicator = _indicatorDiameter + _indicatorSpacing;

            for(int i = 0; i < IndicatorCount; i++)
            {
                results[i] = new CGRect(
                    indicatorsOffsetX + (i * offsetXPerIndicator),
                    indicatorOffsetY,
                    _indicatorDiameter,
                    _indicatorDiameter
                );
            }

            return results;
        }

        public override void MouseUp(NSEvent theEvent)
        {
            base.MouseUp(theEvent);

            CGPoint boundsMouseClick = ConvertPointFromView(theEvent.LocationInWindow, null);
            CGRect clickRect = new CGRect(boundsMouseClick, new CGSize(1f, 1f));

            var indicatorRects = GetIndicatorLocations();

            for(int i = 0; i < indicatorRects.Length; i++)
            {
                CGRect indicator = indicatorRects[i];

                if(indicator.IntersectsWith(clickRect))
                {
                    SelectedIndex = i;
                    break;
                }
            }
        }

        public void Next()
        {
            nint newIndex = SelectedIndex + 1;

            if(ValidIndex(newIndex))
            {
                SelectedIndex = newIndex;
                NeedsDisplay = true;
            }
        }

        public void Previous()
        {
            nint newIndex = SelectedIndex - 1;

            if(ValidIndex(newIndex))
            {
                SelectedIndex = newIndex;
                NeedsDisplay = true;
            }
        }

        private bool ValidIndex(nint newIndex)
        {
            return newIndex >= -1 && newIndex < IndicatorCount;
        }

        protected virtual void OnSelectionChanged(nint oldIndex, nint newIndex)
        {
            SelectionChanged?.Invoke(this, new AppPageIndicatorSelectionEventArgs(oldIndex, newIndex));
        }
    }

    public class AppPageIndicatorSelectionEventArgs : EventArgs
    {
        public nint OldSelectedIndex { get; private set; }
        public nint NewSelectedIndex { get; private set; }

        public AppPageIndicatorSelectionEventArgs(nint oldIndex, nint newIndex)
        {
            OldSelectedIndex = oldIndex;
            NewSelectedIndex = newIndex;
        }
    }
}

