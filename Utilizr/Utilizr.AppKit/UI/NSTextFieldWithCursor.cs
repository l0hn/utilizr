﻿using System;
using AppKit;
using Foundation;

namespace Utilizr.MacKit
{
    [Register(nameof(NSTextFieldWithCursor))]
    public class NSTextFieldWithCursor : NSTextField
    {
        public EventHandler Clicked;

        private NSCursor _cursor;
        public NSCursor Cursor
        {
            get { return _cursor; }
            set
            {
                _cursor = value;
                NeedsDisplay = true;
            }
        }

        #region Constructors
        // Called when created from unmanaged code
        public NSTextFieldWithCursor(IntPtr handle) : base(handle)
        {
            Initialize();
        }

        // Called when created directly from a XIB file
        [Export("initWithCoder:")]
        public NSTextFieldWithCursor(NSCoder coder) : base(coder)
        {
            Initialize();
        }
        #endregion

        // Shared initialization code
        void Initialize()
        {

        }

        public override void MouseDown(NSEvent theEvent)
        {
            base.MouseDown(theEvent);

            if (theEvent.ClickCount < 2)
            {
                OnClicked();
            }
        }

        public override void ResetCursorRects()
        {
            if (Cursor == null)
            {
                base.ResetCursorRects();
                return;
            }

            AddCursorRect(Bounds, Cursor);
        }

        protected virtual void OnClicked()
        {
            Clicked?.Invoke(this, new EventArgs());
        }
    }
}
