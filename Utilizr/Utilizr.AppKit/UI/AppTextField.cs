﻿using System;
using Foundation;
using AppKit;
using Utilizr.Validation;
using CoreGraphics;
using Utilizr.MacKit;
using Extensions;

namespace UI
{
    public partial class AppTextField : NSControl, INSTextFieldDelegate
    {
        public const float DefaultHeight = 50;
        public const float DefaultWidth = 260;

        public NSColor TextColor
        {
            get { return Field.TextColor; }
            set { Field.TextColor = value; }
        }
        public NSFont TextFont
        {
            get { return this.Field.Font; }
            set { Field.Font = value; }
        }
        public string Text
        {
            get { return Field.StringValue; }
            set { Field.StringValue = value; }
        }
        public string PlaceholderText
        {
            get { return Field.Cell.PlaceholderString; }
            set { Field.Cell.PlaceholderString = value; }
        }
        //new public NSView NextKeyView
        //{
        //    get { return Field.NextKeyView; }
        //    set { Field.NextKeyView = value; }
        //}

        public override NSView NextKeyView
        {
            get
            {
                return Field?.NextKeyView;
            }
            set
            {
                if (Field != null && value != null) Field.NextKeyView = value;
            }
        }

        protected NSTextField Field;
        public NSView TextView => Field;
		public bool ValidateOnEndEditing { get; set; }
      public bool ValidateOnInput { get; set; }
      public bool ValidateOnAction { get; set; }
        public NSFont SecureFont { get; set; } = Styles.InputFont;
        // To prevent the validation tooltip being clipped by an unknown container, allow specification of view to show the tooltip in (defaults to AppTextField superview)
        public NSView ValidationDestinationView { get; set; }

        private Validater _inputValidater;
        public Validater InputValidater
        {
            get { return _inputValidater; }
            set
            {
                var current = _inputValidater as Validater;
                if (current != null)
                {
                    current.ClearErrorRequest -= ClearErrorRequest;
                    current.Validated -= ValidationRequested;
                }
                _inputValidater = value;
                value.ClearErrorRequest += ClearErrorRequest;
                value.Validated += ValidationRequested;
            }
        }

        public override bool CanBecomeKeyView
        {
            get
            {
                return true;
            }
        }

        public bool IsSecure
        {
            get { return Field is NSSecureTextField; }
            set
            {
                if (value == IsSecure && Field != null)
                {
                    //no change and initialized
                    return;
                }

                string originalValue = null;
                string originalPlaceholder = null;
                if (Field != null)
                {
                    originalValue = Field.StringValue;
                    originalPlaceholder = Field.Cell.PlaceholderString;

                    Field.Activated -= Field_Activated;
                    Field.RemoveFromSuperview();
                    Field = null;
                }

                if (value)
                {
                    Field = new NSSecureTextField(CGRect.Empty);
                    AddSubview(Field);
                }
                else
                {
                    Field = new NSTextField(CGRect.Empty);
                    AddSubview(Field);
                }

                Field.Delegate = this;
                Field.Continuous = true;
                Field.Cell.SetSendsActionOnEndEditing(false);
                Field.FocusRingType = NSFocusRingType.None;
                Field.Font = SecureFont;
                Field.Bordered = false;
                Field.ApplyHConstraint("H:|-15-[v]-45-|", "v");
                Field.ApplyVConstraint("V:[v(18)]", "v");
                Field.ApplyCenterAlignVertical(-1);
                Field.Activated += Field_Activated;

                if (originalValue != null)
                {
                    Field.StringValue = originalValue;
                }
                if (originalPlaceholder != null)
                {
                    Field.Cell.PlaceholderString = originalPlaceholder;
                }
            }
        }

        private NSImageView ValidationImage;
        private TooltipView ValidationToolTip;

        #region Constructors

        public AppTextField(string placeholder, bool secure = false) : base(new CGRect(0, 0, DefaultWidth, DefaultHeight))
        {
            Initialize(secure);
            PlaceholderText = placeholder;
        }

        // Called when created from unmanaged code
        public AppTextField(IntPtr handle) : base(handle)
        {
            Initialize(false);
        }

        // Called when created directly from a XIB file
        [Export("initWithCoder:")]
        public AppTextField(NSCoder coder) : base(coder)
        {
            Initialize(false);
        }

        #endregion

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            Initialize(false);
        }

        // Shared initialization code
        void Initialize(bool secure)
        {
            WantsLayer = true;
            Layer.BackgroundColor = NSColor.White.CGColor;
            Layer.CornerRadius = 3;

            IsSecure = secure;

            ValidationImage = new NSImageView(CGRect.Empty);
            ValidationImage.Image = NSImage.ImageNamed("textbox_validation_error");
            ValidationImage.ImageScaling = NSImageScale.None;
            ValidationImage.Hidden = true;
            AddSubview(ValidationImage);

            ValidationImage.ApplyHConstraint("H:[v(37)]-4-|", "v");
            ValidationImage.ApplyVConstraint("V:[v(33)]", "v");
            ValidationImage.ApplyCenterAlignVertical();
        }

        public override bool BecomeFirstResponder()
        {
            return Field.BecomeFirstResponder();
        }

        public override bool ResignFirstResponder()
        {
            return Field.ResignFirstResponder();
        }

        public override bool AcceptsFirstResponder()
        {
            return Field.AcceptsFirstResponder();
        }

        private void ValidationRequested(object sender, ValidationResult eventArgs)
        {
            if (!eventArgs.IsValid)
            {
                ValidationImage.Hidden = false;

                ShowValidationTooltip(eventArgs.ToString());
            }
        }

        void Field_Activated(object sender, EventArgs e)
        {
            if (ValidateOnAction && InputValidater != null)
            {
                InputValidater.Validate(Text);
            }

            if (Action != null && Target != null)
            {
                SendAction(Action, Target);
            }
        }

        [Export("controlTextDidChange:")]
        public virtual void Changed(Foundation.NSNotification notification)
        {
            ClearValidation();
			if (ValidateOnInput && InputValidater != null)
			{
				InputValidater.Validate(Text);
			}
        }

        [Export("controlTextDidEndEditing:")]
        public void EditingEnded(NSNotification notification)
        {
            if (ValidateOnEndEditing && InputValidater != null)
            {
                InputValidater.Validate(Text);
            }
        }

        private void ShowValidationTooltip(string message)
        {
            if (ValidationToolTip != null)
            {
                ValidationToolTip.RemoveFromSuperview();
                ValidationToolTip = null;
            }

            ValidationToolTip = new TooltipView(message);

            var frame = ConvertRectToView(ValidationImage.Frame, ValidationDestinationView ?? Superview);
            ValidationToolTip.ShowAtPoint(new CGPoint(frame.GetMidX(), frame.GetMaxY()), ValidationDestinationView ?? Superview);
        }

        private void ClearErrorRequest(object sender, EventArgs eventArgs)
        {
            ClearValidation();
        }

        private void ClearValidation()
        {

            ValidationImage.Hidden = true;
            if (ValidationToolTip != null)
            {
                ValidationToolTip.Dispose();
                ValidationToolTip.RemoveFromSuperview();
                ValidationToolTip = null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (ValidationToolTip != null)
                {
                    ValidationToolTip.Dispose();
                    ValidationToolTip.RemoveFromSuperview();
                    ValidationToolTip = null;
                }
            }
        }
    }
}
