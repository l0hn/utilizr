﻿using System;
using CoreGraphics;
using Foundation;
using AppKit;
using Utilizr.Logging;

namespace SeverityBar
{
    public partial class SeverityBar : NSView
    {
        private int _barHeight = 15;
        public int BarHeight
        {
            get { return _barHeight; }
            set
            {
                bool needsDisplay = _barHeight != value;
                _barHeight = value;
                NeedsDisplay |= needsDisplay;
            }
        }

        private float _Percent;
        public float Percent
        {
            get
            {
                return _Percent;
            }
            set
            {
                _Percent = value;
                if (_Percent < 0) _Percent = 0;
                if (_Percent > 100) _Percent = 100;

                NeedsDisplay = true;
            }
        }

        public nfloat BarY { get; set; } = 15;

        const int _strokeWidth = 2;
        readonly NSColor _strokeColor = NSColor.White;
        const float _triangleWidth = 16.0f;

        public NSColor[] ColorArray { get; set; }
        public bool UseBlockColors { get; set; }

        #region Constructors

        // Called when created from unmanaged code
        public SeverityBar(IntPtr handle) : base(handle)
        {
            Initialize();
        }

        public SeverityBar(CGRect rect) : base(rect)
        {
            Initialize();
        }

        public SeverityBar()
        {
            Initialize();
        }

        // Called when created directly from a XIB file
        [Export("initWithCoder:")]
        public SeverityBar(NSCoder coder) : base(coder)
        {
            Initialize();
        }

        #endregion

        // Shared initialization code
        void Initialize()
        {

        }

        public void SetPercentage(nfloat percentage)
        {
            Percent = (float)percentage;
        }

        public override void DrawRect(CGRect dirtyRect)
        {
            base.DrawRect(dirtyRect);

            _strokeColor.Set();

            var rect = new CGRect(0, BarY, Bounds.Width, BarHeight);
            var path = new NSBezierPath();

            NSColor[] colorArray = ColorArray;
            if (colorArray == null)
            {
                colorArray = new[] { NSColor.Green, NSColor.Yellow, NSColor.Orange, NSColor.Red };
            }

            nfloat colorWidth = (nfloat)Math.Round((rect.Width-_strokeWidth) / colorArray.Length);
            nfloat roundedRectWidth = (colorArray.Length * colorWidth) + _strokeWidth;

            if (UseBlockColors)
            {

                nfloat offset = 1;

                path.LineWidth = _strokeWidth;

                for (int i = 0; i < colorArray.Length; i++)
                {
                    path = NSBezierPath.FromRect(new CGRect(offset, rect.Y, colorWidth, rect.Height));
                    path.LineWidth = _strokeWidth;
                    var c = colorArray[i];
                    c.SetFill();

                    path.Fill();
                    path.Stroke();

                    offset += colorWidth;
                }

            }
            else
            {
                nfloat radius = (rect.Size.Height / 2);

                path.AppendPathWithRoundedRect(rect, radius, radius);

                NSGradient backgroundGradient = new NSGradient(colorArray);
                backgroundGradient.DrawInBezierPath(path, -45);

                path.LineWidth = _strokeWidth;
                path.Stroke();
            }


            NSBezierPath indicator = new NSBezierPath();
            var indicatorPadding = 0;
            nfloat iLeft;
            if (UseBlockColors)
            {
                iLeft = (roundedRectWidth * Percent / 100.0f) - _triangleWidth / 2 - colorWidth / 2; //position arrow in centre of color block
            }
            else
            {
                iLeft = indicatorPadding + ((roundedRectWidth - (2 * indicatorPadding) - _triangleWidth) * Percent / 100.0f);
            }

            //Log.Info($"percent: {Percent}, left = {iLeft}, Width = {Bounds.Width}");

            indicator.MoveTo(new CGPoint(iLeft, BarY - 2));
            indicator.LineTo(new CGPoint(iLeft + _triangleWidth / 2, BarY + 8));
            indicator.LineTo(new CGPoint(iLeft + _triangleWidth, BarY - 2));
            indicator.ClosePath();

            indicator.LineWidth = _strokeWidth + 2;
            indicator.Stroke();

            NSColor indicatorColor = NSColor.Gray;
            indicatorColor.Set();
            indicator.Fill();

            NSBezierPath bottomLine = new NSBezierPath();
            var lineHeight = 3;
            bottomLine.AppendPathWithRect(new CGRect(0, BarY - lineHeight - _strokeWidth/2, roundedRectWidth, lineHeight));
            //CGPoint[] points = { new CGPoint(0, BarY - 3), new CGPoint(roundedRectWidth, BarY - 3) };
            //bottomLine.AppendPathWithPoints(points);

            NSColor bottomLineColor = NSColor.Gray;
            bottomLineColor.Set();

            //bottomLine.LineWidth = _strokeWidth;
            bottomLine.Fill();

            NeedsDisplay = true;
        }
    }
}