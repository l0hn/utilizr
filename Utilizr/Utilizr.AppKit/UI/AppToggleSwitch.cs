﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using AppKit;
using CoreGraphics;
using Utilizr.MacKit;
using CoreAnimation;
using Style;

namespace UI
{
	public partial class AppToggleSwitch : NSButton
	{
		public const float DefaultHeight = 26;
		public const float DefaultWidth = 46;

		private const float TogglePadding = 4;

		private CALayer ToggleLayer;

		#region Constructors

		// Called when created from unmanaged code
		public AppToggleSwitch (IntPtr handle) : base (handle)
		{
//			Initialize ();
		}

		// Called when created directly from a XIB file
		[Export ("initWithCoder:")]
		public AppToggleSwitch (NSCoder coder) : base (coder)
		{

		}

		public AppToggleSwitch () : base (new CGRect (0, 0, DefaultWidth, DefaultHeight))
		{
			Initialize ();
		}


		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();

			Initialize ();
		}

		// Shared initialization code
		void Initialize ()
		{
			WantsLayer = true;

			Title = "";
			Bordered = false;
			SetButtonType (NSButtonType.MomentaryChange);

			ToggleLayer = new CALayer ();
			Layer.AddSublayer (ToggleLayer);
			UpdateToggleFrame ();


			OffColor = Styles.ControlDisabledColor;
			OnColor = Styles.PrimaryColor;
			ToggleColor = Styles.ControlTextColor;

			Layer.CornerRadius = Frame.Height / 2;

		}

		public event EventHandler<bool> Toggled;

		public override bool SendAction (ObjCRuntime.Selector theAction, NSObject theTarget)
		{
			On = !On;
			return true;
		}

		public override CGRect Frame {
			get {
				return base.Frame;
			}
			set {
				base.Frame = value;
				if (Layer != null) {
					Layer.CornerRadius = Frame.Height / 2;
				}
				UpdateToggleFrame ();
			}
		}

		private void UpdateToggleFrame ()
		{
			if (ToggleLayer != null) {
				float toggleWidth = (float)Frame.Height - (TogglePadding * 2);
				if (On) {
					ToggleLayer.Frame = new CGRect (Frame.Width - TogglePadding - toggleWidth, TogglePadding, toggleWidth, toggleWidth);
				} else {
					ToggleLayer.Frame = new CGRect (TogglePadding, TogglePadding, toggleWidth, toggleWidth);
				}

				ToggleLayer.CornerRadius = toggleWidth / 2;
			}
		}

		#endregion

		private bool _stateOn = false;

		public bool On {
			get { return _stateOn; }
			set {
				var old = _stateOn;
				_stateOn = value;

				CABasicAnimation a = new CABasicAnimation ();
				a.KeyPath = "backgroundColor";
				a.SetFrom(Layer.BackgroundColor);

				Layer.BackgroundColor = _stateOn ? OnColor.ToCGColor () : OffColor.ToCGColor ();

				a.SetTo(_stateOn ? OnColor.ToCGColor () : OffColor.ToCGColor ());

				Layer.AddAnimation (a,"bg");

				//					Layer.BackgroundColor = _stateOn ? OnColor.ToCGColor () : OffColor.ToCGColor ();
				UpdateToggleFrame ();

				if (old != _stateOn) {
					//ui update

	
					Toggled?.Invoke (this, _stateOn);
				}
			}
		}

		private Color _offColor = Styles.ControlDisabledColor;

		public Color OffColor {
			get {
				return _offColor;
			}
			set {
				_offColor = value;
				if (!On) {
					Layer.BackgroundColor = _offColor.ToCGColor();
				}
			}
		}

		private Color _onColor = Styles.PrimaryColor;

		public Color OnColor {
			get {
				return _onColor;
			}
			set {
				_onColor = value;
				if (On) {
					Layer.BackgroundColor = _onColor.ToCGColor();
				}
			}
		}

		private Color _toggleColor = Styles.ControlTextColor;

		public Color ToggleColor {
			get {
				return _toggleColor;
			}
			set {
				_toggleColor = value;
				ToggleLayer.BackgroundColor = _toggleColor.ToCGColor();
			}
		}

		//want focus ring to match rounded rect layer background
		public override void DrawFocusRingMask ()
		{
			NSBezierPath bp = new NSBezierPath ();
			bp.AppendPathWithRoundedRect (Bounds, Bounds.Size.Height / 2, Bounds.Size.Height / 2);
			bp.Fill ();
		}

		public override CoreGraphics.CGRect FocusRingMaskBounds {
			get {
				return Bounds;
			}
		}
			
		[Export("moveLeft:")]
		public void MoveLeft(IntPtr sender) {
			On = false;
		}

		[Export("moveRight:")]
		public void MoveRight(IntPtr sender) {
			On = true;
		}

		private bool _dragging = false;

		private nfloat _initialToggleLocation;
		private nfloat _initialDragLocation;

		public override void MouseDragged (NSEvent theEvent)
		{
			if (!_dragging) {
				_dragging = true;

				//keep reference of positions when the drag started - so we can get deltas
				_initialToggleLocation = ToggleLayer.Frame.Location.X;
				_initialDragLocation = theEvent.LocationInWindow.X;
			}

			nfloat deltaX = _initialDragLocation - theEvent.LocationInWindow.X;
			var f = ToggleLayer.Frame;
			f.X = _initialToggleLocation - deltaX;
			//cap movement to within padding bounds
			if (f.X < TogglePadding) {
				f.X = TogglePadding;
			} else if (f.X > Frame.Width - ToggleLayer.Frame.Width - TogglePadding) {
				f.X = Frame.Width - ToggleLayer.Frame.Width - TogglePadding;
			}
			ToggleLayer.Frame = f;

		}

		public override void MouseDown (NSEvent theEvent)
		{
			
		}

		public override void MouseUp (NSEvent theEvent)
		{
			if (_dragging) {
				if (Math.Abs (_initialDragLocation - theEvent.LocationInWindow.X) < 5) {
					PerformClick (this);
				} else {
					//only handle drag if movement has been move than 5 pixels
					On = (ToggleLayer.Frame.GetMidX () > Frame.Width / 2);
				}

			} else {
				PerformClick (this);
			}

			_dragging = false;
		}
	
	}
}
