﻿using System;
using Foundation;
using AppKit;
using Utilizr.MacKit;
using Style;
using CoreGraphics;
using CoreAnimation;
using Utilizr.Logging;

namespace UI
{
	
	public class AppButton : NSControl
	{
		public const float DefaultHeight = 50;
		public const float DefaultWidth = 150;
		public const float DefaultWidthWithIcon = 220;

		public float? LeftPaddingOverride
		{
			get
			{
				return Button.LeftPaddingOverride;
			}
			set
			{
				Button.LeftPaddingOverride = value;
			}
		}
		public float? RightPaddingOverride
		{
			get
			{
				return Button.RightPaddingOverride;
			}
			set
			{
				Button.RightPaddingOverride = value;
			}
		}

		public Color BackgroundActiveColor { get; set; }
		public Color HighlightColor { get; set; }

		public NSCellImagePosition ImagePosition
		{
			get
			{
				return Button.ImagePosition;
			}
			set
			{
				Button.ImagePosition = value;
			}
		}

		protected InnerAppButton Button;
		private NSView BackgroundView;
		public NSCursor Cursor { get; set; } = NSCursor.PointingHandCursor;

		// Called when created directly from a XIB file
		[Export("initWithCoder:")]
		public AppButton(NSCoder coder) : base(coder)
		{
		}

		// Called when created from unmanaged code
		public AppButton(IntPtr handle) : base(handle)
		{
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
			Button = new InnerAppButton(CGRect.Empty);
			Initialize();
		}


		public string Title
		{
			get
			{
				return Button.Title;
			}
			set
			{
				Button.Title = value;
			}
		}

		public NSImage Image
		{
			get
			{
				return Button.Image;
			}
			set
			{
				Button.Image = value;
			}
		}

		public override NSView HitTest(CGPoint aPoint)
		{
			var p = ConvertPointFromView(aPoint, Superview);
			//make sure click doesn't get passed to subviews (i.e. the inner button)
			if (Bounds.Contains(p)) {
				return this;
			}

			return base.HitTest(aPoint);
		}

		public AppButton(string title) : base(new CGRect(0, 0, DefaultWidth, DefaultHeight))
        {
			Button = new InnerAppButton(title);

			Initialize();
		}

		public AppButton(string title, NSImage icon) : base(new CGRect(0, 0, DefaultWidthWithIcon, DefaultHeight))
        {
			Button = new InnerAppButton(title, icon);

			Initialize();
		}

	

		public override NSFont Font
		{
			get { return Button.Font; }
			set { Button.Font = value; }
		}

		public nfloat CornerRadius
		{
			get { return BackgroundView.Layer.CornerRadius; }
			set { BackgroundView.Layer.CornerRadius = value; }
		}

		private void Initialize()
		{

            BackgroundView = new NSView { WantsLayer = true };
			BackgroundView.Frame = this.Bounds;
			CornerRadius = Frame.Height / 2;

			AddSubview(BackgroundView);
			AddSubview(Button);

			WantsLayer = true;


			Layer.BackgroundColor = NSColor.Clear.CGColor;


			BackgroundColor = Styles.PrimaryColor;
			BackgroundActiveColor = Styles.PrimaryActiveColor;
			HighlightColor = Styles.PrimaryHighlightColor;

            //add tracking area so we can show highlight on mouse hover
            var tracking = new NSTrackingArea(Bounds, NSTrackingAreaOptions.ActiveAlways | NSTrackingAreaOptions.MouseEnteredAndExited, this, null);
			AddTrackingArea(tracking);
		}

		public override void Layout()
		{
            if (BackgroundView == null || Button == null)
            {
                base.Layout();
                return;
            }

            BackgroundView.Frame = this.Bounds;
            Button.Frame = this.Bounds;

            base.Layout();
		}

		private Color _backgroundColor;
		public Color BackgroundColor
		{
			get { return _backgroundColor; }
			set
			{
				_backgroundColor = value;
				BackgroundView.Layer.BackgroundColor = _backgroundColor.ToCGColor();
			}
		}

		public override void MouseDown(NSEvent theEvent)
		{
			IsMouseDown = true;
			BackgroundView.Layer.BackgroundColor = BackgroundActiveColor.ToCGColor();
		}

		public override void MouseEntered(NSEvent theEvent)
		{
			BackgroundView.Layer.BackgroundColor = HighlightColor.ToCGColor();
		}

		public override void ResetCursorRects()
		{
			if (Cursor == null)
			{
				base.ResetCursorRects();
			}
			else
			{
				AddCursorRect(Bounds, Cursor);
			}
		}

		//keep mouse down flag so we can use the correct background color when mouse hover changes
		private bool IsMouseDown = false;

		public override void MouseExited(NSEvent theEvent)
		{
			if (IsMouseDown)
			{
				BackgroundView.Layer.BackgroundColor = BackgroundActiveColor.ToCGColor();
			}
			else
			{
				BackgroundView.Layer.BackgroundColor = BackgroundColor.ToCGColor();
			}
		}

		public override void MouseUp(NSEvent theEvent)
		{
			IsMouseDown = false;
			BackgroundView.Layer.BackgroundColor = _backgroundColor.ToCGColor();
			Activated?.Invoke(this, null);
		}

		public new event EventHandler Activated;

	}

	[Foundation.Register("AppButton")]
    public class InnerAppButton : NSButton
    {
        public const float DefaultHeight = 50;
        public const float DefaultWidth = 150;
        public const float DefaultWidthWithIcon = 220;

		public float? LeftPaddingOverride;
		public float? RightPaddingOverride;

        public override NSImage Image
        {
            get { return base.Image; }
            set
            {
                // because it is unfavourable to subclass button cell, the only way to
                // adjust the icon's position is by adding some padding to the image itself

                var img = value;

				if (img == null)
				{
					base.Image = null;
					AlternateImage = null;
					return;
				}

                //first get the vertical space around the image
                var padding = (DefaultHeight - img.Size.Height) / 2;

                //because the button is pill shaped, the side padding should be the same as the vertical padding
				base.Image = CreatePaddedImage(img, LeftPaddingOverride ?? (float)padding, RightPaddingOverride ?? (float)padding);
                AlternateImage = Image;

				ImagePosition = NSCellImagePosition.ImageRight;
            }
        }

        public bool IsAnimatingIcon
        {
            get
            {
                var lyr = ObtainImageLayer();
                if(lyr != null)
                {
                    return (lyr.AnimationForKey("spin") != null);
                }
                return  false;
            }
        }

        public Color TextColor
        {
            get { return _textColor; }
            set
            {
                _textColor = value;
                AttributedTitle = ButtonTitleWithString(AttributedTitle.Value);
            }
        }

        public override string Title
        {
			get { return AttributedTitle?.Value; }
            set { AttributedTitle = ButtonTitleWithString(value); }
        }

        public override CGRect FocusRingMaskBounds { get { return Bounds; } }

        private Color _textColor = Styles.ControlTextColor;

        #region Constructors

        // Called when created from unmanaged code
        public InnerAppButton(IntPtr handle) : base(handle)
        {
            Initialize();

        }

        // Called when created directly from a XIB file
        [Export("initWithCoder:")]
        public InnerAppButton(NSCoder coder) : base(coder)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            Initialize();
        }

        public InnerAppButton(CGRect frameRect) : base(frameRect)
        {
            Initialize();
        }

        public InnerAppButton(string title) : base(new CGRect(0, 0, DefaultWidth, DefaultHeight))
        {
            Initialize();
            Title = title;
        }

        public InnerAppButton(string title, NSImage icon) : base(new CGRect(0, 0, DefaultWidthWithIcon, DefaultHeight))
        {
            Initialize();
            Title = title;
            Image = icon;
        }

        #endregion

        // Shared initialization code
        private void Initialize()
        {
			Cell.HighlightsBy = (int)NSCellStyleMask.ContentsCell;

            WantsLayer = true;
            Bordered = false;
			//Enabled = false;

            //Transparent = true;
			SetButtonType(NSButtonType.MomentaryChange);
			//BezelStyle = NSBezelStyle.RegularSquare;

			Layer.BackgroundColor = NSColor.Clear.CGColor;

            TextColor = Styles.ControlTextColor;

        }

   //     private static NSImage CreatePaddedImage(NSImage input, float padding)
   //     {
			//return CreatePaddedImage(input, padding, padding);
   //     }


        private static NSImage CreatePaddedImage(NSImage input, float leftPadding, float rightPadding)
		{
			var result = new NSImage(new CGSize(input.Size.Width + (leftPadding + rightPadding), input.Size.Height));

			// Composite image appropriately
			result.LockFocus();
			NSGraphicsContext.CurrentContext.ImageInterpolation = NSImageInterpolation.Default;
			input.Draw(new CGRect(new CGPoint(leftPadding, 0), input.Size));
			result.UnlockFocus();

			return result;
		}


		private CALayer ObtainImageLayer()
        {
            CALayer imageLayer = null;
            foreach(var l in Layer.Sublayers)
            {
                if(l.Contents is CGImage)
                {
                    imageLayer = l;
                    break;
                }
            }
            return imageLayer;
        }

        public void StartAnimatingIcon(bool clockwise = true)
        {
            //uber hackage

            CALayer imageLayer = ObtainImageLayer();
            if(imageLayer != null)
            {
                if(imageLayer.AnimationForKey("spin") == null)
                {
                    imageLayer.Position = new CGPoint(imageLayer.Frame.GetMidX(), imageLayer.Frame.GetMidY());
                    imageLayer.AnchorPoint = new CGPoint(0.5, 0.5);

                    var spin = new CABasicAnimation();
                    spin.KeyPath = "transform.rotation";
                    spin.To = NSNumber.FromDouble(Math.PI * (clockwise ? 2 : -2));
                    spin.From = NSNumber.FromFloat(0f);
                    spin.RepeatCount = float.MaxValue;
                    spin.Duration = 1.0;
                    imageLayer.AddAnimation(spin, "spin");
                }
            }
        }

        public void StopAnimatingIcon()
        {
            CALayer imageLayer = ObtainImageLayer();
            if(imageLayer != null)
            {
                imageLayer.RemoveAnimation("spin");
            }
        }

		//public override void MouseUp(NSEvent theEvent)
		//{
		//	//base.MouseUp(theEvent);
		//}

		//public override void MouseDown(NSEvent theEvent)
		//{
		//	//base.MouseDown(theEvent);
		//}

        private NSAttributedString ButtonTitleWithString(string title)
        {
            var pg = new NSMutableParagraphStyle();
            pg.Alignment = NSTextAlignment.Center;
            return new NSAttributedString(title, Font, _textColor.ToNSColor(), null, null, null, null, 0, 0, pg);
        }

        //want focus ring to match rounded rect layer background
        public override void DrawFocusRingMask()
        {
            NSBezierPath bp = new NSBezierPath();
            bp.AppendPathWithRoundedRect(Bounds, Bounds.Size.Height / 2, Bounds.Size.Height / 2);
            bp.Fill();
        }

    
    }
}
