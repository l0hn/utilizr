﻿using System;
using AppKit;
using Style;

namespace Utilizr.MacKit
{
	public static class Styles
	{

		public static Color PrimaryColor = Color.ParseHex("#23cb7e");
		public static Color PrimaryActiveColor = Color.ParseHex("#23cb7ebb");
		public static Color PrimaryHighlightColor = Color.ParseHex("#23cb7edd");

		public static Color WarningColor = Color.ParseHex("#ed494d");

		public static Color ControlDisabledColor = Color.ParseHex("#d3d3d3");

		public static Color ContentTextColor = Color.ParseHex("#aaaaaa");
		public static Color ControlTextColor = Color.White;

		public static NSFont ControlFont = NSFont.BoldSystemFontOfSize(16);
		public static NSFont InputFont = NSFont.BoldSystemFontOfSize(14);

	}
}

