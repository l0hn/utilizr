﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.AndroidPublisher.v2;
using Google.Apis.AndroidPublisher.v2.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;

namespace Utilizr.GooglePlayStoreUploader
{
    class Program
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">"{PathToAndroidCertificate .p12 file},{serviceAccountEmail},{packageName},{ApkBuildNumber},{Track(beta/alpha/prod)},{pathToApk}"</param>
        public static void Main(string[] args)
        {
            if (args == null || args.Length < 6)
            {
                Console.WriteLine("Parameters missing..");
                Console.WriteLine("Expecting.. {PathToAndroidCertificate .p12 file},{serviceAccountEmail},{packageName},{ApkBuildNumber},{Track(beta/alpha/prod)},{pathToApk}");
                Console.WriteLine("Exiting..");
                return;
            }
            if (string.IsNullOrEmpty(args[0]) || !args[0].EndsWith(".p12", true, CultureInfo.InvariantCulture) || !File.Exists(args[0]))
            {
                Console.WriteLine("A valid Google Play Android Developer .p12 certificate not found .. exiting");
                return;
            }
            if (string.IsNullOrEmpty(args[1]))
            {
                Console.WriteLine("Exiting..");
                return;
            }

            if (string.IsNullOrEmpty(args[2]))
            {
                Console.WriteLine("Exiting..");
                return;
            }
            if (string.IsNullOrEmpty(args[3]))
            {
                Console.WriteLine("Exiting..");
                return;
            }
            if (string.IsNullOrEmpty(args[5]) || !args[5].EndsWith(".apk", true, CultureInfo.InvariantCulture) || !File.Exists(args[5]))
            {
                Console.WriteLine("A valid .apk app file not found .. Exiting");
                return;
            }

            string track = string.IsNullOrEmpty(args[4]) ? "beta" : args[4].ToLower();

            Console.WriteLine("Google Play Publish API - Service Account");
            Console.WriteLine("==========================");
            //args = new string[] { "totalav","127"};
            String serviceAccountEmail = args[1];//"android-publisher-servss@api-4828868064209507711-877165.iam.gserviceaccount.com";

            //var filename = Path.Combine(Environment.CurrentDirectory, "GooglePlayUploader", @"Google Play Android Developer-91376632bd48.p12");
            var filename = args[0];
            var certificate = new X509Certificate2(filename, "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(serviceAccountEmail)
               {
                   Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
               }.FromCertificate(certificate));

            // Create the service.
            var service = new AndroidPublisherService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Android Publisher Service"
            });

            try
            {
                int versionCode;
                if (!int.TryParse(args[3], out versionCode))
                {
                    Console.WriteLine("Version Code(Build Number) not supplied, could not upload to playstore.");
                    return;
                }
                var packagename = args[2]; //$"{args[2]}.apk";
                var apk = args[5]; //Path.Combine(Environment.CurrentDirectory, "../BuildTools", packagename);
                                   //var apk = Path.Combine("/Users/Chico/Repos/jdi-ss/mobile/BuildTools", packagename);

                // packagename = $"com.{args[2]?.ToLower()}.android";

                //Console.Write("{{Current Directory}}:" + apk);
                if (!File.Exists(apk))
                {
                    Console.Write("Could not find apk file at -- " + apk);
                    return;
                }

                var newEdit = service.Edits.Insert(new AppEdit(), packagename).Execute();
                using (var fs = new FileStream(apk, FileMode.Open))
                {
                    var x = service.Edits.Apks.Upload(packagename, newEdit.Id, fs, "application/vnd.android.package-archive").Upload();
                    if (x.Status != Google.Apis.Upload.UploadStatus.Completed)
                    {
                        Console.WriteLine("Upload failed.. exiting");
                        return;
                    }
                }
                var tracks = service.Edits.Tracks.Update(new Track() { TrackValue = track, VersionCodes = new List<int?>() { versionCode } }, packagename, newEdit.Id,track).Execute();
                if (tracks == null)
                {
                    Console.WriteLine("Could not set Track");
                    return;
                }
                var valid = service.Edits.Validate(packagename, newEdit.Id).Execute();
                if (valid?.Id == null)
                {
                    Console.WriteLine($"Commit for {packagename} not valid");
                    return;
                }
                var commit = service.Edits.Commit(packagename, newEdit.Id).Execute();
                Console.WriteLine($"Commit for {packagename} successful, Id={commit.Id}");

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Commit for {args[2]} failed, Error={ex.Message}");
            }
        }
    }
}
