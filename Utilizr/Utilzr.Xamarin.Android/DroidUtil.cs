﻿using System;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Preferences;
using Android.Net;
using Java.Util;
using Uri = Android.Net.Uri;
using Android.Widget;
using GetText;

namespace Utilzr.Xamarin.Droid
{
	public static class DroidUtil
	{
		const string APP_PREFERENCE = "APP_PREFERENCE";


		public static void AddShortcutToHomescreen(Type typ, string name, int resid)
		{
			var prefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
			var prefname = typ + name;
			if (prefs.GetBoolean(prefname, false))
				return;
			Intent shortcutIntent = new Intent(Application.Context, typ);

			shortcutIntent.SetAction(Intent.ActionMain);

			Intent addIntent = new Intent();
			addIntent.PutExtra(Intent.ExtraShortcutIntent, shortcutIntent);
			addIntent.PutExtra(Intent.ExtraShortcutName, name);
			addIntent.PutExtra(Intent.ExtraShortcutIconResource, Intent.ShortcutIconResource.FromContext(Application.Context, resid));

			addIntent.SetAction("com.android.launcher.action.INSTALL_SHORTCUT");
			addIntent.PutExtra("duplicate", false);  //may it's already there so don't duplicate

			Application.Context.SendBroadcast(addIntent);

			var editor = prefs.Edit();
			editor.PutBoolean(prefname, true);
			if (Build.VERSION.SdkInt >= BuildVersionCodes.JellyBeanMr2)
				editor.Apply();
			else
				editor.Commit();
		}

		public static void RemoveLauncherShortcut(string packageName,string shortcutname)
		{
			var pm = Application.Context.PackageManager;
			var info = pm.GetApplicationInfo(packageName, 0);
			if (info == null)
				return;
			var shrtCutIntent = pm.GetLaunchIntentForPackage(packageName);
			if (shrtCutIntent == null)
				return;
			var removalIntent = new Intent();
			removalIntent.PutExtra(Intent.ExtraShortcutIntent, shrtCutIntent);
			removalIntent.PutExtra(Intent.ExtraShortcutName, shortcutname);
			removalIntent.PutExtra("duplicate", false);
			removalIntent.SetAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
			Application.Context.SendBroadcast(removalIntent);
		}

		public static void DrawTextInRect(Canvas c, Paint p, string text, RectF r)
		{
			RectF bounds = new RectF(r);
			// measure text width
			bounds.Right = p.MeasureText(text, 0, text.Length);
			// measure text height
			bounds.Bottom = p.Descent() - p.Ascent();

			bounds.Left += (r.Width() - bounds.Right) / 2.0f;
			bounds.Top += (r.Height() - bounds.Bottom) / 2.0f;

			p.Color = (Color.White);
			c.DrawText(text, bounds.Left, bounds.Top - p.Ascent(), p);
		}

        static void BundleToString(StringBuilder builder, Bundle data)
        {
            var keySet = data.KeySet();
            builder.Append("[Bundle with ").Append(keySet.Count).Append(" keys:");
            foreach (var key in keySet)
            {
                builder.Append(' ').Append(key).Append('=');
                Object value = data.Get(key);
                if (value is Bundle)
                {
                    BundleToString(builder, (Bundle)value);
                }
                else
                {
                    builder.Append((value is Object[])
                            ? Arrays.ToString((bool[])value) : value);
                }
            }
            builder.Append(']');
        }

        public static string BundleToString(Bundle data)
        {
            if (data == null)
            {
                return "N/A";
            }
            StringBuilder builder = new StringBuilder();
            BundleToString(builder, data);
            return builder.ToString();
        }

        public static void OpenUrl(string url)
        {
            Uri uri = Uri.Parse(url);
            var intent = new Intent(Intent.ActionView, uri);
            intent.AddFlags(ActivityFlags.NewTask);
			try
			{
                if (intent.ResolveActivity(Application.Context.PackageManager) != null)
                    Application.Context.StartActivity(intent);
                else
                    Toast.MakeText(Application.Context, L._("No browser app found to open webpage"), ToastLength.Long).Show();
            }
			catch (ActivityNotFoundException ex)
			{
                Utilizr.Logging.Log.Exception(ex);
			}
           
        }

		public static void Forgett(this System.Threading.Tasks.Task t)
		{

		}
	}
}
