﻿using System;
using System.IO;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;

namespace Utilzr.Xamarin.Droid
{
	public static class Extensions
	{
		public static TextDirection ToDirectionEnum(this float angle)
		{
			if (angle >= 270 || angle <= 90)
				return TextDirection.Ltr;
			return TextDirection.Rtl;
		}

		public static void DrawTextCentred(this Canvas canvas, Paint paint, string text, float cx, float cy, Rect textBounds)
		{
			paint.GetTextBounds(text, 0, text.Length, textBounds);
			canvas.DrawText(text, cx, cy - textBounds.ExactCenterY(), paint);
		}

		


		//public static void SetGradientBackgroundColor(this Android.Views.View v, Android.Graphics.Color c)
		//{
		//	if (c == null)
		//	{
		//		return;
		//	}
		//	var current = v.Background;

		//	if (current is GradientDrawable)
		//	{
		//		((GradientDrawable)current).SetColor(c.ToArgb());
		//	}
		//	else {
		//		v.SetBackgroundColor(c);
		//	}
		//}

		public static void SetGradientBackgroundColor(this View v, int argb)
		{
			var current = v.Background;

			if (current is GradientDrawable)
			{
				((GradientDrawable)current).SetColor(argb);
			}
			else
			{
				v.SetBackgroundColor(new Color(argb));
			}
		}

		public static Color ToAndroidColor(this int v)
		{
			return new Color(v);
		}

		

		public static bool IsAnyNull(params object[] o)
		{
			foreach (var item in o)
			{
				if (item == null)
					return true;
			}
			return false;
		}

		public static bool IsAnyNullOrDisposed(params Android.Runtime.IJavaObject[] o)
		{
			foreach (var item in o)
			{
				if (item == null || item.Handle == IntPtr.Zero)
					return true;
			}
			return false;
		}

		public static bool IsDisposed(this Android.Runtime.IJavaObject v)
		{
			return v?.Handle == IntPtr.Zero;
		}

		public static TextView MakeVisible(this TextView t, bool v)
		{
			t.Visibility = v ? ViewStates.Visible : ViewStates.Gone;
			return t;

		}

		public static string ToHexColor(this Color color, bool includeAlpha = true)
		{
			if (includeAlpha)
			{
				return "#" + Java.Lang.Integer.ToHexString(color.ToArgb());
			}
			else
			{
				return "#" + Java.Lang.Integer.ToHexString(color.ToArgb()).Substring(2);
			}
		}

		public static Java.Lang.Object ToJavaObject<TObject>(this TObject value)
		{
			if (Equals(value, default(TObject)) && !typeof(TObject).IsValueType)
				return null;
			var holder = new JavaHolder(value);
			return holder;
		}

		public static TObject ToDotNetObject<TObject>(this Java.Lang.Object value)
		{
			if (value == null)
				return default(TObject);
			if (!(value is JavaHolder))
				throw new InvalidOperationException("Unable to convert to .NET object. Only Java.Lang.Object created with .ToJavaObject() can be converted.");
			TObject returnVal;
			try { returnVal = (TObject)((JavaHolder)value).Instance; }
			finally { value.Dispose(); }
			return returnVal;
		}

		public static Byte[] ToByteArray(this Drawable drw)
		{
			byte[] bitmapdata; ;
			try
			{
				using (var stream = new MemoryStream())
				{
					var bitmap = ((BitmapDrawable)drw).Bitmap;
					bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
					bitmapdata = stream.ToArray();
					return bitmapdata;
				}
			}
			catch (Exception ex)
			{

			}
			return null;
		}

		public static Bitmap DecodeSampledBitmapFromResource(this Resources res, int resId, int reqWidth, int reqHeight)
		{

			// First decode with inJustDecodeBounds=true to check dimensions
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.InJustDecodeBounds = true;
			BitmapFactory.DecodeResource(res, resId, options);

			// Calculate inSampleSize
			options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.InJustDecodeBounds = false;
			options.InPurgeable = true;
			options.InInputShareable = true;
			return BitmapFactory.DecodeResource(res, resId, options);
		}


		public static DateTime UnixTimeStampToDateTime(this long unixTimeStamp)
		{
			try
			{
				// Unix timestamp is seconds past epoch
				System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
				dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
				return dtDateTime;
			}
			catch (ArgumentOutOfRangeException ex)
			{
				return default(DateTime);
			}

		}


		//public static DateTime UnixTimeStampToDateTime(this double unixTimeStamp)
		//{
		//	// Unix timestamp is seconds past epoch
		//	System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
		//	dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
		//	return dtDateTime;
		//}

		public static int CalculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight)
		{
			// Raw height and width of image
			int height = options.OutHeight;
			int width = options.OutWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth)
			{

				int halfHeight = height / 2;
				int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2 and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth)
				{
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		public static Java.Lang.ICharSequence ToCharSequence(this string s)
		{
			return new Java.Lang.String(s);
		}

		public static void SetTextColor(this TextView textview, int argb)
		{
			textview.SetTextColor(new Color(argb));
		}
	}
}
