﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Utilizr.Crypto
{
    public static class StringEncrypter
    {
        private static Encoding _encoding = Encoding.UTF8;

        //http://www.dijksterhuis.org/encrypting-decrypting-string/
        /// <summary>
        /// Encrypt a byte[] with a passphrase using TDES
        /// </summary>
        /// <param name="message"></param>
        /// <param name="passphrase"></param>
        /// <returns>Encrypted message as byte[]</returns>
        public static byte[] EncryptTDES(byte[] message, string passphrase)
        {
            byte[] Results;

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below


            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(_encoding.GetBytes(passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the encoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.Zeros;
            TDESAlgorithm.GenerateIV();

            // Step 5. Attempt to encrypt the string
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(message, 0, message.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the encrypted string as a base64 encoded string
            return Results;
        }

        /// <summary>
        /// Decrypt a byte array with a passphrase
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Passphrase"></param>
        /// <returns>decrypted bytes</returns>
        public static byte[] DecryptTDES(byte[] Message, string Passphrase)
        {
            byte[] Results;

            int rem = Message.Length % 8;
            int padding = 0;
            if (rem > 0)
            {
                padding = 8 - rem;
            }
            byte[] paddedMessage = new byte[Message.Length + padding];
            paddedMessage.Initialize();
            Array.Copy(Message, paddedMessage, Message.Length);
            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            byte[] TDESKey;
            MD5CryptoServiceProvider HashProvider;

            HashProvider = new MD5CryptoServiceProvider();
            TDESKey = HashProvider.ComputeHash(_encoding.GetBytes(Passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm;

            TDESAlgorithm = new TripleDESCryptoServiceProvider();



            // Step 3. Setup the decoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.Zeros;
            TDESAlgorithm.GenerateIV();

            // Step 5. Attempt to decrypt the string
            Results = null;
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(paddedMessage, 0, paddedMessage.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the decrypted string in UTF8 format
            return Results;
        }

        /// <summary>
        /// Encrypt a string with a passphrase using TDES
        /// </summary>
        /// <param name="message"></param>
        /// <param name="passphrase"></param>
        /// <returns>Encrypted string</returns>
        public static string EncryptString(string message, string passphrase)
        {
            if (message.IsNullOrEmpty())
                return message;
            
            return Convert.ToBase64String(EncryptTDES(_encoding.GetBytes(message), passphrase));
        }


        /// <summary>
        /// Decrypt a string with a passphrase using TDES
        /// </summary>
        /// <param name="encryptedMessage"></param>
        /// <param name="passphrase"></param>
        /// <returns>decrypted string</returns>
        public static string DecryptString(string encryptedMessage, string passphrase)
        {
            if (encryptedMessage.IsNullOrEmpty())
                return encryptedMessage;

            return _encoding.GetString(DecryptTDES(Convert.FromBase64String(encryptedMessage), passphrase)).TrimEnd('\0');
        }
    }
}
