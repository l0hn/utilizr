﻿using System;

namespace Utilizr.Logging
{
    internal static class Helpers
    {
        internal static string GetLevelName(LoggingLevel level)
        {
            return level.ToString();
        }

        internal static LoggingLevel GetLevelName(string level)
        {
            //TODO: handle passing invalid level
            return (LoggingLevel)Enum.Parse(typeof(LoggingLevel), level, true);
        }
    }
}
