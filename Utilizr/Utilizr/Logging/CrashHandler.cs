﻿using System;
using System.Diagnostics;
using System.Threading;
#if !MONO && !NETCOREAPP
using System.Windows.Threading;
#endif

namespace Utilizr.Logging
{    
    public static class CrashHandler
    {        
        #if !MONO && !NETCOREAPP
        public static void DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
        Log.Exception(LoggingLevel.CRITICAL, e.Exception);
        #if DEBUG
        Debugger.Launch();
        #else
        Environment.Exit(1);
        #endif
        Environment.Exit(1);
        }
        #endif

        public static void HandleException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Exception(LoggingLevel.CRITICAL, (Exception)e.ExceptionObject);
#if DEBUG
            Debugger.Launch();
#else
            Environment.Exit(1);
#endif
            Environment.Exit(1);
        }

        public static void HandleThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Log.Exception(LoggingLevel.CRITICAL, e.Exception);
#if DEBUG
            Debugger.Launch();
#else
            Environment.Exit(1);
#endif
        }
    }
}
