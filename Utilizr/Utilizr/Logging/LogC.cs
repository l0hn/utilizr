﻿#if !ANDROID && !IOS
using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Utilizr.Conversion;
using Utilizr.Info;

namespace Utilizr.Logging
{
    /// <summary>
    /// A logger which compresses it output due to the expected high volume of output.
    /// Automatically rotates files based on <see cref="MaximumFiles"/> and <see cref="MaximumFileSize"/>.
    /// </summary>
    public static class LogC
    {
        public static int MaximumFiles { get; set; } = 20;
        /// <summary>
        /// In bytes and the maximum size for the uncompressed file size.
        /// Thus the compressed size will be much smaller, hopefully...
        /// </summary>
        public static int MaximumFileSize { get; set; } = 1024 * 1024 * 1024; // 1MB

        private static string _logDirectory;
        public static string LogDirectory
        {
            get
            {
                if (_logDirectory.IsNullOrEmpty())
                {
                    _logDirectory = AppInfo.LogDirectory;
                }

                return _logDirectory;
            }
            set
            {
                _logDirectory = value; 
            }
        }

        public static string FileName { get; set; } = "compressed";
        /// <summary>
        /// In bytes.
        /// </summary>
        public static int BufferSize { get; set; } = 4 * 1024 * 1024;
        public static string FileExtensionCompressed => ".logc";
        public static string FileExtensionDecompressed => ".log";

        private static string Timestamp => DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff zzz");
        private static bool IsOpen => _fileStream != null && _compressor != null;

        private static readonly object STREAM_LOCK = new object();
        private static FileStream _fileStream;
        private static DeflateStream _compressor;

        private static long _currentFileBytes;
        

        public static void Exception(Exception ex, string formatMessage = "", params object[] formatParamters)
        {
            var sb = new StringBuilder();

            var customMessage = GetFormattedString(formatMessage, formatParamters);
            if (!customMessage.IsNullOrEmpty())
                sb.AppendLine(customMessage);

            AddException(ex, sb);
            Log(sb.ToString());
        }

        static void AddException(Exception ex, StringBuilder builder)
        {
            Action<Exception> handleEx = (exception) =>
            {
                builder.AppendLine($"{exception.GetType().FullName}: {exception.Message}");
                builder.AppendLine(exception.StackTrace);
            };

            handleEx(ex);
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                handleEx(ex);
            }
        }

        public static void Message(string formatMessage, params object[] formatParamters)
        {
            Log(GetFormattedString(formatMessage, formatParamters));
        }

        static string GetFormattedString(string formatString, params object[] formatParameters)
        {
            if (string.IsNullOrEmpty(formatString))
                return string.Empty;

            string result = string.Empty;
            try
            {
                result = string.Format(formatString, formatParameters ?? new object[0]);
            }
            catch
            {
                string parameters = formatParameters != null
                    ? string.Join(",", formatParameters.Select(p => p.ToString()).ToArray())
                    : "None specified";
                result = $"Failed to generate formatted string for '{formatString}' with parameters '{parameters}'";
            }

            return result;
        }

        static bool Log(string raw)
        {
            if (raw.IsNullOrEmpty())
                return false;

            try
            {
                lock (STREAM_LOCK)
                {
                    //ValidateDirectory(); this is extremely costly to perform for every log write
                    Open(); // Handles already being open

                    string entry = $"{Timestamp} : {raw}{Environment.NewLine}";
                    var strBytes = Encoding.UTF8.GetBytes(entry);
                    _compressor.Write(strBytes, 0, strBytes.Length);
                    _currentFileBytes += strBytes.Length;

                    if (_currentFileBytes > MaximumFileSize)
                        Close();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Failed to log {raw} due to {ex.GetType().FullName}: {ex.Message}");
                return false;
            }

            return true;
        }

        static IEnumerable<FileInfo> GetFiles(string directory)
        {
            var logDir = new DirectoryInfo(directory);

            if (!logDir.Exists)
                return Enumerable.Empty<FileInfo>();

            return logDir.GetFiles($"*{FileExtensionCompressed}");
        }

        static void ValidateDirectory()
        {
            var files = GetFiles(LogDirectory);

            if (files.Count() < MaximumFiles)
                return;

            var toDelete = files.OrderByDescending(p => p.CreationTimeUtc)
                .Skip(MaximumFiles);

            foreach (var expiredFile in toDelete)
            {
                expiredFile.Delete();
            }
        }

        static string CurrentFilePath()
        {
            if (!Directory.Exists(LogDirectory))
                Directory.CreateDirectory(LogDirectory);

            //var files = GetFiles(LogDirectory);
            //if (files.Any())
            //{
            //    var latestFile = files.OrderByDescending(p => p.CreationTime).First();
            //    _currentFileBytes = GetCompressedFileBytes(latestFile.FullName);

            //    if (_currentFileBytes < MaximumFileSize)
            //    {
            //        return latestFile.FullName;
            //    }
            //}

            _currentFileBytes = 0;
            string fileName = $"{FileName}-{DateTime.UtcNow.ToUnixTimestamp()}{FileExtensionCompressed}";
            return Path.Combine(LogDirectory, fileName);
        }

        static long GetCompressedFileBytes(string filePath)
        {
            long uncompressedBytes = 0;
            byte[] buffer = new byte[BufferSize];

            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var decompressor = new DeflateStream(fileStream, CompressionMode.Decompress))
            {
                int length = 0;
                while ((length = decompressor.Read(buffer, 0, buffer.Length)) > 0)
                {
                    uncompressedBytes += length;
                }
            }

            return uncompressedBytes;
        }

        static void Open()
        {
            if (IsOpen)
                return;

            ValidateDirectory();
            _fileStream = new FileStream(CurrentFilePath(), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
            _compressor = new DeflateStream(_fileStream, CompressionMode.Compress, true);
            _compressor.BufferSize = BufferSize;
        }

        static void Close()
        {
            _compressor?.Flush();
            _compressor?.Close();
            _compressor?.Dispose();
            _compressor = null;

            _fileStream?.Flush();
            _fileStream?.Close();
            _fileStream?.Dispose();
            _fileStream = null;

            ValidateDirectory();
        }

        public static bool Flush()
        {
            try
            {
                // From the docs:
                // "The current implementation of this method does not flush the internal buffer.
                // The internal buffer is flushed when the object is disposed."
                Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Failed to flush {typeof(LogC).Name} due to {ex.GetType().FullName}: {ex.Message}");
                return false;
            }

            return true;
        }

        public static void Decompress(string folderOrFilePath, string destinationFolder, bool overwrite)
        {
            Console.WriteLine($"Output for decompressed files will be: {destinationFolder}");
            Console.WriteLine();

            if (folderOrFilePath.EndsWith(FileExtensionCompressed))
            {
                DecompressFile(folderOrFilePath, destinationFolder, overwrite);
                return;
            }

            DecompressFolder(folderOrFilePath, destinationFolder, overwrite);
            Console.WriteLine();
            Console.Write("Done, press enter to continue...");

            ConsoleKeyInfo pressed;
            do
            {
                pressed = Console.ReadKey();
            } while (pressed.Key != ConsoleKey.Enter);
        }

        static void DecompressFolder(string folderPath, string destinationFolder, bool overwrite)
        {
            try
            {
                var logFiles = GetFiles(folderPath);

                if (!logFiles.Any())
                    Console.WriteLine($"No log files with file extension {FileExtensionCompressed} were found in {folderPath}");

                foreach (var logFile in logFiles)
                {
                    DecompressFile(logFile.FullName, destinationFolder, overwrite);
                }
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                AddException(ex, sb);
                Console.WriteLine($"Failed to decompress folder.{Environment.NewLine}{sb.ToString()}");
            }
        }

        static void DecompressFile(string filePath, string destination, bool overwrite)
        {
            string outFile = Path.Combine(destination, $"{Path.GetFileNameWithoutExtension(filePath)}{FileExtensionDecompressed}");
            Console.Write($"Decompressing {Path.GetFileName(filePath)} to {Path.GetFileName(outFile)}...");

            try
            {
                if (overwrite && File.Exists(outFile))
                    File.Delete(outFile);

                using (var fileOut = new FileStream(outFile, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                using (var fileIn = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                using (var decompressor = new DeflateStream(fileIn, CompressionMode.Decompress))
                {
                    int length = 0;
                    byte[] buffer = new byte[BufferSize];

                    while ((length = decompressor.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileOut.Write(buffer, 0, length);
                    }

                    fileOut.Flush();
                }

                Console.WriteLine("Success");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed");
                var sb = new StringBuilder();
                AddException(ex, sb);
                Console.WriteLine($"{Environment.NewLine}Failed to decompress file {filePath}.{Environment.NewLine}{sb.ToString()}");
            }
        }
    }
}
#endif