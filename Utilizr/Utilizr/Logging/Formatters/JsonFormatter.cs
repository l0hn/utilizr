﻿using System.Collections.Generic;
using System.Linq;
using Utilizr.Logging.Interfaces;
using Utilizr.Conversion;
using Newtonsoft.Json;

namespace Utilizr.Logging.Formatters
{
    public class JsonFormatter : IFormatter
    {
        private static readonly List<string> _keyBlacklist = new List<string> {"Asctime", "Msg", "Extra"};

        public string Format(LogRecord record)
        {
            return JsonConvert.SerializeObject(
                ObjectToDictionary.Convert(record)
                    .Where(pair => !_keyBlacklist.Contains(pair.Key))
                    .ToDictionary(kvPair => kvPair.Key, kvPair => kvPair.Value)
            );
        }
    }
}
