﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using Utilizr.Logging.Interfaces;

namespace Utilizr.Logging.Handlers
{
    public abstract class BaseHttpHandler : Handler, IHandler
    {
        protected string GetEncodedMessage(LogRecord record)
        {     
            //TODO: Fix this
//            return WebUtility.UrlEncode(JsonConvert.SerializeObject(MapLogRecord(record)));
            return JsonConvert.SerializeObject(MapLogRecord(record));
        }

        public override void Flush() { }

        public override void Close() { }
    }

    /// <summary>
    /// A class which sends records to a Web server, using POST semantics.
    /// </summary>
    public class HttpPostHandler : BaseHttpHandler
    {
        protected HttpWebRequest request;

        public HttpPostHandler(string uri)
        {
            request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";
        }

        public string ContentType
        {
            get { return request.ContentType; }
            set { request.ContentType = value; }
        }

        protected override void Emit(LogRecord record)
        {
            string message = GetEncodedMessage(record);
            byte[] byteArray = Encoding.UTF8.GetBytes(message);
            request.ContentLength = byteArray.Length;

            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            {
                
            }
        }            
    }

    /// <summary>
    /// A class which sends records to a Web server, using GET semantics.
    /// </summary>
    public class HttpGetHandler : BaseHttpHandler
    {
        private string uri;
        public string messageKey = "message";

        public HttpGetHandler(string uri)
        {
            string seperator = uri.Contains("?") ? "&" : "?";
            this.uri = uri + seperator;
        }

        protected override void Emit(LogRecord record)
        {
            string message = GetEncodedMessage(record);
            string uri = this.uri + messageKey + "=" + message;
            HttpWebRequest request = (HttpWebRequest) HttpWebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            request.GetResponse();
        }
    }
}
