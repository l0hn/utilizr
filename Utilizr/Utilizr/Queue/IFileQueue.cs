﻿namespace Utilizr.Queue
{
    public interface IFileQueue<T>
    {
        int BufferCapacity { get; set; }
        void Enqueue(T item);
        T Dequeue(bool block = true, int timeout = 0);
        long Count { get; }
        bool IsEmpty { get; }
    }
}
