﻿#if IOS || ANDROID
using System.IO.Compression;
#else  
using Ionic.Zlib;
#endif

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Utilizr.Info;
#if MONO || ANDROID || IOS
#else
using Utilizr.Windows;
#endif

namespace Utilizr.Queue
{
    /// <summary>
    /// FileQueue is a thread safe file queue object.
    /// 
    /// FileQueue allows you to Enqueue objects into the queue, the same as a regular Queue but any overflow
    /// (number of items in the queue greater than bufferCapacity) will get compressed and put into a 
    /// temporary file on disk to keep excessive amounts of queued items out of memory.
    /// 
    /// Note: The order of the items Dequeued is guaranteed to be FIFO only if bufferCapacity isn't set.
    /// If bufferCapacity is set it will remain FIFO until the point at which the buffer overflows in which
    /// case you cannot rely on the order of the Dequeued items to be the same as they were Enqueued. It 
    /// isn't recommended to use this queue with bufferCapacity set if the order of the items Dequeued is
    /// important.
    /// </summary>
    /// <typeparam name="T">The type of object that the queue can hold</typeparam>
    public class FileQueue<T> : AbstractFileQueue<T>, IDisposable
    {
        private Queue<T> _buffer;
        protected const string _fileExtension = "queue";
        public string FilePath { get; } = Path.Combine(AppInfo.QueueDirectory, string.Format("{0}.{1}", Path.GetRandomFileName(), _fileExtension));
        private FileStream _readStream;
        private FileStream _writeStream;
        private GZipStream _gzipWriteStreamer;
        private GZipStream _gzipReadStreamer;
        private BinaryWriter _writeStreamer;
        private static int HEADER_LENGTH = sizeof(int);

        /// <summary>
        /// Initialises a new, empty, Utilizr.Queue.FileQueue
        /// </summary>
        public FileQueue()
        {
            _writeStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read);
            _readStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Write);
            _gzipWriteStreamer = new GZipStream(_writeStream, CompressionMode.Compress);
#if !IOS && !ANDROID
            _gzipWriteStreamer.FlushMode = FlushType.Sync;
#endif

            _gzipReadStreamer = new GZipStream(_readStream, CompressionMode.Decompress);
            _writeStreamer = new BinaryWriter(_gzipWriteStreamer);
        }

        /// <summary>
        /// Initialises a new, empty, Utilizr.Queue.FileQueue with an in-memory buffer of the size specified
        /// </summary>
        /// <param name="bufferCapacity">The max size of the in-memory buffer. The buffer will hold this many T objects</param>
        /// <exception cref="ArgumentException">Thrown if bufferCapacity is set to be a negative number.</exception>
        public FileQueue(int bufferCapacity) : this()
        {
            this.BufferCapacity = bufferCapacity;
        }

        private void _flushWriters()
        {
            _gzipWriteStreamer.Flush();
        }

        /// <summary>
        /// This is the size of the in-memory buffer, if set for the first time a buffer will be created else it will be resized.
        /// If the size is set to be lower than the current number of items in the buffer, the items will remain in the buffer but nothing new will be added until it drops below its new size.
        /// </summary>
        /// <exception cref="ArgumentException">bufferCapacity must be set as a positive value.</exception>
        public override int BufferCapacity
        {
            get { return base.BufferCapacity; }
            set
            {
                if (value > 0 && _buffer == null)
                {
                    _buffer = new Queue<T>();
                }
                base.BufferCapacity = value;
            }
        }

        private void _FileEnqueue(T item)
        {
            string jsonItem = JsonConvert.SerializeObject(item);
            byte[] jsonItemBytes = Encoding.Default.GetBytes(jsonItem);
            byte[] bytes = BitConverter.GetBytes(jsonItemBytes.Length);
            _writeStreamer.Write(bytes);
            _writeStreamer.Write(jsonItemBytes);
            _writeStreamer.Flush();
        }

        /// <summary>
        /// Adds an item to the Queue. 
        /// </summary>
        /// <param name="item">The item to put into the queue</param>
        public override void Enqueue(T item)
        {
            lock (LOCK)
            {
                if (_bufferCount < BufferCapacity)
                {
                    _buffer.Enqueue(item);
                }
                else
                {
                    try
                    {
                        _FileEnqueue(item);
                    }
                    catch (IOException ioEx)
                    {
#if MONO || ANDROID || IOS
                        throw;
#else
                        throw ProcessHelper.WhoIsLockingChecker(ioEx, FilePath);
#endif
                    }
                }
                _EnqueueDone();
            }
        }

        private byte[] _FileRead(int size)
        {
            byte[] buf = new byte[size];
            int readCount = 0;
            int amountRead;
            bool flushed = false;

            while (readCount < size)
            {
                while ((amountRead = _gzipReadStreamer.Read(buf, readCount, size - readCount)) == -1) { }

                readCount += amountRead;

                if (amountRead == 0 && flushed) // Shouldn't ever happen, just a safety net in case the file is externally modified
                {
                    _Count = 0;
                    throw new EmptyQueueException();
                }

                if (readCount < size)
                {
                    _flushWriters();
                    flushed = true;
                }
            }
            return buf;
        }

        private string _FileReadString(int size)
        {
            return Encoding.Default.GetString(_FileRead(size), 0, size);
        }

        private int _FileReadInt(int size)
        {
            return BitConverter.ToInt32(_FileRead(size), 0);
        }

        private T _FileDequeue()
        {
            int itemLength = _FileReadInt(HEADER_LENGTH);
            string line = _FileReadString(itemLength);
            T item = JsonConvert.DeserializeObject<T>(line);

            return item;
        }

        /// <summary>
        /// Takes an item from the queue and returns it.
        /// </summary>
        /// <param name="block">If optional arguments block is true (the default), block if necessary until an item is 
        /// available. Otherwise (block is false), return an item if one is immediatly available, else raise an Empty 
        /// exception.</param>
        /// <param name="timeout">The maximum number of milliseconds to block for (this parameter is ignored if block
        /// is set to false)</param>
        /// <returns>An item from the queue.</returns>
        /// <exception cref="Empty">Thrown if no item was available to dequeue</exception>
        /// <exception cref="ArgumentException">Thrown if timeout is passed as a negative value when trying to block</exception>
        public override T Dequeue(bool block = true, int timeout = 0)
        {
            lock (LOCK)
            {
                T item;
                while (true)  //while loops only here if the file became unexpectedly empty it can go back to waiting
                {
                    _BlockCheck(block, timeout);
                    if (_bufferCount > 0)
                    {
                        item = _buffer.Dequeue();
                    }
                    else
                    {
                        try
                        {
                            item = _FileDequeue();
                        }
                        catch (IOException ioEx)
                        {
#if MONO || ANDROID || IOS
                            throw;
#else
                            throw ProcessHelper.WhoIsLockingChecker(ioEx, FilePath);
#endif
                        }
                        catch (EmptyQueueException) 
                        {
                            continue; 
                        }
                    }
                    _DequeueDone();
                    return item;
                }
            }
        }

        private int _bufferCount
        {
            get
            {
                if (_buffer == null)
                {
                    return 0;
                }
                else
                {
                    return _buffer.Count;
                }
            }
        }

        /// <summary>
        /// Disposes the file handles and deletes the file on disk.
        /// </summary>
        public void Dispose()
        {
            _Count = 0;
            try
            {
                _buffer.Clear();
                _writeStreamer.Close();

#if IOS || ANDROID
                _gzipReadStreamer.Close();
#else
                //CRC ZlibException happens when the stream is not at the end of the file when its closed
                //Read to end of file to stop CRC ZlibException before closing the stream
                //This is needed even tho its already at the end of the file and this reads no bytes!
                byte[] buf = new byte[4096];
                //Read to end
                while (_gzipReadStreamer.Read(buf, 0, 1) > 0) { }
                //----------

                try
                {
                    _gzipReadStreamer.Close();
                }
                catch (ZlibException)
                {
                }
#endif

                File.Delete(FilePath);
            }
            catch (Exception)
            {
            }

            try
            {
                //release blocked threads
                Monitor.Pulse(LOCK);
            }
            catch (Exception)
            {
            }
        }
    }
}
