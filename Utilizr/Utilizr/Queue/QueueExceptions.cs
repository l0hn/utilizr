﻿using System;

namespace Utilizr.Queue
{
    public abstract class QueueException : Exception { }

    public class EmptyQueueException : QueueException { }

    public class FullQueueException : QueueException { }
}
