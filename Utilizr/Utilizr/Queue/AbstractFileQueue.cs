﻿using System;
using System.Threading;

namespace Utilizr.Queue
{
    public abstract class AbstractFileQueue<T> : IFileQueue<T>
    {
        private int _bufferCapacity = 0;
        protected long _Count = 0;
        protected object LOCK = new object();

        public virtual int BufferCapacity
        {
            get { return _bufferCapacity; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("'bufferCapacity' argument must be positive");
                }
                else
                {
                    lock (LOCK)
                    {
                        _bufferCapacity = value;
                    }
                }
            }
        }

        public abstract void Enqueue(T item);

        protected void _EnqueueDone()
        {
            _Count++;
            Monitor.Pulse(LOCK);
        }

        public abstract T Dequeue(bool block = true, int timeout = 0);

        protected void _DequeueDone()
        {
            _Count--;
        }

        protected void _BlockCheck(bool block, int timeout)
        {
            if (!block)
            {
                if (IsEmpty)
                {
                    throw new EmptyQueueException();
                }
            }
            else if (timeout == 0)
            {
                while (IsEmpty)
                {
                    Monitor.Wait(LOCK);
                }
            }
            else if (timeout < 0)
            {
                throw new ArgumentException("'timeout' argument must be a positive number");
            }
            else
            {
                DateTime endTime = DateTime.Now.AddMilliseconds(timeout);
                while (IsEmpty)
                {
                    DateTime now = DateTime.Now;
                    if (now > endTime)
                    {
                        throw new EmptyQueueException();
                    }
                    TimeSpan remaining = endTime - now;
                    Monitor.Wait(LOCK, remaining);
                }
            }
        }

        /// <summary>
        /// True if the queue is empty
        /// </summary>
        public bool IsEmpty
        {
            get { return Count <= 0; }
        }

        /// <summary>
        /// Returns the approximate number if items in the Queue, (not reliable !)
        /// </summary>
        public long Count
        {
            get { return _Count; }
        }
    }
}
