﻿using System;
using System.Collections.Generic;

namespace Utilizr.Queue
{

    /// PriorityFileQueue is a thread safe file queue object, with added support for prioritising the 
    /// order in which items are Dequeued.
    /// 
    /// PriorityFileQueue allows you to Enqueue objects into the queue, the same as a regular Queue but
    /// any overflow (number of items in the queue greater than bufferCapacity) will get compressed and 
    /// put into a temporary file on disk to keep excessive amounts of queued items out of memory.
    /// 
    /// Optionally an integer value priority can be passed when Enqueueing items, items Enqueued with a 
    /// higher priority (higher integer value) are guarenteed to be Dequeued before items with a lower 
    /// priority (lower integer value).
    /// 
    /// Note: The order of the items Dequeued that were Enqueued with the same priority is guaranteed to 
    /// be FIFO only if bufferCapacity isn't set. If bufferCapacity is set items of the same priority 
    /// will remain in a FIFO ordering until the point at which the buffer overflows in which case you 
    /// cannot rely on the order of the Dequeued items to be the same as they were Enqueued. It isn't 
    /// recommended to use this queue with bufferCapacity set if the order of the items Dequeued is
    /// important.
    public class PriorityFileQueue<T> : AbstractFileQueue<T>, IDisposable
    {
        private SortedDictionary<int, FileQueue<T>> _queues = 
            new SortedDictionary<int, FileQueue<T>>(new ReverseComparer<int>(Comparer<int>.Default));
        protected bool _hasBuffer = false;

        /// <summary>
        /// Initialises a new, empty, Utilizr.Queue.PriorityFileQueue
        /// </summary>
        /// <param name="defaultPriority">If no priority is specified when Enqueueing an item the
        /// defaultPriority value is used.</param>
        public PriorityFileQueue(int defaultPriority = 1)
        {
            this.defaultPriority = defaultPriority;
        }

        /// <summary>
        /// Initialises a new, empty, Utilizr.Queue.PriorityFileQueue
        /// </summary>
        /// <param name="bufferCapacity">The max size of the in-memory buffer for each priority level.
        /// The buffer will hold at most this many T objects Enqueued with the same priority.</param>
        /// <param name="defaultPriority">If no priority is specified when Enqueuing an item the
        /// defaultPriority value is used.</param>
        public PriorityFileQueue(int bufferCapacity, int defaultPriority = 1) : this(defaultPriority)
        {
            this.BufferCapacity = bufferCapacity;
        }

        /// <summary>
        /// If no priority is specified when Enqueuing an item the defaultPriority value is used.
        /// </summary>
        public int defaultPriority { get; set; }

        /// <summary>
        /// This is the size of the in-memory buffer for each priority level, if set for the first 
        /// time a buffer will be created, else it will be resized. If the size is set to be lower 
        /// than the current number of items in the buffer with that priority, the items will remain 
        /// in the buffer but nothing new will be added until it drops below its new size.
        /// </summary>
        /// <exception cref="ArgumentException">bufferCapacity must be set as a positive value.</exception>
        public override int BufferCapacity
        {
            get
            {
                return base.BufferCapacity;
            }
            set
            {
                base.BufferCapacity = value;
                foreach (FileQueue<T> queue in _queues.Values)
                {
                    queue.BufferCapacity = BufferCapacity;
                }
            }
        }

        private FileQueue<T> _GetQueue(int priority)
        {
            FileQueue<T> queue;
            if (!_queues.TryGetValue(priority, out queue))
            {
                queue = new FileQueue<T>(BufferCapacity);
                _queues.Add(priority, queue);
            }
            return queue;
        }

        /// <summary>
        /// Enqueues an item into the queue with the specified priority.
        /// </summary>
        /// <param name="item">The item to enqueue</param>
        /// <param name="priority">The priority of the item (higher the number, higher the priority)</param>
        public virtual void Enqueue(T item, int priority)
        {
            lock (LOCK)
            {
                _GetQueue(priority).Enqueue(item);
                _EnqueueDone();
            }
        }

        /// <summary>
        /// Enqueues an item into the queue with the specified priority.
        /// </summary>
        /// <param name="item">The item to enqueue</param>
        public override void Enqueue(T item)
        {
            this.Enqueue(item, defaultPriority);
        }

        /// <summary>
        /// Takes an item from the queue and returns it.
        /// </summary>
        /// <param name="block">If optional arguments block is true (the default), block if necessary until an item is 
        /// available. Otherwise (block is false), return an item if one is immediatly available, else raise an Empty 
        /// exception.</param>
        /// <param name="timeout">The maximum number of milliseconds to block for (this parameter is ignored if block
        /// is set to false)</param>
        /// <returns>An item from the queue.</returns>
        /// <exception cref="Empty">If no item was available to dequeue</exception>
        /// <exception cref="ArgumentException">Thrown if timeout is passed as a negative value when trying to block</exception>
        public override T Dequeue(bool block = true, int timeout = 0)
        {
            lock (LOCK)
            {
                while (true)
                {
                    _BlockCheck(block, timeout);
                    foreach (FileQueue<T> queue in _queues.Values)
                    {
                        try
                        {
                            T item = queue.Dequeue(false);
                            _DequeueDone();
                            return item;
                        }
                        catch (EmptyQueueException) { }
                    }
                }
            }
        }

        /// <summary>
        /// Disposes the file handles and deletes the file(s) on disk.
        /// </summary>
        public void Dispose()
        {
            foreach (FileQueue<T> queue in _queues.Values)
            {
                queue.Dispose();
            }
        }
    }
}
