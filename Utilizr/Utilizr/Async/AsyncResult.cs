﻿using System;
using System.Threading;

namespace Utilizr.Async
{
    public class AsyncResult<T> : IAsyncResult
    {
        private T _resultObject;
        private Exception _exception;
        private ManualResetEvent _waitHandle;
        private AsyncCallback _callback;

        public AsyncResult(AsyncCallback callback)
        {
            _callback = callback;
            _waitHandle = new ManualResetEvent(false);
        }

        public object ResultObject
        {
            get
            {
                return _resultObject;
            }
        }

        public WaitHandle AsyncWaitHandle
        {
            get { return _waitHandle; }
        }

        public bool CompletedSynchronously
        {
            get { return false; }
        }

        public bool IsCompleted { get; set; }
        public object AsyncState { get; set; }

        internal void SetComplete(T result)
        {
            _resultObject = result;
            SetComplete();
        }

        internal void SetComplete()
        {
            if (IsCompleted)
            {
                return;
            }
            IsCompleted = true;
            _waitHandle.Set();
            if (_callback != null)
            {
                _callback(this);
            }
        }

        internal void ThrowStoredException()
        {
            if (_exception != null)
            {
                throw _exception;
            }
        }

        internal void SetException(Exception ex)
        {
            _exception = ex;
        }

        public T EndExecute()
        {
            return AsyncHelper<T>.EndExecute(this);
        }
    }
}
