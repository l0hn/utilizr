﻿using System;
using System.Threading;

namespace Utilizr.Async
{
    /// <summary>
    /// A (very) simplified implementation of a synchronization barrier
    /// Do not expect great things here, Only supported functionality currently is AdddParticipant, RemoveParticipant and SignalAndWait.
    /// </summary>
    public class Barrier: IDisposable
    {
        Action _postPhaseAction;

        ManualResetEvent _oddPhaseComplete = new ManualResetEvent(false);
        ManualResetEvent _evenPhaseComplete = new ManualResetEvent(false);

        Exception _oddException;
        Exception _evenException;

        long _participantCount;
        long _currentPhase;

        public Barrier()
        {

        }

        public Barrier(int participantCount, Action postPhaseAction)
        {
            _participantCount = participantCount;
            _postPhaseAction = postPhaseAction;
        }

        public void AddParticipant()
        {
            if (Interlocked.Increment(ref _participantCount) == 1)
            {
                if (IsEven(Interlocked.Read(ref _currentPhase)))
                {
                    _evenException = null;
                }
                else
                {
                    _oddException = null;
                }
            }
            GetCurrentResetEvent().Reset();
        }


        void RemoveParticipant(bool wait)
        {
            if (Interlocked.Read(ref _participantCount) == 0)
                return;

            var phase = Interlocked.Read(ref _currentPhase);
            var resetEvent = GetCurrentResetEvent();
            var remaining = Interlocked.Decrement(ref _participantCount);
            if (remaining == 0)
            {
                Interlocked.Increment(ref _currentPhase);
                //this caller is last and responsible for post phase event
                try
                {
                    _postPhaseAction?.Invoke();
                }
                catch (Exception ex)
                {
                    if (IsEven(phase))
                    {
                        _evenException = ex;
                    }
                    else
                    {
                        _oddException = ex;
                    }
                    throw;
                }
                finally
                {
                    resetEvent.Set();
                }
                return;
            }

            if (wait)
            {
                resetEvent.WaitOne();
                ThrowExceptionForCompletedPhase(phase);
            }
        }

        public void RemoveParticipant()
        {
            RemoveParticipant(false);
        }

        public void SignalAndWait()
        {
            RemoveParticipant(true);
        }

        ManualResetEvent GetCurrentResetEvent()
        {
            if (IsEven(Interlocked.Read(ref _currentPhase)))
            {
                return _evenPhaseComplete;
            }
            return _oddPhaseComplete;
        }

        void ThrowExceptionForCompletedPhase(long phase)
        {
            Exception exception = null;
            if (IsEven(phase))
            {
                exception = _evenException;
            }
            else
            {
                exception = _oddException;
            }
            if (exception != null)
            {
                throw exception;
            }
        }

        bool IsEven(long num)
        {
            if (num % 2 == 0)
                return true;

            return false;
        }

        public void Dispose()
        {
            ((IDisposable) _oddPhaseComplete)?.Dispose();
            ((IDisposable) _evenPhaseComplete)?.Dispose();
        }
    }
}