﻿using System;
using System.Collections.Generic;
using System.Threading;
using Utilizr.Logging;

namespace Utilizr.Async
{
    public delegate void Action();

    public static class AsyncHelper<T>
    {
        public delegate T ExecutionDelegate();
        public static AsyncResult<T> BeginExecute(ExecutionDelegate executionDelegate, AsyncCallback callback = null, bool useThreadPool = true, object state = null, bool log = true)
        {
            AsyncResult<T> asyncResult = new AsyncResult<T>(callback);
            asyncResult.AsyncState = state;
            //Save current culture to apply to running thread
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            var currentUiCulture = Thread.CurrentThread.CurrentUICulture;
            try
            {
                Action<object> d = o =>
                {
                    object result = null;
                    try
                    {
                        //Copy culture to running thread
                        Thread.CurrentThread.CurrentCulture = currentCulture;
                        Thread.CurrentThread.CurrentUICulture = currentCulture;
                        result = executionDelegate();
                    }
                    catch (Exception ex)
                    {
                        if (log)
                            Log.Exception(ex);
                        
                        asyncResult.SetException(ex);
                    }
                    finally
                    {
                        if (result == null)
                        {
                            asyncResult.SetComplete();                            
                        }
                        else
                        {
                            asyncResult.SetComplete((T)result);   
                        }
                    }
                };
                if (useThreadPool)
                {
                    ThreadPool.QueueUserWorkItem(d.Invoke);
                }
                else
                {
                    Thread thread = new Thread(d.Invoke);
                    thread.IsBackground = true;
                    thread.Start();
                }

            }
            catch (Exception ex)
            {
                asyncResult.SetException(ex);
                asyncResult.SetComplete();
            }
            return asyncResult;
        }


        public static T EndExecute(AsyncResult<T> result)
        {
            result.AsyncWaitHandle.WaitOne();
            result.ThrowStoredException();
            return (T)result.ResultObject;
        }

        public static T EndExecute(IAsyncResult result)
        {
            return EndExecute((AsyncResult<T>)result);
        }

        public static void WaitComplete(IEnumerable<AsyncResult<T>> results)
        {
            foreach (var asyncResult in results)
            {
                asyncResult.AsyncWaitHandle.WaitOne();
            }
        }

        private static AsyncResult<T> GetAsyncResult(AsyncCallback callback = null)
        {
            return new AsyncResult<T>(callback) { AsyncState = null };
        }

        public static AsyncResult<T> Success(AsyncCallback callback = null)
        {
            var asyncResult = GetAsyncResult(callback);
            asyncResult.SetComplete();
            return asyncResult;
        }

        public static AsyncResult<T> Success(T result, AsyncCallback callback = null)
        {
            var asyncResult = GetAsyncResult(callback);
            asyncResult.SetComplete(result);
            return asyncResult;
        }

        public static AsyncResult<T> Fail(Exception err, AsyncCallback callback = null)
        {
            var asyncResult = GetAsyncResult(callback);
            asyncResult.SetException(err);
            asyncResult.SetComplete();
            return asyncResult;
        }
    }

    public static class AsyncHelper
    {
        public static IAsyncResult BeginExecute(Action executionDelegate, AsyncCallback callback = null, bool useThreadPool = true, object state = null)
        {
            return (IAsyncResult)AsyncHelper<bool>.BeginExecute(() => { executionDelegate(); return true; }, callback, useThreadPool, state);
        }

        public static void EndExecute(IAsyncResult result)
        {
            AsyncHelper<bool>.EndExecute(result);
        }

        public static void WaitComplete(IEnumerable<IAsyncResult> results)
        {
            foreach (var asyncResult in results)
            {
                asyncResult.AsyncWaitHandle.WaitOne();
            }
        }

        private static AsyncResult<bool> GetAsyncResult(AsyncCallback callback = null)
        {
            return new AsyncResult<bool>(callback) {AsyncState = null};
        }

        public static IAsyncResult Success(AsyncCallback callback = null)
        {
            var asyncResult = GetAsyncResult(callback);
            asyncResult.SetComplete();
            return asyncResult;
        }

        public static IAsyncResult Fail(Exception err, AsyncCallback callback = null)
        {
            var asyncResult = GetAsyncResult(callback);
            asyncResult.SetException(err);
            asyncResult.SetComplete();
            return asyncResult;
        }
    }
}
