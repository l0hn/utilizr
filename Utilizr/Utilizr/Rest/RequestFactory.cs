﻿using System;
using System.Net;
using System.Reflection;
using Utilizr.Logging;

namespace Utilizr.Rest
{
    public static class RequestFactory
    {
#if DEBUG
        const int DEFAULT_TIMEOUT = 20000;
#else
        const int DEFAULT_TIMEOUT = 20000;
#endif
        const int DEFAULT_READWRITE_TIMEOUT = 10000;

        static bool _allowUnsafeHeadersSet = false;
        private static bool _servicePointConfigured;
        
        private static object CONFIGURE_LOCK = new object();

#if __ANDROID__
        public static System.Net.Http.HttpClient Client = new System.Net.Http.HttpClient(new Xamarin.Android.Net.AndroidClientHandler());
#endif

        public static HttpWebRequest CreateStandardGetRequest(string url)
        {
            setAllowUnsafeHeaders();

            HttpWebRequest request = null;
            Uri uri;
            try
            {
                uri = new Uri(url);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, "Failed to create Uri object from provided url argument");
                throw;
            }

            configureServicePoint(uri);

            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Proxy = null;
            request.Method = WebRequestMethods.Http.Get;
            request.Timeout = DEFAULT_TIMEOUT;
            request.ReadWriteTimeout = DEFAULT_READWRITE_TIMEOUT;
            request.AllowWriteStreamBuffering = false;
            request.KeepAlive = true;
            request.ServicePoint.ConnectionLimit = 1000;

            return request;
        }

        public static HttpWebRequest CreateStandardPostRequest(string url, long contentLength, string httpMethod)
        {
            setAllowUnsafeHeaders();

            HttpWebRequest request = null;
            Uri uri;
            try
            {
                uri = new Uri(url);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, "Failed to create Uri object from provided url argument");
                throw;
            }

            configureServicePoint(uri);

            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Proxy = null;
            request.Method = httpMethod;
            request.ContentType = "text/json";
            if (contentLength > 0)
            {
                request.ContentLength = contentLength;
            }
            request.SendChunked = (contentLength == 0);
            request.Timeout = DEFAULT_TIMEOUT;
            request.ReadWriteTimeout = DEFAULT_READWRITE_TIMEOUT;
            request.AllowWriteStreamBuffering = false;
            request.KeepAlive = true;
            request.ServicePoint.ConnectionLimit = 1000;

            return request;
        }

        static void setAllowUnsafeHeaders()
        {
            if (_allowUnsafeHeadersSet)
            {
                return;
            }
            //Get the assembly that contains the internal class
#if !ANDROID && !IOS && !NETCOREAPP
            Assembly aNetAssembly = Assembly.GetAssembly(typeof(System.Net.Configuration.SettingsSection));
            if (aNetAssembly != null)
            {
                //Use the assembly in order to get the internal type for the internal class
                Type aSettingsType = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal");
                if (aSettingsType != null)
                {
                    //Use the internal static property to get an instance of the internal settings class.
                    //If the static instance isn't created allready the property will create it for us.
                    object anInstance = aSettingsType.InvokeMember("Section",
                        BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.NonPublic, null, null, new object[] { });

                    if (anInstance != null)
                    {
                        //Locate the private bool field that tells the framework is unsafe header parsing should be allowed or not
                        FieldInfo aUseUnsafeHeaderParsing = aSettingsType.GetField("useUnsafeHeaderParsing", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (aUseUnsafeHeaderParsing != null)
                        {
                            aUseUnsafeHeaderParsing.SetValue(anInstance, true);
                        }
                    }
                }
            }
#endif
            _allowUnsafeHeadersSet = true;
        }

        static void configureServicePoint(Uri uri)
        {
            if (_servicePointConfigured)
                return;

            lock (CONFIGURE_LOCK)
            {
                if (_servicePointConfigured)
                    return;
                
                ServicePointManager.DefaultConnectionLimit = 200;
                WebRequest.DefaultWebProxy = null;
                ServicePointManager.Expect100Continue = false;

                _servicePointConfigured = true;
            }
        }
    }
}
