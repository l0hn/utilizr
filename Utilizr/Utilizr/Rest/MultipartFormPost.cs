﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Utilizr.Conversion;

namespace Utilizr.Rest
{
    public static class MultipartFormPost
    {
        static Encoding _encoding = Encoding.UTF8;

        public static HttpWebResponse Post(string url, Dictionary<string, object> formParams, Dictionary<string, string> headers, int requestTimeout = 60000)
        {
            var boundary = $"----------{Guid.NewGuid()}";
            var contentType = $"multipart/form-data; boundary={boundary}";

            var lengthContainer = new LengthContainer();
            
            var formGenerator = GenerateFormData(formParams, boundary, out var formLength);

            var request = RequestFactory.CreateStandardPostRequest(url, formLength, "POST");
            request.Timeout = requestTimeout;
            request.ContentType = contentType;
            foreach (var header in headers)
            {
                request.Headers[header.Key] = header.Value;
            }

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    long written = 0;
                    foreach (var bufferReturn in formGenerator)
                    {
                        requestStream.Write(bufferReturn.Buffer, 0, bufferReturn.Length);
                        written += bufferReturn.Length;
                        Debug.WriteLine($"wrote chuck [{bufferReturn.Length}] total:[{written}] target[{formLength}]");
                    }
                    requestStream.Close();
                }

                return request.GetResponse() as HttpWebResponse;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        private static IEnumerable<BufferReturn> GetFormEnumerator(List<ParamHolder> paramList, byte[] crlf, byte[] footer)
        {
            var bufReturn = new BufferReturn();
            bufReturn.Buffer = new byte[100 * 1024];
            var addCrlf = false;

            foreach (var paramHolder in paramList)
            {
                if (addCrlf)
                {
                    yield return new BufferReturn() { Buffer = crlf, Length = crlf.Length };
                }
                addCrlf = true;

                yield return new BufferReturn()
                {
                    Buffer = paramHolder.Header,
                    Length = paramHolder.Header.Length,
                };

                Stream stream;
                bool disposeStream = true;

                if (paramHolder.Param is FileUploadParam fup)
                {
                    if (fup.StreamMode)
                    {
                        stream = fup.FileStream;
                        disposeStream = false;
                    }
                    else
                    {
                        stream = new MemoryStream(fup.FileData);
                    }
                }
                else
                {
                    stream = new MemoryStream(paramHolder.FieldData);
                }

                try
                {
                    stream.Position = 0;
                    long totalRead = 0;
                    while (true)
                    {
                        var amountRead = stream.Read(bufReturn.Buffer, 0, bufReturn.Buffer.Length);
                        if (amountRead == 0)
                            break;
                        totalRead += amountRead;
                        bufReturn.Length = amountRead;

                        Debug.WriteLine($"read {amountRead:N} bytes from form data stream [total={totalRead:N}]");

                        yield return bufReturn;
                    }
                }
                finally
                {
                    if (disposeStream)
                    {
                        stream.Dispose();
                    }
                }
            }

            yield return new BufferReturn() {Buffer = footer, Length = footer.Length};
        }
        
        private static IEnumerable<BufferReturn> GenerateFormData(Dictionary<string, object> formParams, string boundary, out long formLength)
        {
            byte[] data;
            //calculate length
            var fileTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";
            var fieldTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n";
            var footer = _encoding.GetBytes($"\r\n--{boundary}--\r\n");

            var paramList = new List<ParamHolder>();

            long length = 0;

            var crlf = _encoding.GetBytes("\r\n");
            var addCrlf = false;

            foreach (var formParam in formParams)
            {
                var newParam = new ParamHolder();
                newParam.Name = formParam.Key;
                newParam.Param = formParam.Value;
                if (formParam.Value is FileUploadParam fup)
                {
                    newParam.IsFile = true;
                    newParam.DataLength = fup.Length;
                    newParam.Header = _encoding.GetBytes(string.Format(fileTemplate,
                        boundary,
                        newParam.Name,
                        fup.FileName ?? newParam.Name,
                        fup.ContentType ?? "application/octet-stream"));
                }
                else
                {
                    newParam.IsFile = false;
                    newParam.FieldData = _encoding.GetBytes(formParam.Value.ToString());
                    newParam.DataLength = newParam.FieldData.Length;
                    newParam.Header = _encoding.GetBytes(string.Format(fieldTemplate,
                        boundary,
                        newParam.Name));
                }
                paramList.Add(newParam);
                length += newParam.Header.Length + newParam.DataLength;
                if (addCrlf)
                {
                    length += crlf.Length;
                }

                addCrlf = true;
            }

            length += footer.Length;
            formLength = length;

            return GetFormEnumerator(paramList, crlf, footer);
        }
    }

    public class FileUploadParam
    {
        public Stream FileStream { get; set; }
        public byte[] FileData { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }

        public bool StreamMode { get; set; }

        public long Length {
            get { return StreamMode ? FileStream.Length : FileData.Length; }
        }

        public FileUploadParam(Stream stream, string filename, string contentType = "applicaton/octet-stream")
        {
            FileStream = stream;
            StreamMode = true;
            FileName = filename;
            ContentType = contentType;
        }

        public FileUploadParam(byte[] fileData, string fileName, string contentType = "application/octet-stream")
        {
            FileData = fileData;
            FileName = fileName;
            ContentType = contentType;
            StreamMode = false;
        }
    }

    internal class ParamHolder
    {
        public string Name { get; set; }
        public object Param { get; set; }
        public byte[] Header { get; set; }
        public long DataLength { get; set; }

        public bool IsFile { get; set; }

        /// <summary>
        /// Only present if IsFile is False
        /// </summary>
        public byte[] FieldData { get; set; }
    }

    internal class LengthContainer
    {
        public long Length { get; set; }
    }

    internal class BufferReturn
    {
        public byte[] Buffer { get; set; }
        public int Length { get; set; }
    }
}
