#if !MONO
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilizr.Collections;
using Utilizr.Logging;

namespace Utilizr.Rest
{    
    [Serializable]
    [DataContract]
    public class RequestMocker : INotifyPropertyChanged
    {
        private bool _active;
        [DataMember]
        public bool Active
        {
            get { return _active; }
            set
            {
                _active = value;
                OnPropertyChanged(nameof(Active));
            }
        }

        [DataMember]
        public Type RequestType { get; set; }

        [DataMember]
        public string RequestTypeName
        {
            get { return RequestType.Name; }
            set
            {
                foreach (var type in RestClient.RequestTypes)
                {
                    if (value == type.Name)
                    {
                        RequestType = type;
                        OnPropertyChanged(nameof(RequestType));

                        return;
                    }
                }

                RequestType = typeof(AllRequestsType);
                OnPropertyChanged(nameof(RequestType));
            }
        }

        private ApiMockTypeEnum _type;
        [DataMember]
        public ApiMockTypeEnum Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        private string _fullResponse = "{}";
        [DataMember]
        public string FullResponse
        {
            get { return _fullResponse; }
            set
            {
                if (_fullResponse == value)
                    return;

                _fullResponse = value;
                OnPropertyChanged(nameof(FullResponse));
            }
        }

        private ObservableCollection<PartialUpdate> _partialMockUpdates;
        [DataMember]
        public ObservableCollection<PartialUpdate> PartialMockUpdates
        {
            get { return _partialMockUpdates; }
            set
            {
                _partialMockUpdates = value;
                OnPropertyChanged(nameof(PartialMockUpdates));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public RequestMocker()
        {
            _partialMockUpdates = new ObservableCollection<PartialUpdate>();
            _partialMockUpdates.CollectionChanged += PartialMockUpdatesChanged;
        }

        public void PartialMockUpdatesChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (PartialUpdate item in e.OldItems)
                {
                    //Removed items
                    item.PropertyChanged -= PartialUpdatePropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (PartialUpdate item in e.NewItems)
                {
                    //Added items
                    item.PropertyChanged += PartialUpdatePropertyChanged;
                }
            }
        
            OnPropertyChanged(nameof(PartialMockUpdates));
        }

        public void PartialUpdatePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(PartialMockUpdates));
        }

        public string UpdatePathsInPartialMockResponse(string originalResponse)
        {
            if (PartialMockUpdates == null || PartialMockUpdates.Count == 0)
            {
                return originalResponse;
            }

            var response = JObject.Parse(originalResponse);

            foreach (var update in PartialMockUpdates)
            {
#if DEBUG
                Log.Info("REQUEST_MOCKER", "Updating partial value in response {0} : [{1}]", update.Path, update.Value);
#endif
                if (update.Path.Contains("."))
                {
                    response = UpdateResponseWithPartialUpdate(
                        response, 
                        update.Path.Split(".".ToCharArray()),
                        update.Value
                    );
                }
                else
                {
                    response = UpdateResponseWithPartialUpdate(
                        response,
                        new [] {update.Path},
                        update.Value
                    );
                }
            }

            return response.ToString(Formatting.None);
        }

        private JObject UpdateResponseWithPartialUpdate(JObject response, string[] path, string value)
        {
            if (path.Length == 1)
            {
                var property = response.Property(path[0]);
                if (property != null)
                {
                    if (int.TryParse(value, out int intValue))
                    {
                        property.Value = intValue;
                    }
                    else if (int.TryParse(value, out int doubleValue))
                    {
                        property.Value = doubleValue;
                    }
                    else if (value == "true")
                    {
                        property.Value = true;
                    }
                    else if (value == "false")
                    {
                        property.Value = false;
                    }
                    else
                    {
                        property.Value = value;
                    }
                }
            }
            else
            {
                var property = response.Property(path[0]);
                if (property != null)
                {
                    if (property.Value.Type == JTokenType.Object)
                    {
                        property.Value = UpdateResponseWithPartialUpdate(
                            property.Value.ToObject<JObject>(),
                            path.Skip(1).ToArray(), 
                            value
                        );
                    }
                    if (property.Value.Type == JTokenType.Array)
                    {
                        property.Value = UpdateArrayWithPartialUpdate(
                            property.Value.ToObject<JArray>(),
                            path.Skip(1).ToArray(),
                            value
                        );
                    }
                }
            }

            return response;
        }

        private JArray UpdateArrayWithPartialUpdate(JArray response, string[] path, string value)
        {
            if (path.Length == 1)
            {
                if (int.TryParse(path[0], out int intKey))
                {
                    var property = response[intKey];
                    if (property != null)
                    {
                        if (int.TryParse(value, out int intValue))
                        {
                            response[intKey] = intValue;
                        }
                        else if (int.TryParse(value, out int doubleValue))
                        {
                            response[intKey] = doubleValue;
                        }
                        else if (value == "true")
                        {
                            response[intKey] = true;
                        }
                        else if (value == "false")
                        {
                            response[intKey] = false;
                        }
                        else
                        {
                            response[intKey] = value;
                        }
                    }
                }
            }
            else
            {
                if (int.TryParse(path[0], out int intKey))
                {
                    var property = response[intKey];
                    if (property != null)
                    {
                        if (response[intKey].Type == JTokenType.Object)
                        {
                            response[intKey] = UpdateResponseWithPartialUpdate(
                                response[intKey].ToObject<JObject>(),
                                path.Skip(1).ToArray(),
                                value
                            );
                        }
                        if (response[intKey].Type == JTokenType.Array)
                        {
                            response[intKey] = UpdateArrayWithPartialUpdate(
                                response[intKey].ToObject<JArray>(),
                                path.Skip(1).ToArray(),
                                value
                            );
                        }
                    }
                }
            }

            return response;
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }
    }

    [DataContract]
    public class PartialUpdate : INotifyPropertyChanged
    {
        private string _path;
        [DataMember]
        public string Path
        {
            get { return _path; }
            set
            {
                if (_path == value)
                    return;

                _path = value;
                OnPropertyChanged(nameof(Path));
            }
        }

        private string _value;
        [DataMember]
        public string Value
        {
            get { return _value; }
            set
            {
                if (_value == value)
                    return;

                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }
    }

    public class RequestMockerException : Exception
    {
        public Dictionary<string, object> Headers;
        public string Response;
        public int StatusCode;
        public string StatusDescription;
    }

    public enum ApiMockTypeEnum
    {
        FullResponse,
        PartialUpdate
    }

    public class AllRequestsType
    {

    }    
}
#endif