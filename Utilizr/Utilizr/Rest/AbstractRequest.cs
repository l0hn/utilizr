﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Utilizr.Async;
using Utilizr.Conversion;
using Utilizr.Info;
using Utilizr.Logging;

namespace Utilizr.Rest
{
    public abstract class AbstractRequest<TResponse>
    {
        RestClient _client;

        protected RestClient RestClient { get { return _client; } }

        [JsonIgnore]
        public abstract string Endpoint { get; }
        [JsonIgnore]
        public abstract RequestMethod Method { get; }

        [JsonIgnore]
        protected virtual int RequestTimeout => 20000;

        [JsonIgnore] protected virtual bool LogRequests => true;

#if MONO
        protected AbstractRequest()
        {
           
        }
#else
        [JsonIgnore] public RequestMocker RequestMocker { get; set; }
        protected AbstractRequest()
        {
#if DEBUG
            RequestMocker = new RequestMocker();
#endif
        }
#endif

        public IAsyncResult Execute(RestClient client, AsyncCallback callback = null)
        {
#if !MONO
            if (client.RequestMockers != null)
            {
                if (client.RequestMockers.ContainsKey(this.GetType()))
                {
                    RequestMocker = client.RequestMockers[this.GetType()];
                }
                else if (client.RequestMockers.ContainsKey(typeof(AllRequestsType)))
                {
                    RequestMocker = client.RequestMockers[typeof(AllRequestsType)];
                }
                else
                {
                    RequestMocker = new RequestMocker();
                }
            }
            else
            {
                RequestMocker = new RequestMocker();
            }
#endif
            _client = client;
            return AsyncHelper<TResponse>.BeginExecute(execute, callback);
        }

        public TResponse EndExecute(IAsyncResult result)
        {
            return AsyncHelper<TResponse>.EndExecute(result);
        }

        internal string EndpointLogStr
        {
            get
            {
                return "({0}){1}".format(Method, Endpoint);
            }
        }

        internal string FullUrlLogStr
        {
            get
            {
                return "({0}){1}/{2}".format(Method, _client.ServiceUrl, Endpoint);
            }
        }

        internal string FullUrl
        {
            get
            {
                return "{0}/{1}".format(_client.ServiceUrl, Endpoint);
            }
        }

        TResponse execute()
        {
            switch (Method)
            {
                case RequestMethod.GET:
                case RequestMethod.DELETE:
                    return executeGetOrDelete(Method.ToString());
                case RequestMethod.PUT:
                case RequestMethod.POST:
                    return executePutOrPost(Method.ToString());
                default:
                    throw new NotSupportedException("Only GET / PUT / POST / DELETE be supported 'ere they be, now be orf with yer");
            }
        }

        TResponse executeGetOrDelete(string method)
        {
            var fullUrlWithParams = ApplyRequestSpecificQueryParameters(FullUrl);

            var request = RequestFactory.CreateStandardGetRequest(fullUrlWithParams);
            request.Method = method;
            request.Timeout = RequestTimeout;
            ApplyClientSpecificHeaders(request, "");
            ApplyRequestSpecificHeaders(request);
            
            logRequest(request);
            var sw = new Stopwatch();
            sw.Start();
            var responseData = getResponseWithLogging(request);
            sw.Stop();
            var responseObject = deserialiseResponse(responseData);
            PostProcess(responseObject);

            if (sw.ElapsedMilliseconds >= 2000)
            {
                try
                {
                    _client.ReportSlowRequest(this, sw.ElapsedMilliseconds);
                }
                catch (Exception e)
                {
                    Log.Exception(e);
                }
            }
            
            return responseObject;
        }

        private void PostProcess(TResponse responseObject)
        {
            PostProcessing(responseObject);
            RestClient.NotifyRequestCompleted(this, responseObject);
        }

        TResponse deserialiseResponse(string responseStr)
        {
            try
            {
                if (responseStr.IsNullOrEmpty())
                {
                    return EmptyResponseHandler();
                }

                return _client.Serializer.DeserializeResponseData<TResponse>(responseStr);
            }
            catch (Exception ex)
            {
                Log.Exception("REST", ex, "Invalid data returned from {0}".format(EndpointLogStr));
                throw;
            }
        }

        protected virtual TResponse EmptyResponseHandler()
        {
            return default;
        }

        TResponse executePutOrPost(string httpMethod)
        {
            var serializedRequest = _client.Serializer.SerializeRequestData(this); 
            var postData = Encoding.UTF8.GetBytes(serializedRequest);
            var contentLength = postData.Length;
            var fullUrlWithParams = ApplyRequestSpecificQueryParameters(FullUrl);
            var request = RequestFactory.CreateStandardPostRequest(fullUrlWithParams, contentLength, httpMethod);
            request.Timeout = RequestTimeout;
            ApplyClientSpecificHeaders(request, serializedRequest);
            ApplyRequestSpecificHeaders(request);
            logRequest(request, serializedRequest);
#if DEBUG && !MONO
            if (!RequestMocker.Active || (RequestMocker.Active && RequestMocker.Type == ApiMockTypeEnum.PartialUpdate))
            {
                try
                {
                    using (var requestStream = request.EndGetRequestStream(request.BeginGetRequestStream(null, null)))
                    {
                        requestStream.Write(postData, 0, postData.Length);
                    }
                }
                catch (WebException ex)
                {
                    logWebException(ex);
                    throw;
                }
            }
#else
            try
            {
                using (var requestStream = request.EndGetRequestStream(request.BeginGetRequestStream(null, null)))
                {
                    requestStream.Write(postData, 0, postData.Length);
                }
            }
            catch (WebException ex)
            {
                logWebException(ex);
                throw;
            }
#endif

            var sw = new Stopwatch();
            sw.Start();
            var responseData = getResponseWithLogging(request);
            sw.Stop();
            //json decode response
            var responseObj = deserialiseResponse(responseData);
            //perform post processing on 200 Status codes
            PostProcess(responseObj);

            try
            {
                if (sw.ElapsedMilliseconds >= 2000)
                {
                    _client.ReportSlowRequest(this, sw.ElapsedMilliseconds);
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
            
            return responseObj;
        }

        void ApplyClientSpecificHeaders(HttpWebRequest request, string postData)
        {
            try
            {
                _client.AddRequestHeaders(request, postData, this);
            }
            catch (Exception ex)
            {
                Log.Exception("REST", ex, "Error adding headers to request");
                throw;
            }
        }

        void logWebException(WebException ex)
        {
            try
            {
                using (var httpResponse = (HttpWebResponse) ex.Response)
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var error = reader.ReadToEnd();
                    logWebError(
                        ex.Response.Headers.AllKeys.ToDictionary(i => i, i => (object) ex.Response.Headers[i]),
                        error,
                        (int) httpResponse.StatusCode,
                        httpResponse.StatusDescription
                    );
                }
            }
            catch (ApiException)
            {
                throw;
            }
            catch (Exception)
            {

            }
        }

        void logWebError(Dictionary<string, object> headers, string response, int statusCode, string statusDescription)
        {
            var debugDict = new Dictionary<string, object>();
            try
            {
                debugDict = headers;
                debugDict["Status Code"] = statusCode;
                debugDict["Status Description"] = statusDescription;
                debugDict["Error Response"] = response;
                throw new ApiException(statusCode, statusDescription, response);
            }
            catch (ApiException apiEx)
            {
                if (debugDict.Count == 0)
                {
                    Log.Error("REST", apiEx.ToString());
                }
                if (apiEx.StatusCode == (int)HttpStatusCode.Unauthorized)
                {
                    _client.MarkAuthenticationInvalid(apiEx.StatusCode, apiEx.Message);
                }
                throw;
            }
            catch (Exception)
            {

            }
            finally
            {
                if (debugDict.Count > 0)
                {
                    Log.Objects(LoggingLevel.ERROR, "REST", new[] { debugDict }, "Request error from {0}".format(EndpointLogStr));
                    _client.ReportHttpError(this, statusCode, response);
                }
            }
        }

        void logRequest(HttpWebRequest request, string postData = "")
        {
            if (!LogRequests)
                return;

            var debugDict = new Dictionary<string, object>();
            debugDict["Endpoint"] = Endpoint;
            debugDict["Headers"] = request.Headers.AllKeys.ToDictionary(i => i, i => request.Headers[i]);
            debugDict["RequestParams"] = GetObjectForRequestLogging();
            debugDict["FullUrl"] = FullUrlLogStr;

            Log.Objects(LoggingLevel.INFO, "REST", new[] { debugDict }, " Executing api request {0}".format(EndpointLogStr));
        }


        string getResponseWithLogging(HttpWebRequest request)
        {
#if DEBUG && !MONO
            if (RequestMocker.Active && RequestMocker.Type == ApiMockTypeEnum.FullResponse)
            {
                try
                {
                    return RequestMocker.FullResponse;
                }
                catch (RequestMockerException e)
                {
                    logWebError(
                        e.Headers,
                        e.Response,
                        e.StatusCode,
                        e.StatusDescription
                    );
                    throw;
                }
            }
#endif

            var responseData = "";

            try
            {
                using (var response = (HttpWebResponse)request.EndGetResponse(request.BeginGetResponse(null, null)))
                using (var responseStream = response.GetResponseStream())
                using (var reader = new StreamReader(responseStream, Encoding.UTF8))
                {
                    responseData = reader.ReadToEnd();
#if DEBUG && !MONO
                    if (RequestMocker.Active && RequestMocker.Type == ApiMockTypeEnum.PartialUpdate)
                    {
                        responseData = RequestMocker.UpdatePathsInPartialMockResponse(responseData);
                    }
#endif
                    logResponse(responseData, response.Headers.AllKeys.ToDictionary(i => i, i => response.Headers[i]));
                }
            }
            catch (WebException ex)
            {
                logWebException(ex);
                throw;
            }

            return responseData;
        }

        void logResponse(string responseData, object headers)
        {
#if DEBUG
            Log.Info("REST", "Received response (RAW) from api request {0} : [{1}]", EndpointLogStr, responseData);
#endif
            
            if (LogRequests)
            {
                var debugDict = new Dictionary<string, object>();
                debugDict["Endpoint"] = Endpoint;
                debugDict["Headers"] = headers;
                try
                {
                    debugDict["responseData"] = GetObjectForResponseLogging(_client.Serializer.DeserializeResponseData<TResponse>(responseData));
                }
                catch (Exception)
                {
                    debugDict["responseData"] = responseData;
                }

                Log.Objects(LoggingLevel.INFO, "REST", new[] { debugDict }, "Received response from api request {0}".format(EndpointLogStr));
            }
        }

        protected virtual void ApplyRequestSpecificHeaders(HttpWebRequest request) {}

        /// <summary>
        /// Add any URL query parameters before to the given url.
        /// </summary>
        protected virtual string ApplyRequestSpecificQueryParameters(string fullUrl) { return fullUrl; }

        /// <summary>
        /// Override post processing to perform internal tasks upon a successful response
        /// NOTE: this method is NOT async, do not write blocking code in this method
        /// </summary>
        /// <param name="response">Response.</param>
        protected virtual void PostProcessing(TResponse response) { }

        /// <summary>
        /// Gets the object for request logging.
        /// You should override this method is for example you need to remove any sensitive information from a specific object before logging
        /// </summary>
        /// <returns>The object for request logging.</returns>
        protected virtual object GetObjectForRequestLogging()
        {
            return this;
        }

        /// <summary>
        /// Gets the object for response logging. Override this method if you need to remove sensitive data before logging
        /// Note, the changes you make on this response object only affect logging.
        /// </summary>
        /// <returns>The object for response logging.</returns>
        protected virtual TResponse GetObjectForResponseLogging(TResponse response)
        {
            return response;
        }
    }

    public enum RequestMethod
    {
        GET,
        PUT,
        POST,
        DELETE,
    }
    
}
