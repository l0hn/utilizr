﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilizr.Rest
{
    public interface IRestSerializer
    {
        TResponse DeserializeResponseData<TResponse>(string responseStr);

        string SerializeRequestData<TResponse>(AbstractRequest<TResponse> request);
    }
}