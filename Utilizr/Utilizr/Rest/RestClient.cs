﻿using System;
using System.Collections.Generic;
using System.Net;
using Utilizr.Logging;

namespace Utilizr.Rest
{
    public delegate void PostRequestHook<TRequest, TResponse>(TRequest request, TResponse response) where TRequest : AbstractRequest<TResponse>;

    public delegate void AnyResponseHook(object data, Type requestType);

    class HookHolder
    {
        public object HookDelegate { get; set; }
        public Type RequestType { get; set; }
    }
    
    public class AuthErrorEventArgs : EventArgs
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }

    public class SlowRequestEventArgs : EventArgs
    {
        public string RequestEndpoint { get; }
        public long RequestTime { get;  }

        public SlowRequestEventArgs(string endpoint, long requestTime)
        {
            RequestEndpoint = endpoint;
            RequestTime = requestTime;
        }
    }
    
    public class HttpErrorEventArgs : EventArgs
    {
        public string RequestEndpoint { get; }
        public int StatusCode { get;  }
        public string Response { get;  }

        public HttpErrorEventArgs(string endpoint, int statusCode, string response)
        {
            RequestEndpoint = endpoint;
            StatusCode = statusCode;
            Response = response;
        }
    }

    public abstract class RestClient
    {
#if !MONO
        public Dictionary<Type, RequestMocker> RequestMockers { get; set; }
#endif
        public event EventHandler<HttpErrorEventArgs> AnyHttpError;
        public event EventHandler<SlowRequestEventArgs> AnySlowRequest;

        private static List<Type> _requestTypes;
        public static List<Type> RequestTypes
        {
            get
            {
                if (_requestTypes != null)
                {
                    return _requestTypes;
                }

                _requestTypes = new List<Type>();

                foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var t in asm.GetTypes())
                    {
                        if (!t.IsAbstract &&
                            t.IsClass &&
                            t.BaseType != null &&
                            t.BaseType.IsGenericType &&
                            t.BaseType.GetGenericTypeDefinition() == typeof(AbstractRequest<>))
                        {
                            _requestTypes.Add(t);
                        }
                    }
                }

                return _requestTypes;
            }
        }

        

        public event AnyResponseHook ResponseRecieved;
        private List<HookHolder> _hooks = new List<HookHolder>();
        /// <summary>
        /// Occurs when api credentials become invalid.
        /// User will be required to re-authenticate if this event is raised.
        /// </summary>
        public event EventHandler<AuthErrorEventArgs> AuthenticationError;
        public virtual string ServiceUrl { get; set; }
        public IRestSerializer Serializer { get; set; }

        protected RestClient(IRestSerializer serializer)
        {
            Serializer = serializer;
        }

        /// <summary>
        /// Executes the request synchronously, Please only use this for specific scenarios such as unit testing
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="request">Request.</param>
        /// <typeparam name="TResponse">The 1st type parameter.</typeparam>
        public TResponse ExecuteRequest<TResponse>(AbstractRequest<TResponse> request)
        {
            return EndExecuteRequest(BeginExecuteRequest(request, null), request);
        }

        public IAsyncResult BeginExecuteRequest<TResponse>(AbstractRequest<TResponse> request, AsyncCallback callback = null)
        {
            return request.Execute(this, callback);
        }

        public TResponse EndExecuteRequest<TResponse>(IAsyncResult result, AbstractRequest<TResponse> request)
        {
            return request.EndExecute(result);
        }

        internal void ReportSlowRequest<TResponse>(AbstractRequest<TResponse> request, long requestTime)
        {
            try
            {
                OnAnySlowResponse(request, requestTime);
            }
            catch (Exception e)
            {
                Log.Exception("api_client", e);
            }
            
        }

        internal void ReportHttpError<TResponse>(AbstractRequest<TResponse> request, int code, string message)
        {
            try
            {
                OnAnyHttpError(request, code, message);
            }
            catch (Exception e)
            {
                Log.Exception("api_client", e);
            }
        }
        
        internal void MarkAuthenticationInvalid(int code, string message)
        {
            OnAuthenticationError(code, message);
        }

        protected virtual void OnAuthenticationError(int code, string message)
        {
            AuthenticationError?.Invoke(this, new AuthErrorEventArgs() { Code = code, Message = message });
        }

        internal void NotifyRequestCompleted<TResponse>(AbstractRequest<TResponse> request, TResponse response)
        {
            foreach (var hookHolder in _hooks)
            {
                try
                {
                    if (hookHolder.RequestType == request.GetType())
                    {
                        ((Delegate)hookHolder.HookDelegate).DynamicInvoke(request, response);
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception("REST_HOOKS", ex);
                }
            }

            OnAnyResponseRecieved(response, request.GetType());
        }

        /// <summary>
        /// Hook will fire before the <see cref="ResponseRecieved"/> fires on the same response.
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="hook"></param>
        public void AddCustomRequestCompleteHook<TRequest, TResponse>(PostRequestHook<TRequest, TResponse> hook)
            where TRequest : AbstractRequest<TResponse>
        {
            var hookHolder = new HookHolder()
            {
                HookDelegate = hook,
                RequestType = typeof(TRequest)
            };
            _hooks.Add(hookHolder);
        }

        protected virtual void OnAnyResponseRecieved(object baseResponseData, Type requestType)
        {
            try
            {
                ResponseRecieved?.Invoke(baseResponseData, requestType);
            }
            catch (Exception ex)
            {
                Log.Exception("REST_HOOK", ex);
            }
        }

        protected virtual void OnAnyHttpError<TResponse>(AbstractRequest<TResponse> request, int statusCode, string response)
        {
            try
            {
                AnyHttpError?.Invoke(this, new HttpErrorEventArgs(request.EndpointLogStr, statusCode, response));
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        protected virtual void OnAnySlowResponse<TResponse>(AbstractRequest<TResponse> request, long requestTime)
        {
            try
            {
                AnySlowRequest?.Invoke(this, new SlowRequestEventArgs(request.EndpointLogStr, requestTime));
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        public abstract void AddRequestHeaders<TResponse>(HttpWebRequest request, string postData, AbstractRequest<TResponse> requestObj);
    }

    public class AnyResponseData
    {
        public object ResultObject { get; set; }
        public Type RequestType { get; set; }

        public static AnyResponseData FromResponseData<TResponse>(TResponse response, Type requestType)
        {
            return new AnyResponseData()
            {
                ResultObject = response,
                RequestType = requestType,
            };
        }

        private AnyResponseData()
        {

        }
    }
}
