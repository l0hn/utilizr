﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Utilizr.Rest
{
    public class JsonRestSerializer: IRestSerializer
    {
        public TResponse DeserializeResponseData<TResponse>(string responseStr)
        {
            return JsonConvert.DeserializeObject<TResponse>(responseStr);
        }

        public string SerializeRequestData<TResponse>(AbstractRequest<TResponse> request)
        {
            return JsonConvert.SerializeObject(request);
        }
    }
}
