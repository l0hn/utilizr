﻿using System;
namespace Utilizr
{
    public class MapUtil
    {
        public double NwSwLog = 0;
        public double SwNeLog = 0;
        public double WeDirect = 0;
        public double NsDirect = 0;
        public double MapWidth { get; set; }
        public double MapHeight { get; set; }

        public MapUtil()
        {
            Calibrate(-61, 295, 306, -57, 1000, 500);
        }

        public void Calibrate(
            double nwSwLog,
            double swNeLog,
            double weDirect,
            double nsDirect,
            double nSLog,
            double nSLogGranularity)
        {
            this.NwSwLog = nwSwLog;
            this.SwNeLog = swNeLog;
            this.WeDirect = weDirect;
            this.NsDirect = nsDirect;
            this.MapWidth = nSLog;
            this.MapHeight = nSLogGranularity;
        }

        public MapPoint Project(double lon, double lat)
        {
            double RadiansPerDegree = Math.PI / 180;
            double Rad = lat * RadiansPerDegree;
            double FSin = Math.Sin(Rad);
            double DegreeEqualsRadians = 0.017453292519943;
            double EarthsRadius = 6378137;

            double y = EarthsRadius / 2.0 * Math.Log((1.0 + FSin) / (1.0 - FSin));
            double x = lon * DegreeEqualsRadians * EarthsRadius;

            return new MapPoint(x, y);

        }

        public MapPoint ConvertLatLongToXY(double lat, double lon)
        {
            var mapWidth = MapWidth;
            var mapHeight = MapHeight;

            var mapLonLeft = NwSwLog;
            var mapLonRight = SwNeLog;
            var mapLonDelta = mapLonRight - mapLonLeft;

            var mapLatBottom = NsDirect;
            var mapLatBottomDegree = mapLatBottom * Math.PI / 180;

            var x = ((lon - mapLonLeft) * (mapWidth / mapLonDelta)) + WeDirect;

            lat = lat * Math.PI / 180;
            var worldMapWidth = ((mapWidth / mapLonDelta) * 360) / (2 * Math.PI);
            var mapOffsetY = (worldMapWidth / 2 * Math.Log((1 + Math.Sin(mapLatBottomDegree)) / (1 - Math.Sin(mapLatBottomDegree))));
            var y = mapHeight - ((worldMapWidth / 2 * Math.Log((1 + Math.Sin(lat)) / (1 - Math.Sin(lat)))) - mapOffsetY);

            return new MapPoint(x, y);
        }
    }

    public class MapPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public MapPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
