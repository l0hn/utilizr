﻿using System;
using Utilizr.Async;
using Utilizr.Logging;
using Action = Utilizr.Async.Action;

namespace Utilizr
{
    public class RateLimiter
    {
        DateTime _nextEventAllowedDrop = DateTime.MinValue;
        DateTime _nextEventAllowedWait = DateTime.MinValue;
        DateTime _nextEventAllowedDropGuarantee = DateTime.MinValue;

        public int MinimumInterval { get; set; }

        readonly object _dropGuaranteeLock = new object();
        private bool _dropGuaranteeWaiting;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minimumInterval">
        /// <summary>
        /// The amount of time in milliseconds that must elapse before an action is executed
        /// </summary>
        /// </param>
        public RateLimiter(int minimumInterval)
        {
            MinimumInterval = minimumInterval;
        }

        [Obsolete("Use 'ExecuteActionDrop' or 'ExecuteActionWait'")]
        /// <summary>
        /// Execute an action with rate limiting.
        /// NOTE: There is no guarantee that the action will be invoked. i.e. if rate limiting occurs the action will be dropped
        /// </summary>
        /// <param name="action"></param>
        /// <param name="wait">True to block caller until rate limit expires, False to drop the requested action if rate limit is exceeded</param>
        public void ExecuteAction(Action action, bool wait = false) //here to maintain compatibility
        {
            if (wait)
            {
                ExecuteActionWait(action);
            }
            else
            {
                ExecuteActionDrop(action);
            }
        }

        /// <summary>
        /// Execute an action with rate limiting.
        /// NOTE: There is no guarantee that the action will be invoked. i.e. if rate limiting occurs the action will be dropped
        /// </summary>
        /// <param name="action"></param>
        public void ExecuteActionDrop(Action action)
        {
            var now = DateTime.UtcNow;
            if (_nextEventAllowedDrop > now)
                return;

            _nextEventAllowedDrop = now.AddMilliseconds(MinimumInterval);
            action();
        }

        /// <summary>
        /// Execute an action with rate limiting. This differs from <see cref="ExecuteActionDrop(Async.Action)"/> by guaranteeing
        /// the action will be executed once after the <see cref="MinimumInterval"/> has elapsed when there has been one or more
        /// calls while the interval has not yet elapsed.
        /// </summary>
        /// <param name="action">May not be invoked on the UI thread</param>
        public void ExecuteActionDropGuarantee(Action action)
        {
            lock (_dropGuaranteeLock)
            {
                if (_dropGuaranteeWaiting)
                    return;

                void actionUpdateTimestamp(DateTime dt)
                {
                    _nextEventAllowedDropGuarantee = dt.AddMilliseconds(MinimumInterval);
                    action();
                }

                if (_nextEventAllowedDropGuarantee < DateTime.UtcNow)
                {
                    actionUpdateTimestamp(DateTime.UtcNow);
                    return;
                }

                // confirm not a close call
                var wait = Math.Max(0, (int)(_nextEventAllowedDropGuarantee - DateTime.UtcNow).TotalMilliseconds);
                if (wait < 1)
                {
                    actionUpdateTimestamp(DateTime.UtcNow);
                    return;
                }

                _dropGuaranteeWaiting = true;
                try
                {
                    AsyncHelper.BeginExecute(() =>
                    {
                        Sleeper.Sleep(wait);
                        try
                        {
                            actionUpdateTimestamp(DateTime.UtcNow);
                        }
                        finally
                        {
                            _dropGuaranteeWaiting = false;
                        }
                    });
                }
                catch(Exception ex)
                {
                    Log.Exception(nameof(RateLimiter), ex);
                    _dropGuaranteeWaiting = false;
                }
            }
        }

        /// <summary>
        /// Execute an action with rate limiting.
        /// NOTE: This will block until the minimum time has passed, then allowing the action to be invoked.
        /// This should nearly always never be used, this will slow down any background processing, favour ExecuteAction wherever possible.
        /// </summary>
        /// <param name="action"></param>
        public void ExecuteActionWait(Action action)
        {
            var now = DateTime.UtcNow;
            if (_nextEventAllowedWait > now)
            {
                // +1 to account for casting to int from double
                var toWaitMs = (int)(_nextEventAllowedWait - now).TotalMilliseconds + 1;
                Async.Sleeper.Sleep(toWaitMs);
            }

            _nextEventAllowedWait = now.AddMilliseconds(MinimumInterval);
            action();
        }
    }
}