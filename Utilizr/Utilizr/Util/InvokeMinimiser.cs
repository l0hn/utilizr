﻿using System;
using Utilizr.Async;
using Utilizr.Logging;

namespace Utilizr.Util
{
    /// <summary>
    /// Guarantees an action is called, but will only be called once for multiple invocations
    /// after the currently running action is completed.
    /// 
    /// Useful for scenarios such as reloading a file from disk for the latest values.
    /// </summary>
    public class InvokeMinimiser
    {
        private int _invokedCount;
        readonly object INVOKE_LOCK = new object();

        /// <summary>
        /// Action to invoke on a non-ui thread.
        /// </summary>
        public void InvokeAsync(System.Action action, string actionNameForLog = null)
        {
            lock (INVOKE_LOCK)
            {
                _invokedCount++;

                if (_invokedCount > 1)
                    return;
            }

            try
            {
                AsyncHelper.BeginExecute(() => InvokeImpl(action, actionNameForLog));
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        void InvokeImpl(System.Action action, string actionNameForLog)
        {
            try
            {
                action();
            }
            finally
            {
                bool callAgain = false;

                lock(INVOKE_LOCK)
                {
                    _invokedCount = Math.Max(0, _invokedCount - 1);

                    if (_invokedCount > 0)
                    {
                        if (_invokedCount > 1)
                        {
                            Log.Info($"{nameof(InvokeMinimiser)} about to call once saving {_invokedCount:N0} calls on '{actionNameForLog ?? "not specified"}'");
                        }

                        // other calls queue, clear all and call again
                        _invokedCount = 0;
                        callAgain = true;
                    }
                }

                if (callAgain)
                {
                    InvokeImpl(action, actionNameForLog);
                }
            }
        }
    }
}