﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Utilizr.Util
{
    public static class CertificateUtil
    {
        public static X509Certificate GetCertifacte(string filePath)
        {
            try
            {
                var cert = X509Certificate.CreateFromSignedFile(filePath);                
                return cert;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetPublisher(this X509Certificate cert)
        {
            try
            {
                if (cert == null)
                    return null;

                var parts = cert.Subject.Split('=');

                if (parts.Length < 2)
                    return null;
                
                for (int i = 0; i < parts.Length-1; i++) {
                    if (parts[i].EndsWith("CN"))
                    {
                        var nameParts = parts[i + 1].Split('"');
                        if (nameParts.Length == 1)
                            return nameParts[0].Split(',')[0];
                        
                        return nameParts.FirstOrDefault(n => !String.IsNullOrEmpty(n));
                    }
                }
            }
            catch (Exception e)
            {

#if DEBUG
                Console.WriteLine(e);
#endif
                return null;
            }

            return null;
        }
    }
}
