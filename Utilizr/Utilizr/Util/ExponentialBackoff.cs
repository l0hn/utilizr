﻿using System;
using System.Diagnostics;
using System.Timers;
using Utilizr.Async;

namespace Utilizr
{
    /// <summary>
    /// Exponential backoff helper
    /// </summary>
    public class ExponentialBackoff
    {
        /// <summary>
        /// Total sleeps that have been invoked on the instance.
        /// </summary>
        public int Iteration { get; private set; }

        private int _maxDelayMs = 1 * 60 * 1000;
        private int _minDelayMs;
        private Random _random;
        private bool _hitMaxDelay;
        private bool _cancelAsyncSleep;

        /// <summary>
        /// Exponential backoff helper
        /// </summary>
        /// <param name="MAX_DELAY">Maximum delay for a sleep operation</param>
        public ExponentialBackoff(int maxDelayMs = 1*60*1000, int minDelayMs = 0)
        {
            _maxDelayMs = maxDelayMs;
            _minDelayMs = minDelayMs;
            _random = new Random();
        }

        /// <summary>
        /// Each time Sleep() is called thread.sleep() will be executed for an exponentially longer amount of time
        /// up to a maximum of 1 minute. This method blocks on the current thread.
        /// </summary>
        public void Sleep()
        {
            int delayMs = GenerateDelayMs();

            if (delayMs < 1)
                return;

            //Debug.WriteLine(string.Format("SLEEPING FOR {0:N0}ms", delayMs));
            Sleeper.Sleep(delayMs);
        }



        /// <summary>
        /// Each time Sleep() is called thread.sleep() will be executed for an exponentially longer amount of time
        /// up to a maximum of 1 minute.
        /// <paramref name="sleepFinished">Callback after sleep finished, and whether the sleep was cancelled.</paramref>
        /// <paramref name="delayCallback">Seconds to wait before Sleep will return.</paramref>
        /// <paramref name="internvalCallback">Optional callback for each second remaining</paramref>
        /// </summary>
        public IAsyncResult SleepAsync(Action<bool> sleepFinished, Action<int> initialDelayCallback = null, Action<int> internvalCallback = null)
        {
            return AsyncHelper.BeginExecute(() => SleepImpl(sleepFinished, initialDelayCallback, internvalCallback));
        }


        void SleepImpl(Action<bool> sleepFinished, Action<int> delayCallback = null, Action<int> internvalCallback = null)
        {
            int delayMs = GenerateDelayMs();
            Debug.WriteLine(string.Format("SLEEPING FOR {0:N0}ms", delayMs));

            Timer internvalTimer = null;
            var stoppingAt = DateTime.UtcNow.AddMilliseconds(delayMs);
            void internvalTimer_Elapsed(object sender, ElapsedEventArgs e)
            {
                if (!internvalTimer.Enabled || _cancelAsyncSleep)
                    return; // prevent elapsed firing just after stopping

                var remainingSeconds = Math.Max(0, (int)(stoppingAt - DateTime.UtcNow).TotalSeconds);
                internvalCallback.Invoke(remainingSeconds);
            }

            if (internvalCallback != null)
            {
                internvalTimer = new Timer(1000);
                internvalTimer.Elapsed += internvalTimer_Elapsed;
                internvalTimer.Start();
            }

            delayCallback?.Invoke(delayMs);
            Sleeper.Sleep(delayMs);

            if (internvalCallback != null)
            {
                internvalTimer.Stop();
                internvalTimer.Elapsed -= internvalTimer_Elapsed;
            }

            sleepFinished.Invoke(_cancelAsyncSleep);
            _cancelAsyncSleep = false;
        }


        public void CancelSleepAsync()
        {
            // TODO: Find a way to actually cancel the sleep. Use timers (messy)? CancellationToken .NET 4.0...
            _cancelAsyncSleep = true;
        }


        /// <summary>
        /// Reset exponential backoff to the state where Sleep() has never been invoked.
        /// <see cref="Iteration"/> will return to zero.
        /// </summary>
        public void Reset()
        {
            Iteration = 0;
            _hitMaxDelay = false;
            _cancelAsyncSleep = false;
        }

        int GenerateDelayMs()
        {
            int delayMs = _maxDelayMs; //just incase
            if (_hitMaxDelay)
            {
                delayMs = _random.Next(_maxDelayMs / 2, _maxDelayMs);
            }
            else
            {
                delayMs = (int)(Math.Pow(2, ++Iteration) * 1000) + _random.Next(0, 1000);
            }

            if (!_hitMaxDelay && delayMs >= _maxDelayMs)
            {
                delayMs = _maxDelayMs;
                _hitMaxDelay = true;
            }

            delayMs = Math.Max(delayMs, _minDelayMs);

            return delayMs;
        }
    }
}