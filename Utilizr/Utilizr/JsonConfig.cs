﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Utilizr.Async;
using Utilizr.Windows;
using System.Security;
using System.Linq;

namespace Utilizr
{
    public static class JsonConfig<T> where T : Loadable<T>, new()
    {        
        static readonly object LOCK = new object();

        static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (LOCK)
                {
                    if (_instance == null)
                        _instance = LoadFromConfigFile(new T());
                }

                return _instance;
            }
        }

        internal static void Reload()
        {
            lock (LOCK)
            {
                _instance = LoadFromConfigFile(Instance);
            }
        }

        static T LoadFromConfigFile(T currentT)
        {
            try
            {
                return Loadable<T>.Load(currentT);
            }
            catch { }
            return default(T);
        }
    }

    public abstract class Loadable<T> where T: Loadable<T>, new()
    {
        private static readonly object ATOMIC_LOCK = new object();
        public event EventHandler Saved;

        public static T Instance => JsonConfig<T>.Instance;

        /// <summary>
        /// Indicates if the config file exists on disk or not
        /// </summary>
        [JsonIgnore]
        public bool Exists => File.Exists(LoadPath);

        /// <summary>
        /// The amount of times to retry if saving fails. Will always try at least once.
        /// </summary>
        [JsonIgnore]
        public int SaveRetries { get; set; } = 3;

        /// <summary>
        /// If true, this will write to disk the current instance's data when the file has been
        /// deleted since it was originally loaded.
        /// </summary>
        [JsonIgnore]
        public virtual bool SaveOnLoadFaliure { get; set; } = false;

        /// <summary>
        /// Resource name identifying the embedded file in the currently executing assembly (will take priority in loading sequence)
        /// </summary>
        [JsonIgnore] public virtual string EmbeddedResourceName { get; } = null;

        /// <summary>
        /// Setting ReadOnly to true will cause an exception if a SaveInstance() attempt is made on the instance without explicitly providing a file path
        /// </summary>
        [JsonIgnore] public virtual bool ReadOnly => false;

        [JsonIgnore]
        public string LoadPath => getLoadPath();

        public virtual void Reload()
        {
            ReloadInstance();
        }

        public static void ReloadInstance()
        {
            JsonConfig<T>.Reload();
        }        

        public virtual IAsyncResult SaveAsync(string customFilePath = "", AsyncCallback callback = null)
        {
            return AsyncHelper.BeginExecute(() => Save(customFilePath), callback);
        }

        public void EndSaveAsync(IAsyncResult result)
        {
            AsyncHelper.EndExecute(result);
        }

        /// <summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// </summary>
        public virtual void Save(string customFilePath = "")
        {
            Save(Instance, customFilePath);
        }

        /// <summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// </summary>
        public virtual void Save(T currentT, string customFilePath = "")
        {
            if (currentT.ReadOnly && customFilePath.IsNullOrEmpty())
                throw new InvalidOperationException($"Attempted to call {nameof(Save)}() or {nameof(SaveInstance)}() on a readonly loadable");
            
            int localRetryCount = SaveRetries;
            bool done = false;
            try
            {
                while (!done)
                {
                    try
                    {
                        var path = customFilePath.IsNullOrEmpty()
                            ? LoadPath
                            : customFilePath;

                        var dir = Path.GetDirectoryName(path);
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }

                        lock (this)
                        {
                            var json = JsonConvert.SerializeObject(this);
                            json = CustomSerializeStep(json);
                            File.WriteAllText(path, json);
                        }
                        done = true;
                    }
                    catch (Exception)
                    {
                        localRetryCount--;

                        if (localRetryCount <= 0)
                            throw;

                        Sleeper.Sleep(2);
                    }
                }
            }
            catch (IOException ioEx)
            {
#if MONO || ANDROID || IOS
                throw;
#else
                throw ProcessHelper.WhoIsLockingChecker(ioEx, LoadPath);
#endif
            }

            OnSaved();
        }

        /// <summary>
        /// Locks this instance for the duration of updateAction and saves back to disk when complete
        /// NOTE: this is only atomic with other calls to AtomicUpdate or AtomicRead. if you directly access Instance or manually call SaveInstance you're on your own.
        /// IMPORTANT! Do NOT call saveinstance or load instance inside your updateAction.
        /// </summary>
        /// <param name="updateAction"></param>
        public static void AtomicUpdate(Action<T> updateAction)
        {
            lock (ATOMIC_LOCK)
            {
                updateAction(Instance);
                SaveInstance();
            }
        }

        /// <summary>
        /// Locks this instance for the duration of readAction
        /// NOTE: this is only atomic with other calls to AtomicUpdate or AtomicRead. if you directly access Instance or manually call SaveInstance you're on your own.
        /// IMPORTANT! Do NOT call saveinstance or load instance inside your readAction.
        /// </summary>
        /// <param name="readAction"></param>
        public static void AtomicRead(Action<T> readAction)
        {
            lock (ATOMIC_LOCK)
            {
                readAction(Instance);
            }
        }

        public static T Load(string customLoadPath = "")
        {
            // Could be first load, or invoked on previously loaded instance.
            // Null check so we can use the current values in memory if file deleted, etc
            var t = Instance ?? new T();
            return Load(t, customLoadPath);
        }

        public static T Load(T t, string customLoadPath = "")
        {
            var newT = Load(t, out bool failed, customLoadPath);

            if (failed && newT.SaveOnLoadFaliure && !newT.ReadOnly)
                newT.Save(newT, customLoadPath);

            return newT;
        }

        public static T Load(T t, out bool loadFailed, string customLoadPath = "")
        {
            loadFailed = true;
            try
            {
                string json = null;
                //try embedded resource first if windows
#if !MONO
                try
                {
                    if (t.EmbeddedResourceName.IsNotNullOrEmpty())
                    {
                        var resourceData = Win32.LoadResourceFile(t.EmbeddedResourceName, customLoadPath);
                        if (resourceData != null)
                        {
                            json = Encoding.UTF8.GetString(resourceData);
                        }
                    }
                }
                catch (Exception)
                {

                }
#endif

                if (json.IsNullOrEmpty())
                {
                    var loadPath = customLoadPath.IsNullOrEmpty() ? t.LoadPath : customLoadPath;
                    if (!File.Exists(loadPath))
                        return t;

                    json = File.ReadAllText(loadPath);
                }

                if (string.IsNullOrEmpty(json))
                    return t;

                json = t.CustomDeserializeStep(json);

                if (json.IsNullOrEmpty())
                    return t;

                var loadedObj = JsonConvert.DeserializeObject<T>(json);
                if (loadedObj != null)
                {
                    loadFailed = false;
                    return loadedObj;
                }
            }
            catch { }
            return t;
        }

        /// <summary>
        /// Allows the string read from file to be manipulated before json decode takes place
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected abstract string CustomDeserializeStep(string source);

        /// <summary>
        /// Allows the json encoded string to be manipulated before writing to file
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected abstract string CustomSerializeStep(string source);

        /// <summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// </summary>
        public static void SaveInstance()
        {
            Instance.Save();
        }

        public static IAsyncResult SaveInstanceAsync(AsyncCallback callback = null)
        {
            return Instance.SaveAsync(callback:callback);
        }

        public static void EndSaveInstanceAsync(IAsyncResult result)
        {
            AsyncHelper.EndExecute(result);
        }

        protected abstract string getLoadPath();

        protected virtual void OnSaved()
        {
            Saved?.Invoke(this, new EventArgs());
        }
    }
}