﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Utilizr.GetText.Helpers
{
    public static class SourceList
    {
        private static string[] _extensions = new string[]{
            ".cs",
            ".h",
            ".m",
            ".kt"//kotlin
        };

        public static long GenerateSourceList(string srcRootDirectory, string outputFilePath)
        {
            int count = 0;
            using (StreamWriter writer = new StreamWriter(File.Open(outputFilePath, FileMode.Create, FileAccess.Write)))
            {
                foreach (string file in GetSourceFiles(srcRootDirectory))
                {
                    writer.WriteLine(file);
                    Console.WriteLine("file: " + file);
                    count++;
                }
            }
            return count;
        }

        private static IEnumerable<string> GetSourceFiles(string directory)
        {
            var files = (from i in Directory.GetFiles(directory)
                         where _extensions.Contains(Path.GetExtension(i).ToLower())
                         select i);

            foreach (var file in files)
            {
                yield return file;
            }

            foreach (string dir in Directory.GetDirectories(directory))
            {
                foreach (string file in GetSourceFiles(dir))
                {
                    yield return file;
                }
            }
        }
    }
}
