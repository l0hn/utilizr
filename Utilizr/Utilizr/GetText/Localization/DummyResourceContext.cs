﻿using System;
using System.Collections.Generic;
using GetText.Parsers;

namespace GetText.Localization
{
    internal class DummyResourceContext : ResourceContext
    {
		internal delegate string Single(string s);
		internal delegate string Plural(string s, string p, long n);

        private Single S;
        private Plural P;

        public DummyResourceContext(string ieftTag, Single s, Plural p) : base(ieftTag, new MoHeader(), null)
        {
            S = s;
            P = p;
        }

        public override string LookupPluralString(string s, string p, long n)
        {
            return P?.Invoke(s, p, n);
        }

        public override string LookupString(string s)
        {
			return S?.Invoke(s);
        }
    }
}
