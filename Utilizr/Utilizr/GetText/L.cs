﻿using GetText.Events;
using GetText.Localization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Threading;
using Utilizr.Logging;
//#if !ANDROID
using System.Globalization;
using System.Collections.ObjectModel;
using System.Text;
using Utilizr;
//#endif
#if MONO
using Utilizr.Collections;
#endif

namespace GetText
{
    /// <summary>
    /// Provides Gnu GetText implementation that works better than gettext-cs-util
    /// </summary>
    public static class L
    {
        public static Event LocaleChanged = new Event();

#if MONO
        private static ConcurrentDictionary<string, ResourceContext> _lookupDictionary = new ConcurrentDictionary<string, ResourceContext>();
#else
        private static Dictionary<string, ResourceContext> _lookupDictionary = new Dictionary<string, ResourceContext>();
#endif
        private static Dictionary<string, string> _moFileLookup = new Dictionary<string, string>();
        private static bool _indexedMoFiles = false;
        private const string _logCat = "GetText";

        public static string CurrentLanguage { get; private set; }

#if DEBUG
        public static SupportedLanguage DebugLanguage { get; }
#endif

        static L()
        {
#if DEBUG
            DebugLanguage = new SupportedLanguage()
            {
                Name = "Blank",
                NativeName = "*****",
                IetfLanguageTag = "blank",
            };
#endif
        }

        private static ReadOnlyCollection<SupportedLanguage> _supportedLanguages;
        /// <summary>
        /// The list of supported languages, in their native name.
        /// E.g. English, Español, Français, Deutsche
        /// </summary>
        public static ReadOnlyCollection<SupportedLanguage> SupportedLanguages
        {
            get
            {
                var supported = new List<SupportedLanguage>();
                try
                {
                    if (_supportedLanguages != null)
                        return _supportedLanguages;

                    if (!_indexedMoFiles)
                    {
                        IndexMoFiles();
                    }

                    var allCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);

                    foreach (var culture in allCultures)
                    {
                        if (!_moFileLookup.Keys.Contains(culture.IetfLanguageTag))
                            continue;

                        supported.Add(new SupportedLanguage
                        {
                            Name = culture.TextInfo.ToTitleCase(culture.DisplayName),
                            NativeName = culture.TextInfo.ToTitleCase(culture.NativeName),
                            IetfLanguageTag = culture.IetfLanguageTag,
                        });
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(_logCat, ex, "Failed to get list, defaulting to English only");
                }

                // We always have English without en-GB.po
                supported.Add(new SupportedLanguage
                {
                    Name = "English",
                    NativeName = "English",
                    IetfLanguageTag = "en",
                });

#if DEBUG
                //add a dummy 'blank' language that always returns a blanked out string - helpful for finding missing translations/errors
                supported.Add(DebugLanguage);
#endif

                _supportedLanguages = supported.AsReadOnly();
                return _supportedLanguages;
            }
        }

        public static void SetLanguage(string ietfLanguageTag)
        {
            ietfLanguageTag = ietfLanguageTag.ToLower();
            if (string.IsNullOrEmpty(ietfLanguageTag))
            {
#if IOS || ANDROID
                ietfLanguageTag = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
#else
                ietfLanguageTag = Thread.CurrentThread.CurrentCulture.IetfLanguageTag;
#endif

            }
            if (!_indexedMoFiles)
            {
                IndexMoFiles();
            }

            PreloadLanguage(ietfLanguageTag);

			bool changed = false;
			if (CurrentLanguage != ietfLanguageTag)
			{
				changed = true;
			}
			CurrentLanguage = ietfLanguageTag;
			if (changed && LocaleChanged != null)
			{
				LocaleChanged.RaiseEvent();
			}
		}

        private static void PreloadLanguage(string ietfLanguageTag)
        {
            ietfLanguageTag = ietfLanguageTag.ToLower();
			if (!_lookupDictionary.ContainsKey(ietfLanguageTag))
			{
				//load the mo file for the specified language code
				try
				{
					if (_moFileLookup.ContainsKey(ietfLanguageTag))
					{
						_lookupDictionary[ietfLanguageTag] = ResourceContext.FromFile(_moFileLookup[ietfLanguageTag], ietfLanguageTag);
					}
					else if (_moFileLookup.ContainsKey(ietfLanguageTag.Substring(0, 2)))
					{
						_lookupDictionary[ietfLanguageTag] = ResourceContext.FromFile(_moFileLookup[ietfLanguageTag.Substring(0, 2)], ietfLanguageTag);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}

#if DEBUG
				//add a dummy 'blank' language that always returns a blanked out string - helpful for finding missing translations/errors
				if (ietfLanguageTag == "blank")
				{
					_lookupDictionary.Add("blank", new DummyResourceContext("blank", (s) =>
					{
						var chars = new char[s.Length];
						for (int i = 0; i < chars.Length; i++)
						{
                            chars[i] = char.IsWhiteSpace(s[i])
                                ? s[i]
                                : '*';
                        }
                        return new string(chars);
					}, (s, p, n) =>
					{
						var chars = new char[s.Length];
                        int insideFormatPlaceholder = 0;
						for (int i = 0; i < chars.Length; i++)
						{
                            // Don't replace character if whitespace to avoid one long string of *******
                            // Don't replace string format placeholders, such as {0:N0}

                            chars[i] = s[i];
                            if (char.IsWhiteSpace(s[i]))
                                continue;

                            if (s[i] == '{')
                            {
                                insideFormatPlaceholder++;
                                continue;
                            }

                            if (s[i] == '}')
                            {
                                insideFormatPlaceholder--;
                                continue;
                            }

                            if (insideFormatPlaceholder > 0)
                                continue;

                            chars[i] = '*';

                        }
                        return new string(chars);
                    }));
				}
#endif
			}
        }

		private static void IndexMoFiles()
		{
#if IOS
			string moFileBase = "Locales"; // Path.GetDirectoryName("Locales");
#else
			string moFileBase = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
#endif
			IndexMoFiles(moFileBase);
			moFileBase = Path.Combine(moFileBase, "locale");
			IndexMoFiles(moFileBase);
			_indexedMoFiles = true;
		}

        private static void IndexMoFiles(string dir)
        {
            if (!Directory.Exists(dir)) return;
            try
            {
                foreach (string file in Directory.GetFiles(dir))
                {
                    if (Path.GetExtension(file).ToLower() == ".mo")
                    {
                        string locale = Path.GetFileNameWithoutExtension(file);
                        string lang = locale.Substring(0, 2);
                        _moFileLookup[locale.ToLower()] = file;
                        _moFileLookup[lang] = file;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

		/// <summary>
		/// Indexes the mo file with the specified language. Use with android as assets do not have paths.
		/// </summary>
		/// <param name="ietfLanguageTag">Ietf language tag.</param>
		/// <param name="s">Asset Stream of mo file.</param>
		public static void IndexMoFile(string ietfLanguageTag, Stream s)
		{
			if (_lookupDictionary.ContainsKey(ietfLanguageTag))
				return;
			try
			{
				_lookupDictionary.Add(ietfLanguageTag, ResourceContext.FromStream(s, ietfLanguageTag));
                _moFileLookup[ietfLanguageTag] = "";
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}

        public static string _(string T, params object[] args)
        {
            string res = "";
            try
            {
                if (CurrentLanguage != null && _lookupDictionary.ContainsKey(CurrentLanguage))
                {
                    if (_lookupDictionary[CurrentLanguage] != null)
                    {
                        res = _lookupDictionary[CurrentLanguage].LookupString(T);
                        if (args.Length > 0)
                        {
                            res = string.Format(res, args);
                        }
                        return res;
                    }
                }
                res = T;
                if (args.Length > 0)
                {
                    res = string.Format(res, args);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return res;
        }

        public static string _p(string T, string TPlural, long n, params object[] args)
        {
            string res = "";
            if (CurrentLanguage != null && _lookupDictionary.ContainsKey(CurrentLanguage))
            {
                if (_lookupDictionary[CurrentLanguage] != null)
                {
                    res = _lookupDictionary[CurrentLanguage].LookupPluralString(T, TPlural, n);
                    if (args.Length > 0)
                    {
                        res = string.Format(res, args);
                    }
                    return res;
                }
            }
            //couldn't find resource context so return default values
            res = n == 1 ? T : TPlural;
            if (args.Length > 0)
            {
                res = string.Format(res, args);
            }
            return res;
        }        

        [Obsolete("Use _ip() which returns an ITranslatable object. Can then use the Translation property.", false)]
        public static string _p(MP mp, long n, params object[] args)
        {
            return _p(mp.T, mp.TPlural, n, args);
        }

        /// <summary>
        /// marks a string for translation and returns the original (non-translated string)
        /// useful for storing translatable string in variables
        /// </summary>
        /// <param name="T"></param>
        /// <returns></returns>
        public static string _m(string T)
        {
            return T;
        }

        /// <summary>
        /// Depreciated. Use <see cref="_m(string, string, Func{long}, Func{LArgsInfo})"/> instead.
        /// <param name="T"></param>
        /// <param name="TPlural"></param>
        /// <returns></returns>
        [Obsolete("Use _ip which returns ITranslatable. Calling Translation on MP object will throw when created here!", false)]
        public static MP _m(string T, string TPlural)
        {
            return new MP(T, TPlural, null, null);
        }

        /// <summary>
        /// Returns an <see cref="ITranslatable"/> object which generates the localised string
        /// at the time of invocation, using a lambda to get the latest string format arguments
        /// </summary>
        /// <param name="t">Singular English string</param>
        /// <param name="args">Optional to return a <see cref="LArgsInfo"/> object.</param>
        /// <returns></returns>
        public static ITranslatable _i(string t, Func<LArgsInfo> args = null)
        {
            return new MS(t, args);
        }

        /// <summary>
        /// Returns an <see cref="ITranslatable"/> object which generates the localised string
        /// at the time of invocation, using a lambda to get the latest string format arguments
        /// </summary>
        /// <param name="t">Singular English text</param>
        /// <param name="tPlural">Plural English text</param>
        /// <param name="n">Not null. Returns the value to determine whether to show the singular or plural version.</param>
        /// <param name="args">Optional to return a <see cref="LArgsInfo"/> object.</param>
        /// <returns></returns>
        public static ITranslatable _ip(string t, string tPlural, Func<long> n, Func<LArgsInfo> args = null)
        {
            return new MP(t, tPlural, n, args);
        }

        /// <summary>
        /// Static helper to create LArgsInfo without writing 'new LArgsInfo();'
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static LArgsInfo args(params object[] fmtArgs)
        {
            return new LArgsInfo(fmtArgs);
        }

        public static void AddCustomTranslation(string ietfLanguageTag, string id, string translation)
        {
            if (ietfLanguageTag.IsNullOrEmpty())
                return;
            
            if (id.IsNullOrEmpty())
                return;

            if (translation.IsNullOrEmpty())
                return;
            
            ietfLanguageTag = ietfLanguageTag.ToLower();
            if (!_indexedMoFiles)
                IndexMoFiles();
            
            PreloadLanguage(ietfLanguageTag);
            ResourceContext context = null;
            if (!_lookupDictionary.TryGetValue(ietfLanguageTag, out context))
                _lookupDictionary.TryGetValue(ietfLanguageTag.Substring(0, 2), out context);

            if (context == null)
                return;
            
            context.AddCustomTranslation(id, translation);
        }
    }

    public interface ITranslatable
    {
        // Note: Changing these property names will break existing bindings
        string English { get; }
        string Translation { get; }
    }

    /// <summary>
    /// Allows translatable singular string to be stored in variables
    /// </summary>
    [DebuggerDisplay("English = {English}\n, Translation = {Translation}")]
    public class MS : ITranslatable
    {
        public string T { get; set; }
        public string Translation
        {
            get
            {
                if (string.IsNullOrEmpty(T))
                    return T;

                var lArgs = _formatArgs?.Invoke();
                return lArgs == null
                    ? L._(T)
                    : L._(T, lArgs.FormatArgs);
            }
        }
        public string English
        {
            get
            {
                var lArgs = _formatArgs?.Invoke();
                return lArgs == null
                    ? T
                    : string.Format(T, lArgs.FormatArgs);
            }
        }

        readonly Func<LArgsInfo> _formatArgs;

        internal MS(string t, Func<LArgsInfo> formatArgs)
        {
            T = t;
            _formatArgs = formatArgs;
        }
    }

    public static class ITranslatableExtensions
    {
        /// <summary>
        /// Wraps a normal non-translatable string as an ITranslatable
        /// </summary>
        /// <returns>The string as a translatable.</returns>
        /// <param name="text">Text.</param>
        public static ITranslatable WrapAsTranslatable(this string text)
        {
            return L._i("{0}", () => L.args(text));
        }

        /// <summary>
        /// Merge multiple ITranslatable instance into one. Useful when dealing with multiple plurals within one piece of text.
        /// </summary>
        /// <param name="iTranslatables">Instances to merge.</param>
        /// <param name="separator">Specific optional separator. Default is no separator.</param>
        /// <returns></returns>
        public static ITranslatable Merge(this IEnumerable<ITranslatable> iTranslatables, string separator = null)
        {
            if (iTranslatables == null)
                throw new ArgumentException($"{nameof(iTranslatables)} cannot be null");

            var formatStringBuilder = new StringBuilder();
            for (int i = 0; i < iTranslatables.Count(); i++)
            {
                if (separator == null)
                    formatStringBuilder.Append($"{{{i}}}");
                else
                    formatStringBuilder.Append($"{{{i}}}{separator}");
            }

            return L._i(formatStringBuilder.ToString().Trim(), () => L.args(iTranslatables.Select(p => p.Translation).ToArray()));
        }
    }

    /// <summary>
    /// Allows translatable plural string to be stored in variables
    /// </summary>
    [DebuggerDisplay("English = {English}\n, Translation = {Translation}")]
    public class MP : ITranslatable
    {
        public string T { get; set; }
        public string TPlural { get; set; }
        public string Translation
        {
            get
            {
                long count = _counter.Invoke();

                if (string.IsNullOrEmpty(T) && count == 1)
                    return T;

                if (string.IsNullOrEmpty(TPlural) && count != 1)
                    return TPlural;

                var lArgs = _formatArgs?.Invoke();
                return lArgs == null
                    ? L._p(T, TPlural, count)
                    : L._p(T, TPlural, count, lArgs.FormatArgs);
            }
        }
        public string English
        {
            get
            {
                long count = _counter.Invoke();
                var lArgs = _formatArgs?.Invoke();

                return lArgs == null
                    ? count == 1 
                        ? T 
                        : TPlural
                    : count == 1 
                        ? string.Format(T, lArgs.FormatArgs) 
                        : string.Format(TPlural, lArgs.FormatArgs);
            }
        }

        readonly Func<long> _counter;
        readonly Func<LArgsInfo> _formatArgs;

        internal MP(string t, string tplural, Func<long> counter, Func<LArgsInfo> formatArgs)
        {
            T = t;
            TPlural = tplural;
            _counter = counter;
            _formatArgs = formatArgs;
        }
    }

    /// <summary>
    /// Wrapper just to make it a little nicer to return an array of object for MS and MP
    /// Declaring new object[] { obj1, obj2, ...} bit more cumbersome than new LArgsInfo(obj1, obj2, ...)
    /// </summary>
    public class LArgsInfo
    {
        public object[] FormatArgs { get; set; }

        public LArgsInfo(params object[] formatArgs)
        {
            FormatArgs = formatArgs;
        }
    }

    [DebuggerDisplay("Name={Name}, NativeName={NativeName}, IetfLanguageTag={IetfLanguageTag})")]
    public class SupportedLanguage
    {
        public string Name { get; set; }
        public string NativeName { get; set; }
        public string IetfLanguageTag { get; set; }
    }
}
