﻿using System;
#if MONO
using Mono.Data.Sqlite;
#elif NETCOREAPP
using SqliteConnection = System.Data.SQLite.SQLiteConnection;
using SqliteCommand = System.Data.SQLite.SQLiteCommand;
#else
using Community.CsharpSqlite.SQLiteClient;
#endif

namespace Utilizr.Database
{
    public class DataController: IDisposable
    {
        private SqliteConnection _db;

        public DataController(string dbFilePath)
        {
            _db = ConnectionPool.GetConnection(dbFilePath);
        }

        public SqliteCommand GetCommand()
        {
            if (_db.State == System.Data.ConnectionState.Closed)
            {
                _db.Open();
            }
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = _db;
            cmd.CommandTimeout = 1000;
            return cmd;
        }

        public void CloseConnection()
        {
            if (_db != null && _db.State != System.Data.ConnectionState.Closed)
            {
                _db.Close();
            }
        }

        public void Dispose()
        {
            try
            {
                CloseConnection();
            }
            catch (Exception)
            {
               
            }
        }
    }
}
