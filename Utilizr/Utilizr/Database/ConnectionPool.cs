﻿#if MONO
using Mono.Data.Sqlite;
#elif NETCOREAPP
using SqliteConnection = System.Data.SQLite.SQLiteConnection;
#else
using Community.CsharpSqlite.SQLiteClient;
#endif
using System.Collections.Generic;

namespace Utilizr.Database
{
    public static class ConnectionPool
    {
        private static Dictionary<string, SqliteConnection> _connections = new Dictionary<string, SqliteConnection>();
        private static object LOCK = new object();

        public static SqliteConnection GetConnection(string databaseFilepath)
        {
            lock (LOCK)
            {
                if (!_connections.ContainsKey(databaseFilepath))
                {
#if MONO || NETCOREAPP
                    _connections[databaseFilepath] = new SqliteConnection(string.Format("Data Source={0};Version=3;", databaseFilepath));
#else
                    _connections[databaseFilepath] = new SqliteConnection(string.Format("Data Source=file://{0}", databaseFilepath));
#endif
                }
                return _connections[databaseFilepath];
            }
        }
    }
}
