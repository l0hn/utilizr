﻿#if MONO
using Mono.Data.Sqlite;
using System.ComponentModel;
#elif NETCOREAPP
using SqliteCommand = System.Data.SQLite.SQLiteCommand;
using SqliteDataReader = System.Data.SQLite.SQLiteDataReader;
using SqliteException = System.Data.SQLite.SQLiteException;
#else
using Community.CsharpSqlite.SQLiteClient;
#endif


using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Utilizr.Info;

namespace Utilizr.Database
{
    /// <summary>
    /// Extend Tuple<T> for zero config database mapped objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Tuple<T> where T : Tuple<T>
    {
        #region static fields and props
        private static bool _configured = false;
        private static object CONFIG_LOCK = new object();
        private static DataController _dataController;
        private static FieldInfo _whereField;
        private static List<KeyValuePair<FieldInfo, string>> _fieldCache;
        private static Dictionary<string, FieldInfo> _fieldLookup = new Dictionary<string, FieldInfo>();
        #endregion

        private static T _infoInstance;

        /// <summary>
        /// Database filename excluding the path
        /// Default is utilizr.db
        /// </summary>
        protected virtual string DatabaseFileName
        {
            get { return "utilizr.db"; }
        }

        protected virtual string DatabaseDirectory => AppInfo.DataDirectory;

        /// <summary>
        /// Database table name
        /// Default is the name of type T
        /// </summary>
        protected virtual string TableName
        {
            get { return typeof(T).Name; }
        }

        protected virtual bool IncludePrivateFields => false;

        public event EventHandler Loaded;
        public event EventHandler Saved;

        #region public methods
        /// <summary>
        /// Save the object to the underlying database
        /// </summary>
        public void Save()
        {
            Configure();
            string fields = "";
            string values = "";
            using (var command = _dataController.GetCommand())
            {
                foreach (var field in GetFieldNamesAndTypes())
                {
                    fields += field.Key.Name + ",";
                    values += "@" + field.Key.Name + ",";
                    var param = command.CreateParameter();
                    param.ParameterName = "@" + field.Key.Name;
                    param.Value = GetObjectToSave(field.Key);
                    command.Parameters.Add(param);
                }
                fields = fields.TrimEnd(',');
                values = values.TrimEnd(',');
                string cmdString = string.Format("INSERT OR REPLACE INTO {0} ({1}) VALUES ({2})", TableName, fields, values);
                command.CommandText = cmdString;
                command.ExecuteNonQuery();
            }
            OnSaved();
        }

        /// <summary>
        /// Load the object data from the underlying database by querying the primary key attribute
        /// </summary>
        public virtual void Load()
        {
            Configure();
            string fields = GetFieldCSV();
            string sql = string.Format("SELECT {0} FROM {1} WHERE {2}=@{2}", fields, TableName, _whereField.Name);
            using (var command = _dataController.GetCommand())
            {
                command.CommandText = sql;
                var wparam = command.CreateParameter();
                wparam.ParameterName = "@" + _whereField.Name;
                wparam.Value = _whereField.GetValue(this);
                command.Parameters.Add(wparam);

                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows && reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string name = reader.GetName(i).ToUpperInvariant();
                            if (_fieldLookup.ContainsKey(name))
                            {
                                object value = reader.GetValue(i);
                                LoadObjectValue(_fieldLookup[name], value, this);
                            }
                        }
                    }
                }
            }
            OnLoaded();
        }

        /// <summary>
        /// Delete the current instance from the underlying database
        /// </summary>
        public void Delete()
        {
            Configure();
            string sql = string.Format("DELETE FROM {0} WHERE {1}=@{1}", TableName, _whereField.Name);
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = sql;
                var param = cmd.CreateParameter();
                param.ParameterName = "@" + _whereField.Name;
                param.Value = _whereField.GetValue(this);
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Yield a collection of objects for the specified where clause
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static IEnumerable<T> LoadMany(string where = null)
        {
            Configure();
            string fields = GetFieldCSV();
            where = CleanWhere(where);
            string sql = string.Format("SELECT {0} FROM {1} {2}", fields, _infoInstance.TableName, where);

            using (SqliteCommand cmd = _dataController.GetCommand())
            {
                cmd.CommandText = sql;
                using (SqliteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.HasRows && reader.Read())
                    {
                        yield return LoadNextObject(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Count the rows for the specified where clause
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static int Count(string where = null)
        {
            Configure();
            where = CleanWhere(where);
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", _infoInstance.TableName, where);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        /// <summary>
        /// Count the rows for the specified where clause, exceptions will be swallowed
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static int TryCount(string where = null)
        {
            try
            {
                return Count(where);
            }
            catch (Exception)
            {
                return 0;
            }
        }


        /// <summary>
        /// Delete rows with supplied where clause
        /// NOTE: A null or empty string will cause all rows to be deleted
        /// </summary>
        /// <param name="where"></param>
        /// <returns>number of rows deleted</returns>
        public static int DeleteMany(string where = null)
        {
            Configure();
            where = CleanWhere(where);
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("DELETE FROM {0} {1}", _infoInstance.TableName, where);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete rows with supplied where clause, exceptions will be swallowed
        /// NOTE: A null or empty string will cause all rows to be deleted
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public int TryDeleteMany(string where = null)
        {
            try
            {
                return DeleteMany(where);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Release the database connection if connected.
        /// NOTE: subsequent calls to load or save entries will initiate a new connection
        /// </summary>
        public static void DisconnectDB()
        {
            _dataController?.CloseConnection();
        }

        #endregion

        private static string CleanWhere(string where)
        {
            if (string.IsNullOrEmpty(where))
            {
                return where;
            }
            where = where.Trim();
            if (!where.ToLower().StartsWith("where"))
            {
                where = "WHERE " + where;
            }
            return where;
        }

        private static void Configure()
        {
            lock (CONFIG_LOCK)
            {
                if (_configured)
                {
                    return;
                }
                _infoInstance = (T)Activator.CreateInstance(typeof(T), true);
                _dataController = new DataController(Path.Combine(_infoInstance.DatabaseDirectory, _infoInstance.DatabaseFileName));
                StringBuilder tableSb = new StringBuilder();
                StringBuilder colsSb = new StringBuilder();

                string cols = "";

                foreach (var field in GetFieldNamesAndTypes())
                {
                    _fieldLookup[field.Key.Name.ToUpperInvariant()] = field.Key;

                    cols += string.Format("{0} {1} {2},", field.Key.Name, field.Value, field.Key.ToColumnAttrString());

                    colsSb.AppendLine(string.Format(
@"ALTER TABLE {0} ADD COLUMN {1} {2} {3};",
_infoInstance.TableName, field.Key.Name, field.Value, field.Key.ToColumnAttrString())).AppendLine();

                    //set the where for primary key
                    if (field.Key.IsPrimaryKey())
                    {
                        _whereField = field.Key;
                    }
                }
                cols = cols.TrimEnd(',');
                tableSb.AppendLine(string.Format(
@"CREATE TABLE IF NOT EXISTS {0} ({1});", _infoInstance.TableName, cols));

                using (var command = _dataController.GetCommand())
                {
                    command.CommandText = tableSb.ToString();
                    command.ExecuteNonQuery();
                    try
                    {
                        command.CommandText = colsSb.ToString();
                        command.ExecuteNonQuery();
                    }
                    catch (SqliteException)
                    {
                    }

                }
                _configured = true;
            }
        }

        public static void UnConfigure()
        {
            DisconnectDB();
            _dataController?.Dispose();
            _dataController = null;
            _infoInstance = null;
            _configured = false;
        }

        private static List<KeyValuePair<FieldInfo, string>> GetFieldNamesAndTypes()
        {
            if (_fieldCache == null)
            {
                _fieldCache = new List<KeyValuePair<FieldInfo, string>>();
                foreach (var field in typeof(T).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if (field.HasIgnoreAttribute())
                        continue;

                    //special case for events
                    if (field.FieldType == typeof(System.ComponentModel.PropertyChangedEventHandler))
                        continue;

                    if (field.IsPublic || _infoInstance.IncludePrivateFields)
                    {
                        _fieldCache.Add(new KeyValuePair<FieldInfo, string>(field, field.FieldType.GetDbType()));
                    }
                }
            }
            return _fieldCache;
        }

        private static string GetFieldCSV()
        {
            string fields = "";
            foreach (var field in GetFieldNamesAndTypes())
            {
                fields += field.Key.Name + ",";
            }
            fields = fields.TrimEnd(',');
            return fields;
        }

        private static T LoadNextObject(SqliteDataReader reader)
        {
            object result = Activator.CreateInstance(typeof(T), true);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string name = reader.GetName(i).ToUpperInvariant();
                if (_fieldLookup.ContainsKey(name))
                {
                    object value = reader.GetValue(i);
                    LoadObjectValue(_fieldLookup[name], value, result);
                }
            }
            return (T)result;
        }

        private static void LoadObjectValue(FieldInfo field, object value, object target)
        {
            if (field.FieldType.IsEnum)
            {
                if (value is string)
                {
                    int intObj = Convert.ToInt32(value);
                    field.SetValue(target, intObj);
                }
                else
                {
                    field.SetValue(target, Convert.ChangeType(value, typeof(Int32)));
                }
            }
            else if (field.FieldType == typeof(DateTime))
            {
                field.SetValue(target, DateTime.FromBinary((long)value));
            }
            else if (field.FieldType == typeof(Single))
            {
                field.SetValue(target, Convert.ToSingle(value));
            }
            else
            {
                field.SetValue(target, Convert.ChangeType(value, field.FieldType));
            }
        }

        private object GetObjectToSave(FieldInfo field)
        {
            object value = field.GetValue(this);
            if (field.FieldType == typeof(DateTime))
            {
                return ((DateTime)value).ToUniversalTime().ToBinary();
            }
            else
            {
                return value;
            }
        }

        protected virtual void OnLoaded()
        {
            Loaded?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnSaved()
        {
            Saved?.Invoke(this, EventArgs.Empty);
        }
    }
}
