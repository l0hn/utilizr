﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Utilizr.Database
{
    public class PrimaryKeyAttribute : Attribute
    {
    }

    public class NotNullAttribute : Attribute
    {
    }

    public class UniqueAttribute : Attribute
    {
    }

    public class DefaultValueAttribute : Attribute
    {
        public string Value { get; set; }
        public DefaultValueAttribute(string value)
        {
            Value = value;
        }
    }

    public class DBIgnoreAttribute : Attribute
    {}

    public static class AttributeHelper
    {
        private static List<Type> _intTypes = new List<Type>
        {
            typeof(int), 
            typeof(long), 
            typeof(Int16), 
            typeof(Int32), 
            typeof(Int64),
            typeof(short)
        };
        private static List<Type> _numericTypes = new List<Type>
        {
            typeof(float),
            typeof(double)
        };
        private static List<Type> _blobTypes = new List<Type>
        {
            typeof(byte[]),
            typeof(char[])
        };

        public static string ToColumnAttrString(this FieldInfo field)
        {
            string attrString = "";
            foreach (var attr in field.GetCustomAttributes(true))
            {
                if (attr is PrimaryKeyAttribute)
                {
                    attrString += " PRIMARY KEY ";
                }
                if (attr is NotNullAttribute)
                {
                    attrString += " NOT NULL ";
                }
                if (attr is DefaultValueAttribute)
                {
                    string quote = "";
                    if (field.FieldType.GetDbType() == "TEXT")
                    {
                        quote = "'";
                    }
                    attrString += string.Format(" DEFAULT({0}{1}{0}) ", quote, ((DefaultValueAttribute)attr).Value);
                }
            }
            return attrString.Trim();
        }

        public static string GetDbType(this Type type)
        {
            if (_intTypes.Contains(type))
            {
                return "INTEGER";
            }
            else if (_numericTypes.Contains(type))
            {
                return "NUMERIC";
            }
            else if (_blobTypes.Contains(type))
            {
                return "BLOB";
            }
            else if (type == typeof(DateTime))
            {
                return "INTEGER";
            }
            return "TEXT";
        }

        public static bool IsPrimaryKey(this FieldInfo field)
        {
            foreach (var attr in field.GetCustomAttributes(true))
            {
                if (attr is PrimaryKeyAttribute)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool HasIgnoreAttribute(this FieldInfo field)
        {
            return field.GetCustomAttributes(true).Any(i => i is DBIgnoreAttribute);
        }
    }
}
