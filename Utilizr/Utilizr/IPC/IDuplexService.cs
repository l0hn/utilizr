﻿#if !MONO
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
#if NETCOREAPP
using System.Threading.Tasks;
using UiPath.CoreIpc;    
#endif


namespace Utilizr.IPC
{
#if !NETCOREAPP
    [ServiceContract]
#endif
    public interface IDuplexService
    {
#if NETCOREAPP
        Task RegisterForCallbacks(Message message);
#else
        [OperationContract(IsOneWay = true)]
        void RegisterForCallbacks();
#endif

#if NETCOREAPP
        Task Ping();
#else
        [OperationContract(IsOneWay = true)]
        void Ping();
#endif
        
    }
    

#if !NETCOREAPP
    [ServiceContract]
#endif
    public interface IAuthenticatedService
    {
#if NETCOREAPP
        Task<AuthResult> Authenticate(string token, Message message);
#else
        [OperationContract()]
        AuthResult Authenticate(string token);
#endif
    }

    [Serializable]
    public class AuthResult
    {
        public bool Success { get; set; }
    }
}
#endif