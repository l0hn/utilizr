﻿
using System.Data;
using System.Threading;
#if !MONO
using System;
using System.ServiceModel;

#if NETCOREAPP
using System.Threading.Tasks;
using System.IO.Pipes;
using UiPath.CoreIpc;
using UiPath.CoreIpc.NamedPipe;
#else
using System.ServiceModel.Description;
using System.Threading;
#endif
using GetText;
using Utilizr.Async;
using Utilizr.IPC;
using Utilizr.Logging;

namespace Utilizr.IPC
{
    public class ClientContainer<TService, TCallbackInterface>
        where TService: class
        where TCallbackInterface: class, ICallbackContext
    {
#if NETCOREAPP
        private bool _connected = false;
#else
        private ICommunicationObject _iCommObj;
        private InstanceContext _instanceContext;
#endif
        
        private TService _client;
        private ICallbackContext _callbackContext;
        private string _ipcAddress;
        private readonly string _typeName = typeof (TService).Name;
        
        private readonly object SETUP_LOCK = new object();
        public event EventHandler Connected;
        public event EventHandler Disconnected;

        /// <summary>
        /// The remoted object of type TService
        /// NOTE: Access to this property causes potentially blocking operations
        /// </summary>
        public TService Client
        {
            get
            {
                SetupClient();
                return _client;
            }
        }
        
        /// <summary>
        /// Simplify the process of connecting to a wcf service, with an optional callback context. And auto reconnection
        /// NOTE: If using a callback context the TService must implement IDuplexService!!!
        /// </summary>
        /// <param name="ipcAddress"></param>
        /// <param name="callbackImplementationObject"></param>
        internal ClientContainer(string ipcAddress, ICallbackContext callbackImplementationObject = null)
        {
            if (callbackImplementationObject != null
                && typeof(TService).GetInterface(nameof(IDuplexService)) == null)
            {
                throw new ArgumentException("TService MUST implement IDuplexService if you want use callbacks");
            }
            _ipcAddress = ipcAddress;
            _callbackContext = callbackImplementationObject;
            
            BeginPing();
        }
        
        public IAsyncResult BeginInvoke(Action<TService> action, AsyncCallback callback)
        {
            if (_client == null)
            {
                SetupClient();
            }

            return action.BeginInvoke(_client, callback, action);
        }

        public void EndInvoke(IAsyncResult result)
        {
            Action<TService> action = (Action<TService>)result.AsyncState;
            try
            {
                action.EndInvoke(result);
            }
            catch (ObjectDisposedException)
            {
                //todo: catch all the exceptions that indicate named pipe connection error and mark not connected / raise events
                _client = null;
                OnDisconnected();
                throw;
            }
        }

        void BeginPing()
        {
#if !NETCOREAPP
            return;
#endif
            AsyncHelper.BeginExecute(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    
                    var duplexClient = _client as IDuplexService;

                    try
                    {
#if NETCOREAPP
                        duplexClient?.Ping().Wait();
#else
                    //duplexClient.Ping(); not required when using WCF
#endif
                    }
                    catch (Exception e)
                    {
                        Log.Exception(e, _ipcAddress);
                    }
                }
            }, null, false);
        }

        void SetupClient()
        {
#if NETCOREAPP
            SetupIpcCoreClient();
#else
            SetupWCFClient();
#endif
        }

#if NETCOREAPP
        void SetupIpcCoreClient()
        {
            lock (SETUP_LOCK)
            {
                var provider = ServiceConfig.GetClientConfig<TService>();
                if (_client == null)
                {
                    _client = new NamedPipeClientBuilder<TService, TCallbackInterface>(_ipcAddress, provider)
                        .BeforeCall(async (info, token) =>
                        {
                            var proxy = _client as IpcProxy;
                            var serviceClient = (ServiceClient<TService>) proxy.ServiceClient;

                            if (info.NewConnection)
                            {
                                (_client as IDuplexService).RegisterForCallbacks(new Message()).Wait(TimeSpan.FromSeconds(10));
                                serviceClient.Disconnected += (sender, args) => OnDisconnected();
                                OnConnected();
                            }
                        })
                        .CallbackInstance((TCallbackInterface)_callbackContext)
                        .Build();
                }
            }
            
        }   
#else
        void SetupWCFClient()
        {
            lock (SETUP_LOCK)
            {
                if (_iCommObj == null || _iCommObj.State != CommunicationState.Opened)
                {
                    _client = default(TService);
                    CreateWCFClientInstance();
                    _iCommObj = (ICommunicationObject)_client;
                    _iCommObj.Opening += (sender, args) =>
                    {
                        Log.Info("WCF_CLIENT_HELPER", $"WCF Service Opening ({_typeName})");
                    };
                    _iCommObj.Opened += (sender, args) =>
                    {
                        Log.Info("WCF_CLIENT_HELPER", $"WCF Service Opened ({_typeName})");
                    };
                    _iCommObj.Faulted += (sender, args) =>
                    {
                        Log.Error("WCF_CLIENT_HELPER", $"WCF Service Faulted ({_typeName})");
                        OnDisconnected();
                    };
                    _iCommObj.Closed += (sender, args) =>
                    {
                        Log.Info("WCF_CLIENT_HELPER", $"WCF Service Closed ({_typeName})");
                        OnDisconnected();
                    };

                    //try multiple times to open the wcf comms
                    //bit nasty would prefer something nicer :s
                    for (int i = 0; i < 3; i++)
                    {
                        if (_iCommObj.State != CommunicationState.Opened)
                        {
                            Log.Info("WCF_CLIENT_HELPER", $"Attempt #{i} opening icommobj");
                            _iCommObj.Open(TimeSpan.FromSeconds(10));
                        }
                    }
                    //make sure to throw an error if not connected
                    if (_iCommObj.State != CommunicationState.Opened)
                    {
                        Log.Error("WCF_CLIENT_HELPER", $"Failed to open connection to {_typeName} after multiple attempts");
                        throw new IPCException(L._("Service Error: Unable to communicate with the {0} Service. Please try again.", _typeName));
                    }
                    Log.Info("WCF_CLIENT_HELPER", $"{_typeName} connected ok");

                    if (_instanceContext != null)
                    {
                        ((IDuplexService)_client).RegisterForCallbacks();
                        Log.Info("WCF_CLIENT_HELPER", $"Registered for service callbacks ({_typeName})");
                    }

                    OnConnected();
                }
            }
        }

        void CreateWCFClientInstance()
        {
            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = short.MaxValue;
            EndpointAddress ep = new EndpointAddress(_ipcAddress);
            TService client;
            if (_callbackContext == null)
            {
                client = ChannelFactory<TService>.CreateChannel(binding, ep);
            }
            else
            {
                _instanceContext = new InstanceContext(_callbackContext);
                _instanceContext.Closed += (sender, args) => Log.Info("IPC", $"Callback context closed ({_typeName})");
                _instanceContext.Opened += (sender, args) => Log.Info("IPC", $"Callback context opened ({_typeName})");
                _instanceContext.Faulted += (sender, args) => Log.Error("IPC", $"Callback context faulted ({_typeName})");
                var factory = new DuplexChannelFactory<TService>(_instanceContext, binding, ep);
#if DEBUG
                factory.Endpoint.Behaviors.Add(new CallbackDebugBehavior(true));
#endif
                client = factory.CreateChannel();
            }
            _client = client;
        }

#endif

        void OnDisconnected()
        {
#if NETCOREAPP
            if (!_connected)
            {
                return;
            }
            _connected = false;
#endif
            _callbackContext?.RaiseDisconnectEvent();
            Disconnected?.Invoke(this, EventArgs.Empty);
        }

        void OnConnected()
        {
#if NETCOREAPP
            _connected = true;
#endif
            Connected?.Invoke(this, EventArgs.Empty);
        }
    }

    public static class ClientContainer
    {
        public static ClientContainer<TService, IEmptyCallback> Create<TService>(string address)
            where TService : class
        {
            return new ClientContainer<TService, IEmptyCallback>(address);
        }

        public static ClientContainer<TService, TCallbackInterface> CreateDuplex<TService, TCallbackInterface>(string address,
            TCallbackInterface callbackContext) 
            where TService : class, IDuplexService
            where TCallbackInterface : class, ICallbackContext
        {
            return new ClientContainer<TService, TCallbackInterface>(address, callbackContext);
        }
    }
}
#endif