﻿using System;
using System.Collections.Generic;
using System.Text;

#if NETCOREAPP
using System.Threading.Tasks;
#endif

namespace Utilizr.IPC
{
    public interface ICallbackContext
    {
        event EventHandler Disconnected;
#if NETCOREAPP
        Task RaiseDisconnectEvent();
#else
        void RaiseDisconnectEvent();
#endif
    }

    public interface IEmptyCallback: ICallbackContext
    {
        
    }
}