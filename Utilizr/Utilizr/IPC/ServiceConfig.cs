﻿#if NETCOREAPP
using System;
using System.ServiceModel;
using Microsoft.Extensions.DependencyInjection;
using UiPath.CoreIpc;
using Utilizr.Statistics;

namespace Utilizr.IPC
{
    public static class ServiceConfig
    {
        public static IServiceProvider GetServerConfig<TInterface>(TInterface instance)
            where TInterface: class
            // where TConcrete: class, TInterface
        {
            return new ServiceCollection()
                .AddLogging()
                .AddIpc()
                .AddSingleton(instance)
                .BuildServiceProvider();
        }

        public static IServiceProvider GetClientConfig<TInterface>()
            where TInterface: class
        {
            return new ServiceCollection()
                .AddLogging()
                .AddIpc()
                .BuildServiceProvider();
        }
    }
}
#endif