﻿#if !MONO
using System;
using System.Collections.Generic;
using System.ServiceModel;

using Utilizr.Logging;

#if NETCOREAPP
using UiPath.CoreIpc;
#endif

namespace Utilizr.IPC
{
    /// <summary>
    /// Callback manager with auto unsubscribe for wcf callback interfaces
    /// </summary>
    /// <typeparam name="TCallbackInterface">Must be an interface</typeparam>
    public class ServiceCallbackManager<TCallbackInterface> where TCallbackInterface: class
    {
        private static readonly object _invokeLookupLock = new object();
#if NETCOREAPP
        private Dictionary<IClient, TCallbackInterface> _invokeLookup = new Dictionary<IClient, TCallbackInterface>();
#else
        private Dictionary<string, TCallbackInterface> _invokeLookup = new Dictionary<string, TCallbackInterface>();
#endif
        
        public List<TCallbackInterface> Invokers
        {
            get
            {
                lock (_invokeLookupLock)
                {
                    return new List<TCallbackInterface>(_invokeLookup.Values);
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <exception cref="System.ArgumentException"></exception>
        public ServiceCallbackManager()
        {
            if (!typeof(TCallbackInterface).IsInterface)
            {
                throw new ArgumentException("type of TCallbackInterface must be an interface");
            }
        }

#if NETCOREAPP
        public void AddSubscriber(Message message)
        {
            var client = message.Client;
            
            if (_invokeLookup.ContainsKey(client))
                return;
            
            
            var callbackContext = message.GetCallback<TCallbackInterface>();
            var ipcProxy = callbackContext as IpcProxy;
            var svcClient = (ServiceClient<TCallbackInterface>) ipcProxy?.ServiceClient;
            if (svcClient != null)
            {
                _invokeLookup[client] = callbackContext;
                svcClient.Disconnected += (sender, args) => RemoveSubscriber(client);
                Console.WriteLine("added subscriber");
            }
        }
#else
        public void AddSubscriber(OperationContext context)
        {
            if (_invokeLookup.ContainsKey(context.SessionId)) return;
            
            string sessionId = context.SessionId;
            _invokeLookup[sessionId] = context.GetCallbackChannel<TCallbackInterface>();
            context.Channel.Closed += (s, e) => RemoveSubscriber(sessionId);
            Log.Info("ipc", "added subscriber with id {0}", sessionId);
        }
#endif
        

        public void InvokeAll(Action<TCallbackInterface> predicate)
        {
            foreach (var callbackInterface in Invokers)
            {
                try
                {
                    predicate.Invoke(callbackInterface);
                }
                catch (Exception ex)
                {
                    Log.Exception($"CALLBACK_MANAGER[{typeof(TCallbackInterface).Name}]", ex);
                }
            }
        }

#if NETCOREAPP
        internal void RemoveSubscriber(IClient client)
        {
            lock (_invokeLookupLock)
            {
                _invokeLookup.Remove(client);
                Console.WriteLine("removed subscriber");
            }
        }
#else
        internal void RemoveSubscriber(string sessionId)
        {
            lock (_invokeLookupLock)
            {
                _invokeLookup.Remove(sessionId);
                Log.Info("ipc", "removed subscriber with id {0}", sessionId);
            }
        }
#endif

    }
}
#endif