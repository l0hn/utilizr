﻿#if !MONO
using System;
using System.Security.Principal;
using System.ServiceModel;

#if NETCOREAPP
using Microsoft.Extensions.DependencyInjection;
using System.IO.Pipes;
using UiPath.CoreIpc;
using UiPath.CoreIpc.NamedPipe;
#else
using System.ServiceModel.Description;
#endif

using Utilizr.Logging;

namespace Utilizr.IPC
{
    public static class SingletonServiceHelper<TConcrete, TInterface, TCallbackInterface>
        where TInterface: class
        where TConcrete: class, TInterface
        where TCallbackInterface: class
    {
        public static event EventHandler Faulted;
        private static ServiceHost _serviceHost;

#if !NETCOREAPP
        private static NetNamedPipeBinding _binding;
        private static ServiceEndpoint _endpoint;
#endif

        private static string _address;
        static readonly object SINGLETON_CREATE_LOCK = new object();

        private static TConcrete _instance;
        public static TConcrete Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (SINGLETON_CREATE_LOCK)
                {
                    if (_instance == null)
                    {
                        _instance = Activator.CreateInstance<TConcrete>();
                    }
                }

                return _instance;
            }
        }

#if NETCOREAPP
        public static void StartServer(string address)
        {
            _address = address;

            var provider = ServiceConfig.GetServerConfig<TInterface>(Instance);

            if (_serviceHost == null)
            {
                _serviceHost = new ServiceHostBuilder(provider)
                    .UseNamedPipes(new NamedPipeSettings(_address)
                    {
                        AccessControl = ac => ac.Allow(WellKnownSidType.WorldSid, PipeAccessRights.ReadWrite),
                        EncryptAndSign = false
                    })
                    .AddEndpoint<TInterface, TCallbackInterface>()
                    .Build();
            }   

            _serviceHost.RunAsync();
        }

        public static void StopServer()
        {
            if (_serviceHost == null)
            {
                return;
            }
        
            _serviceHost.Dispose();
            _serviceHost = null;
        }
#else
        public static void StartServer(string address)
        {
            if (!typeof(TInterface).IsInterface)
            {
                throw new ArgumentException("Type of TInterface must be of type interface");
            }

            if (!typeof(TInterface).IsAssignableFrom(typeof(TConcrete)))
            {
                throw new ArgumentException("Type of TConcrete must implement TInterface");
            }

            if (_serviceHost == null)
            {
                _address = address;
                _serviceHost = new ServiceHost(Instance);
                _serviceHost.Opened += _serviceHost_Opened;
                _serviceHost.Closed += _serviceHost_Closed;
                _serviceHost.Faulted += _serviceHost_Faulted;
                _serviceHost.UnknownMessageReceived += ServiceHostOnUnknownMessageReceived;
                _binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                _binding.ReceiveTimeout = TimeSpan.MaxValue;
                _binding.SendTimeout = TimeSpan.MaxValue;
                _binding.MaxReceivedMessageSize = int.MaxValue;
                _binding.ReaderQuotas.MaxStringContentLength = short.MaxValue;

                _endpoint = _serviceHost.AddServiceEndpoint(typeof(TInterface), _binding, _address);
                _serviceHost.Open(TimeSpan.FromSeconds(2));
            }
            else if (_serviceHost.State != CommunicationState.Opened &&
                _serviceHost.State != CommunicationState.Opening)
            {
                try
                {
                    _serviceHost.Open();
                }
                catch (ObjectDisposedException)
                {
                    _serviceHost = null;
                    StartServer(address);
                }
                catch (Exception ex)
                {
                    Log.Exception("singleton_service", ex, "", _serviceHost, _address, _binding);
                    throw;
                }
            }
        }

        private static void ServiceHostOnUnknownMessageReceived(object sender, UnknownMessageReceivedEventArgs e)
        {
            Log.Error("ipc", "unknown message received", new object[]{e});
        }

        public static void StopServer()
        {
            if (_serviceHost == null)
                return;

            try
            {
                _serviceHost.Close(TimeSpan.FromSeconds(2));
            }
            catch (Exception)
            {  
            }
            _serviceHost = null;
        }

        public static void RestartServer()
        {
            if (_serviceHost == null)
            {
                throw new InvalidOperationException("The server must be started before restart can be invoked");
            }
            if (_serviceHost.State != CommunicationState.Opened &&
                _serviceHost.State != CommunicationState.Opening)
            {
                _serviceHost.Open();
            }
        }
#endif


        static void _serviceHost_Faulted(object sender, EventArgs e)
        {
            Log.Info("service_host", "ServiceHost for service {0} Faulted", typeof(TConcrete).Name);
            Faulted?.Invoke(Instance, EventArgs.Empty);
        }

        static void _serviceHost_Closed(object sender, EventArgs e)
        {
            Log.Info("service_host", "ServiceHost for service {0} Closed", typeof(TConcrete).Name);
        }

        static void _serviceHost_Opened(object sender, EventArgs e)
        {
            Log.Info("service_host", "ServiceHost for service {0} Opened", typeof(TConcrete).Name);
        }
    }
}
#endif