﻿#if !MONO
using GetText;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Security;
using System.ServiceModel;

using Utilizr.Conversion;
using Utilizr.Logging;

#if NETCOREAPP
using System.IO.Pipes;
using UiPath.CoreIpc;
#endif
namespace Utilizr.IPC
{
    public class Authenticator<TCallbackInterface> where TCallbackInterface: class
    {
        private SecureString _key;
        private string _svcName;
#if NETCOREAPP
        private Dictionary<IClient, IClientInfo> _authenticatedClients = new Dictionary<IClient, IClientInfo>();
#else
        private Dictionary<InstanceContext, bool> _authenticatedClients = new Dictionary<InstanceContext, bool>();
#endif
        

        private object DICT_LOCK = new object();

        private bool _allowNullContext;

        public Authenticator(string key, string svcName, bool allowNullCtx)
        {
            _allowNullContext = allowNullCtx;
            _key = key.ToSecureString();
            _svcName = svcName;
        }

        public bool IsAuthenticated(
#if NETCOREAPP
            Message message
#endif
            ) 
        {

#if NETCOREAPP
            var ctx = message.Client;
#else
            var ctx = OperationContext.Current?.InstanceContext;
#endif

            if (ctx == null)
                return _allowNullContext;

            var authenticated = false;

            lock (DICT_LOCK)
            {
#if NETCOREAPP
                if (_authenticatedClients.TryGetValue(message.Client, out var info))
                    authenticated = info.IsAuthenticated;

#else
                _authenticatedClients.TryGetValue(ctx, out authenticated);
#endif

            }
            
            if (!authenticated)
            {
                Log.Warning(_svcName, "An unauthenticated client attempted an authenticated only procedure");
                try
                {
#if NETCOREAPP
                    var proxy = message.GetCallback<TCallbackInterface>() as IpcProxy;
                    proxy?.CloseConnection();
#else
                    if (ctx.State == CommunicationState.Opened)
                    {
                        ctx.Close(TimeSpan.FromMilliseconds(2000));
                    }
#endif

                }
                catch (Exception e)
                {
                }
            }

            return authenticated;
        }

        public AuthResult Authenticate(
            string token
#if NETCOREAPP
            ,Message message 
#endif
        )
        {
            CleanupAuthenticatedClients();

            var result = new AuthResult();
            foreach (var minuteVal in new[] { DateTime.UtcNow.AddMinutes(-1).Minute, DateTime.UtcNow.Minute, DateTime.UtcNow.AddMinutes(1).Minute })
            {
                var hash = $"{minuteVal}.{_key.ToUnsecureString()}".HashMD5();
                if (hash == token)
                {
                    result.Success = true;
                    lock (DICT_LOCK)
                    {
#if NETCOREAPP
                        var proxy = message.GetCallback<TCallbackInterface>() as IpcProxy;
                        _authenticatedClients[message.Client] = new IClientInfo(proxy, true);
#else
                        _authenticatedClients[OperationContext.Current.InstanceContext] = true;
#endif
                    }
                    break;
                }
            }

            return result;
        }

        void CleanupAuthenticatedClients()
        {
            try
            {
                lock (DICT_LOCK)
                {
#if NETCOREAPP
                    var disconnected = _authenticatedClients
                        .Where(kvp => !IsConnectionValid(kvp.Value))
                        .Select(i => i.Key);
#else
                    var states = new[] { CommunicationState.Closed, CommunicationState.Closing, CommunicationState.Faulted };
                    var disconnected = _authenticatedClients.Keys.Where(i => i.State.In(states));
#endif
      
                    foreach (var ctx in disconnected)
                    {
                        _authenticatedClients.Remove(ctx);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Exception(_svcName, e);
            }
        }


#if NETCOREAPP
        bool IsConnectionValid(IClientInfo clientInfo)
        {
            var svcClient = clientInfo.Proxy.ServiceClient as ServiceClient<TCallbackInterface>;
            var npCon = (NamedPipeServerStream) svcClient?.Connection?.Network;
            return npCon?.IsConnected ?? false;
        }
#endif
    }
}

#if NETCOREAPP
class IClientInfo
{
    public IpcProxy Proxy { get; set; }

    public bool IsAuthenticated { get; set; }

    public IClientInfo(IpcProxy proxy, bool isAuthenticated)
    {
        Proxy = proxy;
        IsAuthenticated = isAuthenticated;
    }
}
#endif

public class IPCAuthErrorException : Exception
{
    public IPCAuthErrorException()
        : base("IPC authentication error")
    {

    }
}

#endif