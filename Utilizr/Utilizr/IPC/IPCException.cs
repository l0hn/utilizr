﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilizr.IPC
{
    public class IPCException: Exception
    {
        public IPCException(string message):base(message)
        {
            
        }
    }
}
