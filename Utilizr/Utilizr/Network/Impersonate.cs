﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using Utilizr.Info;

namespace Utilizr.Network
{
#if !NETCOREAPP

    //http://msdn.microsoft.com/en-us/library/windows/desktop/aa378184(v=vs.85).aspx
    //http://msdn.microsoft.com/en-us/library/windows/desktop/aa366877(v=vs.85).aspx

    public class Impersonate : IDisposable
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern int FormatMessage(int dwFlags, IntPtr lpSource, int dwMessageId, int dwLanguageId, [Out] StringBuilder lpBuffer, int nSize, string[] arguments);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private extern static bool DuplicateToken(IntPtr existingTokenHandle, int SECURITY_IMPERSONATION_LEVEL, ref IntPtr duplicateTokenHandle);

        // logon types
        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_LOGON_NETWORK = 3;
        private const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

        // logon providers
        private const int LOGON32_PROVIDER_DEFAULT = 0;
        private const int LOGON32_PROVIDER_WINNT50 = 3;
        private const int LOGON32_PROVIDER_WINNT40 = 2;
        private const int LOGON32_PROVIDER_WINNT35 = 1;

        private bool _disposed = false;

        private WindowsImpersonationContext _impersonatedUser;
        public WindowsImpersonationContext impersonatedUser
        {
            get { return _impersonatedUser; }
        }
            
        public IntPtr _token;
        public IntPtr Token { get { return _token; } }

        private IntPtr _dupToken;
        public IntPtr DupToken {  get { return _dupToken; }}

        public Impersonate(string user, string password, string targetShare)
        {
            if (!Platform.IsWindows)
            {
                throw new PlatformNotSupportedException("Impersonate is only supported on the windows operating system");
            }
            _token = IntPtr.Zero;
            _dupToken = IntPtr.Zero;

            bool isSuccess = LogonUser(user, targetShare, password, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_DEFAULT, ref _token);

            if (!isSuccess)
            {
                RaiseLastError();
            }

            isSuccess = DuplicateToken(Token, 2, ref _dupToken);

            if (!isSuccess)
            {
                RaiseLastError();
            }

            WindowsIdentity newIdentity = new WindowsIdentity(DupToken);
            _impersonatedUser = newIdentity.Impersonate();
        }

        //public void impersonate

        // GetErrorMessage formats and returns an error message
        // corresponding to the input errorCode.
        public static string GetErrorMessage(int errorCode)
        {
            int FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
            int FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
            int FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;

            int messageSize = 255;
            StringBuilder lpMsgBuf = new StringBuilder();
            int dwFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;

            IntPtr ptrlpSource = IntPtr.Zero;

            int retVal = FormatMessage(dwFlags, ptrlpSource, errorCode, 0, lpMsgBuf, messageSize, new string[0]);

            CloseHandle(ptrlpSource);

            if (retVal == 0)
            {
                throw new ApplicationException(string.Format("Failed to format message for error code '{0}'.", errorCode));
            }

            return lpMsgBuf.ToString();
        }

        private static void RaiseLastError()
        {
            int errorCode = Marshal.GetLastWin32Error();
            string errorMessage = GetErrorMessage(errorCode);
            throw new ApplicationException(errorMessage);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed)
                return;

            try
            {
                _impersonatedUser.Undo();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            bool tokenSuccess = CloseHandle(Token);
            bool dupTokenSucess = CloseHandle(DupToken);

            _disposed = true;
        }
    }
#endif
}
