﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Utilizr;
using Utilizr.Async;
using Utilizr.Logging;

namespace Utilizr.Network
{
    public delegate void ErrorHandler(object sender, Exception ex);
    /// <summary>
    /// Simple async telnet client
    /// </summary>
    public class TelnetClient: IDisposable
    {

        /// <summary>
        /// Fired when a line is recieved from the server.
        /// </summary>
        public event EventHandler<LineRecievedEventArgs> LineReceieved;
        public event ErrorHandler Error;
        public event EventHandler Disconnected;

        private TcpClient _tcpClient;
        private Stream _tcpStream;
        private ManualResetEvent _sendReady;
        private Queue<string> _sendQueue;
        private ManualResetEvent _receiveReady;
        private Queue<string> _receiveQueue;
        private object WRITE_LOCK = new object();
        private object READ_LOCK = new object();
        private string _host;
        private int _port;
        private bool _connected = false;
        private bool _isDisposing;

        public bool Connected => _connected;
        public MessageRouter Router { get; private set; }

        /// <summary>
        /// Will cause a line received event if an exact match is received without a newline
        /// </summary>
        public List<String> MagicPhrases { get; }

        public TelnetClient(string host, int port, int sendTimeout = 0, int recieveTimeout = 0)
        {
            Router = new MessageRouter(this);
            _sendReady = new ManualResetEvent(false);
            _sendQueue = new Queue<string>();
            _receiveReady = new ManualResetEvent(false);
            _receiveQueue = new Queue<string>();
            _host = host;
            _port = port;
            _tcpClient = new TcpClient();
            _tcpClient.SendTimeout = sendTimeout;
            _tcpClient.ReceiveTimeout = recieveTimeout;
            MagicPhrases = new List<string>();
        }

        public IAsyncResult Connect(AsyncCallback callback)
        {
            return AsyncHelper.BeginExecute(() =>
            {
                _sendQueue.Clear();
                _receiveQueue.Clear();
                _tcpClient.Connect(_host, _port);
                _tcpStream = _tcpClient.GetStream();

                AsyncHelper.BeginExecute(SendLoop, ar =>
                {
                    try
                    {
                        AsyncHelper.EndExecute(ar);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception("TELNET", ex);
                    }
                }, false);

                AsyncHelper.BeginExecute(RecieveLoop, ar =>
                {
                    try
                    {
                        AsyncHelper.EndExecute(ar);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception("TELNET", ex);
                        Log.Info("TELNET", $"Unprocessed messages: send:{_sendQueue.Count}, recieve:{_receiveQueue.Count}");
                    }
                    _sendReady.Set();
                    _receiveReady.Set();
                    OnDisconnected();
                }, false);

                AsyncHelper.BeginExecute(RecieveProcessLoop, ar =>
                {
                    try
                    {
                        AsyncHelper.EndExecute(ar);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception("TELNET", ex);
                    }
                }, false);

                _connected = true;
            }, callback);
            
        }

        public void Disconnect()
        {
            _tcpClient.Close();
        }

        /// <summary>
        /// Queue a message to be sent
        /// </summary>
        /// <param name="line"></param>
        public void Send(string line)
        {
            lock (WRITE_LOCK)
            {
                _sendQueue.Enqueue(line);
                _sendReady.Set();
            }
        }

        void SendLoop()
        {
            while (_tcpClient.Connected)
            {
                _sendReady.WaitOne();
                lock (WRITE_LOCK)
                {
                    if (_sendQueue.Count <= 0)
                    {
                        _sendReady.Reset();
                        continue;
                    }
                    var nextLine = _sendQueue.Dequeue();
                    nextLine = nextLine.Trim('\r','\n') + "\r\n";
                    var bytes = Encoding.UTF8.GetBytes(nextLine);
                    var ar = _tcpStream.BeginWrite(bytes, 0, bytes.Length, null, null);
                    _tcpStream.EndWrite(ar);
                    _tcpStream.Flush();
                }
            }
        }

        void RecieveLoop()
        {
            int read;
            var buf = new byte[1*1024];
            string currentChunk = "";
            while (_tcpClient.Connected)
            {
                read = _tcpStream.EndRead(_tcpStream.BeginRead(buf, 0, buf.Length, null, null));
                if (read <= 0)
                {
                    continue;
                }
                currentChunk += Encoding.UTF8.GetString(buf, 0, read);
                
                var i = 0;
                var remainder = currentChunk.Length;
                while ((i = currentChunk.IndexOf("\r\n")) > -1)
                {
                    var line = currentChunk.Substring(0, i);
                    lock (READ_LOCK)
                    {
                        _receiveQueue.Enqueue(line);
                        _receiveReady.Set();
                    }
                    remainder = currentChunk.Length - (i + "\r\n".Length);
                    currentChunk = currentChunk.Substring(currentChunk.Length - remainder, remainder);
                }

                //annoyginly we sometimes need to raise a line received event even if the message does not end with \r\n
                if (MagicPhrases.Contains(currentChunk))
                {
                    lock (READ_LOCK)
                    {
                        _receiveQueue.Enqueue(currentChunk);
                        _receiveReady.Set();
                    }
                    currentChunk = "";
                }
            }
        }

        void RecieveProcessLoop()
        {
            while (_tcpClient.Connected)
            {
                try
                {
                    _receiveReady.WaitOne();
                    string line = "";
                    lock (READ_LOCK)
                    {
                        if (_receiveQueue.Count == 0)
                        {
                            _receiveReady.Reset();
                            continue;
                        }
                        line = _receiveQueue.Dequeue();
                    }
                    if (line.IsNullOrEmpty())
                    {
//                        Log.Info("TELNET_READ_PROCESSING", "Skipping empty line");
                        continue;
                    }
#if DEBUG
                    Console.WriteLine($"processing {line}");
#endif 
                    Router.Process(new LineRecievedEventArgs() { Message = line });
                    OnLineRecieved(line);
                }
                catch (Exception ex)
                {
                    Log.Exception("TELNET_READ_PROCESSING", ex);
                }
            }
        }

        protected virtual void OnLineRecieved(string line)
        {
            var lineArgs = new LineRecievedEventArgs() { Message = line };
            LineReceieved?.Invoke(this, lineArgs);
            if (!lineArgs.ResponseMessage.IsNullOrEmpty())
            {
                Send(lineArgs.ResponseMessage);
            }
        }
        
        protected virtual void OnError(Exception ex)
        {
            Error?.Invoke(this, ex);
        }

        protected virtual void OnDisconnected()
        {
            _connected = false;
            Disconnected?.Invoke(this, new EventArgs());
        }

        public void Dispose()
        {
            if (_isDisposing)
                return;

            _isDisposing = true;

            try
            {
                _tcpClient?.Close();
                _tcpStream?.Dispose();
            }
            catch (Exception ex)
            {
                Log.Exception("TELNET", ex);
            }
        }
    }

    public class LineRecievedEventArgs : EventArgs
    {
        /// <summary>
        /// Line recieved from the server
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Set this if you want to respond to the message
        /// </summary>
        public string ResponseMessage { get; set; } = null;

        internal void Reset()
        {
            ResponseMessage = null;
        }
    }

    public class MessageRouter
    {
        private List<Handler> _handlers;
        private TelnetClient _telnetClient;

        public MessageRouter(TelnetClient telnetClient)
        {
            _telnetClient = telnetClient;
            _handlers = new List<Handler>();
        }

        public void AddHandler(string stringToCompare, 
            Action<LineRecievedEventArgs> handler, 
            StringComparison comparison = StringComparison.OrdinalIgnoreCase, 
            bool startswith = true, 
            bool contains = false,
            bool endswith = false)
        {
            _handlers.Add(new Handler()
            {
                StringComparison = comparison,
                StringToCompare = stringToCompare,
                StartsWith = startswith,
                EndsWith = endswith,
                Contains = contains,
                Action = handler,
            });
        }

        public void Process(LineRecievedEventArgs args)
        {
            foreach (var handler in _handlers)
            {
                args.Reset();
                handler.ProcessMessage(args);

                if (args.ResponseMessage.IsNullOrEmpty())
                    continue;

                _telnetClient.Send(args.ResponseMessage);
            }
        }
    }

    internal class Handler
    {
        public bool StartsWith { get; set; }
        public bool Contains { get; set; }
        public bool EndsWith { get; set; }
        public StringComparison StringComparison { get; set; }
        public Action<LineRecievedEventArgs> Action { get; set; }
        public string StringToCompare { get; set; }

        internal Handler()
        {
        }

        public void ProcessMessage(LineRecievedEventArgs args)
        {
            
            var fire = (StartsWith && args.Message.StartsWith(StringToCompare, StringComparison)) ||
                       (EndsWith && args.Message.EndsWith(StringToCompare, StringComparison)) ||
                       (Contains && args.Message.IndexOf(StringToCompare, StringComparison) > -1);

            if (fire)
            {
                Action(args);
            }
        }
    }
}
