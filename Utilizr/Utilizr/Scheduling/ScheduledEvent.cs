﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Utilizr.Scheduling
{
    public delegate void ScheduledEventHandler(string eventName);

    public class ScheduledEvent : IDisposable
    {
        private ScheduleDBItem _scheduleItem;
        public event ScheduledEventHandler ScheduleEventDue;

        public string Name { get { return _scheduleItem.name; } }

        private Timer _timer;

        private ScheduledEvent()
        {

        }

        public static ScheduledEvent CreateDaily(string name, int hour, int minute)
        {
            ScheduledEvent sEvent = new ScheduledEvent();
            sEvent._scheduleItem = new ScheduleDBItem()
            {
                scheduleType = ScheduleType.Daily,
                startHour = hour,
                startMinute = minute,
                name = name
            };
            return sEvent;
        }

        public static ScheduledEvent CreateWeekly(string name, int hour, int minute, DaysOfWeek daysOfWeek)
        {
            ScheduledEvent sEvent = new ScheduledEvent();
            sEvent._scheduleItem = new ScheduleDBItem()
            {
                name = name,
                scheduleType = ScheduleType.SpecifiedDaysOfWeek,
                daysOfWeek = daysOfWeek,
                startHour = hour,
                startMinute = minute
            };
            return sEvent;
        }

        public static ScheduledEvent CreateHourly(string name, int minute)
        {
            ScheduledEvent sEvent = new ScheduledEvent();
            sEvent._scheduleItem = new ScheduleDBItem()
            {
                scheduleType = ScheduleType.Hourly,
                startMinute = minute,
                name = name
            };
            return sEvent;
        }

        public static ScheduledEvent FromDBItem(ScheduleDBItem dbItem)
        {
            ScheduledEvent sEvent = new ScheduledEvent()
            {
                _scheduleItem = dbItem
            };
            return sEvent;
        }

        public void Save()
        {
            _scheduleItem.Save();
        }

        public void Delete()
        {
            _scheduleItem.Delete();
        }

        public void Run()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }

            TimeSpan waitTime = (this.NextEventDue() - DateTime.UtcNow);
            Debug.WriteLine(string.Format("waiting {0} until firing event ({1})", waitTime, _scheduleItem.name));
            _timer = new Timer(TimerCallback, null, Math.Max(0, (int)waitTime.TotalMilliseconds), Timeout.Infinite);
        }

        public TimeSpan TimeUntilNextEvent()
        {
            return NextEventDue() - DateTime.UtcNow;
        }

        public DateTime NextEventDue()
        {
            TimeSpan timeFromLastEvent = DateTime.UtcNow - _scheduleItem.lastEventTime.ToUniversalTime();
            DateTime now = DateTime.UtcNow;
            DateTime todayStart = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc).Add(_scheduleItem.StartTime);
            switch (_scheduleItem.scheduleType)
            {
                case ScheduleType.Hourly:
                    DateTime thisHour = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0).AddMinutes(_scheduleItem.startMinute);
                    if (_scheduleItem.lastEventTime >= thisHour)
                    {
                        return thisHour.AddHours(1);
                    }
                    return thisHour;
                case ScheduleType.Daily:

                    if (_scheduleItem.lastEventTime >= todayStart) // has already run today
                    {
                        //due tomorrow
                        DateTime tomorrowStart = todayStart.AddDays(1);
                        return tomorrowStart;
                    }
                    else
                    {
                        //due today
                        return todayStart;
                    }
                case ScheduleType.SpecifiedDaysOfWeek:
                    for (int i = 0; i < 7; i++)
                    {
                        if (i != 0)
                        {
                            todayStart = todayStart.AddDays(1);
                        }
                        //is it scheduled for this weekday
                        if ((todayStart.DayOfWeek.ToSchedulerDayOfWeek() & _scheduleItem.daysOfWeek) != todayStart.DayOfWeek.ToSchedulerDayOfWeek())
                            continue;

                        //has it run on or after the start time
                        if (_scheduleItem.lastEventTime >= todayStart)
                            continue;

                        //it's due today
                        return todayStart;
                    }
                    break;
                default:
                    break;
            }
            throw new ArgumentException("Schedule item has no configured event times");
        }

        private void TimerCallback(object o)
        {
            _scheduleItem.lastEventTime = DateTime.UtcNow;
            OnScheduledEventDue(_scheduleItem.name);
            Run();
        }

        protected virtual void OnScheduledEventDue(string eventName)
        {
            if (ScheduleEventDue != null)
            {
                ScheduleEventDue(eventName);
            }
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }
    }

    public enum ScheduleType
    {
        Hourly = 0,
        Daily = 1,
        SpecifiedDaysOfWeek = 2
    }
}
