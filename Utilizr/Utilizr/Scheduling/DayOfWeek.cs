﻿using System;
using System.Collections.Generic;

namespace Utilizr.Scheduling
{
    public static class DayOfWeekHelper
    {
        private static Dictionary<DayOfWeek, DaysOfWeek> _dayOfWeekEnumLookup = new Dictionary<DayOfWeek, DaysOfWeek>()
        {
            {DayOfWeek.Monday, DaysOfWeek.Monday},
            {DayOfWeek.Tuesday, DaysOfWeek.Tuesday},
            {DayOfWeek.Wednesday, DaysOfWeek.Wednesday},
            {DayOfWeek.Thursday, DaysOfWeek.Thursday},
            {DayOfWeek.Friday, DaysOfWeek.Friday},
            {DayOfWeek.Saturday, DaysOfWeek.Saturday},
            {DayOfWeek.Sunday, DaysOfWeek.Sunday}
        };

        private static Dictionary<DaysOfWeek, DayOfWeek> _daysOfWeekEnumLookup = new Dictionary<DaysOfWeek, DayOfWeek>()
        {
            {DaysOfWeek.Monday, DayOfWeek.Monday},
            {DaysOfWeek.Tuesday, DayOfWeek.Tuesday},
            {DaysOfWeek.Wednesday, DayOfWeek.Wednesday},
            {DaysOfWeek.Thursday, DayOfWeek.Thursday},
            {DaysOfWeek.Friday, DayOfWeek.Friday},
            {DaysOfWeek.Saturday, DayOfWeek.Saturday},
            {DaysOfWeek.Sunday, DayOfWeek.Sunday}
        };

        public static DayOfWeek ToSystemDayOfWeek(this DaysOfWeek d)
        {
            return _daysOfWeekEnumLookup[d];
        }

        public static DaysOfWeek ToSchedulerDayOfWeek(this DayOfWeek d)
        {
            return _dayOfWeekEnumLookup[d];
        }
    }

    public enum DaysOfWeek
    {
        Monday = 1 << 0,
        Tuesday = 1 << 1,
        Wednesday = 1 << 2,
        Thursday = 1 << 3,
        Friday = 1 << 4,
        Saturday = 1 << 5,
        Sunday = 1 << 6,
    }
}
