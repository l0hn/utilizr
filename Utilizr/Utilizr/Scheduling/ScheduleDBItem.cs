﻿using System;
using Utilizr.Database;

namespace Utilizr.Scheduling
{
    public class ScheduleDBItem : Utilizr.Database.Tuple<ScheduleDBItem>
    {
        [PrimaryKey]
        public string name;

        public ScheduleType scheduleType;

        public DaysOfWeek daysOfWeek;

        public int startHour;
        public int startMinute;
        public int startSecond;

        public DateTime lastEventTime;

        public TimeSpan StartTime
        {
            get
            {
                return new TimeSpan(startHour, startMinute, startSecond);
            }
        }

        protected override string DatabaseFileName
        {
            get
            {
                return "utilizr_scheduler";
            }
        }
    }
}
