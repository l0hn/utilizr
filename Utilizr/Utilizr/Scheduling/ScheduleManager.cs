﻿using System;
using System.Collections.Generic;

namespace Utilizr.Scheduling
{
    public class ScheduleManager : List<ScheduledEvent>, IDisposable
    {
        public event ScheduledEventHandler ScheduledEventDue;

        public ScheduleManager()
            : base()
        {

        }

        public void LoadSchedule()
        {
            foreach (var item in this)
            {
                item.ScheduleEventDue -= item_ScheduleEventDue;
                item.Dispose();
            }
            Clear();
            foreach (var item in ScheduleDBItem.LoadMany(""))
            {
                Add(ScheduledEvent.FromDBItem(item));
            }
            RunAll();
        }

        new public void Add(ScheduledEvent item)
        {
            base.Add(item);
            item.ScheduleEventDue += item_ScheduleEventDue;
            item.Run();
        }

        new public void Remove(ScheduledEvent item)
        {
            if (Contains(item))
            {
                item.ScheduleEventDue -= item_ScheduleEventDue;
                item.Dispose();
                base.Remove(item);
            }
        }

        private void RunAll()
        {
            foreach (var item in this)
            {
                item.ScheduleEventDue += item_ScheduleEventDue;
                item.Run();
            }
        }

        void item_ScheduleEventDue(string eventName)
        {
            OnScheduledEventDue(eventName);
        }

        public virtual void OnScheduledEventDue(string name)
        {
            if (ScheduledEventDue != null)
            {
                ScheduledEventDue(name);
            }
        }

        public void SaveAll()
        {
            foreach (var item in this)
            {
                item.Save();
            }
        }

        public void Dispose()
        {
            foreach (var item in this)
            {
                item.ScheduleEventDue -= item_ScheduleEventDue;
                item.Dispose();
            }
            Clear();
        }
    }
}
