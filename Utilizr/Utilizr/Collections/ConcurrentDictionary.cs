﻿#if NET3 || MONO
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilizr.Collections
{
    public class ConcurrentDictionary<TKey, TVal>
    {
        private object LOCK = new object();
        private Dictionary<TKey, TVal> _dictionary;

        public ConcurrentDictionary()
        {
            _dictionary = new Dictionary<TKey, TVal>();
        }

        public bool TryAdd(TKey key, TVal val)
        {
            lock (LOCK)
            {
                if (_dictionary.ContainsKey(key))
                    return false;

                _dictionary.Add(key, val);
                return true;
            }
        }

        public void AddOrUpdate(TKey key, TVal val)
        {
            this[key] = val;
        }

        public TVal GetOrAdd(TKey key, TVal val)
        {
            lock (LOCK)
            {
                TVal existingVal;
                if (_dictionary.TryGetValue(key, out existingVal))
                {
                    return existingVal;
                }
                _dictionary[key] = val;
                return val;
            }
        }

        public bool TryGetValue(TKey key, out TVal val)
        {
            lock (LOCK)
            {
                return _dictionary.TryGetValue(key, out val);
            }
        }

        public bool TryRemove(TKey key, out TVal val)
        {
            lock (LOCK)
            {
                if (_dictionary.TryGetValue(key, out val))
                {
                    _dictionary.Remove(key);
                    return true;
                }
                return false;
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (LOCK)
            {
                return _dictionary.ContainsKey(key);
            }
        }

        public void Clear()
        {
            lock (LOCK)
            {
                _dictionary.Clear();
            }
        }

        public void Add(TKey key, TVal val)
        {
            lock (LOCK)
            {
                _dictionary.Add(key, val);
            }
        }

        public TVal this[TKey index]
        {
            get
            {
                lock (LOCK)
                {
                    return _dictionary[index];
                }
            }
            set
            {
                lock (LOCK)
                {
                    _dictionary[index] = value;
                }
            }
        }
    }
}
#endif