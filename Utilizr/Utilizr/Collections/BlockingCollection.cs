﻿#if NET3 || MONO
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Utilizr.Collections
{
    public class BlockingCollection<T>
    {
        private object LOCK = new object();
        private ManualResetEvent _newItemAvailable;
        private ManualResetEvent _capacityAvailable;
        List<T> _collection;
        private int _capacity;

        public int Count
        {
            get
            {
                lock (LOCK)
                {
                    return _collection.Count;
                }
            }
        }

        public BlockingCollection(int capacity = Int32.MaxValue)
        {
            _capacity = capacity;
            _newItemAvailable = new ManualResetEvent(false);
            _capacityAvailable = new ManualResetEvent(true);
            _collection = new List<T>();
        } 

        public void Add(T item)
        {
            lock (LOCK)
            {
                if (_collection.Count < _capacity)
                {
                    _collection.Add(item);
                    _newItemAvailable.Set();
                    return;
                }
                _capacityAvailable.Reset();
            }
            _capacityAvailable.WaitOne();
            Add(item);
        }

        public bool TryTake(out T item)
        {
            return TryTake(-1, out item);
        }

        public bool TryTake(int timeout, out T item)
        {
            item = Take(timeout);
            if (item == null)
            {
                return false;
            }
            return true;
        }
        

        public T Take()
        {
            return Take(-1);
        }

        public T Take(int timeout)
        {
            lock (LOCK)
            {
                var count = _collection.Count;
                if (count > 0)
                {
                    T item = _collection[count-1];
                    _collection.RemoveAt(count - 1);
                    _capacityAvailable.Set();
                    return item;
                }
                else
                {
                    _newItemAvailable.Reset();
                }
            }
            if (timeout == 0)
            {
                return default(T);
            } else if (timeout == -1)
            {
                _newItemAvailable.WaitOne();
            }
            else
            {
                if (!_newItemAvailable.WaitOne(timeout, false))
                {
                    return default(T);
                }
            }
            return Take(timeout);
        }

        public IEnumerable<T> All()
        {
            lock (LOCK)
            {
                return _collection.ToArray();
            }
        } 
    }
}
#endif