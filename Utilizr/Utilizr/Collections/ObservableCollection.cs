﻿#if!MONO
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Utilizr.Collections
{
    [DebuggerDisplay("Count={Count}")]
    public class ObservableCollection : IList, INotifyCollectionChanged, INotifyPropertyChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public int Count { get { return _backingList.Count; } }
        public bool IsFixedSize { get { return false; } }
        public bool IsReadOnly { get { return false; } }
        public bool IsSynchronized { get { return false; } }

        public object this[int index]
        {
            get { return _backingList[index]; }
            set { Insert(index, value); }
        }

        public object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                    Interlocked.CompareExchange<object>(ref _syncRoot, new object(), null);

                return _syncRoot;
            }
        }

        private readonly List<object> _backingList;
        private object _syncRoot;

        public ObservableCollection()
        {
            _backingList = new List<object>();
        }

        public int Add(object value)
        {
            _backingList.Add(value);
            OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
            OnPropertyChanged(nameof(Count));
            return IndexOf(value);
        }

        public void Clear()
        {
            _backingList.Clear();
            OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnPropertyChanged(nameof(Count));
        }

        public bool Contains(object value)
        {
            return _backingList.Any(p => p == value);
        }

        public void CopyTo(Array array, int index)
        {
            int j = index;
            for (int i = 0; i < _backingList.Count; i++)
            {
                array.SetValue(_backingList[i], j);
                j++;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return _backingList.GetEnumerator();
        }

        public int IndexOf(object value)
        {
            return _backingList.IndexOf(value);
        }

        public void Insert(int index, object value)
        {
            _backingList.Insert(index, value);
            OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
            OnPropertyChanged(nameof(Count));
        }

        public void Remove(object value)
        {
            _backingList.Remove(value);
            OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, value));
            OnPropertyChanged(nameof(Count));
        }

        public void RemoveAt(int index)
        {
            var item = _backingList[index];
            Remove(item);
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnNotifyCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            CollectionChanged?.Invoke(this, e);
        }
    }
}
#endif