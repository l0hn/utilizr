﻿#if!MONO
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;

namespace Utilizr.Collections
{
    [DebuggerDisplay("Count={Count}")]
    public class ObservableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public ICollection<TKey> Keys { get { return _dictionary.Keys; } }
        public ICollection<TValue> Values { get { return _dictionary.Values; } }
        public int Count { get { return _dictionary.Count; } }
        public bool IsReadOnly { get { return false; } }
        public TValue this[TKey key]
        {
            get { return _dictionary[key]; }
            set
            {   
                if(_dictionary.ContainsKey(key))
                {
                    TValue old = _dictionary[key];
                    _dictionary[key] = value;
                    NotifyChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, old));
                }
                else
                {
                    _dictionary[key] = value;
                    NotifyChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
                }
            }
        }

        private readonly Dictionary<TKey, TValue> _dictionary;

        public ObservableDictionary()
        {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        public bool ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            _dictionary.Add(key, value);
            NotifyChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
        }

        public bool Remove(TKey key)
        {
            var result = _dictionary.Remove(key);
            NotifyChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, Values));
            return result;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        public void Clear()
        {
            _dictionary.Clear();
            NotifyChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _dictionary.ContainsKey(item.Key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<TKey, TValue>>)_dictionary).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        private void NotifyChanges(NotifyCollectionChangedEventArgs e)
        {
            OnCollectionChanged(e);
            OnPropertyChagned(nameof(Count), nameof(Keys), nameof(Values));
        }

        protected virtual void OnPropertyChagned(params string[] properties)
        {
            foreach (var propertyName in properties)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            CollectionChanged?.Invoke(this, e);
        }
    }
}
#endif