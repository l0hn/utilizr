﻿using System;
using System.Collections.Generic;
using System.IO;
using Utilizr.Conversion;
using Utilizr.Database;
using Utilizr.Info;
using System.Linq;
using Newtonsoft.Json;
using System.Collections;
#if MONO
using Mono.Data.Sqlite;
#elif NETCOREAPP
using SqliteException = System.Data.SQLite.SQLiteException;
#else
using Community.CsharpSqlite.SQLiteClient;
#endif

namespace Utilizr.Collections
{
    /// <summary>
    /// Provides persistent generic dictionary with disk backing
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PersistentDictionary<T> : IDictionary<string, T>
    {
        private DataController _dataController;
        private string _tableName;
        private bool _useJson;

        /// <summary>
        /// Create a new peristent dictionary
        /// </summary>
        /// <param name="name">Name of the dictionary, this name will be used as the filename</param>
        /// <param name="overwrite">if true, any existing data will be cleared, otherwise PeristentDictionary will
        /// connect re-use exsting data</param>
        public PersistentDictionary(string name, bool overwrite)
            : base()
        {
            if (!typeof(T).IsConvertibleFrom<string>())
            {
                _useJson = true;
            }

            _tableName = name;
            name += ".dict";
            var path = Path.Combine(AppInfo.DataDirectory, name);

            if (overwrite)
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception e)
                {
                    
                }
            }
            
            _dataController = new DataController(path);
            Configure();
        }

        private void Configure()
        {
            using (var cmd = _dataController.GetCommand())
            {
                try
                {
                    cmd.CommandText = string.Format("create table if not exists {0} (key TEXT PRIMARY KEY, value TEXT)", _tableName);
                    cmd.ExecuteNonQuery();
                    foreach (string sql in new string[] { 
                    "ALTER TABLE {0} ADD COLUMN key TEXT PRIMARY KEY", 
                    "ALTER TABLE {0} ADD COLUMN value TEXT" })
                    {
                        try
                        {
                            cmd.CommandText = string.Format(sql, _tableName);
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqliteException)
                        {
                        }
                    }
                }
                finally
                {
                    _dataController.CloseConnection();
                }
            }
        }

        private void Insert(string key, T value)
        {
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("INSERT OR REPLACE INTO {0} (key, value) VALUES (@key, @value)", _tableName);
                var keyParam = cmd.CreateParameter();
                var valueParam = cmd.CreateParameter();
                keyParam.ParameterName = "@key";
                keyParam.Value = key;
                valueParam.ParameterName = "@value";
                var valueStr = ConvertToString(value);
                valueParam.Value = valueStr;
                cmd.Parameters.Add(keyParam);
                cmd.Parameters.Add(valueParam);
                if (cmd.ExecuteNonQuery() != 1)
                {
                    throw new KeyInsertException(key, valueStr);
                }
            }
        }

        private T ConvertFromString(string value)
        {
            if (_useJson)
            {
                return JsonConvert.DeserializeObject<T>(value);
            }
            else
            {
                return value.ConvertTo<T>();
            }
        }

        private string ConvertToString(T value)
        {
            if (_useJson)
            {
                return JsonConvert.SerializeObject(value);
            }
            else
            {
                return value.ConvertTo<string>();
            }
        }

        private bool GetValue(string key, out T val)
        {
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("SELECT value FROM {0} WHERE key=@key LIMIT 1", _tableName);
                var param = cmd.CreateParameter();
                param.ParameterName = "@key";
                param.Value = key;
                cmd.Parameters.Add(param);
                string value = (string)cmd.ExecuteScalar();
                if (value != null)
                {
                    val = ConvertFromString(value);
                    return true;
                }

                val = default(T);
                return false;
            }
        }

        private bool HasKey(string key)
        {
            try
            {
                using (var cmd = _dataController.GetCommand())
                {
                    cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} WHERE key=@key LIMIT 1", _tableName);
                    var param = cmd.CreateParameter();
                    param.ParameterName = "@key";
                    param.Value = key;
                    cmd.Parameters.Add(param);
                    if ((int)(long)cmd.ExecuteScalar() == 1)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        private int CountImpl()
        {
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("SELECT COUNT (*) FROM {0}", _tableName);
                object o = cmd.ExecuteScalar();
                return (int)(long)o;
            }
        }

        private IEnumerable<Type> GetFieldValues<Type>(TableField field)
        {
            using (var cmd = _dataController.GetCommand())
            {
                string sql = "SELECT {0} FROM {1} LIMIT {2} OFFSET {3}";
                int offset = 0;
                int count = 1000;
                while (true)
                {
                    cmd.CommandText = string.Format(sql, field.ToString(), _tableName, count, offset);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            break;
                        }
                        while (reader.Read())
                        {
                            if (field == TableField.key || !_useJson)
                            {
                                yield return reader.GetString(0).ConvertTo<Type>();
                            }
                            else
                            {
                                yield return JsonConvert.DeserializeObject<Type>(reader.GetString(0));
                            }
                        }
                    }
                    offset += count;
                }
            }
        }

        private IEnumerator<KeyValuePair<string, T>> GetEnumeratorImpl()
        {
            using (var cmd = _dataController.GetCommand())
            {
                string sql = "SELECT key, value FROM {0} LIMIT {1} OFFSET {2}";
                int offset = 0;
                int count = 1000;
                while (true)
                {
                    cmd.CommandText = string.Format(sql, _tableName, count, offset);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            break;
                        }
                        while (reader.Read())
                        {
                            string key = reader.GetString(0);
                            string strValue = reader.GetString(1);
                            T value = ConvertFromString(strValue);
                            yield return new KeyValuePair<string, T>(key, value);
                        }
                    }
                    offset += count;
                }
            }
        }

        private void Delete(string key)
        {
            using (var cmd = _dataController.GetCommand())
            {
                string sql = "DELETE FROM {0} WHERE key=@key";
                cmd.CommandText = string.Format(sql, _tableName);
                var param = cmd.CreateParameter();
                param.ParameterName = "@key";
                param.Value = key;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
            }
        }

        private void DeleteAll()
        {
            using (var cmd = _dataController.GetCommand())
            {
                cmd.CommandText = string.Format("DELETE FROM {0}", _tableName);
                cmd.ExecuteNonQuery();
            }
        }

        private enum TableField
        {
            key,
            value
        }

        #region IDictionary implementation

        public void Add(string key, T value)
        {
            Insert(key, value);
        }

        public bool ContainsKey(string key)
        {
            return HasKey(key);
        }

        public ICollection<string> Keys
        {
            get { return GetFieldValues<string>(TableField.key).ToList(); }
        }

        public bool Remove(string key)
        {
            try
            {
                Delete(key);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool TryGetValue(string key, out T value)
        {
            try
            {
                var found = GetValue(key, out value);
                return found;
            }
            catch (Exception)
            {
            }
            value = default(T);
            return false;
        }

        public ICollection<T> Values
        {
            get
            {
                return GetFieldValues<T>(TableField.value).ToList();
            }
        }

        public T this[string key]
        {
            get
            {
                var found = GetValue(key, out var tval);
                if (!found)
                {
                    throw new KeyNotFoundException();
                }

                return tval;
            }
            set
            {
                Insert(key, value);
            }
        }

        public void Add(KeyValuePair<string, T> item)
        {
            Insert(item.Key, item.Value);
        }

        public void Clear()
        {
            DeleteAll();
        }

        public bool Contains(KeyValuePair<string, T> item)
        {
            try
            {
                var found = GetValue(item.Key, out var tval);
                if (!found)
                {
                    return false;
                }
                if (tval.ToString() == item.Value.ToString())
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        public void CopyTo(KeyValuePair<string, T>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get
            {
                try
                {
                    return CountImpl();
                }
                catch (Exception)
                {
                }
                return 0;
            }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<string, T> item)
        {
            if (Contains(item))
            {
                Delete(item.Key);
                return true;
            }
            return false;
        }

        public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
        {
            return GetEnumeratorImpl();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumeratorImpl();
        }

        #endregion
    }
}