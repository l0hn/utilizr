﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Utilizr.Logging;

namespace Utilizr.Collections
{
    /// <summary>
    /// Based on http://www.codeproject.com/Articles/34405/WPF-Data-Virtualization
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class VirtualizingCollection<T> : IList, IList<T>
    {
        public bool IsFixedSize => true;
        public bool IsReadOnly => true;
        public bool IsSynchronized => false;
        public object SyncRoot => this;

        private int _count = -1;
        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// The first time this property is accessed, it will fetch the count from the underlying collection.
        /// </summary>
        public virtual int Count
        {
            get
            {
                if (_count == -1)
                    LoadCount();

                return _count;
            }
            protected set { _count = value; }
        }

        public object this[int index]
        {
            get
            {
                Log.Debug($"Getting index {index} for {typeof(T).Name}");
                // determine which page and offset within page
                int pageIndex = index / _pageSize;
                int pageOffset = index % _pageSize;

                // request primary page
                RequestPage(pageIndex);

                // if accessing upper 50% then request next page
                if (pageOffset > _pageSize / 2 && pageIndex < Count / _pageSize)
                    RequestPage(pageIndex + 1);

                // if accessing lower 50% then request prev page
                if (pageOffset < _pageSize / 2 && pageIndex > 0)
                    RequestPage(pageIndex - 1);

                // remove stale pages
                CleanUpPages();

                // defensive check in case of async load
                if (_pages[pageIndex] == null)
                    return default(T);

                // return requested item
                return _pages[pageIndex][pageOffset];
            }
            set { throw new NotSupportedException(); }
        }

        T IList<T>.this[int index]
        {
            get { return (T)this[index]; }
            set { throw new NotImplementedException(); }
        }

        private readonly IList<T> _virtualizedCollection;
        private readonly int _pageSize;
        private readonly int _pageTimeoutMs;
        private readonly Dictionary<int, IList<T>> _pages;
        private readonly Dictionary<int, DateTime> _pageTouchTimes;

        public VirtualizingCollection(IList<T> toVirtualize, int pageSize = 100, int pageTimeoutMs = 10 * 1000)
        {
            _virtualizedCollection = toVirtualize;
            _pageSize = pageSize;
            _pageTimeoutMs = pageTimeoutMs;
            _pages = new Dictionary<int, IList<T>>();
            _pageTouchTimes = new Dictionary<int, DateTime>();
        }



        /// <summary>
        /// Not supported
        /// </summary>
        public void Add(T item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public int Add(object value)
        {
            throw new NotSupportedException();
        }

        public void Clear()
        {
            _virtualizedCollection.Clear();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public bool Contains(T item)
        {
            return false;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public bool Contains(object value)
        {
            return Contains((T)value);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void CopyTo(Array array, int index)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public int IndexOf(T item)
        {
            return -1;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public int IndexOf(object value)
        {
            return IndexOf((T)value);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void Insert(int index, T item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void Insert(int index, object value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public bool Remove(T item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void Remove(object value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public void RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _virtualizedCollection.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _virtualizedCollection.GetEnumerator();
        }



        /// <summary>
        /// Makes a request for the specified page, creating the necessary slots in the dictionary,
        /// and updating the page touch time.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        protected virtual void RequestPage(int pageIndex)
        {
            if (!_pages.ContainsKey(pageIndex))
            {
                _pages.Add(pageIndex, null);
                _pageTouchTimes.Add(pageIndex, DateTime.UtcNow);
                Log.Debug($"Added page {pageIndex} for {typeof(T).Name}");
                LoadPage(pageIndex);
            }
            else
            {
                _pageTouchTimes[pageIndex] = DateTime.UtcNow;
            }
        }        

        /// <summary>
        /// Loads the page of items.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        protected virtual void LoadPage(int pageIndex)
        {
            PopulatePage(pageIndex, FetchPage(pageIndex));
        }

        /// <summary>
        /// Populates the page within the dictionary.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="page">The page.</param>
        protected virtual void PopulatePage(int pageIndex, IList<T> page)
        {
            Log.Debug($"Page {pageIndex} populated for {typeof(T).Name}");

            if (_pages.ContainsKey(pageIndex))
                _pages[pageIndex] = page;
        }

        /// <summary>
        /// Cleans up any stale pages that have not been accessed in the period dictated by PageTimeout.
        /// </summary>
        public void CleanUpPages()
        {
            List<int> keys = new List<int>(_pageTouchTimes.Keys);
            foreach (int key in keys)
            {
                // page 0 is a special case, since WPF ItemsControl access the first item frequently
                if (key != 0 && (DateTime.UtcNow - _pageTouchTimes[key]).TotalMilliseconds > _pageTimeoutMs)
                {
                    _pages.Remove(key);
                    _pageTouchTimes.Remove(key);
                    Log.Debug($"Removed page {key} for {typeof(T).Name}");
                }
            }
        }

        /// <summary>
        /// Loads the count of items.
        /// </summary>
        protected virtual void LoadCount()
        {
            Count = _virtualizedCollection.Count;
        }

        protected virtual IList<T> FetchPage(int pageIndex)
        {
            return _virtualizedCollection.Skip(pageIndex * _pageSize)
                .Take(_pageSize)
                .ToList();
        }
    }
}