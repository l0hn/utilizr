﻿using System;

namespace Utilizr.Collections
{
    public class KeyInsertException: Exception
    {
        public KeyInsertException(string keyName, string value): 
            base(string.Format("Failed to add key '{0}' to collection with value [{1}]", keyName, value))
        {

        }
    }
}
