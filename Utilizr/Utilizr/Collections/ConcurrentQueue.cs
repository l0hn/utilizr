﻿using System;
using System.Collections.Generic;

namespace Utilizr.Collections
{
    public class ConcurrentQueue<T>
    {
        private Queue<T> _queue;
        private object OPERATION_LOCK = new object();

        public int Count
        {
            get
            {
                lock(OPERATION_LOCK)
                {
                    return _queue.Count;
                }
            }
        }

        public ConcurrentQueue()
        {
            _queue = new Queue<T>();
        }

        public ConcurrentQueue(int capacity)
        {
            _queue = new Queue<T>(capacity);
        }

        public ConcurrentQueue(IEnumerable<T> collection)
        {
            _queue = new Queue<T>(collection);
        }

        public void Enqueue(T item)
        {
            lock (OPERATION_LOCK)
            {
                _queue.Enqueue(item);
            }
        }

        public void Enqueue(IEnumerable<T> items)
        {
            lock(OPERATION_LOCK)
            {
                foreach (var item in items)
                {
                    _queue.Enqueue(item);
                }
            }
        }

        public T Dequeue()
        {
            lock (OPERATION_LOCK)
            {
                return _queue.Dequeue();
            }
        }

        public bool TryDequeue(out T item)
        {
            lock(OPERATION_LOCK)
            {
                if(_queue.Count > 0)
                {
                    item = _queue.Dequeue();
                    return true;
                }

                item = default(T);
                return false;
            }
        }

        public T Peek()
        {
            lock (OPERATION_LOCK)
            {
                return _queue.Peek();
            }
        }

        public bool TryPeek(out T item)
        {
            lock (OPERATION_LOCK)
            {
                if (_queue.Count > 0)
                {
                    item = _queue.Peek();
                    return true;
                }

                item = default(T);
                return false;
            }
        }

        public void Clear()
        {
            lock(OPERATION_LOCK)
            {
                _queue.Clear();
            }
        }

        public bool Contains(T item)
        {
            lock(OPERATION_LOCK)
            {
                return _queue.Contains(item);
            }
        }
    }
}
