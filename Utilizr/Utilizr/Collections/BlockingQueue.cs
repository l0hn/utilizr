﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Utilizr.Collections
{
    public class BlockingQueue<T>
    {
        private readonly Queue<T> _queue;
        private readonly AutoResetEvent _autoResetEvent;

        object LOCK = new object();

        public int Count
        {
            get
            {
                lock (_queue)
                {
                    return _queue.Count;
                }
            }
        }

        public BlockingQueue()
        {
            _queue = new Queue<T>();
        }

        public void Enqueue(T item)
        {
            lock (_queue)
            {
                _queue.Enqueue(item);
            }

            lock (LOCK)
            {
                Monitor.PulseAll(LOCK);
            }
        }

        public T Dequeue()
        {
            lock (_queue)
            {
                if (_queue.Count > 0)
                    return _queue.Dequeue();
            }


            lock (LOCK)
            {
                Monitor.Wait(LOCK);
            }

            return Dequeue();
        }
    }
}
