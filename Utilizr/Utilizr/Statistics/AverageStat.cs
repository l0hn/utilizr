﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilizr.Statistics
{
    public class AverageStat
    {
        object LIST_LOCK = new object();
        List<Stat> _stats;
        long _ttl;
        
        public long StatCount { get { return _stats.Count; } }
        long _allTimeCount;
        public long AllTimeCount { get { return _allTimeCount; } }

        public double EntriesPerSecond
        {
            get
            {
                removeExpired();
                double multiplier = (double)1000 / (double)_ttl;
                return (double)_stats.Count * multiplier;
            }
        }
        public double EntriesPerMinute
        {
            get
            {
                removeExpired();
                double multiplier = (double)60000 / (double)_ttl;
                return (double)_stats.Count * multiplier;
            }
        }

        double getAverage(long duration)
        {
            removeExpired();
            DateTime sinceTime = DateTime.UtcNow.AddMilliseconds(-duration);
            if (duration == 0) sinceTime = DateTime.MinValue.ToUniversalTime();

            double sum = 0;
            double count = 0;

            List<Stat> sampleData = new List<Stat>();
            lock (LIST_LOCK)
            {
                sampleData = _stats.Where(o => o.StatTime > sinceTime).ToList();
            }

            foreach (var stat in sampleData)
            {
                sum += stat.StatValue;
                count++;
            }
            sum = sum / count;
            return sum;
        }

        public double getAverage()
        {
            return getAverage(0);
        }

        public AverageStat(long ttl)
        {
            _ttl = ttl;
            _stats = new List<Stat>();
        }

        public void add(double val)
        {
            lock (LIST_LOCK)
            {
                _stats.Add(new Stat(val, _ttl));
                _allTimeCount++;
            }
        }

        void removeExpired()
        {
            DateTime now = DateTime.UtcNow;
            lock (LIST_LOCK)
            {
                _stats.RemoveAll(o => o.ExpiryTime < now);
            }
        }
    }

    public class Stat
    {
        public DateTime StatTime { get; set; }
        public DateTime ExpiryTime { get; set; }
        public double StatValue { get; set; }

        public Stat(double val, long ttl)
        {
            StatTime = DateTime.UtcNow;
            ExpiryTime = StatTime.AddMilliseconds(ttl);
            StatValue = val;
        }
    }

}

