﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Utilizr.Logging;
using static Utilizr.Windows.Win32;

namespace Utilizr.Windows
{
    public static class Processes
    {
        public static void KillProcess(UInt32 processId)
        {
            UInt32 lpExitCode = 0;
            UInt32 ret = 0;
            UInt32 usafe = 0;
            IntPtr[] hProcess = new IntPtr[1];
            hProcess[0] = IntPtr.Zero;

            hProcess[0] = OpenProcess(ProcessAccessFlags.QueryInformation | ProcessAccessFlags.Terminate | ProcessAccessFlags.Synchronize, 0, processId);
            if (hProcess[0] == IntPtr.Zero)
                throw new Win32Exception();
            // ask nice
            GetExitCodeProcess(hProcess[0], out lpExitCode);
            ret = (NtTerminateProcess(hProcess[0], lpExitCode));
            // wait for it
            do
            {
                ret = (WaitForMultipleObjects(1, hProcess, false, 100));
                usafe++;
            }
            while ((ret != (WAIT_OBJECT_0)) && (usafe < 100));
        }

        public static void KillProcess(Process process)
        {
            KillProcess((UInt32)process.Id);
        }

        public static bool LaunchAndWaitAsUser(string appCmdLine, uint processId, bool userInteractive)
        {
            return LaunchAsUser(appCmdLine, processId, userInteractive, true);
        }

        public static bool LaunchAsUser(string appCmdLine, uint processId, bool userInteractive, bool wait = false)
        {
            return LaunchAsUser(appCmdLine, processId, userInteractive, out _, wait);
        }

        public static bool LaunchAsUser(string appCmdLine, uint processId, bool userInteractive, out uint? exitCode, bool wait = false)
        {
            bool ret = false;
            exitCode = null;

            //IntPtr token = Security.GetPrimaryToken(processId);
            IntPtr token = Security.DuplicateProcessToken(processId);

            if (token != IntPtr.Zero)
            {
                IntPtr envBlock = GetEnvironmentBlock(token);
                ret = LaunchProcessAsUser(appCmdLine, token, envBlock, userInteractive, wait, out exitCode);

                if (envBlock != IntPtr.Zero)
                    DestroyEnvironmentBlock(envBlock);

                CloseHandle(token);
            }
            return ret;
        }

        public static IntPtr GetEnvironmentBlock(IntPtr token)
        {
            IntPtr envBlock = IntPtr.Zero;
            Environment.SetEnvironmentVariable("__compat_layer", "RunAsInvoker");
            bool retVal = CreateEnvironmentBlock(ref envBlock, token, true);
            if (retVal == false)
            {
                //Environment Block, things like common paths to My Documents etc.
                //Will not be created if "false"
                //It should not adversely affect CreateProcessAsUser.
                Log.Exception(new Win32Exception(Marshal.GetLastWin32Error()), "CreateEnvironmentBlock Error");
            }
            return envBlock;
        }

        public static bool LaunchProcess(string cmdLine, IntPtr envBlock, uint creationFlags, bool waitForExit = true)
        {
            bool result = false;
            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            SECURITY_ATTRIBUTES saProcess = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES saThread = new SECURITY_ATTRIBUTES();
            saProcess.nLength = (uint)Marshal.SizeOf(saProcess);
            saThread.nLength = (uint)Marshal.SizeOf(saThread);

            STARTUPINFO si = new STARTUPINFO();
            si.cb = (uint)Marshal.SizeOf(si);

            //if this member is NULL, the new process inherits the desktop
            //and window station of its parent process. If this member is
            //an empty string, the process does not inherit the desktop and
            //window station of its parent process; instead, the system
            //determines if a new desktop and window station need to be created.
            //If the impersonated user already has a desktop, the system uses the
            //existing desktop.

            //si.lpDesktop = @"WinSta0\Default"; //Modify as needed
            si.lpDesktop = "";
            si.dwFlags = (uint)(STARTUPINFO_FLAGS.STARTF_USESHOWWINDOW | STARTUPINFO_FLAGS.STARTF_FORCEONFEEDBACK);
            si.wShowWindow = SW_SHOW;

            result = CreateProcess(
                null,
                cmdLine,
                ref saProcess,
                ref saThread,
                false,
                creationFlags,
                envBlock,
                null,
                ref si,
                out pi
            );

            if (result == false)
            {
                int error = Marshal.GetLastWin32Error();
                Log.Exception(new Win32Exception(error));
                Log.Debug($"CreateProcessAsUser Error: {error}");
                return result;
            }

            if (!waitForExit)
            {
                return true;
            }

            WaitForSingleObject(pi.hProcess, INFINITE);

            uint exitCode;
            result = GetExitCodeProcess(pi.hProcess, out exitCode);
            CloseHandle(pi.hProcess);

            if (exitCode != 0)
            {
                Log.Exception(new Exception($"Started {cmdLine} but exited with {exitCode}"));
                return false;
            }

            return result;
        }

        public static bool LaunchProcessAsUser(string cmdLine, IntPtr token, IntPtr envBlock, bool userInteractive, bool waitForExit)
        {
            return LaunchProcessAsUser(cmdLine, token, envBlock, userInteractive, waitForExit, out _);
        }

        public static bool LaunchProcessAsUser(string cmdLine, IntPtr token, IntPtr envBlock, bool userInteractive, bool waitForExit, out uint? exitCode)
        {
            bool result = false;
            exitCode = null;

            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            SECURITY_ATTRIBUTES saProcess = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES saThread = new SECURITY_ATTRIBUTES();
            saProcess.nLength = (uint)Marshal.SizeOf(saProcess);
            saThread.nLength = (uint)Marshal.SizeOf(saThread);

            STARTUPINFO si = new STARTUPINFO();
            
            si.cb = (uint)Marshal.SizeOf(si);

            //if this member is NULL, the new process inherits the desktop
            //and window station of its parent process. If this member is
            //an empty string, the process does not inherit the desktop and
            //window station of its parent process; instead, the system
            //determines if a new desktop and window station need to be created.
            //If the impersonated user already has a desktop, the system uses the
            //existing desktop.

            //si.lpDesktop = @"winsta0\default"; //Modify as needed
            si.lpDesktop = null;
            si.dwFlags = (uint)(STARTUPINFO_FLAGS.STARTF_USESHOWWINDOW | STARTUPINFO_FLAGS.STARTF_FORCEONFEEDBACK);
            si.wShowWindow = SW_SHOW;
            si.hStdError = IntPtr.Zero;
            si.hStdInput = IntPtr.Zero;
            si.hStdOutput = IntPtr.Zero;
            si.lpReserved2 = IntPtr.Zero;
            si.cbReserved2 = 0;
            si.lpTitle = null;


            //When INTERACTIVE, service run locally, and needs to use CreateProcess since service 
            //running under logged in user's context, not local system. Account will not have
            //SE_INCREASE_QUOTA_NAME, and will fail with ERROR_PRIVILEGE_NOT_HELD (1314)


            // Environment.UserInteractive always return true for .net core, expose so callee
            // can set explicitly: https://github.com/dotnet/runtime/issues/770

            if (userInteractive)
            {
                result = CreateProcess(
                    null,
                    cmdLine,
                    ref saProcess,
                    ref saThread,
                    false,
                    CREATE_UNICODE_ENVIRONMENT | CREATE_NEW_CONSOLE,
                    envBlock,
                    null,
                    ref si,
                    out pi
                );
            }
            else
            {
                result = CreateProcessAsUser(
                    token,
                    null,
                    cmdLine,
                    ref saProcess,
                    ref saThread,
                    false,
                    CREATE_UNICODE_ENVIRONMENT,
                    envBlock,
                    null,
                    ref si,
                    out pi
                );
            }

            if (result == false)
            {
                int error = Marshal.GetLastWin32Error();
                Log.Exception(new Win32Exception(error));
                Log.Debug($"CreateProcessAsUser Error: {error}");
                return result;
            }

            if (!waitForExit)
            {
                return true;
            }

            WaitForSingleObject(pi.hProcess, INFINITE);

            result = GetExitCodeProcess(pi.hProcess, out uint ec);
            CloseHandle(pi.hProcess);
            exitCode = ec;

            if (exitCode != 0)
            {
                Log.Exception(new Exception($"Started {cmdLine} but exited with {exitCode}"));
                return false;
            }

            return result;
        }

        /// <summary>
        /// Get the filepath of the process, without an access denied error.
        /// </summary>
        public static string GetProcessFilename(Process p)
        {
            int capacity = 2000;
            var builder = new StringBuilder(capacity);
            var hProcess = OpenProcess(ProcessAccessFlags.QueryLimitedInformation, 0, (uint)p.Id);

            if (hProcess == IntPtr.Zero)
                throw new Win32Exception(Marshal.GetLastWin32Error());

            bool success = false;
            try
            {
                success = QueryFullProcessImageName(hProcess, 0, builder, ref capacity);
            }
            finally
            {
                CloseHandle(hProcess);
            }

            return success
                ? builder.ToString()
                : string.Empty;
        }

        /// <summary>
        /// Wait for a process to exit, without an access denied error.
        /// </summary>
        /// <param name="p"></param>
        public static void WaitForExit(Process p)
        {
            var flags = ProcessAccessFlags.QueryLimitedInformation | ProcessAccessFlags.Synchronize;

            var hProcess = OpenProcess(flags, 0, (uint)p.Id);
            if (hProcess == IntPtr.Zero)
                throw new Win32Exception(Marshal.GetLastWin32Error());

            try
            {
                if (WaitForSingleObject(hProcess, INFINITE) != 0)
                    throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            finally
            {
                CloseHandle(hProcess);
            }
        }
    }
}