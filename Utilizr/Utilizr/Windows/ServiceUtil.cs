﻿#if !MONO
using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using Utilizr.Async;
using Utilizr.Logging;

namespace Utilizr.Windows
{
    public static class ServiceUtil
    {
        //public static ManualResetEvent ServiceRestarting = new ManualResetEvent(true);

        public static bool IsInstalled(string serviceName)
        {
            try
            {
                var state = new ServiceController(serviceName).Status;
                return true;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }

        public static bool IsRunning(string serviceName)
        {
            try
            {
                return GetServiceStatus(serviceName) == ServiceControllerStatus.Running;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }

        private static ServiceControllerStatus GetServiceStatus(string serviceName)
        {
            using (var controller = new ServiceController(serviceName))
            {
                return controller.Status;
            }
        }

        public static void StartWindowsService(string serviceName)
        {
            //ServiceRestarting.WaitOne();

            using (var controller = new ServiceController(serviceName))
            {
                if (controller.Status != ServiceControllerStatus.Running)
                {
                    Log.Info("ENGINE", $"Starting {serviceName} service");
                    controller.Start();
                    controller.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(120));
                }
            }
        }

        public static void StopWindowsService(string serviceName, TimeSpan? timeout = null)
        {
            try
            {
                if (!IsInstalled(serviceName))
                {
                    return;
                }

                var controller = new ServiceController(serviceName);
                controller.Stop();

                if (timeout != null)
                    controller.WaitForStatus(ServiceControllerStatus.Stopped, timeout.Value);
                else
                    controller.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            catch (Exception ex)
            {
                Log.Exception("ENGINE", ex);
            }
        }

        public static void StartServiceWait(string serviceName)
        {
            Log.Info("ENGINE", "Starting service");
            while (GetServiceStatus(serviceName) != ServiceControllerStatus.Running)
            {
                try
                {
                    Log.Info("ENGINE", $"Service Status: {GetServiceStatus(serviceName)}");

                    StartWindowsService(serviceName);

                    if (GetServiceStatus(serviceName) != ServiceControllerStatus.Running)
                        Sleeper.Sleep(500);

                }
                catch (Exception e)
                {
                    Log.Exception("ENGINE", e, "Failed to start service!");
                }
            }
            Log.Info("ENGINE", $"Service status: {GetServiceStatus(serviceName)}");
        }

        public static void StopServiceWait(string serviceName)
        {
            Log.Info("ENGINE", "Stopping service");

            while (GetServiceStatus(serviceName) != ServiceControllerStatus.Stopped)
            {
                try
                {
                    Log.Info("ENGINE", $"Service Status: {ServiceUtil.GetServiceStatus(serviceName)}");

                    ServiceUtil.StopWindowsService(serviceName, TimeSpan.FromSeconds(20));

                    //If its stuck running ater 20 seconds - kill the process!
                    if (ServiceUtil.GetServiceStatus(serviceName) != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        Log.Info("ENGINE", $"Force stop service {ServiceUtil.GetServiceStatus(serviceName)}");
                        ForceStopService(serviceName);
                    }

                    if (ServiceUtil.GetServiceStatus(serviceName) != ServiceControllerStatus.Stopped)
                        Sleeper.Sleep(500);
                }
                catch (Exception e)
                {
                    Log.Exception("ENGINE", e, "Failed to start service!");
                }
            }
            Log.Info("ENGINE", $"Service status: {ServiceUtil.GetServiceStatus(serviceName)}");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName">name of service</param>
        /// <param name="action">Action to perform while service is stopped before being restarted</param>
        public static void RestartService(string serviceName, System.Action action)
        {
//            ServiceRestarting.Reset();
            try
            {
                Log.Info("ENGINE", "Stopping service");
                StopServiceWait(serviceName);                
                try
                {
                    Log.Info("ENGINE", "Performing restart action");
                    action?.Invoke();
                }
                finally
                {
                    Log.Info("ENGINE", "Restarting service");
//                    ServiceRestarting.Set();
                    StartServiceWait(serviceName);
                }
            }
            catch (Exception ex)
            {
                Log.Exception("ENGINE", ex);
                throw;
            }
        }

        private static void ForceStopService(string serviceName)
        {
            var serviceProc = Process.GetProcessesByName(serviceName).FirstOrDefault();
            if (serviceProc != null)
                serviceProc.Kill();
        }

        public static bool RunAsUser(string file, uint? pid = null)
        {
            if (pid == null)
            {
                var explorer = Process.GetProcessesByName("explorer").FirstOrDefault();
                if (explorer == null)
                    throw new ArgumentException($"{nameof(pid)} is null, unable to get ID for explorer.exe as backup");

                pid = (uint)explorer.Id;
            }

            try
            {
                bool userInteractive = false;
#if DEBUG
                userInteractive = true;
#endif
                return Processes.LaunchAndWaitAsUser(file, pid.Value, userInteractive);
            }
            catch (Exception ex)
            {
                Log.Exception(nameof(RunAsUser), ex);
                return false;
            }
        }
        
    }
}
#endif