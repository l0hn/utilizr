﻿using System;
using System.Runtime.InteropServices;

namespace Utilizr.Windows
{
    public static class NativeMethodResolver
    {
        const string _kernalDll = "Kernel32.dll";

        public static bool MethodExists(string libraryName, string methodName)
        {
            var libraryPtr = LoadLibrary(libraryName);
            var procPtr = GetProcAddress(libraryPtr, methodName);

            return libraryPtr != UIntPtr.Zero && procPtr != UIntPtr.Zero;
        }

        [DllImport(_kernalDll, SetLastError = true, CharSet = CharSet.Unicode)]
        static extern UIntPtr LoadLibrary(string lpFileName);

        [DllImport(_kernalDll, SetLastError = true, CharSet = CharSet.Ansi)]
        static extern UIntPtr GetProcAddress(UIntPtr hModule, string lpProcName);
    }

    public abstract class SafeNativeMethodResolver
    {
        bool _exists;
        bool _resolved;
        readonly string _libraryName;
        readonly string _methodName;

        protected SafeNativeMethodResolver(string libraryName, string methodName)
        {
            _libraryName = libraryName;
            _methodName = methodName;
        }

        protected bool CanInvoke
        {
            get
            {
                if (!_resolved)
                {
                    _exists = NativeMethodResolver.MethodExists(_libraryName, _methodName);
                    _resolved = true;
                }

                return _exists;
            }
        }
    }
}