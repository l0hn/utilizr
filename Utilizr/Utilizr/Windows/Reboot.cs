﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using static Utilizr.Windows.Win32;

namespace Utilizr.Windows
{
    /// <summary>
    /// Restarts the computer and exits application.
    /// Function will only return if unable to reboot.
    /// </summary>
    public static class Reboot
    {
        public static void RebootNow()
        {
            var tokenHandle = IntPtr.Zero;
            var rebootFailed = false;

            try
            {
                // get process token
                if (OpenProcessToken(Process.GetCurrentProcess().Handle, TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES, ref tokenHandle) == 0)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to open process token handle");
                }

                // lookup the shutdown privilege
                TOKEN_PRIVILEGES tokenPrivs = new TOKEN_PRIVILEGES();
                tokenPrivs.PrivilegeCount = 1;
                tokenPrivs.Privileges = new LUID_AND_ATTRIBUTES();
                tokenPrivs.Privileges.Attributes = SE_PRIVILEGE_ENABLED;

                if (!LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tokenPrivs.Privileges.pLuid))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to open lookup shutdown privilege");
                }

                // add the shutdown privilege to the process token
                if (!AdjustTokenPrivileges(tokenHandle, false, ref tokenPrivs, 0, IntPtr.Zero, IntPtr.Zero))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to adjust process token privileges");
                }

                // reboot
                if (!ExitWindowsEx(ExitWindows.Reboot, ShutdownReason.MajorApplication | ShutdownReason.MinorMaintenance | ShutdownReason.FlagPlanned))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to reboot system");
                }
            }
            catch
            {
                rebootFailed = true;
            }
            finally
            {
                // close the process token
                if (tokenHandle != IntPtr.Zero)
                {
                    CloseHandle(tokenHandle);
                }

                // close app for reboot
                if (!rebootFailed)
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}