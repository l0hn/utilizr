﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Utilizr.Windows
{
    public static class CloudPlaceholder
    {
        static SafeRtlIsPartialPlaceholder _partialPlaceholder;
        static SafeRtlIsCloudFilesPlaceholder _cloudFilesPlacholder;

        /// <summary>
        /// Returns true if the given parameters represent a file / directory which is not fully hydrated / populated.
        /// </summary>
        public static bool IsPartialPlaceholder(FileSystemInfo info)
        {
            return IsPartialPlaceholder(info.FullName, (uint)info.Attributes);
        }

        /// <summary>
        /// Returns true if the given parameters represent a file / directory which is not fully hydrated / populated.
        /// </summary>
        public static bool IsPartialPlaceholder(string path, uint attributes)
        {
            throw new Exception("Fix, not yet working correctly. Returns incorrect values."); // TODO
            if (_partialPlaceholder == null)
                _partialPlaceholder = new SafeRtlIsPartialPlaceholder();

            var reparsePointTag = GetReparsePoint(path, attributes);
            return _partialPlaceholder.Invoke(attributes, reparsePointTag);
        }

        /// <summary>
        /// Returns true if the given parameters represent a cloud placeholder file / directory regardless of hydrated / populated state.
        /// </summary>
        public static bool IsCloudFilesPlaceholder(FileSystemInfo info)
        {
            return IsCloudFilesPlaceholder(info.FullName, (uint)info.Attributes);
        }

        /// <summary>
        /// Returns true if the given parameters represent a cloud placeholder file / directory regardless of hydrated / populated state.
        /// </summary>
        public static bool IsCloudFilesPlaceholder(string path, uint attributes)
        {
            throw new Exception("Fix, not yet working correctly. Returns incorrect values."); // TODO
            if (_cloudFilesPlacholder == null)
                _cloudFilesPlacholder = new SafeRtlIsCloudFilesPlaceholder();

            var reparsePointTag = GetReparsePoint(path, attributes);
            return _cloudFilesPlacholder.Invoke(attributes, reparsePointTag);
        }

        static uint GetReparsePoint(string path, uint attributes)
        {
            if ((attributes & Win32.FILE_ATTRIBUTE_REPARSE_POINT) != Win32.FILE_ATTRIBUTE_REPARSE_POINT)
                return sizeof(uint);

            var data = new Win32.WIN32_FIND_DATAW();
            var handle = Win32.FindFirstFileW(path, out data);
            if (handle == new IntPtr(-1))
                return sizeof(uint);

            var reparsePoint = data.dwReserved0;
            Win32.FindClose(handle);
            return reparsePoint;
        }
    }

    sealed class SafeRtlIsPartialPlaceholder : SafeNativeMethodResolver
    {
        const string _dll = "Ntdll.dll";

        public SafeRtlIsPartialPlaceholder() 
            : base(_dll, "RtlIsPartialPlaceholder")
        {
        }

        /// <summary>
        /// Returns true if the given parameters represent a file / directory which is not fully hydrated / populated.
        /// </summary>
        public bool Invoke(UInt32 fileAttributes, UInt32 reparseTag)
        {
            return (CanInvoke ? RtlIsPartialPlaceholder(fileAttributes, reparseTag) : false);
        }

        [DllImport(_dll, SetLastError = true)]
        static extern bool RtlIsPartialPlaceholder(UInt32 fileAttributes, UInt32 reparseTag);
    }

    sealed class SafeRtlIsCloudFilesPlaceholder : SafeNativeMethodResolver
    {
        const string _dll = "Ntdll.dll";

        public SafeRtlIsCloudFilesPlaceholder()
            : base(_dll, "RtlIsCloudFilesPlaceholder")
        {
        }

        /// <summary>
        /// Returns true if the given parameters represent a cloud placeholder file / directory regardless of hydrated / populated state.
        /// </summary>
        public bool Invoke(UInt32 fileAttributes, UInt32 reparseTag)
        {
            return (CanInvoke ? RtlIsCloudFilesPlaceholder(fileAttributes, reparseTag) : false);
        }

        [DllImport(_dll, SetLastError = true)]
        static extern bool RtlIsCloudFilesPlaceholder(UInt32 fileAttributes, UInt32 reparseTag);
    }
}