﻿#if !MONO

using System;
using System.ServiceProcess;
using System.Threading;
using Utilizr.Async;
using Utilizr.Logging;
using TimeoutException = System.ServiceProcess.TimeoutException;

namespace Utilizr.Windows
{
    public class ServiceWatcher: IDisposable
    {
        public event EventHandler Started;
        public event EventHandler Stopped;
        
        private const string LOG_CAT = "service_watcher";
        public string ServiceName { get; set; }
        public bool Watching { get; set; } = false;

        private ServiceController _sc;
        
        public ServiceWatcher(string serviceName)
        {
            ServiceName = serviceName;
        }

        public void WatchAsync()
        {
            AsyncHelper.BeginExecute(Watch, useThreadPool:false);
        }
        
        private void Watch()
        {
            Log.Info(LOG_CAT, $"monitoring started for service: {ServiceName}");

            var state = ServiceUtil.IsRunning(ServiceName);
            Log.Info(LOG_CAT, $"initial state is {state}");
            _sc?.Dispose();
            _sc = new ServiceController(ServiceName);
            Watching = true;
            
            while (Watching)
            {
                try
                {
                    var waitState = state ? ServiceControllerStatus.Stopped : ServiceControllerStatus.Running;
                    _sc.WaitForStatus(waitState, TimeSpan.FromMinutes(5));
                    state = waitState == ServiceControllerStatus.Running ? true : false;
                    
                    if (state)
                        OnStarted();
                    else
                        OnStopped();
                }
                catch (TimeoutException)
                {
                    //this is fine
                }
                catch (Exception e)
                {
                    if (!Watching)
                        return;
                    
                    Log.Exception(LOG_CAT, e);
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }
            }
        }

        private void StopWatching()
        {
            Watching = false;
            _sc.Dispose();
        }

        public void Dispose()
        {
            StopWatching();
            _sc?.Dispose();
        }

        protected virtual void OnStarted()
        {
            if (!Watching)
                return;
            
            Log.Info(LOG_CAT, $"The {ServiceName} service started");
            Started?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnStopped()
        {
            if (!Watching)
                return;
            
            Log.Info(LOG_CAT, $"The {ServiceName} service stopped");
            Stopped?.Invoke(this, EventArgs.Empty);
        }
    }
}

#endif