﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Utilizr.Logging;

namespace Utilizr.Windows
{
    public static class Security
    {
        public static Boolean AdjustToken(Boolean Enable, string[] rights)
        {
            IntPtr hToken = IntPtr.Zero;
            IntPtr hProcess = IntPtr.Zero;
            Win32.LUID tLuid = new Win32.LUID();
            Win32.TOKEN_PRIVILEGES NewState = new Win32.TOKEN_PRIVILEGES();
            UInt32 uPriv = (UInt32)(Win32.ETOKEN_PRIVILEGES.TOKEN_ADJUST_PRIVILEGES | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY_SOURCE);

            try
            {
                hProcess = Win32.OpenProcess(Win32.ProcessAccessFlags.All, 0, Win32.GetCurrentProcessId());
                if (hProcess == IntPtr.Zero)
                    return false;
                if (Win32.OpenProcessToken(hProcess, uPriv, ref hToken) == 0)
                    return false;
                for (Int32 i = 0; i < rights.Length; i++)
                {
                    // Get the local unique id for the privilege.
                    if (!Win32.LookupPrivilegeValue(null, rights[i], ref tLuid))
                        return false;
                }
                // Assign values to the TOKEN_PRIVILEGE structure.
                NewState.PrivilegeCount = 1;
                NewState.Privileges.pLuid = tLuid;
                NewState.Privileges.Attributes = (Enable ? Win32.SE_PRIVILEGE_ENABLED : 0);
                // Adjust the token privilege
                //IntPtr pState = IntPtr.Zero;
                //Marshal.StructureToPtr(NewState, pState, true);
                return (Win32.AdjustTokenPrivileges(hToken, false, ref NewState, (uint)Marshal.SizeOf(NewState), IntPtr.Zero, IntPtr.Zero));
            }
            finally
            {
                if (hToken != IntPtr.Zero)
                    Win32.CloseHandle(hToken);
                if (hProcess != IntPtr.Zero)
                    Win32.CloseHandle(hProcess);
            }
        }

        public static Boolean IsAdmin()
        {
            IntPtr hToken = IntPtr.Zero;
            IntPtr hProcess = IntPtr.Zero;
            Win32.LUID tLuid = new Win32.LUID();
            UInt32 uPriv = (UInt32)(Win32.ETOKEN_PRIVILEGES.TOKEN_ADJUST_PRIVILEGES | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY_SOURCE);

            try
            {
                hProcess = Win32.OpenProcess(Win32.ProcessAccessFlags.All, 0, Win32.GetCurrentProcessId());
                if (hProcess == IntPtr.Zero)
                    return false;
                if (Win32.OpenProcessToken(hProcess, uPriv, ref hToken) == 0)
                    return false;
                return (Win32.LookupPrivilegeValue(null, Win32.SE_TCB_NAME, ref tLuid));
            }
            finally
            {
                if (hToken != IntPtr.Zero)
                    Win32.CloseHandle(hToken);
                if (hProcess != IntPtr.Zero)
                    Win32.CloseHandle(hProcess);
            }
        }

        public static IntPtr DuplicateProcessToken()
        {
            return DuplicateProcessToken(Win32.GetCurrentProcessId());
        }

        public static IntPtr DuplicateProcessToken(uint pid)
        {
            try
            {
                IntPtr hProcess = IntPtr.Zero;
                IntPtr hToken = IntPtr.Zero;

                hProcess = Win32.OpenProcess(Win32.ProcessAccessFlags.All, 0, pid);
                if (hProcess == IntPtr.Zero)
                    return IntPtr.Zero;

                //UInt32 uPriv = (UInt32)(Win32.ETOKEN_PRIVILEGES.TOKEN_DUPLICATE
                //    | Win32.ETOKEN_PRIVILEGES.TOKEN_IMPERSONATE
                //    | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY
                //    | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY_SOURCE);

                uint uPriv = (uint)Win32.ETOKEN_PRIVILEGES.TOKEN_ALL_ACCESS;

                if (Win32.OpenProcessToken(hProcess, uPriv, ref hToken) == 0)
                    return IntPtr.Zero;

                return hToken;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

            return IntPtr.Zero;
        }

        public static IntPtr GetPrimaryToken(int pid)
        {
            IntPtr token = IntPtr.Zero;
            IntPtr primaryToken = IntPtr.Zero;
            bool retVal = false;
            Process p = Process.GetProcessById(pid);

            //Gets impersonation token
            retVal = Win32.OpenProcessToken(p.Handle, (uint)Win32.ETOKEN_PRIVILEGES.TOKEN_DUPLICATE, ref token) != 0;
            if (retVal)
            {
                var sa = new Win32.SECURITY_ATTRIBUTES();
                sa.nLength = (uint)Marshal.SizeOf(sa);

                uint desiredAccess = (uint)(Win32.ETOKEN_PRIVILEGES.TOKEN_DUPLICATE
                    | Win32.ETOKEN_PRIVILEGES.TOKEN_QUERY
                    | Win32.ETOKEN_PRIVILEGES.TOKEN_ASSIGN_PRIMARY);

                //Convert the impersonation token into Primary token
                retVal = Win32.DuplicateTokenEx(
                    token,
                    desiredAccess,
                    ref sa,
                    (int)Win32.SECURITY_IMPERSONATION_LEVEL.SecurityIdentification, //works, but process not elevated locally
                                                                              //(int)SECURITY_IMPERSONATION_LEVEL.SecurityDelegation,
                    (int)Win32.TOKEN_TYPE.TokenPrimary,
                    ref primaryToken
                );

                //Close the Token that was previously opened.
                Win32.CloseHandle(token);

                if (retVal == false)
                    Log.Exception(new Win32Exception(Marshal.GetLastWin32Error()), "DuplicateTokenEx Error");
            }
            else
            {
                Log.Exception(new Win32Exception(Marshal.GetLastWin32Error()), "OpenProcessToken Error");
            }


            // Assign values to the TOKEN_PRIVILEGE structure.
            var tLuid = new Win32.LUID();
            var newState = new Win32.TOKEN_PRIVILEGES();

            if (!Win32.LookupPrivilegeValue(null, Win32.SE_RELABEL_NAME, ref tLuid))
            {
                Win32.CloseHandle(primaryToken);
                return IntPtr.Zero;
            }

            newState.PrivilegeCount = 1;
            newState.Privileges.pLuid = tLuid;
            newState.Privileges.Attributes = Win32.SE_PRIVILEGE_ENABLED;

            if (!Win32.AdjustTokenPrivileges(primaryToken, false, ref newState, (uint)Marshal.SizeOf(newState), IntPtr.Zero, IntPtr.Zero))
            {
                Win32.CloseHandle(primaryToken);
                return IntPtr.Zero;
            }

            //We'll Close this token after it is used.
            return primaryToken;
        }
    }
}
