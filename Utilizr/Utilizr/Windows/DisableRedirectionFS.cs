﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Utilizr.Info;

namespace Utilizr.Windows
{
    public class DisableRedirectionFS: IDisposable
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);

        private IntPtr _wow64 = IntPtr.Zero;

        public DisableRedirectionFS()
        {
            if (Platform.IsXPOrLower)
                return;

            Wow64DisableWow64FsRedirection(ref _wow64);
        }

        public void Dispose()
        {
            if (Platform.IsXPOrLower)
                return;
            
            Wow64RevertWow64FsRedirection(_wow64);
        }
    }
}
