﻿using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
#if !MONO

#endif
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using Utilizr.Info;
using Utilizr.Logging;
using FILETIME = System.Runtime.InteropServices.ComTypes.FILETIME;

namespace Utilizr.Windows
{
    public static class Win32
    {
        public class ErrorCodes
        {
            /// <summary>
            /// 0 - The operation completed successfully.
            /// </summary>
            public const uint ERROR_SUCCESS = 0x0;
            /// <summary>
            /// 6 - The handle is invalid.
            /// </summary>
            public const uint ERROR_INVALID_HANDLE = 0x6;

            /// <summary>
            /// 1062 - The service has not been started.
            /// </summary>
            public const uint ERROR_SERVICE_NOT_ACTIVE = 0x426;

        }

        // findfile
        public const Int32 MAX_PATH = 260;
        public const Int32 MAX_ALTERNATE = 14;
        public const Int32 SYNCHRONIZE = 0x100000;
        public const Int32 STANDARD_RIGHTS_REQUIRED = 0xF0000;
        public const Int32 FILE_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0xFFF);
        public const Int32 PROCESS_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0xFFF);

        // mem flags
        public const UInt32 MEM_COMMIT =        0x1000;
        public const UInt32 MEM_RELEASE =       0x8000;

        public const UInt32 PAGE_READWRITE =            0x0004;
        public const UInt32 PAGE_EXECUTE =              0x0010;
        public const UInt32 PAGE_EXECUTE_READ =         0x0020;
        public const UInt32 PAGE_EXECUTE_WRITECOPY =    0x0080;

        // movefileex
        public const Int32 MOVEFILE_REPLACE_EXISTING = 0x1;
        public const Int32 MOVEFILE_DELAY_UNTIL_REBOOT = 0x4;
        public const Int32 MOVEFILE_WRITE_THROUGH = 0x8;

        // crypto
        public const UInt32 CRYPT_VERIFYCONTEXT = 0xF0000000;
        public const Int32 PROV_RSA_FULL = 0x1;
        public const string MS_ENHANCED_PROV = "Microsoft Enhanced Cryptographic Provider v1.0";

        // file flags
        public const uint GENERIC_ALL =        0x10000000;
        public const uint GENERIC_EXECUTE =    0x20000000;
        public const uint GENERIC_WRITE =      0x40000000;
        public const uint GENERIC_READ =       0x80000000;

        public const Int32 FILE_SHARE_NONE =        0x0;
        public const Int32 FILE_SHARE_READ =        0x1;
        public const Int32 FILE_SHARE_WRITE =       0x2;
        public const Int32 FILE_SHARE_DELETE =      0x3;

        public const short FILE_HANDLE_INVALID = -1;

        public const Int32 CREATE_NEW = 1;
        public const Int32 CREATE_ALWAYS = 2;
        public const Int32 OPEN_EXISTING = 3;
        public const Int32 OPEN_ALWAYS = 4;
        public const Int32 TRUNCATE_EXISTING = 5;

        public const Int32 FILE_BEGIN = 0;
        public const Int32 FILE_CURRENT = 1;
        public const Int32 FILE_END = 2;
        public const UInt32 FILE_MOVE_FAILED = 0xFFFFFFFF;

        // attributes
        public const Int32 FILE_ATTRIBUTE_INVALID                   = -1;
        public const Int32 FILE_ATTRIBUTE_READONLY                  = 0x00000001;
        public const Int32 FILE_ATTRIBUTE_HIDDEN                    = 0x00000002;
        public const Int32 FILE_ATTRIBUTE_SYSTEM                    = 0x00000004;
        public const Int32 FILE_ATTRIBUTE_DIRECTORY                 = 0x00000010;
        public const Int32 FILE_ATTRIBUTE_ARCHIVE                   = 0x00000020;
        public const Int32 FILE_ATTRIBUTE_DEVICE                    = 0x00000040;
        public const Int32 FILE_ATTRIBUTE_NORMAL                    = 0x00000080;
        public const Int32 FILE_ATTRIBUTE_TEMPORARY                 = 0x00000100;
        public const Int32 FILE_ATTRIBUTE_SPARSE_FILE               = 0x00000200;
        public const Int32 FILE_ATTRIBUTE_REPARSE_POINT             = 0x00000400;
        public const Int32 FILE_ATTRIBUTE_COMPRESSED                = 0x00000800;
        public const Int32 FILE_ATTRIBUTE_OFFLINE                   = 0x00001000;
        public const Int32 FILE_ATTRIBUTE_NOT_CONTENT_INDEXED       = 0x00002000;
        public const Int32 FILE_ATTRIBUTE_ENCRYPTED                 = 0x00004000;
        /// <summary>
        /// Win 10 1703+ only
        /// </summary>
        public const Int32 FILE_ATTRIBUTE_RECALL_ON_OPEN = 0x00040000;
        /// <summary>
        /// Win 10 1703+ only
        /// </summary>
        public const Int32 FILE_ATTRIBUTE_RECALL_ON_DATA_ACCESS = 0x00400000;


        public const UInt32 WRITE_THROUGH = 0x80000000;

        // deviveIO
        public const UInt32 FsctlDeleteObjectId = (0x00000009 << 16) | (40 << 2) | 0 | (0 << 14);

        // process
        public const Int32 WM_CLOSE = 0x10;
        public const Int32 WAIT_OBJECT_0 = 0x00000000;
        public const Int32 WAIT_TIMEOUT = 258;
        public const Int32 STILL_ACTIVE = 259;
        public const Int32 QS_ALLINPUT = 0x04FF;
        public const UInt32 STATUS_INFO_LENGTH_MISMATCH = 0xC0000004;
        public const Int32 DUPLICATE_SAME_ACCESS = 0x2;
        public const Int32 DUPLICATE_CLOSE_SOURCE = 0x1;
        public const UInt32 FILE_SEQUENTIAL_ONLY = 0x00000004;

        // privilege
        public const Int32 SE_PRIVILEGE_ENABLED = 0x00000002;
        public const Int32 TOKEN_QUERY = 0x00000008;
        public const Int32 TOKEN_ADJUST_PRIVILEGES = 0x00000020;
        public const string SE_ASSIGNPRIMARYTOKEN_NAME = "SeAssignPrimaryTokenPrivilege";
        public const string SE_AUDIT_NAME = "SeAuditPrivilege";
        public const string SE_BACKUP_NAME = "SeBackupPrivilege";
        public const string SE_CHANGE_NOTIFY_NAME = "SeChangeNotifyPrivilege";
        public const string SE_CREATE_GLOBAL_NAME = "SeCreateGlobalPrivilege";
        public const string SE_CREATE_PAGEFILE_NAME = "SeCreatePagefilePrivilege";
        public const string SE_CREATE_PERMANENT_NAME = "SeCreatePermanentPrivilege";
        public const string SE_CREATE_SYMBOLIC_LINK_NAME = "SeCreateSymbolicLinkPrivilege";
        public const string SE_CREATE_TOKEN_NAME = "SeCreateTokenPrivilege";
        public const string SE_DEBUG_NAME = "SeDebugPrivilege";
        public const string SE_ENABLE_DELEGATION_NAME = "SeEnableDelegationPrivilege";
        public const string SE_IMPERSONATE_NAME = "SeImpersonatePrivilege";
        public const string SE_INC_BASE_PRIORITY_NAME = "SeIncreaseBasePriorityPrivilege";
        public const string SE_INCREASE_QUOTA_NAME = "SeIncreaseQuotaPrivilege";
        public const string SE_INC_WORKING_SET_NAME = "SeIncreaseWorkingSetPrivilege";
        public const string SE_LOAD_DRIVER_NAME = "SeLoadDriverPrivilege";
        public const string SE_LOCK_MEMORY_NAME = "SeLockMemoryPrivilege";
        public const string SE_MACHINE_ACCOUNT_NAME = "SeMachineAccountPrivilege";
        public const string SE_MANAGE_VOLUME_NAME = "SeManageVolumePrivilege";
        public const string SE_PROF_SINGLE_PROCESS_NAME = "SeProfileSingleProcessPrivilege";
        public const string SE_RELABEL_NAME = "SeRelabelPrivilege";
        public const string SE_REMOTE_SHUTDOWN_NAME = "SeRelabelPrivilege";
        public const string SE_RESTORE_NAME = "SeRestorePrivilege";
        public const string SE_SECURITY_NAME = "SeRestorePrivilege";
        public const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        public const string SE_SYNC_AGENT_NAME = "SeSyncAgentPrivilege";
        public const string SE_SYSTEM_ENVIRONMENT_NAME = "SeSystemEnvironmentPrivilege";
        public const string SE_SYSTEM_PROFILE_NAME = "SeSystemProfilePrivilege";
        public const string SE_SYSTEMTIME_NAME = "SeSystemtimePrivilege";
        public const string SE_TAKE_OWNERSHIP_NAME = "SeTakeOwnershipPrivilege";
        public const string SE_TCB_NAME = "SeTcbPrivilege";
        public const string SE_TIME_ZONE_NAME = "SeTimeZonePrivilege";
        public const string SE_TRUSTED_CREDMAN_ACCESS_NAME = "SeTrustedCredManAccessPrivilege";
        public const string SE_UNDOCK_NAME = "SeUndockPrivilege";
        public const string SE_UNSOLICITED_INPUT_NAME = "SeUnsolicitedInputPrivilege";



        [Flags]
        public enum ETOKEN_PRIVILEGES : uint
        {
            TOKEN_ASSIGN_PRIMARY = 0x1,
            TOKEN_DUPLICATE = 0x2,
            TOKEN_IMPERSONATE = 0x4,
            TOKEN_QUERY = 0x8,
            TOKEN_QUERY_SOURCE = 0x10,
            TOKEN_ADJUST_PRIVILEGES = 0x20,
            TOKEN_ADJUST_GROUPS = 0x40,
            TOKEN_ADJUST_DEFAULT = 0x80,
            TOKEN_ADJUST_SESSIONID = 0x100,
            TOKEN_STANDARD_RIGHTS_READ = 0x2000,
            TOKEN_STANDARD_RIGHTS_REQUIRED = 0xF0000,
            TOKEN_READ = TOKEN_STANDARD_RIGHTS_REQUIRED | TOKEN_QUERY,
            TOKEN_ALL_ACCESS = TOKEN_STANDARD_RIGHTS_REQUIRED
                | TOKEN_ASSIGN_PRIMARY
                | TOKEN_DUPLICATE
                | TOKEN_IMPERSONATE
                | TOKEN_QUERY
                | TOKEN_QUERY_SOURCE
                | TOKEN_ADJUST_PRIVILEGES
                | TOKEN_ADJUST_GROUPS
                | TOKEN_ADJUST_DEFAULT
                | TOKEN_ADJUST_SESSIONID
        }

        public enum ObjectInformationClass
        {
            ObjectBasicInformation = 0,
            ObjectNameInformation = 1,
            ObjectTypeInformation = 2,
            ObjectAllTypesInformation = 3,
            ObjectHandleInformation = 4
        }

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All =                           0x001F0FFF,
            Terminate =                     0x00000001,
            CreateThread =                  0x00000002,
            VMOperation =                   0x00000008,
            VMRead =                        0x00000010,
            VMWrite =                       0x00000020,
            DupHandle =                     0x00000040,
            SetInformation =                0x00000200,
            QueryInformation =              0x00000400,
            QueryLimitedInformation =       0x00001000,
            Synchronize =                   0x00100000,
        }

        public enum TOKEN_INFORMATION_CLASS
        {
            TokenUser = 1,
            TokenGroups,
            TokenPrivileges,
            TokenOwner,
            TokenPrimaryGroup,
            TokenDefaultDacl,
            TokenSource,
            TokenType,
            TokenImpersonationLevel,
            TokenStatistics,
            TokenRestrictedSids,
            TokenSessionId,
            TokenGroupsAndPrivileges,
            TokenSessionReference,
            TokenSandBoxInert,
            TokenAuditPolicy,
            TokenOrigin,
            TokenElevationType,
            TokenLinkedToken
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct TOKEN_USER
        {
            public SID_AND_ATTRIBUTES User;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SID_AND_ATTRIBUTES
        {
            public IntPtr Sid;
            public int Attributes;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct WIN32_FIND_DATAW
        {
            public Int32 dwFileAttributes;
            public FILETIME ftCreationTime;
            public FILETIME ftLastAccessTime;
            public FILETIME ftLastWriteTime;
            public UInt32 nFileSizeHigh;
            public UInt32 nFileSizeLow;
            public UInt32 dwReserved0;
            public UInt32 dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_PATH)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_ALTERNATE)]
            public string cAlternateFileName;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct LUID
        {
            public Int32 LowPart;
            public Int32 HighPart;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct LUID_AND_ATTRIBUTES
        {
            public LUID pLuid;
            public Int32 Attributes;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TOKEN_PRIVILEGES
        {
            public Int32 PrivilegeCount;
            public LUID_AND_ATTRIBUTES Privileges;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct OBJECT_BASIC_INFORMATION
        { // Information Class 0
            public int Attributes;
            public int GrantedAccess;
            public int HandleCount;
            public int PointerCount;
            public int PagedPoolUsage;
            public int NonPagedPoolUsage;
            public int Reserved1;
            public int Reserved2;
            public int Reserved3;
            public int NameInformationLength;
            public int TypeInformationLength;
            public int SecurityDescriptorLength;
            public System.Runtime.InteropServices.ComTypes.FILETIME CreateTime;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct OBJECT_TYPE_INFORMATION
        { // Information Class 2
            public UNICODE_STRING Name;
            public int ObjectCount;
            public int HandleCount;
            public int Reserved1;
            public int Reserved2;
            public int Reserved3;
            public int Reserved4;
            public int PeakObjectCount;
            public int PeakHandleCount;
            public int Reserved5;
            public int Reserved6;
            public int Reserved7;
            public int Reserved8;
            public int InvalidAttributes;
            public GENERIC_MAPPING GenericMapping;
            public int ValidAccess;
            public byte Unknown;
            public byte MaintainHandleDatabase;
            public int PoolType;
            public int PagedPoolUsage;
            public int NonPagedPoolUsage;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct OBJECT_NAME_INFORMATION
        { // Information Class 1
            public UNICODE_STRING Name;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct UNICODE_STRING
        {
            public ushort Length;
            public ushort MaximumLength;
            public IntPtr Buffer;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GENERIC_MAPPING
        {
            public int GenericRead;
            public int GenericWrite;
            public int GenericExecute;
            public int GenericAll;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct COPYDATASTRUCT : IDisposable
        {
            public IntPtr dwData;
            public int cbData;
            public IntPtr lpData;

            private IntPtr objPtr;

            public void Dispose()
            {
                if (lpData != IntPtr.Zero)
                {
                    Marshal.FreeCoTaskMem(lpData);
                    lpData = IntPtr.Zero;
                    cbData = 0;
                }

                if (objPtr != IntPtr.Zero)
                {
                    Marshal.FreeCoTaskMem(objPtr);
                }

            }
            public string AsAnsiString { get { return Marshal.PtrToStringAnsi(lpData, cbData); } }
            public string AsUnicodeString { get { return Marshal.PtrToStringUni(lpData); } }
            public static COPYDATASTRUCT CreateForString(uint dwData, string value, bool Unicode = false)
            {
                if (!value.EndsWith("\0"))
                    value = $"{value}\0";

                var result = new COPYDATASTRUCT();
                result.dwData = new IntPtr(dwData);
                result.lpData = Marshal.AllocCoTaskMem(value.Length);
                result.lpData = Unicode
                    ? Marshal.StringToCoTaskMemUni(value)
                    : Marshal.StringToCoTaskMemAnsi(value);
                result.cbData = value.Length + 1;

                IntPtr objPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(result));
                Marshal.StructureToPtr(result, objPtr, true);

                return result;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SYSTEM_HANDLE_INFORMATION
        { // Information Class 16
            public int ProcessID;
            public byte ObjectTypeNumber;
            public byte Flags; // 0x01 = PROTECT_FROM_CLOSE, 0x02 = INHERIT
            public ushort Handle;
            public int Object_Pointer;
            public UInt32 GrantedAccess;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public uint dwProcessId;
            public uint dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public uint nLength;
            public IntPtr lpSecurityDescriptor;
            public bool bInheritHandle;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct STARTUPINFO
        {
            public uint cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        public const short SW_SHOW = 5;
        public const uint INFINITE = 0xFFFFFFFF;
        public const uint CREATE_UNICODE_ENVIRONMENT = 0x00000400;
        public const uint CREATE_NEW_CONSOLE = 0x00000010;
        public const uint CREATE_NEW_PROCESS_GROUP = 0x00000200;
        public const uint CREATE_NO_WINDOW = 0x08000000;
        public const uint CREATE_BREAKAWAY_FROM_JOB = 0x01000000;
        public const uint CREATE_PROTECTED_PROCESS = 0x00040000;

        public enum STARTUPINFO_FLAGS : uint
        {
            None = 0,
            STARTF_USESHOWWINDOW = 0x00000001,
            STARTF_USESIZE = 0x00000002,
            STARTF_USEPOSITION = 0x00000004,
            STARTF_USECOUNTCHARS = 0x00000008,
            STARTF_USEFILLATTRIBUTE = 0x00000010,
            STARTF_RUNFULLSCREEN = 0x00000020,
            STARTF_FORCEONFEEDBACK = 0x00000040,
            STARTF_FORCEOFFFEEDBACK = 0x00000080,
            STARTF_USESTDHANDLES = 0x00000100,
            STARTF_USEHOTKEY = 0x00000200,
            STARTF_TITLEISLINKNAME = 0x00000800,
            STARTF_TITLEISAPPID = 0x00001000,
            STARTF_PREVENTPINNING = 0x00002000,
            STARTF_UNTRUSTEDSOURCE = 0x00008000,
        }

        public enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        public enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation
        }

        // Window Messages flags
        public const uint WM_CLEAN = 0x0303;
        public const uint WM_COPY = 0x0301;
        public const uint WM_CUT = 0x0300;
        public const uint WM_PASTE = 0x0302;
        public const uint WM_USER = 0x0400;
        public const uint WM_COPYDATA = 0x004A;
        public const uint WM_FONTCHANGE = 0x1D;

        //Send message broadcast
        public const uint HWND_BROADCAST = 0xffff;

        // GetStdHandle handle types
        public const int STD_INPUT_HANDLE = -10;
        public const int STD_OUTPUT_HANDLE = -11;
        public const int STD_ERROR_HANDLE = -12;


        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr CreateFileW(
            string lpFileName, 
            uint dwDesiredAccess, 
            uint dwShareMode, 
            IntPtr lpSecurityAttributes,
            uint dwCreationDisposition, 
            uint dwFlagsAndAttributes, 
            IntPtr hTemplateFile);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool CreateDirectoryW(string directory, IntPtr lpSecurityAttributes);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean SetFilePointerEx(IntPtr hFile, long liDistanceToMove, [Out, Optional] IntPtr lpNewFilePointer, UInt32 dwMoveMethod);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr VirtualAlloc(IntPtr lpAddress, UInt32 dwSize, UInt32 flAllocationType, UInt32 flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean VirtualFree(IntPtr lpAddress, UInt32 dwSize, UInt32 dwFreeType);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean GetFileSizeEx(IntPtr hFile, out Int64 lpFileSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern Int32 WriteFile(IntPtr hFile, IntPtr lpBuffer, UInt32 nNumberOfBytesToWrite, ref UInt32 lpNumberOfBytesWritten, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern Int32 DeleteFileW([MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern Int32 MoveFileExW([MarshalAs(UnmanagedType.LPWStr)] string lpExistingFileName, [MarshalAs(UnmanagedType.LPWStr)] string lpNewFileName, Int32 dwFlags);

        [DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean DeviceIoControl(IntPtr hDevice, UInt32 dwIoControlCode, IntPtr lpInBuffer, UInt32 nInBufferSize,
        IntPtr lpOutBuffer, [Optional] UInt32 nOutBufferSize, out UInt32 lpBytesReturned, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern Int32 GetShortPathNameW([MarshalAs(UnmanagedType.LPWStr)] string lLongPath, [MarshalAs(UnmanagedType.LPWStr)] string lShortPath, Int32 lBuffer);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern Int32 RemoveDirectoryW([MarshalAs(UnmanagedType.LPWStr)] string lpPathName);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern Int32 CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern Int32 ReadFile(IntPtr hFile, IntPtr lpBuffer, uint nNumberOfBytesToRead, ref UInt32 lpNumberOfBytesRead, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteFile(IntPtr hFile, IntPtr lpBuffer, uint nNumberOfBytesToWrite, UInt32 lpNumberOfBytesWritten, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint QueryDosDevice(string lpDeviceName, StringBuilder lpTargetPath, int ucchMax);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DuplicateHandle(IntPtr hSourceProcessHandle, ushort hSourceHandle,
            UInt32 hTargetProcessHandle, out IntPtr lpTargetHandle, UInt32 dwDesiredAccess, 
            [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, UInt32 dwOptions);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule,
        [MarshalAs(UnmanagedType.LPStr)] string procName);

        //WOW32
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

        // ntapi
        [DllImport("ntdll.dll", SetLastError = false)]
        public static extern UInt32 RtlCompareMemory(IntPtr Source1, IntPtr Source2, UInt32 length);

        [DllImport("ntdll.dll", SetLastError = true)]
        public static extern void RtlZeroMemory(IntPtr Destination, uint length);

        [DllImport("ntdll.dll", SetLastError = false)]
        public static extern Int32 RtlFillMemory([In] IntPtr Destination, UInt32 length, byte fill);

        [DllImport("ntdll.dll")]
        public static extern int NtQueryObject(IntPtr ObjectHandle, int ObjectInformationClass,
            IntPtr ObjectInformation, int ObjectInformationLength, ref int returnLength);

        [DllImport("ntdll.dll")]
        public static extern uint NtQuerySystemInformation(int SystemInformationClass, 
            IntPtr SystemInformation, int SystemInformationLength, ref int returnLength);



        // crypto
        [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean CryptAcquireContextW(ref IntPtr hProv, [MarshalAs(UnmanagedType.LPWStr)] string pszContainer, [MarshalAs(UnmanagedType.LPWStr)] string pszProvider, UInt32 dwProvType, UInt32 dwFlags);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean CryptGenRandom(IntPtr hProv, UInt32 dwLen, IntPtr pbBuffer);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean CryptReleaseContext(IntPtr hProv, UInt32 dwFlags);

        // findfile
        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr FindFirstFileW([MarshalAs(UnmanagedType.LPWStr)] string lpFileName, out WIN32_FIND_DATAW lpFindFileData);

        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean FindNextFileW(IntPtr hFindFile, out WIN32_FIND_DATAW lpFindFileData);

        [DllImport("kernel32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean FindClose(IntPtr hFindFile);

        // explorer selection
        [DllImport("shell32.dll", SetLastError = true)]
        public static extern int SHOpenFolderAndSelectItems(IntPtr pidlFolder, uint cidl, [In, MarshalAs(UnmanagedType.LPArray)] IntPtr[] apidl, uint dwFlags);

        [DllImport("shell32.dll", SetLastError = true)]
        public static extern void SHParseDisplayName([MarshalAs(UnmanagedType.LPWStr)] string name, IntPtr bindingContext, [Out] out IntPtr pidl, uint sfgaoIn, [Out] out uint psfgaoOut);

        public enum UserNotificationState
        {
            /// <summary>
            /// A screen saver is displayed, the machine is locked,
            /// or a nonactive Fast User Switching session is in progress.
            /// </summary>
            NotPresent = 1,

            /// <summary>
            /// A full-screen application is running or Presentation Settings are applied.
            /// Presentation Settings allow a user to put their machine into a state fit
            /// for an uninterrupted presentation, such as a set of PowerPoint slides, with a single click.
            /// </summary>
            Busy = 2,

            /// <summary>
            /// A full-screen (exclusive mode) Direct3D application is running.
            /// </summary>
            RunningDirect3dFullScreen = 3,

            /// <summary>
            /// The user has activated Windows presentation settings to block notifications and pop-up messages.
            /// </summary>
            PresentationMode = 4,

            /// <summary>
            /// None of the other states are found, notifications can be freely sent.
            /// </summary>
            AcceptsNotifications = 5,

            /// <summary>
            /// Introduced in Windows 7. The current user is in "quiet time", which is the first hour after
            /// a new user logs into his or her account for the first time. During this time, most notifications
            /// should not be sent or shown. This lets a user become accustomed to a new computer system
            /// without those distractions.
            /// Quiet time also occurs for each user after an operating system upgrade or clean installation.
            /// </summary>
            QuietTime = 6
        };

        [DllImport("shell32.dll")]
        public static extern int SHQueryUserNotificationState(out UserNotificationState userNotificationState);

        // process
        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean EnumProcesses([MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.U4)][In][Out] UInt32[] processIds,
          UInt32 arraySizeBytes, [MarshalAs(UnmanagedType.U4)] out UInt32 bytesCopied);

        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean EnumProcessModules(IntPtr hProcess, [MarshalAs(UnmanagedType.LPArray,
        ArraySubType = UnmanagedType.U4)] [In][Out] UInt32[] lphModule, UInt32 cb, [MarshalAs(UnmanagedType.U4)] out UInt32 lpcbNeeded);

        [DllImport("psapi.dll", SetLastError = true)]
        public static extern UInt32 GetModuleFileNameExA(IntPtr hProcess, IntPtr hModule,
        [Out] StringBuilder lpBaseName, [In][MarshalAs(UnmanagedType.U4)] UInt32 nSize);

        [DllImport("ntdll.dll", SetLastError = true)]
        public static extern UInt32 NtTerminateProcess(IntPtr ProcessHandle, UInt32 ExitStatus);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean GetExitCodeProcess(IntPtr hProcess, out UInt32 lpExitCode);

        [DllImport("kernel32.dll")]
        public static extern uint WaitForMultipleObjects(uint nCount, IntPtr[] pHandles,
        Boolean bWaitAll, uint dwMilliseconds);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, Int32 blnheritHandle, UInt32 dwAppProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool QueryFullProcessImageName(
              [In] IntPtr hProcess,
              [In] int dwFlags,
              [Out] StringBuilder lpExeName,
              ref int lpdwSize);

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool CreateProcessAsUser(
            IntPtr hToken,
            string lpApplicationName,
            string lpCommandLine,
            ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation
        );

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern bool CreateProcess(
            string lpApplicationName,
            string lpCommandLine,
            ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation
        );

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CreatePipe(
            out SafeFileHandle hReadPipe,
            out SafeFileHandle hWritePipe,
            ref SECURITY_ATTRIBUTES lpPipeAttributes,
            int nSize
        );

        [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx", SetLastError = true)]
        public static extern bool DuplicateTokenEx(
            IntPtr hExistingToken,
            uint dwDesiredAccess,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            Int32 ImpersonationLevel,
            Int32 dwTokenType,
            ref IntPtr phNewToken
        );

        [DllImport("userenv.dll", SetLastError = true)]
        public static extern bool CreateEnvironmentBlock(ref IntPtr lpEnvironment, IntPtr hToken, bool bInherit);

        [DllImport("userenv.dll", SetLastError = true)]
        public static extern bool DestroyEnvironmentBlock(IntPtr lpEnvironment);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);


        // privilege
        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AdjustTokenPrivileges(IntPtr TokenHandle, [MarshalAs(UnmanagedType.Bool)] bool DisableAllPrivileges, ref TOKEN_PRIVILEGES NewState,
        UInt32 BufferLength, IntPtr PreviousState, IntPtr ReturnLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern UInt32 GetCurrentProcessId();

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern Int32 OpenProcessToken(IntPtr ProcessHandle, UInt32 DesiredAccess, ref IntPtr TokenHandle);

        [DllImport("advapi32", CharSet = CharSet.Auto)]
        public static extern bool GetTokenInformation(IntPtr hToken, TOKEN_INFORMATION_CLASS tokenInfoClass, IntPtr TokenInformation, int tokeInfoLength, ref int reqLength);

        [DllImport("advapi32", CharSet = CharSet.Auto)]
        public static extern bool ConvertSidToStringSid(IntPtr pSID, [In, Out, MarshalAs(UnmanagedType.LPTStr)] ref string pStringSid);

        [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool LookupPrivilegeValue(string lpSystemName, [MarshalAs(UnmanagedType.LPWStr)] string lpName, ref LUID lpLuid);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("shlwapi.dll", BestFitMapping = false, CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = false, ThrowOnUnmappableChar = true)]
        public static extern int SHLoadIndirectString(string pszSource, StringBuilder pszOutBuf, uint cchOutBuf, IntPtr ppvReserved);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, ref COPYDATASTRUCT lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr PostMessage(IntPtr hWnd, UInt32 wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

        [DllImport("kernel32")]
        public static extern IntPtr CreateRemoteThread(
            IntPtr hProcess,
            IntPtr lpThreadAttributes,
            uint dwStackSize,
            IntPtr lpStartAddress, // raw Pointer into remote process
            IntPtr lpParameter,
            uint dwCreationFlags,
            out uint lpThreadId
            );

        public const int GWL_STYLE = -16;
        public const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        public const int WS_MINIMIZEBOX = 0x20000; //minimize button

        [DllImport("kernel32.dll")]
        public static extern bool GetExitCodeThread(IntPtr hThread, out uint lpExitCode);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool GetFileInformationByHandleEx(IntPtr hFile, FILE_INFO_BY_HANDLE_CLASS infoClass, out FILE_ID_BOTH_DIR_INFO dirInfo, uint dwBufferSize);

        [DllImport("kernel32.dll")]
        public static extern uint GetConsoleOutputCP();


        [DllImport("kernel32.dll")]
        public static extern FileType GetFileType(IntPtr hFile);

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, out RECT rect);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ExitWindowsEx(ExitWindows uFlags, ShutdownReason dwReason);

        [DllImport("ole32.dll")]
        public static extern int CoRegisterClassObject(
            [MarshalAs(UnmanagedType.LPStruct)] Guid rclsid,
            [MarshalAs(UnmanagedType.IUnknown)] object pUnk,
            uint dwClsContext,
            uint flags,
            out uint lpdwRegister);

        [DllImport("old32.dll")]
        public static extern int CoRevokeClassObject(uint dwRegister);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [Flags]
        public enum ExitWindows : uint
        {
            // ONE of the following five:
            LogOff = 0x00,
            ShutDown = 0x01,
            Reboot = 0x02,
            PowerOff = 0x08,
            RestartApps = 0x40,
            // plus AT MOST ONE of the following two:
            Force = 0x04,
            ForceIfHung = 0x10,
        }

        [Flags]
        public enum ShutdownReason : uint
        {
            MajorApplication = 0x00040000,
            MajorHardware = 0x00010000,
            MajorLegacyApi = 0x00070000,
            MajorOperatingSystem = 0x00020000,
            MajorOther = 0x00000000,
            MajorPower = 0x00060000,
            MajorSoftware = 0x00030000,
            MajorSystem = 0x00050000,

            MinorBlueScreen = 0x0000000F,
            MinorCordUnplugged = 0x0000000b,
            MinorDisk = 0x00000007,
            MinorEnvironment = 0x0000000c,
            MinorHardwareDriver = 0x0000000d,
            MinorHotfix = 0x00000011,
            MinorHung = 0x00000005,
            MinorInstallation = 0x00000002,
            MinorMaintenance = 0x00000001,
            MinorMMC = 0x00000019,
            MinorNetworkConnectivity = 0x00000014,
            MinorNetworkCard = 0x00000009,
            MinorOther = 0x00000000,
            MinorOtherDriver = 0x0000000e,
            MinorPowerSupply = 0x0000000a,
            MinorProcessor = 0x00000008,
            MinorReconfig = 0x00000004,
            MinorSecurity = 0x00000013,
            MinorSecurityFix = 0x00000012,
            MinorSecurityFixUninstall = 0x00000018,
            MinorServicePack = 0x00000010,
            MinorServicePackUninstall = 0x00000016,
            MinorTermSrv = 0x00000020,
            MinorUnstable = 0x00000006,
            MinorUpgrade = 0x00000003,
            MinorWMI = 0x00000015,

            FlagUserDefined = 0x40000000,
            FlagPlanned = 0x80000000
        }

#region LoadLibrary
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr LoadLibraryEx(string lpFileName, IntPtr hFile, uint dwFlags);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr FindResource(IntPtr hModule, string lpName, uint lpType);
        [DllImport("kernel32.dll")]
        static extern IntPtr FindResource(IntPtr hModule, int lpID, string lpType);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr LoadResource(IntPtr hModule, IntPtr hResInfo);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern uint SizeofResource(IntPtr hModule, IntPtr hResInfo);

        const uint LOAD_LIBRARY_AS_DATAFILE = 0x00000002;
        private const uint RT_RCDATA = 10;

        /// <summary>
        /// Load a resource from an exe / dll
        /// </summary>
        /// <param name="resourceName">Name of the resource to load</param>
        /// <param name="fromFile">Path to the exe / dll. Leave null to use currently executing assembly</param>
        /// <param name="resourceType">type of resource e.g. ICON RCDATA etc..</param>
        /// <returns></returns>
        public static byte[] LoadResourceFile(string resourceName, string fromFile = null, uint resourceType = RT_RCDATA)
        {
            if (fromFile.IsNullOrEmpty())
               fromFile = Assembly.GetExecutingAssembly().Location;

#if NETCOREAPP
            fromFile = Process.GetCurrentProcess().MainModule?.FileName;
#endif
            
            IntPtr hMod = LoadLibraryEx(fromFile, IntPtr.Zero, LOAD_LIBRARY_AS_DATAFILE);
            if (hMod == IntPtr.Zero)
                throw new Win32Exception();

            IntPtr hRes = FindResource(hMod, resourceName, resourceType);
            if (hRes == IntPtr.Zero)
                return null;

            uint size = SizeofResource(hMod, hRes);
            if (size == 0)
                return null;

            IntPtr pt = LoadResource(hMod, hRes);
            if (pt == IntPtr.Zero)
                return null;

            byte[] bPtr = new byte[size];
            Marshal.Copy(pt, bPtr, 0, (int)size);
            return bPtr;
        }


#endregion

        public enum FileType : uint
        {
            Char = 0x0002,
            Disk = 0x0001,
            Pipe = 0x0003,
            Remote = 0x8000,
            Unknown = 0x0000,
        }

        public enum FILE_INFO_BY_HANDLE_CLASS
        {
            FileBasicInfo = 0,
            FileStandardInfo = 1,
            FileNameInfo = 2,
            FileRenameInfo = 3,
            FileDispositionInfo = 4,
            FileAllocationInfo = 5,
            FileEndOfFileInfo = 6,
            FileStreamInfo = 7,
            FileCompressionInfo = 8,
            FileAttributeTagInfo = 9,
            FileIdBothDirectoryInfo = 10,// 0x0A
            FileIdBothDirectoryRestartInfo = 11, // 0xB
            FileIoPriorityHintInfo = 12, // 0xC
            FileRemoteProtocolInfo = 13, // 0xD
            FileFullDirectoryInfo = 14, // 0xE
            FileFullDirectoryRestartInfo = 15, // 0xF
            FileStorageInfo = 16, // 0x10
            FileAlignmentInfo = 17, // 0x11
            FileIdInfo = 18, // 0x12
            FileIdExtdDirectoryInfo = 19, // 0x13
            FileIdExtdDirectoryRestartInfo = 20, // 0x14
            MaximumFileInfoByHandlesClass
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct FILE_ID_BOTH_DIR_INFO
        {
            public uint NextEntryOffset;
            public uint FileIndex;
            public long CreationTime;
            public long LastAccessTime;
            public long LastWriteTime;
            public long ChangeTime;
            public long EndOfFile;
            public long AllocationSize;
            public uint FileAttributes;
            public uint FileNameLength;
            public uint EaSize;
            public char ShortNameLength;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 12)]
            public string ShortName;
            public long FileId;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1)]
            public string FileName;
        }

        // getfileattributes
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetFileAttributesEx(string lpFileName, GET_FILEEX_INFO_LEVELS fInfoLevelId, out WIN32_FILE_ATTRIBUTE_DATA fileData);

        public enum GET_FILEEX_INFO_LEVELS
        {
            GetFileExInfoStandard,
            GetFileExMaxInfoLevel
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct WIN32_FILE_ATTRIBUTE_DATA
        {
            public int dwFileAttributes;
            public FILETIME ftCreationTime;
            public FILETIME ftLastAccessTime;
            public FILETIME ftLastWriteTime;
            public uint nFileSizeHigh;
            public uint nFileSizeLow;
        }


        public static Int64 FileSize(IntPtr hFile)
        {
            Int64 nFileLen = 0;
            GetFileSizeEx(hFile, out nFileLen);
            return nFileLen;
        }

        public static void SetFilePointer(IntPtr hFile, long liDistanceToMove, IntPtr lpNewFilePointer, UInt32 dwMoveMethod)
        {
            if (SetFilePointerEx(hFile, liDistanceToMove, lpNewFilePointer, dwMoveMethod) == false)
                throw new Win32Exception();
        }

        public static void DeleteFile(string FileName)
        {
            if (DeleteFileW(FileName) == 0)
                throw new Win32Exception();
        }

        public static void RemoveDirectory(string sDir)
        {
            if (RemoveDirectoryW(sDir) == 0)
                throw new Win32Exception();
        }

        public static string ShortPath(string sPath)
        {
            Int32 iLen;
            string sBuffer = String.Empty;
            sBuffer.PadRight(255, char.MinValue);
            iLen = GetShortPathNameW(sPath, sBuffer, 255);
            return sBuffer.Trim();
        }

        public static void DestroyOnRestart(string sPath)
        {
            string sSource = ShortPath(sPath);
            if (sSource.Length == 0)
                sSource = sPath;
            if (MoveFileExW(sSource, String.Empty, MOVEFILE_DELAY_UNTIL_REBOOT) == 0)
                throw new Win32Exception();
        }

        public static void MoveOnRestart(string existingPath, string newPath)
        {
            string sSource = ShortPath(existingPath);
            if (sSource.Length == 0)
                sSource = existingPath;

            string sDestination = ShortPath(newPath);
            if (sDestination.Length == 0)
                sDestination = newPath;

            if (MoveFileExW(sSource, sDestination, MOVEFILE_DELAY_UNTIL_REBOOT) == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        public static void OrphanFile(string pName)
        {
            UInt32 lpBytesReturned = 0;
            IntPtr hFile = CreateFileW(pName, GENERIC_WRITE, FILE_SHARE_NONE, IntPtr.Zero, OPEN_EXISTING, WRITE_THROUGH, IntPtr.Zero);
            if (DeviceIoControl(hFile, FsctlDeleteObjectId, IntPtr.Zero, 0, IntPtr.Zero, 0, out lpBytesReturned, IntPtr.Zero))
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }


        /// <summary>
        /// Indicates if win 32 method exists
        /// </summary>
        /// <param name="moduleName">DLL module name to check</param>
        /// <param name="methodName">Method name to check</param>
        /// <returns></returns>
        public static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }

        public static bool ProcessOwnedByUser(int pid, string userSID)
        {
            IntPtr pToken = IntPtr.Zero;
            var process = Process.GetProcessById(pid);

            if (OpenProcessToken(process.Handle, TOKEN_QUERY, ref pToken) != 0)
            {
                IntPtr pSidPtr = IntPtr.Zero;
                if (ProcessTokenToSID(pToken, out pSidPtr))
                {
                    string pSidStr = string.Empty;
                    ConvertSidToStringSid(pSidPtr, ref pSidStr);
                    return userSID.Equals(pSidStr, StringComparison.OrdinalIgnoreCase);
                }
            }
            return false;
        }

        private static bool ProcessTokenToSID(IntPtr token, out IntPtr pSID)
        {
            pSID = IntPtr.Zero;
            TOKEN_USER tokUser;
            const int bufLength = 256;
            IntPtr tu = Marshal.AllocHGlobal(bufLength);
            bool result = false;
            try
            {
                int cb = bufLength;
                result = GetTokenInformation(token, TOKEN_INFORMATION_CLASS.TokenUser, tu, cb, ref cb);
                if (result)
                {
                    tokUser = (TOKEN_USER)Marshal.PtrToStructure(tu, typeof(TOKEN_USER));
                    pSID = tokUser.User.Sid;
                }
                return result;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                Marshal.FreeHGlobal(tu);
            }
        }


        [Flags]
        public enum ThreadAccess : int
        {
            TERMINATE = (0x0001),
            SUSPEND_RESUME = (0x0002),
            GET_CONTEXT = (0x0008),
            SET_CONTEXT = (0x0010),
            SET_INFORMATION = (0x0020),
            QUERY_INFORMATION = (0x0040),
            SET_THREAD_TOKEN = (0x0080),
            IMPERSONATE = (0x0100),
            DIRECT_IMPERSONATION = (0x0200)
        }
        [DllImport("kernel32.dll")]
        static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);
        [DllImport("kernel32.dll")]
        static extern uint SuspendThread(IntPtr hThread);
        [DllImport("kernel32.dll")]
        static extern int ResumeThread(IntPtr hThread);


        public static void ResumeProcess(Process p)
        {
            foreach (ProcessThread processThread in p.Threads)
            {
                var pThread = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)processThread.Id);
                if (pThread == IntPtr.Zero)
                {
                    Log.Info("Resuming proc thread");
                    continue;
                }

                ResumeThread(pThread);
                CloseHandle(pThread);
            }
        }

        // wininet
        public const int INTERNET_COOKIE_THIRD_PARTY = 0x00010;
        public const int INTERNET_COOKIE_HTTPONLY = 0x02000;
        public const int INTERNET_FLAG_RESTRICTED_ZONE = 0x20000;

        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetCookieEx(string lpszUrl, string lpszCookieName, StringBuilder lpszCookieData, ref int lpdwSize, int dwFlags, IntPtr lpReserved);

        public enum InternetCookieState
        {
            COOKIE_STATE_UNKNOWN = 0x0,
            COOKIE_STATE_ACCEPT = 0x1,
            COOKIE_STATE_PROMPT = 0x2,
            COOKIE_STATE_LEASH = 0x3,
            COOKIE_STATE_DOWNGRADE = 0x4,
            COOKIE_STATE_REJECT = 0x5,
            COOKIE_STATE_MAX = COOKIE_STATE_REJECT
        }

        [DllImport("wininet.dll", SetLastError = true)]
        public static extern InternetCookieState InternetSetCookieEx(string lpszUrl, string lpszCookieName, string lpszCookieData, int dwFlags, IntPtr dwReserved);

        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool SetThreadToken([In] IntPtr Thread, [In] IntPtr Token);

        public struct TOKEN_LINKED_TOKEN
        {
            public IntPtr LinkedToken;
        }

        public enum TOKEN_ELEVATION_TYPE
        {
            TokenElevationTypeDefault = 1,
            TokenElevationTypeFull,
            TokenElevationTypeLimited
        }
    }
}
