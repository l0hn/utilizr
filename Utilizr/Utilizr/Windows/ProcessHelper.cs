﻿#if !MONO
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
#if !NETCOREAPP
using System.Management;
#endif
using System.Runtime.InteropServices;
using System.Text;
using Utilizr.Async;
using Utilizr.Info;
using Utilizr.Logging;

namespace Utilizr.Windows
{
    public static class ProcessHelper
    {
        const int RmRebootReasonNone = 0;
        const int CCH_RM_MAX_APP_NAME = 255;
        const int CCH_RM_MAX_SVC_NAME = 63;

        enum RM_APP_TYPE
        {
            RmUnknownApp = 0,
            RmMainWindow = 1,
            RmOtherWindow = 2,
            RmService = 3,
            RmExplorer = 4,
            RmConsole = 5,
            RmCritical = 1000
        }

        [StructLayout(LayoutKind.Sequential)]
        struct RM_UNIQUE_PROCESS
        {
            public int dwProcessId;
            public System.Runtime.InteropServices.ComTypes.FILETIME ProcessStartTime;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct RM_PROCESS_INFO
        {
            public RM_UNIQUE_PROCESS Process;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = CCH_RM_MAX_APP_NAME + 1)]
            public string strAppName;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = CCH_RM_MAX_SVC_NAME + 1)]
            public string strServiceShortName;

            public RM_APP_TYPE ApplicationType;
            public uint AppStatus;
            public uint TSSessionId;
            [MarshalAs(UnmanagedType.Bool)]
            public bool bRestartable;
        }

        [DllImport("rstrtmgr.dll", CharSet = CharSet.Unicode)]
        static extern int RmRegisterResources(
            uint pSessionHandle,
            UInt32 nFiles,
            string[] rgsFilenames,
            UInt32 nApplications,
            [In] RM_UNIQUE_PROCESS[] rgApplications,
            UInt32 nServices,
            string[] rgsServiceNames);

        [DllImport("rstrtmgr.dll", CharSet = CharSet.Auto)]
        static extern int RmStartSession(out uint pSessionHandle, int dwSessionFlags, string strSessionKey);

        [DllImport("rstrtmgr.dll")]
        static extern int RmEndSession(uint pSessionHandle);

        [DllImport("rstrtmgr.dll")]
        static extern int RmGetList(
            uint dwSessionHandle,
            out uint pnProcInfoNeeded,
            ref uint pnProcInfo,
            [In, Out] RM_PROCESS_INFO[] rgAffectedApps,
            ref uint lpdwRebootReasons);


        /// <summary>
        /// Find out what process(es) have a lock on the specified file.
        /// </summary>
        /// <param name="path">Paths of files to check.</param>
        /// <returns>Returns empty list if no lock found, otherwise list of processes locking. Throws exception on any error.</returns>
        static public List<Process> WhoIsLocking(params string[] paths)
        {
            if (!Platform.IsVistaAndHigher)
                throw new RmException("Vista for higher is required to use the Restart Manager API.");

            string key = Guid.NewGuid().ToString();
            var processes = new List<Process>();

            int result = RmStartSession(out uint handle, 0, key);
            if (result != 0)
                throw new RmException("Could not begin restart session. Unable to determine file locker.");

            try
            {
                const int ERROR_MORE_DATA = 234;
                uint pnProcInfo = 0;
                uint lpdwRebootReasons = RmRebootReasonNone;

                result = RmRegisterResources(handle, (uint)paths.Length, paths, 0, null, 0, null);

                if (result != 0)
                    throw new RmException("Could not register resource.");

                // Note: there's a race condition here -- the first call to RmGetList() returns
                // the total number of process. However, when we call RmGetList() again to get
                // the actual processes this number may have increased.
                result = RmGetList(handle, out uint pnProcInfoNeeded, ref pnProcInfo, null, ref lpdwRebootReasons);

                if (result == ERROR_MORE_DATA)
                {
                    var processInfo = new RM_PROCESS_INFO[pnProcInfoNeeded];
                    pnProcInfo = pnProcInfoNeeded;

                    // Get the list
                    result = RmGetList(handle, out pnProcInfoNeeded, ref pnProcInfo, processInfo, ref lpdwRebootReasons);
                    if (result == 0)
                    {
                        processes = new List<Process>((int)pnProcInfo);

                        // Enumerate all of the results and add them to the 
                        // list to be returned
                        for (int i = 0; i < pnProcInfo; i++)
                        {
                            try
                            {
                                processes.Add(Process.GetProcessById(processInfo[i].Process.dwProcessId));
                            }
                            // catch the error -- in case the process is no longer running
                            catch (ArgumentException)
                            {
                            }
                        }
                    }
                    else
                    {
                        throw new RmException("Could not list processes locking resource.");
                    }
                }
                else if (result != 0)
                {
                    throw new RmException("Could not list processes locking resource. Failed to get size of result.");
                }
            }
            finally
            {
                RmEndSession(handle);
            }

            return processes;
        }

        /// <summary>
        /// Wrapper around <see cref="WhoIsLocking(string[])"/> which will also generate exception with message containing locking process(es) info.
        // </summary>
        public static Exception WhoIsLockingChecker(IOException ex, string filePath)
        {
            try
            {
                var lockingProcesses = WhoIsLocking(filePath);
                if (!lockingProcesses.Any())
                    return ex;

                var sb = new StringBuilder();
                foreach (var p in lockingProcesses)
                {
                    sb.AppendLine($"{p.ProcessName} ({p.Id}), {p.MainModule?.FileName ?? "unknown file path"}");
                }

                string id = null;
                try { id = Process.GetCurrentProcess().Id.ToString(); }
                catch { }

                return new IOException($"[PID:{id ?? "unknown"}] Detected process(es) locking {filePath}.{Environment.NewLine}{sb.ToString()}", ex);
            }
            catch (RmException rmEx)
            {
                return new IOException($"Failed to determine what was locking {filePath}: {rmEx.Message}. See InnerException for original IOException", ex);
            }
        }

        public static WMIProcessInfo GetRunningProcess(string processName)
        {
            return GetRunningProcesses(processName).FirstOrDefault();
        }

        public static IEnumerable<WMIProcessInfo> GetRunningProcesses(string processName)
        {
            return InternalGetRunningProcesses($"WHERE Name=\"{processName}\"");
        }

        public static WMIProcessInfo GetRunningProcess(int pid)
        {
            return InternalGetRunningProcesses($"WHERE ProcessId={pid}").FirstOrDefault();
        }

        public static IEnumerable<WMIProcessInfo> GetRunningProcesses()
        {
            return InternalGetRunningProcesses(null);
        }

        static IEnumerable<WMIProcessInfo> InternalGetRunningProcesses(string whereClause = null)
        {
            var wmiQueryString = string.IsNullOrEmpty(whereClause)
                ? $"SELECT ProcessId, ExecutablePath, CommandLine, ParentProcessId FROM Win32_Process"
                : $"SELECT ProcessId, ExecutablePath, CommandLine, ParentProcessId FROM Win32_Process {whereClause}";

#if NETCOREAPP

            // WbemScripting is in the COM library "Microsoft WMI Scripting v1.2 Library"
            // WbemScripting.SWbemLocatorClass locator = new WbemScripting.SWbemLocatorClass();            
            var wmiResults = new[] { new { Pid = 0U, Cmd = "", Path = "", ParentPid = 0U } }.ToList();
            using (dynamic locator = COMObject.CreateObject("WbemScripting.SWbemLocator"))
            using (dynamic service = locator.ConnectServer(".", @"Root\Cimv2"))
            using (var resultsSet = service.ExecQuery(wmiQueryString))
            {
                foreach (var obj in resultsSet.Instance)
                {
                    using (dynamic wrapper = new COMObject(obj))
                    {
                        wmiResults.Add(new
                        {
                            Pid = (uint)wrapper.ProcessId,
                            Cmd = wrapper.CommandLine as string, // may be DBNull
                            Path = wrapper.ExecutablePath as string, // as above
                            ParentPid = (uint)wrapper.ParentProcessId,
                        });
                    }
                }
            }

            var results = Process.GetProcesses()
                .Join(
                    wmiResults,
                    p => (uint)p.Id,
                    wmiObj => wmiObj.Pid,
                    (p, wmiObj) => new WMIProcessInfo()
                    {
                        Process = p,
                        ExecutablePath = wmiObj.Path,
                        CommandLine = wmiObj.Cmd,
                        ParentProcessID = wmiObj.ParentPid,
                    }
                )
                // appears to be null if lacking permissions, filter out any
                .Where(p => p.ExecutablePath.IsNotNullOrEmpty())
                .ToList();

            return results;

#else
            using (var searcher = new ManagementObjectSearcher(wmiQueryString))
            using (var results = searcher.Get())
            {
                var query = from p in Process.GetProcesses()
                            join mo in results.Cast<ManagementObject>()
                            on p.Id equals (int)(uint)mo["ProcessId"]
                            select new WMIProcessInfo()
                            {
                                Process = p,
                                ExecutablePath = (string)mo["ExecutablePath"],
                                CommandLine = (string)mo["CommandLine"],
                                ParentProcessID = (uint)mo["ParentProcessId"],
                            };
                return query.ToList();
            }
#endif
        }

        public static IEnumerable<WMIProcessInfo> GetRunningProcessesForFile(string filePath)
        {
            return
                GetRunningProcesses()
                    .Where(i => i.ExecutablePath != null && i.ExecutablePath.Equals(filePath, StringComparison.InvariantCultureIgnoreCase));
        }

        public static WMIProcessInfo GetRunningParentProcess()
        {
            return GetRunningParentProcess(Process.GetCurrentProcess().Id);
        }

        public static WMIProcessInfo GetRunningParentProcess(int processID)
        {
            var running = GetRunningProcesses();
            var pInfo = running.FirstOrDefault(p => p.Process.Id == processID);

            if (pInfo == null)
                return null;

            return running.FirstOrDefault(p => p.Process.Id == pInfo.ParentProcessID);
        }

        /// <summary>
        /// Get full process path regardless of x32 / x34.
        /// </summary>
        /// <returns>Null if unable to find process.</returns>
        public static string GetProcessFullPath(uint processId)
        {
            try
            {
                var p = Process.GetProcessById((int)processId);
                return p.MainModule.FileName;
            }
            catch (Exception)
            {
                //x64
                try
                {
                    foreach (var item in GetRunningProcesses())
                    {
                        if (item.Process.Id == processId)
                            return item.ExecutablePath;
                    }
                }
                catch (Exception)
                {

                }
            }
            return null;
        }

        /// <summary>
        /// Relaunch the current process and exit this one.
        /// Guaranteed to stop the current, even if it fails to launch another instance of the executable.
        /// </summary>
        public static void Relaunch(bool runAsAdmin, params string[] args)
        {
            try
            {
                var exe = Process.GetCurrentProcess().MainModule.FileName;
                var fmtArgs = args == null
                    ? string.Empty
                    : string.Join(" ", args.Select(p => $"\"{p}\"").ToArray());

                var psi = new ProcessStartInfo(exe, fmtArgs);
                if (runAsAdmin)
                    psi.Verb = "runas";

                Process.Start(psi);
                Sleeper.Sleep(Time.Time.SECOND);
            }
            catch(Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                Environment.Exit(0);
            }
        }
    }

    [DebuggerDisplay("ExecutablePath={ExecutablePath}")]
    public class WMIProcessInfo
    {
        public Process Process { get; set; }
        public string ExecutablePath { get; set; }
        public string CommandLine { get; set; }
        public uint ParentProcessID { get; set; }
    }

    public class RmException : Exception
    {
        public RmException()
        {

        }

        public RmException(string message)
            : base(message)
        {

        }
    }
}
#endif