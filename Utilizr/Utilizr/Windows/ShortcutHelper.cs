﻿#if !MONO
using System;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Utilizr.Logging;


namespace Utilizr.Windows
{
    /// <summary>
    /// http://astoundingprogramming.wordpress.com/2012/12/17/how-to-get-the-target-of-a-windows-shortcut-c/
    /// </summary>
    public class ShortcutHelper
    {
// #if !NETCOREAPP

        public static string GetTargetPath(string filePath)
        {
            string targetPath = ResolveMsiShortcut(filePath);
            if (targetPath == null)
                targetPath = ResolveShortcut(filePath);

            return targetPath;
        }
        
// #endif
        static string ResolveMsiShortcut(string file)
        {
            StringBuilder product = new StringBuilder(NativeMethods.MaxGuidLength + 1);
            StringBuilder feature = new StringBuilder(NativeMethods.MaxFeatureLength + 1);
            StringBuilder component = new StringBuilder(NativeMethods.MaxGuidLength + 1);

            NativeMethods.MsiGetShortcutTarget(file, product, feature, component);

            int pathLength = NativeMethods.MaxPathLength;
            StringBuilder path = new StringBuilder(pathLength);

            NativeMethods.InstallState installState = NativeMethods.MsiGetComponentPath(product.ToString(), component.ToString(), path, ref pathLength);

            if (installState == NativeMethods.InstallState.Local)
            {
                return path.ToString();
            }

            return null;
        }

#if NETCOREAPP
        static string ResolveShortcut(string filePath)
        {
            // IWshRuntimeLibrary is in the COM library "Windows Script Host Object Model"
            // IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            using (dynamic shell = COMObject.CreateObject("WScript.Shell"))
            {
                try
                {
                    // IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(filePath);
                    dynamic shortcut = shell.CreateShortcut(filePath);

                    if (!File.Exists(shortcut.TargetPath))
                        return shortcut.TargetPath.Replace(" (x86)", "");

                    return shortcut.TargetPath;
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, "Failed to get shortcut target");
                    return null;
                }
            }
        }
#else
        static string ResolveShortcut(string filePath)
        {
            // IWshRuntimeLibrary is in the COM library "Windows Script Host Object Model"
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();

            try
            {
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(filePath);

                if (!File.Exists(shortcut.TargetPath))
                    return shortcut.TargetPath.Replace(" (x86)", "");
                
                return shortcut.TargetPath;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, "Failed to get shortcut target");
                return null;
            }
        }
#endif
        
        public static bool CreateAllUsersStartupShortcut(string shortcutTarget)
        {
            return CreateShortcut(KnownFolders.GetPath(KnownFolder.CommonStartup), shortcutTarget);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shortcutDir">Folder to contain the shortcut</param>
        /// <param name="shortcutTarget">Target of the shortcut</param>
        /// <param name="throwOnError">Whether exceptions are thrown on an error.</param>
        /// <returns>True if successfully created</returns>
        public static bool CreateShortcut(string shortcutDir, string shortcutTarget, bool throwOnError = false)
        {
            var shortcutPath = Path.Combine(shortcutDir, $"{Path.GetFileNameWithoutExtension(shortcutTarget)}.lnk");
            try
            {
                if (File.Exists(shortcutPath))
                {
                    if (throwOnError)
                        throw new Exception($"Unable to create shortcut, file '{shortcutPath}' already exists");

                    return false;
                }
#if NETCOREAPP
                dynamic shell = COMObject.CreateObject("WScript.Shell");
                dynamic shortcut = shell.CreateShortcut(shortcutPath);
#else
                var wsh = new IWshRuntimeLibrary.IWshShell_Class();
                IWshRuntimeLibrary.IWshShortcut shortcut = wsh.CreateShortcut(path) as IWshRuntimeLibrary.IWshShortcut;
#endif
                shortcut.TargetPath = shortcutTarget;
                shortcut.WorkingDirectory = Path.GetDirectoryName(shortcutTarget);
                shortcut.Save();

#if NETCOREAPP
                shell.Dispose();
#endif
                return true;
            }
            catch (Exception e)
            {
                Log.Exception("UTILIZR", e, "Create startup shortcut failed");

                if (throwOnError)
                    throw;
            }

            return false;
        }

        public static bool DeleteAllUsersStartupShortcut(string shortcutPath)
        {
            try
            {
                string allUsersStartupDirectory = KnownFolders.GetPath(KnownFolder.CommonStartup);

                System.IO.File.Delete(allUsersStartupDirectory + $"\\{Path.GetFileNameWithoutExtension(shortcutPath)}.lnk");
                return true;
            }
            catch (Exception e)
            {
                Log.Exception("UTILIZR", e, "Delete startup shortcut failed");
            }

            return false;
        }
      
        private class NativeMethods
        {
            [DllImport("msi.dll", CharSet = CharSet.Auto)]
            public static extern uint MsiGetShortcutTarget(string targetFile, StringBuilder productCode, StringBuilder featureID, StringBuilder componentCode);

            [DllImport("msi.dll", CharSet = CharSet.Auto)]
            public static extern InstallState MsiGetComponentPath(string productCode, string componentCode, StringBuilder componentPath, ref int componentPathBufferSize);

            public const int MaxFeatureLength = 38;
            public const int MaxGuidLength = 38;
            public const int MaxPathLength = 1024;

            public enum InstallState
            {
                NotUsed = -7,
                BadConfig = -6,
                Incomplete = -5,
                SourceAbsent = -4,
                MoreData = -3,
                InvalidArg = -2,
                Unknown = -1,
                Broken = 0,
                Advertised = 1,
                Removed = 1,
                Absent = 2,
                Local = 3,
                Source = 4,
                Default = 5
            }
        }
    }
}
#endif