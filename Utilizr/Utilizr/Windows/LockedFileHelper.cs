﻿#if !MONO && !ANDROID && !IOS

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Utilizr.Logging;
using Utilizr.Windows;


namespace Utilizr.Windows
{
    public static class LockedFileHelper
    {
        public static IEnumerable<Process> WhosLocking(string path)
        {
            Dictionary<int, Process> _procDict = new Dictionary<int, Process>();
            foreach (var handleInfo in GetHandlesForFile(path))
            {
                if (_procDict.ContainsKey(handleInfo.ProcessId))
                    continue;

                Process process;
                try
                {
                    process = Process.GetProcessById(handleInfo.ProcessId);
                }
                catch (Exception)
                {
                    continue;   
                }
                
                _procDict[handleInfo.ProcessId] = process;
                yield return process;
            }
        }

        public static IEnumerable<HandleInfo> GetHandlesForFile(string file)
        {
            foreach (var handleInfo in HandleUtil.GetHandles())
            {
                if (handleInfo.Name != null && handleInfo.Name.Contains("testlock"))
                {
                    Console.WriteLine(handleInfo.Name);
                }

                if (handleInfo.DosName.IsNullOrEmpty())
                    continue;

                if (!file.Equals(handleInfo.DosName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Log.Info($"{handleInfo.ProcessId} {handleInfo.DosName}");
                    continue;
                }


                yield return handleInfo;
            }
        }

        public static void CloseHandlesForFile(string file)
        {
            foreach (var handleInfo in GetHandlesForFile(file))
            {
                //close the handle? can it be this easy?
                var hnd = (IntPtr)handleInfo.Handle;
                Console.WriteLine($"Attempting to close handle [{hnd.ToString("x2")}] for [{file}]");
                Log.Info($"Attempting to close handle [{hnd.ToString("x2")}] for [{file}]");

                CloseDuplicateHandle(handleInfo.Handle, handleInfo.ProcessId);
            }
        }

        public static void CloseDuplicateHandle(ushort handle, int processId)
        {
            var sourceProcessHandle = NativeMethods.OpenProcess(0x40 /* dup_handle */, true, processId);
            if (sourceProcessHandle != IntPtr.Zero)
            {
                try
                {
                    IntPtr realHandle = IntPtr.Zero;
                    var result = Win32.DuplicateHandle(sourceProcessHandle, handle, (uint) Process.GetCurrentProcess().Handle,
                        out realHandle, 0, true, Win32.DUPLICATE_CLOSE_SOURCE);
                    if (result)
                    {
                        Console.WriteLine($"closing real handle [{realHandle.ToString("x2")}]");
                        int closeResult = Win32.CloseHandle(realHandle);
                        Log.Info($"Attempt to close handle [{realHandle.ToString("x2")}] result = {closeResult}");
                        Console.WriteLine($"Attempt to close handle [{realHandle.ToString("x2")}] result = {closeResult}");
                    }
                }
                finally
                {
                    Win32.CloseHandle(sourceProcessHandle);
                }
            }
        }
    }
}
#endif