﻿using System;
using System.Threading;
using Utilizr.Async;
using Action = Utilizr.Async.Action;

//using Action = System.Action;

namespace Utilizr.Timers
{
    public class ActionTimer : IDisposable
    {
        private ManualResetEvent _waitDone;
        private AutoResetEvent _sleepDone;
        /// <summary>
        /// The Action to be executed after each interval
        /// </summary>
        public Action Action { get; set; }

        private bool _alive = false;
        private bool _stop = false;
        private bool _executing;
        private bool _disposing = false;


        /// <summary>
        /// Timer is running
        /// </summary>
        public bool IsRunning => _alive;

        /// <summary>
        /// Timer is in the process of stopping
        /// </summary>
        public bool IsStopping => _stop;


        /// <summary>
        /// Instance is disposing
        /// </summary>
        public bool IsDisposing => _disposing;


        /// <summary>
        /// Wether the supplied action is currently being executed
        /// </summary>
        public bool IsDelegateExecuting => _alive && _executing;

        /// <summary>
        /// Interval between each execution of Action.
        /// </summary>
        public int Interval { get; set; }


        /// <summary>
        /// ActionTimer will execute your action at a set interval
        /// </summary>
        /// <param name="action"></param>
        /// <param name="interval"></param>
        public ActionTimer(Action action, int interval = 1000)
        {
            _waitDone = new ManualResetEvent(false);
            _sleepDone = new AutoResetEvent(false);
            Interval = interval;
            Action = action;
        }

        /// <summary>
        /// Forces the action to be executed once now, next action will be executed after a full interval after this execution.
        /// NOTE: if the timer is not running the action will still be executed once
        /// </summary>
        public void Pulse()
        {
            if (IsRunning && !IsStopping)
            {
                _sleepDone.Set();
            }
            else
            {
                AsyncHelper.BeginExecute(() => Action.Invoke());
            }
        }

        /// <summary>
        /// start executing the delegate at the specified interval
        /// </summary>
        public void Start()
        {
            if (!_alive)
            {
                AsyncHelper.BeginExecute(() =>
                {
                    _alive = true;
                    _stop = false;
                    while (!_stop && !_disposing)
                    {
                        _waitDone.WaitOne();
                        _executing = true;
                        Action?.Invoke();
                        _executing = false;
                        _sleepDone.WaitOne(Interval, false);
                    }
                }, (o) => _alive = false, false);
            }
            _waitDone.Set();
            _sleepDone.Reset();
        }

        /// <summary>
        /// Stops the delegate timer
        /// </summary>
        public void Stop()
        {
            _stop = true;
            _waitDone.Reset();
            _sleepDone.Reset();
        }

        public void Dispose()
        {
            _disposing = true;
            Action = null;
            Stop();
            _waitDone?.Close();
            _sleepDone?.Close();
        }
    }
}
