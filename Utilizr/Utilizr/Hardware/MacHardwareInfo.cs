﻿using System;

namespace Utilizr.Hardware
{
    public class MacHardwareInfo: HardwareInfo
    {
        private static string _motherBoardSerial = string.Empty;
        private static string _hddSerial = string.Empty;
        private static string _processorModel = string.Empty;

        public override string GetMotherboardSerialNumber()
        {
            if (string.IsNullOrEmpty(_motherBoardSerial))
            {
                var result = Shell.Exec("/bin/bash", "-c \"system_profiler SPHardwareDataType | awk '/Serial/ {print $4}'\"");
                _motherBoardSerial = result.Output.Trim(' ', '"');    
            }
            return _motherBoardSerial;
        }

        public override string GetProcessorID()
        {
            if (string.IsNullOrEmpty(_processorModel))
            {
                var result = Shell.Exec("/bin/bash", "-c \"sysctl -n machdep.cpu.brand_string\"");
                _processorModel = result.Output.Trim(' ', '"');    
            }
            return _processorModel;
        }

        /// <summary>
        /// On mac this method just returns the physical root hdd serial
        /// </summary>
        /// <returns></returns>
        public override string GetVolumeSerial()
        {
            return GetVolumeSerial(string.Empty);
        }

        /// <summary>
        /// On mac this method just returns the physical root hdd serial
        /// </summary>
        /// <returns></returns>
        public override string GetVolumeSerial(string volume)
        {
            return GetRootHDDPhysicalSerial();
        }

        

        public override string GetComputerName()
        {
            return Environment.MachineName;
        }

        public override string GetRootHDDPhysicalSerial()
        {
            if (string.IsNullOrEmpty(_hddSerial))
            {
                var result = Shell.Exec("/bin/bash", "-c \" ioreg -rd1", "-w0", "-c", "AppleAHCIDiskDriver | awk '/Serial/ {print $4}'\"");
                _hddSerial = result.Output.Trim(' ', '"');
            }
            return _hddSerial;
        }
    }
}
