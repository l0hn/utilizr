﻿using System;

namespace Utilizr.Hardware
{
    public static class UniqueID
    {
        /// <summary>
        /// Generates a unique hardware id based on various hardware serial numbers for the current system.
        /// Uses a combination of motherboard | hdd_physical_serial | processor_id
        /// </summary>
        /// <returns></returns>
        public static string GenerateUniqueHardwareID()
        {
            var hardware = HardwareInfo.GetHardwareInfo();
            var motherboardSerial = "none";
            var processorSerial = "none";
            var hddSerial = "none";
            try
            {
                motherboardSerial = hardware.GetMotherboardSerialNumber();
            }
            catch (Exception)
            {
            }
            try
            {
                processorSerial = hardware.GetProcessorID();
            }
            catch (Exception)
            {
            }
            try
            {
                hddSerial = hardware.GetRootHDDPhysicalSerial();
            }
            catch (Exception)
            {
            }
            motherboardSerial = motherboardSerial.ToLower().HashMD5();
            processorSerial = processorSerial.ToLower().HashMD5();
            hddSerial = hddSerial.ToLower().HashMD5();
            return string.Format("{0}|{1}|{2}", motherboardSerial, hddSerial, processorSerial).ToLower();
        }
    }
}
