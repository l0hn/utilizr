﻿using System;

namespace Utilizr.Hardware
{
    public class HardwareInfoException : Exception
    {
        public HardwareInfoException(string message): base(message)
        {

        }

        public HardwareInfoException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
