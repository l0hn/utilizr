﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using Utilizr.Info;

namespace Utilizr.Hardware
{
    public abstract class HardwareInfo
    {
        #region abstract methods
        public abstract string GetMotherboardSerialNumber();
        public abstract string GetProcessorID();
        public abstract string GetVolumeSerial();
        public abstract string GetVolumeSerial(string volume);
        public abstract string GetComputerName();
        public abstract string GetRootHDDPhysicalSerial();
        #endregion

        #region factory
        public static HardwareInfo GetHardwareInfo()
        {
            if(Platform.IsMac)
                return new MacHardwareInfo();

            if(Platform.IsPosix)
                return new LinuxHardwareInfo();

            if(Platform.IsWindows)
                return new WindowsHardwareInfo();

            throw new NotSupportedException("Unable to detect the platform for this OS.");
        }
        #endregion

        #region non-platform dependant
        public string[] GetMacAddresses()
        {
            return (from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.NetworkInterfaceType != NetworkInterfaceType.Loopback
                    && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel
                && !string.IsNullOrEmpty(nic.GetPhysicalAddress().ToString())
                    select nic.GetPhysicalAddress().ToString()).ToArray();
        }
        #endregion
    }
}
