﻿using Microsoft.Win32;
using System;
using System.IO;
using Utilizr.Info;

namespace Utilizr.Browsers
{
    public static class Chrome
    {
        public static bool IsInstalled()
        {
            if (Platform.IsMac) //TODO: Implement for mac
                throw new NotImplementedException();

            var localAppData = Environment.ExpandEnvironmentVariables("%localappdata%");
            var userData = Path.Combine(localAppData, @"Google\Chrome\User Data");
            return Directory.Exists(userData);
        }
		#if !MONO
        public static void InstallExtension(string extensionID)
        {
            if (!IsInstalled())
                throw new InvalidOperationException("Chrome doesn't appear to be installed");

            if (Platform.IsMac)
            {
                InstallExtensionMac(extensionID);
                return;
            }

            InstallExtensionWindows(extensionID);
        }

        static void InstallExtensionWindows(string extensionID)
        {
            string regPath = Platform.Is64BitOS
                ? @"Software\Wow6432Node\Google\Chrome\Extensions"
                : @"Software\Google\Chrome\Extensions";

            var key = Registry.LocalMachine.OpenSubKey(regPath, true);

            if(key == null)
                key = Registry.LocalMachine.CreateSubKey(regPath);

            var extRegKey = key.CreateSubKey(extensionID);
            extRegKey.SetValue("update_url", @"https://clients2.google.com/service/update2/crx", RegistryValueKind.String);

            extRegKey.Close();
            key.Close();
        }
		#endif
        static void InstallExtensionMac(string extensionID)
        {
            throw new NotImplementedException(); //TODO Implement for mac
        }

    }
}
