﻿#if !MONO && !IOS && !ANDROID

#endif
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using GetText;
using Utilizr.Extensions;
using Utilizr.Windows;
#if !NETCOREAPP && !MONO
using System.Management;
#endif

namespace Utilizr.Info
{
    public static class Platform
    {
        static Dictionary<string, OperatingSystem> _winOSLookup = new Dictionary<string, OperatingSystem>()
        {
            {"5.1", OperatingSystem.XP},
            {"5.2", OperatingSystem.XP},
            {"6.0", OperatingSystem.Vista},
            {"6.1", OperatingSystem.Win7},
            {"6.2", OperatingSystem.Win8},
            {"6.3", OperatingSystem.Win8_1},
            {"10.0", OperatingSystem.Win10},
            {"10.0.22000", OperatingSystem.Win11},
        };

        static string _cachedMacOSLookup;

        static Dictionary<string, OperatingSystem> _macOSLookup = new Dictionary<string, OperatingSystem>()
        {
            {"10.0", OperatingSystem.Cheetah},
            {"10.1", OperatingSystem.Puma},
            {"10.2", OperatingSystem.Jaguar},
            {"10.3", OperatingSystem.Panther},
            {"10.4", OperatingSystem.Tiger},
            {"10.5", OperatingSystem.Leopard},
            {"10.6", OperatingSystem.SnowLeopard},
            {"10.7", OperatingSystem.Lion},
            {"10.8", OperatingSystem.MountainLion},
            {"10.9", OperatingSystem.Mavericks},
            {"10.10", OperatingSystem.Yosemite},
            {"10.11", OperatingSystem.ElCapitan},
            {"10.12", OperatingSystem.Sierra},
            {"10.13", OperatingSystem.HighSierra},
            {"10.14", OperatingSystem.Mojave},
        };

        [DllImport("libc")]
        static extern int uname(IntPtr buf);

        public static bool IsWindows
        {
            get
            {
#if NETCOREAPP
                return RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
#else
                switch (Environment.OSVersion.Platform)
                {
                    case PlatformID.Xbox:
                    case PlatformID.MacOSX:
                    case PlatformID.Unix:
                        return false;
                    default:
                        return true;
                }
#endif
            }
        }

        public static bool IsXPOrLower => IsWindows && OSVersion < OperatingSystem.Vista;

        public static bool IsVistaAndLower => IsWindows && OSVersion < OperatingSystem.Win7;

        public static bool IsVistaAndHigher => IsWindows && OSVersion > OperatingSystem.XP;

        public static bool IsWin7OrHigher => IsWindows && OSVersion >= OperatingSystem.Win7;

        public static bool IsWin8AndHigher => IsWindows && OSVersion >= OperatingSystem.Win8;

		/// <summary>
		/// Is windows 8 or windows 8.1
		/// </summary>
		public static bool IsWin8Variant => IsWindows && (OSVersion == OperatingSystem.Win8 || OSVersion == OperatingSystem.Win8_1);

		public static bool IsWin10AndHigher => IsWindows && OSVersion >= OperatingSystem.Win10;

        private static Version RS5Version = new Version(10,0,17763);
        public static bool IsRS5OrHigher => Environment.OSVersion.Version >= RS5Version;

		private static Version Win10_20H1 = new Version(10, 0, 19000);
		public static bool IsWin10_20H1Plus => Environment.OSVersion.Version >= Win10_20H1;

		/// <summary>
		/// Lowest windows 11 version number since it still starts with 10.0
		/// </summary>
		public static Version Win11_Min => new Version(10, 0, 22000);


		private static bool? _isMac;
		public static bool IsMac
		{
			get
			{
				if (IsWindows)
				{ //the isMac check is slow when run on windows.
					return false;
				}
#if NETCOREAPP
                return RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
#else
                if (_isMac == null)
				{
					IntPtr buf = IntPtr.Zero;
					try
					{
						buf = Marshal.AllocHGlobal(8192);
						// This is a hacktastic way of getting sysname from uname ()
						// Non-Mono will fail on uname, as DllImport[libc], will default to false which will be correct
						if (uname(buf) == 0)
						{
							string os = Marshal.PtrToStringAnsi(buf);
							if (os == "Darwin")
								_isMac = true;
						}
					}
					catch
					{
					}
					finally
					{
						_isMac = _isMac ?? false;
						if (buf != IntPtr.Zero)
							Marshal.FreeHGlobal(buf);
					}
				}
				return _isMac ?? false;
#endif
            }
        }

		public static bool IsIOS
		{
			get
			{
#if IOS
				return true;
#else
				return false;
#endif
			}
		}

		public static bool IsIOSTablet
		{
			get
			{
#if IOS
				return UIKit.UIDevice.CurrentDevice.UserInterfaceIdiom == UIKit.UIUserInterfaceIdiom.Pad;
#else
				return false;
#endif
			}
		}

		public static bool IsAndroid
		{
			get
			{
#if ANDROID
                return true;
#else
				return false;
#endif
			}
		}

		public static bool IsAndroidTablet
		{
			get
			{
#if ANDROID
                //TODO: Possibly slightly underestimated as action bar not included?
                var dm = Android.Content.Res.Resources.System.DisplayMetrics;
                var yInch = dm.HeightPixels / dm.Ydpi;
                var xInch = dm.WidthPixels / dm.Xdpi;
                var screenSizeDiag = Math.Sqrt(Math.Pow(yInch, 2) + Math.Pow(xInch, 2));
                return screenSizeDiag > 7.0;
#else
				return false;
#endif
			}
		}

		public static bool IsPosix
		{
			get
			{
				return Environment.OSVersion.Platform == PlatformID.Unix ||
						Environment.OSVersion.Platform == PlatformID.MacOSX;
			}
		}

		/// <summary>
		/// Identifies if the OS is 64bit
		/// </summary>
		/// <remarks>
		/// This method replaces Is64Bit method, it now returns the OS Bitness instead of the running process Bitness
		/// If you require the old functionality please use Is64BitProcess instead
		/// </remarks>
		public static bool Is64BitOS
		{
			get
			{
#if NETCOREAPP
                return RuntimeInformation.OSArchitecture == Architecture.X64 ||
                       RuntimeInformation.OSArchitecture == Architecture.Arm64;
#endif

#if !MONO
                if (IntPtr.Size == 8)
                {
                    return true; //64 bit
                }
                else  //64 or 32 bit?
                {
                    //Check if 32 bit process is running on a 64 bit system
                    bool isWow64;

                    return Windows.Win32.DoesWin32MethodExist("kernel32.dll", "IsWow64Process") &&
                        Windows.Win32.IsWow64Process(Windows.Win32.GetCurrentProcess(), out isWow64)
                        && isWow64;
                }
#else
				return (IntPtr.Size == 8);
#endif
			}
		}

		/// <summary>
		/// Indicates if the application process is a 32Bit
		/// </summary>
		public static bool Is32BitProcess
        {
            get
            {
#if NETCOREAPP
                return RuntimeInformation.ProcessArchitecture == Architecture.X86 ||
                       RuntimeInformation.ProcessArchitecture == Architecture.Arm;
#endif
                return IntPtr.Size == 4;
            }
        }

		/// <summary>
		/// Indicates if the application process is a 64Bit
		/// </summary>
		public static bool Is64BitProcess
        {
            get
            {
#if NETCOREAPP
                return RuntimeInformation.ProcessArchitecture == Architecture.X64 ||
                       RuntimeInformation.ProcessArchitecture == Architecture.Arm64;
#endif
                return IntPtr.Size == 8;
            }
        }


		public static OperatingSystem OSVersion
		{
			get
			{
#if IOS
				return OperatingSystem.IOS;
#endif
#if ANDROID
                    return OperatingSystem.Android;
#endif
				var returnVal = OperatingSystem.Other;
				if (IsWindows)
				{
					int major = Environment.OSVersion.Version.Major;
					int minor = Environment.OSVersion.Version.Minor;
					string vLookup = $"{major}.{minor}";
					_winOSLookup.TryGetValue(vLookup, out returnVal);
					
					// A little hacky since all other OS just need major.minor, but win 11
					// needs major.minor.build
					if (returnVal == OperatingSystem.Win10)
					{
						returnVal = Environment.OSVersion.Version >= Win11_Min
							? OperatingSystem.Win11
							: OperatingSystem.Win10;
					}
				}
				else if (IsMac)
				{
					if (string.IsNullOrEmpty(_cachedMacOSLookup))
					{
						try
						{
							var result = Shell.Exec("sw_vers", "-productVersion");
							if (result.ExitCode == 0)
							{
								_cachedMacOSLookup = result.Output.StringBeforeLastIndefOf('.');
							}
						}
						catch (Exception)
						{
							//resort to:
							_cachedMacOSLookup = "{0}.{1}".format(Environment.OSVersion.Version.Major, Environment.OSVersion.Version.Minor);
						}
					}
					_macOSLookup.TryGetValue(_cachedMacOSLookup, out returnVal);

				}
				else if (IsPosix)
					returnVal = OperatingSystem.Linux;

				return returnVal;
			}
		}

		/// <summary>
		/// Human readable version of OSVersion. E.g. Microsoft Windows 10 Pro
		/// </summary>
		public static string OSVersionHuman
		{
			get
			{
#if !MONO
				if (IsWindows)
                {
#if !MONO && !IOS && !ANDROID
                    string windowsVer = null;
                    try
                    {
                        var key = RegistryHelper.GetKeyFromPath(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion");
                        windowsVer = key?.GetValue("ProductName", string.Empty) as string;
                    }
                    catch { /* swallow */ }

                    if (windowsVer.IsNotNullOrWhitespace())
                        return windowsVer;

                    return Environment.OSVersion.ToHuman();
                
#endif
					}
                else if (IsMac)
                {
                    // TODO
                    throw new NotImplementedException();
                }
                else if (IsPosix)
                {
                    // TODO
                    throw new NotImplementedException();
                }
#endif
				return L._("Unknown operating system");
            }
        }

		//3.0.04506.2152 or higher?
		static Version _net3spversion = new Version(3, 0, 04506, 2152);
		private static bool? _isNet3SP2OrHigher;
		public static bool IsNet3SP2OrHigher
		{
			get
			{
				if (!_isNet3SP2OrHigher.HasValue)
				{
					try
					{
						var waitMethod = typeof(ManualResetEvent).GetMethod("WaitOne", new[] { typeof(int) });
						_isNet3SP2OrHigher = waitMethod != null;
					}
					catch (Exception)
					{
						_isNet3SP2OrHigher = false;
					}
				}
				return _isNet3SP2OrHigher ?? false;
			}
		}

		private static ulong? _ram;
		/// <summary>
		/// Total amount of RAM installed on the computer. Not how much is in use.
		/// </summary>
		public static ulong Ram
		{
			get
			{
				if (!_ram.HasValue)
				{
					//TODO: Implement for all other platforms, only windows done so far
#if IOS
                    _ram = MachMemory();
#elif ANDROID
                    throw new NotImplementedException();
#elif MONO
					_ram = MachMemory();
#elif NETCOREAPP
					var query = "SELECT * From Win32_ComputerSystem";
					using (dynamic locator = COMObject.CreateObject("WbemScripting.SWbemLocator"))
					{
						dynamic service = locator.ConnectServer(".", @"Root\Cimv2");
						var info = service.ExecQuery(query);

						foreach (var first in info.Instance)
						{
							using (dynamic wrapper = new COMObject(first))
							{
								_ram = Convert.ToUInt64(wrapper.TotalPhysicalMemory);
								break;
							}
						}
					}
#else
					using (ManagementClass mc = new ManagementClass("Win32_ComputerSystem"))
					using (ManagementObjectCollection moc = mc.GetInstances())
					{
						foreach (var item in moc)
						{
							_ram = Convert.ToUInt64(item.Properties["TotalPhysicalMemory"].Value);
						}
					}
#endif
				}

				return _ram.Value;
			}
		}

#if MONO || IOS
		[DllImport("__Internal")]
		public extern static uint mach_host_self();

		[DllImport("__Internal")]
		public extern static uint host_statistics(uint host_port, uint type, out vm_statistics_data_t stats, out int host_size);

		[DllImport("__Internal")]
		public extern static uint host_page_size(uint host_port, out UIntPtr vm_size_t);


		[StructLayout(LayoutKind.Sequential)]
		public struct vm_statistics_data_t
		{
			public uint free_count;       /* # of pages free */
			public uint active_count;     /* # of pages active */
			public uint inactive_count;       /* # of pages inactive */
			public uint wire_count;       /* # of pages wired down */
			public uint zero_fill_count;  /* # of zero fill pages */
			public uint reactivations;        /* # of pages reactivated */
			public uint pageins;      /* # of pageins */
			public uint pageouts;     /* # of pageouts */
			public uint faults;           /* # of faults */
			public uint cow_faults;       /* # of copy-on-writes */
			public uint lookups;      /* object cache lookups */
			public uint hits;         /* object cache hits */
			public uint purgeable_count;  /* # of pages purgeable */
			public uint purges;           /* # of pages purged */
			public uint speculative_count;    /* # of pages speculative */
		}

		//NOT always accurate :S but usually fairly close
		//adapted from https://github.com/Shmoopi/iOS-System-Services/blob/master/System%20Services/Utilities/SSMemoryInfo.m
		private static ulong MachMemory()
		{
			var hostPort = mach_host_self();
			UIntPtr pagesize;
			int host_size = Marshal.SizeOf(typeof(vm_statistics_data_t)) / Marshal.SizeOf(typeof(int));
			host_page_size(hostPort, out pagesize);

			vm_statistics_data_t vm_stat;
			if (host_statistics(hostPort, 2, out vm_stat, out host_size) == 0)
			{

				var totalPages = vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count + vm_stat.purgeable_count + vm_stat.speculative_count + vm_stat.free_count;
				var bytes = totalPages * pagesize.ToUInt64();
				int totalMB = (int)(bytes / 1024 / 1024);

				// Round to the nearest multiple of 256mb - Almost all RAM is a multiple of 256mb (I do believe)
				int toNearest = 256;
				int remainder = totalMB % toNearest;

				if (remainder >= toNearest / 2)
				{
					// Round the final number up
					totalMB = (totalMB - remainder) + 256;
				}
				else {
					// Round the final number down
					totalMB = totalMB - remainder;
				}
				return (ulong)totalMB * 1024 * 1024;
			}

			return 0;
		}
#endif

#if !MONO && !IOS && !ANDROID
        public static string[] DotNetFrameworksInstalled => DotNet.InstalledFrameworks().ToArray();
#endif

    }


    public enum OperatingSystem
	{
		XP,
		Vista,
		Win7,
		Win8,
		Win8_1,
		Win10,
		Win11,
		Cheetah,
		Puma,
		Jaguar,
		Panther,
		Tiger,
		Leopard,
		Lion,
		MountainLion,
		Mavericks,
		Linux,
		SnowLeopard,
		Yosemite,
		ElCapitan,
		Sierra,
        HighSierra,
        Mojave,
        Other,
		IOS,
		Android,
	}
}