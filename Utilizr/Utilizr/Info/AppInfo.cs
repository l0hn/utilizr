﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

#if !IOS && !ANDROID
using Utilizr.Logging;
#endif

#if IOS
using Foundation;
#endif

#if ANDROID
using Android.Content;
#endif

using Utilizr.FileSystem;

#if IOS || ANDROID
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
#else
using Ionic.Zip;
#endif



namespace Utilizr.Info
{
    /// <summary>
    /// Determine the root location for any of the app's folders.
    /// </summary>
    public enum AppInfoRoot
    {
        /// <summary>
        /// Inside app's install directory (default)
        /// </summary>
        InstallDirectory,

        /// <summary>
        /// Use the user local appdata folder (or the C:\ProgramData folder for non-user processes).
        /// </summary>
        AppData,

        /// <summary>
        /// Always use non-user specific location of C:\ProgramData.
        /// </summary>
        ProgramData,
    }

    public static class AppInfo
    {
        /// <summary>
        /// Specify the root for any of the folders specified on properties within this class.
        /// See <see cref="AppInfoRoot"/> for valid values.
        /// </summary>
        public static AppInfoRoot DirectoryRoot { get; set; }

        private static string _directoryRootPath;
        /// <summary>
        /// Root path for supporting folders (/logs, /data, etc). Will differ depending on <see cref="DirectoryRoot"/>.
        /// Note: Not the same as <see cref="AppDirectory"/>!
        /// </summary>
        public static string DirectoryRootPath
        {
            get
            {
                CreateAppDirectory(ref _directoryRootPath, "");
                return _directoryRootPath;
            }
        }

        public static string AppName { get; set; }

        private static string _appDirectory;
        /// <summary>
        /// Directory of executing assembly (install directory) and will not differ depending on <see cref="DirectoryRoot"/>.
        /// </summary>
        public static string AppDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(_appDirectory))
                {
					#if IOS
					var dirs = NSSearchPath.GetDirectories (NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User, true);
					_appDirectory = dirs [0];
					#else
					_appDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
					#endif
                }
                return _appDirectory;
            }
        }

		#if ANDROID
		//need to set manually at startup once application Context has been obtained
		public static void InitAppDirectory(Context context) {
			_appDirectory = context.ApplicationContext.FilesDir.AbsolutePath;
		}
		#endif

        private static string _urlDataDirectory;
        public static string UrlDataDirectory
        {
            get
            {
                CreateAppDirectory(ref _urlDataDirectory, "url_data");
                return _urlDataDirectory;
            }
        }

        private static string _libsDirectory;
        public static string LibsDirectory {
            get {
                CreateAppDirectory (ref _libsDirectory, "libs");
                return _libsDirectory;
            }
        }

        private static void CreateAppDirectory(ref string directory_string, string dir_name)
        {
            if (string.IsNullOrEmpty(directory_string))
            {
                string directory = Path.Combine(AppDirectory, dir_name);
                if (Platform.IsWindows)
                {
                    directory = GetAppDirectoryWin(dir_name);
                }
                else if (Platform.IsMac) //check windows first to prevent 300ms delay to check the IsMac property on a windows machine
                {
                    directory = GetAppDirectoryMac(dir_name);
                }

                directory_string = directory;
            }
            if (!Directory.Exists(directory_string))
            {
                Directory.CreateDirectory(directory_string);
            }
        }

        private static string GetAppDirectoryWin(string dirName)
        {
            return GetAppDirectoryWin(dirName, DirectoryRoot);
        }

        private static string GetAppDirectoryWin(string dirName, AppInfoRoot rootDir)
        {
            if (string.IsNullOrEmpty(AppName))
                throw new InvalidOperationException($"Unable to get window's directory with null/empty '{nameof(AppName)}' for '{dirName}' with root '{rootDir}'");

            string programData()
            {
                return PathHelper.Combine((Platform.IsXPOrLower ? "%ALLUSERSPROFILE%" : "%PROGRAMDATA%").ExpandVars(), AppName, dirName);
            }

            if (rootDir == AppInfoRoot.AppData)
            {
                if (!Environment.UserInteractive) //is most likely a service
                {
                    return programData();
                }
                return PathHelper.Combine((Platform.IsXPOrLower ? "%APPDATA%" : "%LOCALAPPDATA%").ExpandVars(), AppName, dirName);
            }
            else if (rootDir == AppInfoRoot.ProgramData)
            {
                return programData();
            }

            //AppInfoRoot.InstallDirectory
            return PathHelper.Combine(AppDirectory, dirName);
        }
        
        private static string GetAppDirectoryMac(string dirName)
        {
            var home = Environment.GetEnvironmentVariable("HOME");
            //TODO: come up with a better default for mac
            var name = AppName;
            if (string.IsNullOrEmpty(name))
            {
                var entry = Assembly.GetEntryAssembly();

                if (entry != null)
                {
                    name = Path.GetFileName(entry.Location);
                    var i = name.LastIndexOf('.');
                    if (i > 0)
                        name = name.Substring(0, i);
                }
                else
                {
                    name = "Utilizr";
                }
            }

            return PathHelper.Combine(home, "Library/Application Support", name, dirName);
        }

        private static string _dataDirectory;
        public static string DataDirectory
        {
            get
            {
                CreateAppDirectory(ref _dataDirectory, "data");
                return _dataDirectory;
            }
        }

        private static string _queueDirectory;
        public static string QueueDirectory
        {
            get
            {
                CreateAppDirectory(ref _queueDirectory, "queues");
                return _queueDirectory;
            }
        }

        private static string _logDirectory;
        public static string LogDirectory
        {
            get
            {
                CreateAppDirectory(ref _logDirectory, "logs");
                return _logDirectory; 
            }
        }

        private static string _updatesDirectory;
        public static string UpdatesDirectory
        {
            get
            {
                CreateAppDirectory(ref _updatesDirectory, "updates");
                return _updatesDirectory;
            }
        }

        private static string _cacheDir;

        public static string CacheDir
        {
            get
            {
                CreateAppDirectory(ref _cacheDir, "cache");
                return _cacheDir;
            }
        }
        
        public static void ZipLogs(string destinationFile)
        {
            ZipLogs(destinationFile, new AdditionalItem[]{}, new AdditionalItem[]{});
        }

        public static void ZipLogs(string destinationFile, AdditionalItem[] additionalDirs, AdditionalItem[] additionalFiles) 
        {
            if (File.Exists(destinationFile))
            {
                File.Delete(destinationFile);
            }

            using (var zip = new ZipFile(destinationFile)) 
            {

#if IOS || ANDROID
                zip.AddDirectory(LogDirectory);
                zip.AddDirectory(DataDirectory);
#else
				zip.AlternateEncoding = Encoding.UTF8;
				zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                zip.AddDirectory(LogDirectory, "logs");
                zip.AddDirectory(DataDirectory, "data");
                if (!AppName.IsNullOrEmpty())
                {
                    var progDataLogsDir = GetAppDirectoryWin("logs", AppInfoRoot.ProgramData);
                    if (Directory.Exists(progDataLogsDir))
                    {
                        zip.AddDirectory(progDataLogsDir, "progdata_logs");
                    }
                }
#endif
                
                foreach (var additionalDir in additionalDirs)
                {
                    try
                    {
                        if (!Directory.Exists(additionalDir.Path))
                            continue;
#if IOS || ANDROID
                        zip.AddDirectory(System.IO.Path.Combine(additionalDir.Path, additionalDir.PathInArchive));
#else
                        zip.AddDirectory(additionalDir.Path, additionalDir.PathInArchive);
#endif
                    }
                    catch (Exception e)
                    {
                    }
                }

                foreach (var additionalFile in additionalFiles)
                {
                    try
                    {
                        if (!File.Exists(additionalFile.Path))
                            continue;
#if IOS || ANDROID
                        zip.AddDirectory(System.IO.Path.Combine(additionalFile.Path, additionalFile.PathInArchive));
#else
                        zip.AddFile(additionalFile.Path, additionalFile.PathInArchive);
#endif
                    }
                    catch (Exception e)
                    {
                    }
                }
                
#if IOS || ANDROID
                zip.CommitUpdate();
                zip.Close();
#else
                zip.Save();
#endif
            }
        }

#if !IOS && !ANDROID
        public static bool DecompressCurrentLogs(string logDir)
        {
            LogC.Flush();

            var logFiles = Directory.GetFiles(logDir, "*.logc");
            if (logFiles.Length < 1)
                return false;

            foreach (var file in logFiles)
            {
                LogC.Decompress(file, logDir, true);
            }

            return true;
        }
#endif

    }
    
    public struct AdditionalItem
    {
        public string Path;
        public string PathInArchive;
    }
}