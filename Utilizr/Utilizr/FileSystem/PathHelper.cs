﻿using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Utilizr.Logging;
using Utilizr.Windows;

namespace Utilizr.FileSystem
{
    public static class PathHelper
    {
        const string _uncPrefix = @"\\?\";
        const int _maxPath = 260;

        /// <summary>
        /// Path.Combine() can only accept two parameters in .NET 3.0. This is a convenience method to 
        /// replicate support of more parameters that is available in Path.Combine() in .NET 4.0+.
        /// </summary>
        /// <param name="path1">First path</param>
        /// <param name="path2">Second path</param>
        /// <param name="morePaths">Any additional paths</param>
        /// <returns></returns>
        public static string Combine(string path1, string path2, params string[] morePaths)
        {
            string result = Path.Combine(path1, path2);

            foreach (var path in morePaths)
            {
                result = Path.Combine(result, path);
            }

            return result;
        }

        /// <summary>
        /// Same as Path.GetPathRoot() but safe on long paths
        /// </summary>
        /// <returns></returns>
        public static string GetPathRoot(string path)
        {
            // Path.GetPathRoot(null) returns null
            if (path == null)
                return path;

            // Path.GetPathRoot(string.Empty) throws ArgumentException
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException($"The {nameof(path)} is not of legal form");

            var vIndex = path.IndexOf(Path.VolumeSeparatorChar);
            if (vIndex == -1)
                return string.Empty;

            var root = $"{path.Substring(0, vIndex)}{Path.VolumeSeparatorChar}{Path.DirectorySeparatorChar}";

            // remove any potential UNC prefix
            return root.TrimStart(@"\\?\");
        }

        public static string UncFullPathIfNeeded(string source)
        {
            if (source == null || source.Length < _maxPath)
                return source;

            return UncFullPath(source);
        }

        /// <summary>
        /// Create a UNC equivalent for the specified path.
        /// </summary>
        /// <param name="source">Original directory.</param>
        /// <param name="optionalSuffix">Optionally add to the end of the path, e.g. '*'</param>
        /// <returns></returns>
        public static string UncFullPath(string source, string optionalSuffix = null)
        {
            if (source.StartsWith(_uncPrefix) || IsUncPath(source))
            return optionalSuffix == null
                ? source
                : Combine(source, optionalSuffix);

            // Don't include environment variables
            source = Environment.ExpandEnvironmentVariables(source);

            return optionalSuffix == null
                ? $"{_uncPrefix}{source}"
                : $"{_uncPrefix}{Combine(source, optionalSuffix)}";
        }

        /// <summary>
        /// Convert a UNC path back to a normal file path
        /// </summary>
        /// <returns></returns>
        public static string RemoveUncFullPath(string source)
        {
            if (source?.StartsWith(_uncPrefix) == true)
            {
                return source.Substring(_uncPrefix.Length);
            }

            return source;
        }

        /// <summary>
        /// Same functionality as Path.GetDirectoryName but safe on long paths,
        /// will not throw PathTooLongException.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static string GetDirectoryName(string directory)
        {
            if (string.IsNullOrEmpty(directory))
                return directory;

            var dirSepIndex = directory.LastIndexOf(Path.DirectorySeparatorChar);
            if (dirSepIndex >= 0)
                return directory.Substring(0, dirSepIndex);

            var volSepIndex = directory.IndexOf(Path.VolumeSeparatorChar);
            if (volSepIndex >= 0)
                return directory.Substring(0, volSepIndex);

            return string.Empty;
        }

        /// <summary>
        /// Same functionality as Path.GetFileName but safe on long paths,
        /// will not throw PathTooLongException.
        public static string GetFileName(string filePath)
        {
            var trimmed = filePath?.TrimEnd(Path.DirectorySeparatorChar);
            var dirCharIndex = trimmed?.LastIndexOf(Path.DirectorySeparatorChar);
            if (dirCharIndex == null)
                return null;

            if (dirCharIndex < 0)
            {
                if (trimmed.Length > 0)
                    return trimmed;

                return null;
            }

            var result = trimmed.Substring(dirCharIndex.Value);
            return result.TrimStart(Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Increments filename with numbers, only returning when the file path doesn't exist.
        /// The same path will be returned to that provided if the file doesn't already exist.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string DistinctlyNumberedFileName(string filePath)
        {
            var ext = Path.GetExtension(filePath); // handles long paths
            var filePathNoExtension = filePath;
            if (ext.Length < filePathNoExtension.Length)
                filePathNoExtension = filePathNoExtension.Substring(0, filePathNoExtension.Length - ext.Length);

            int count = 1;
            while (FileExists(filePath))
            {
                filePath = $"{filePathNoExtension} - ({count}){ext}";
                count++;
            }

            return filePath;
        }

        /// <summary>
        /// Make an absolute path a subdirectory to the provided parameter.
        /// E.g. F:\Homework\Maths\test.txt => C:\Users\student\Desktop\F\Homework\Maths\test.txt
        /// </summary>
        /// <param name="subPathRoot">The absolute path to a folder which will contain the subdirectory.</param>
        /// <param name="absolutePath">The absolute path that will become a subdirectory within <paramref name="subPathRoot"/></param>
        /// <returns></returns>
        public static string AbsolutePathToSubdirectory(string subPathRoot, string absolutePath)
        {
            var root = GetPathRoot(absolutePath);
            var rootAsFolder = root.TrimEnd(new char[] { Path.VolumeSeparatorChar, Path.DirectorySeparatorChar });

            var combined = Path.Combine(subPathRoot, rootAsFolder);

            if (root.Length < absolutePath.Length)
            {
                combined = Path.Combine(combined, absolutePath.Substring(root.Length));
                combined = UncFullPathIfNeeded(combined);
            }

            return combined;
        }

        /// <summary>
        /// Equivalent of File.Exists() but can accept long paths. Converts to UNC, safe if already UNC path.
        /// </summary>
        public static bool FileExists(string file)
        {
            file = UncFullPath(file);

            if (!Win32.GetFileAttributesEx(file, Win32.GET_FILEEX_INFO_LEVELS.GetFileExInfoStandard, out Win32.WIN32_FILE_ATTRIBUTE_DATA fData))
                return false;

            return fData.dwFileAttributes != Win32.FILE_ATTRIBUTE_INVALID &&
                   (fData.dwFileAttributes & Win32.FILE_ATTRIBUTE_DIRECTORY) == 0;
        }

        /// <summary>
        /// Equivalent of Directory.Exists() but can accept long paths. Converts to UNC, safe if already UNC path.
        /// </summary>
        public static bool DirectoryExists(string directory)
        {
            directory = UncFullPath(directory);

            if (!Win32.GetFileAttributesEx(directory, Win32.GET_FILEEX_INFO_LEVELS.GetFileExInfoStandard, out Win32.WIN32_FILE_ATTRIBUTE_DATA fData))
                return false;

            return fData.dwFileAttributes != Win32.FILE_ATTRIBUTE_INVALID &&
                   (fData.dwFileAttributes & Win32.FILE_ATTRIBUTE_DIRECTORY) != 0;
        }

        public static bool IsUncPath(string path)
        {
            return Uri.TryCreate(path, UriKind.Absolute, out Uri uri) && uri.IsUnc;
        }

        /// <summary>
        /// Must be disposed after finished. Converts to UNC only if not already a UNC path.
        /// </summary>
        public static SafeFileHandle GetCreateFileForWrite(string file)
        {
            file = UncFullPath(file);

            var hFile = Win32.CreateFileW(
                file,
                Win32.GENERIC_ALL,
                Win32.FILE_SHARE_NONE,
                IntPtr.Zero,
                Win32.CREATE_NEW,
                Win32.FILE_ATTRIBUTE_NORMAL,
                IntPtr.Zero
            );

            if (hFile.ToInt32() < 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());

            return new SafeFileHandle(hFile, true);
        }

        /// <summary>
        /// Equivalent of Directory.CreateDirectory() but can accept long paths. Converts to UNC only if not already a UNC path.
        /// </summary>
        public static void CreateDirectory(string directory)
        {
            if (string.IsNullOrEmpty(directory))
                throw new ArgumentNullException(nameof(directory));

            var directoryNames = directory.TrimStart(@"\\?\").Split(Path.DirectorySeparatorChar);
            string path = string.Empty;
            foreach (var dName in directoryNames)
            {
                if (string.IsNullOrEmpty(dName))
                    continue;

                path = string.IsNullOrEmpty(path)
                    ? UncFullPath($"{dName}{Path.DirectorySeparatorChar}")
                    : Path.Combine(path, dName);

                if (DirectoryExists(path))
                    continue;

                if (!Win32.CreateDirectoryW(path, IntPtr.Zero))
                    throw new Win32Exception(Marshal.GetLastWin32Error(), $"Failed to create directory {directory}");
            }
        }

        /// <summary>
        /// Highlights a file or folder within Windows's explorer. Handles long paths unlike
        /// using the /select argument with explorer.exe.
        /// </summary>
        /// <param name="folder">Parent folder of the items.</param>
        /// <param name="items">Individual folder and file names to select in the containing folder.
        /// Can be empty if only wanting to show folder without selecting items.</param>
        public static void ShowItemsInExplorer(string folder, params string[] items)
        {
            try
            {
                Win32.SHParseDisplayName(folder, IntPtr.Zero, out IntPtr nativeFolder, 0, out uint psfgaoOut);

                if (nativeFolder == IntPtr.Zero)
                    return; // folder not found

                var nativeItems = new List<IntPtr>();
                foreach (var item in items)
                {
                    IntPtr nativeItem = IntPtr.Zero;
                    Win32.SHParseDisplayName(Combine(folder, item), IntPtr.Zero, out nativeItem, 0, out psfgaoOut);

                    if (nativeItem == IntPtr.Zero)
                        continue; // not found in folder

                    nativeItems.Add(nativeItem);
                }

                Win32.SHOpenFolderAndSelectItems(nativeFolder, (uint)nativeItems.Count, nativeItems.ToArray(), 0);

                Marshal.FreeCoTaskMem(nativeFolder);
                foreach (var item in nativeItems)
                {
                    Marshal.FreeCoTaskMem(item);
                }
            }
            catch(Exception ex)
            {
                Log.Exception(nameof(PathHelper), ex);
            }
        }

        /// <summary>
        /// Looks up UNC path of mapped drives
        /// </summary>
        /// <param name="netdrives">List of mapped drives</param>
        /// <returns>List of UNC paths</returns>
        private static List<string> UncPathsFromMappedDrives(List<string> netdrives)
        {
            string userRoot = "HKEY_CURRENT_USER";
            string subkey = "Network";
            string valueName = "RemotePath";

            return netdrives.Select(s =>
            {
                string keyName = userRoot + "\\" + subkey + "\\" + s.Split(":")[0];
                return Registry.GetValue(keyName, valueName, string.Empty).ToString();
            }).ToList();
        }

        /// <summary>
        /// Return UNC paths of users mapped network drives
        /// </summary>
        /// <param name="paths">populated with found paths</param>
        /// <returns>true on success</returns>
        public static bool GetMappedNetworkDrivePaths(ref string[] paths)
        {
            try
            {
                var netdrives = User.RunAsImpersonated(() =>
                {
                    string[] usersDrives = Environment.GetLogicalDrives();

                    return usersDrives.Where(drive =>
                        new DriveInfo(drive).DriveType == DriveType.Network).ToList();
                });

                // I wanted to return the drive letters - the scanner has access issues with those, RunAsImpersonated doesn't even solve them
                paths = UncPathsFromMappedDrives(netdrives).ToArray();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}