﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Utilizr.Conversion;
using Utilizr.Info;
using Utilizr.Logging;
using Utilizr.Windows;

namespace Utilizr.FileSystem
{
    public class DirectoryResult
    {
        public string Path { get; private set; }
        public string[] Directories { get; set; }
        public string[] Files { get; set; } 

        internal DirectoryResult(string path, string[] directories, string[] files)
        {
            this.Path = path;
            this.Directories = directories;
            this.Files = files;
        }
    }

    public delegate void OnError(string filePath, Exception error);

    public static class FileSystem
    {
        private delegate string[] directoryFiltererDelegate(string[] directories);
        private static readonly directoryFiltererDelegate directoryFilterer;

        /// <summary>
        /// For each directory in the directory tree rooted at the directoryPath given, yield a DirectoryResult which
        /// contains the current path and a string[] of files and a string[] of directories
        /// 
        /// When topDown is true, the caller can change the directories property of the resulting DirectoryResult,
        /// either by replacing it or by modifying it in place, and walk will only recurse into the subdirectories
        /// whose paths remain in the string[]. This can be used to prune the search, or to impose a specific order
        /// of visiting.  Modifying the directories property when topDown is false is ineffective.
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="topDown">When topDown is true or not specified, the result is yielded before the results of any 
        /// if its subdirectories (directories are generated top down). If topDown is false, the results are yielded
        /// after all results from subdirectories have been yielded (directories are generated bottom up).</param>
        /// <param name="onError">By default the errors from listing directories are ignored. If optional argument
        /// onError is supplied it will be called with the current folder path an instance of the Exception that occured.
        /// It can report the error to continue with the walk, or raise the exception to abort the walk.</param>
        /// <returns></returns>
        public static IEnumerable<DirectoryResult> Walk(string directoryPath, bool topDown = true, OnError onError = null)
        {
            return Walk(directoryPath, "*", topDown, onError);
        }

        public static IEnumerable<DirectoryResult> Walk(string directoryPath, string searchPattern, bool topDown = true,
            OnError onError = null)
        {
            
            string[] files;
            string[] directories;

            try
            {
                files = Directory.GetFiles(directoryPath, searchPattern);
                directories = Directory.GetDirectories(directoryPath);
            }
            catch (Exception error)
            {
                if (onError != null)
                    onError(directoryPath, error);
                yield break;
            }

            directories = directoryFilterer(directories);

            var directoryResult = new DirectoryResult(directoryPath, directories, files);

            if (topDown)
                yield return directoryResult;
            
            foreach (var subDirectoryResult in directoryResult.Directories.SelectMany(path => Walk(path, searchPattern, topDown, onError)))
            {
                yield return subDirectoryResult;
            }

            if (!topDown)
                yield return directoryResult;
        }



        static FileSystem()
        {
            //Fixes an obscure windows bug where Directory.GetDirectories recurses infinitely into folders containing just spaces
            if (Platform.IsWindows)
                directoryFilterer = directories => directories.Where(directory =>
                {
                    var dirName = Path.GetFileName(directory);
                    return dirName == null || dirName.Trim() != String.Empty;
                }).ToArray();
            else
                directoryFilterer = directories => directories;
        }

        private static OnError GetOnError(bool ignoreErrors = false, OnError onError = null)
        {
            if (ignoreErrors)
                return (ePath, err) => { };
            if (onError == null)
                return (ePath, err) => { throw err; };
            return onError;
        }

        /// <summary>
        /// Recursively delete a directory tree.
        /// 
        /// If ignoreErrors is set, errors are ignored; otherwise, if onError is set, it is called
        /// to handle the error with arguments (path, exception) where path where the error occured and exception is the 
        /// instance of the error exception that occured. If ignoreErrors is false and onError is null, any exceptions are thrown.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ignoreErrors"></param>
        /// <param name="onError"></param>
        public static void RemoveTree(string path, bool ignoreErrors = false, OnError onError = null)
        {
            onError = GetOnError(ignoreErrors, onError);

            ClearTree(path, ignoreErrors, onError);

            try
            {
                Directory.Delete(path);
            }
            catch (Exception err)
            {
                onError(path, err);
            }
        }

        /// <summary>
        /// Recursively deletes all folders/files in a directory, but leaves the directory path passed
        /// </summary>
        /// <param name="path"></param>
        /// <param name="ignoreErrors"></param>
        /// <param name="onError"></param>
        public static void ClearTree(string path, bool ignoreErrors = false, OnError onError = null)
        {
            onError = GetOnError(ignoreErrors, onError);

            foreach (var dirResult in Walk(path, false, onError))
            {
                foreach (var filePath in dirResult.Files)
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception err)
                    {
                        onError(filePath, err);
                    }
                }

                foreach (var folderPath in dirResult.Directories)
                {
                    try
                    {
                        Directory.Delete(folderPath);
                    }
                    catch (Exception err)
                    {
                        onError(folderPath, err);
                    }
                }
            } 
        }


        /// <summary>
        /// Recursive directory copy helper. NOTE: this method will not copy any .ds_store files
        /// </summary>
        /// <param name="sourceDirectory">Source directory.</param>
        /// <param name="destinationDirectory">Destination directory.</param>
        /// <param name="onlyCopyIfAlreadyExists">If set to <c>true</c> only copy if already exists.</param>
        /// <param name="recursive">If set to <c>true</c> recursive.</param>
        /// <param name="ignoreFolderNames">Ignore folder names.</param>
        public static void CopyDirectoryContents(
            string sourceDirectory, 
            string destinationDirectory, 
            bool onlyCopyIfAlreadyExists = false, 
            bool recursive = false, 
            params string[] ignoreFolderNames)
        {
            CopyDirectoryContents(
                sourceDirectory,
                destinationDirectory,
                onlyCopyIfAlreadyExists,
                recursive,
                ignoreFolderNames: ignoreFolderNames,
                ignoreFileExtensions: null
            );
        }

        /// <summary>
        /// Recursive directory copy helper. NOTE: this method will not copy any .ds_store files
        /// </summary>
        /// <param name="sourceDirectory">Source directory.</param>
        /// <param name="destinationDirectory">Destination directory.</param>
        /// <param name="onlyCopyIfAlreadyExists">If set to <c>true</c> only copy if already exists.</param>
        /// <param name="recursive">If set to <c>true</c> recursive.</param>
        /// <param name="ignoreFolderNames">Ignore folder names.</param>
        /// <param name="ignoreFileExtensions">File without a matching extension will not be copied.</param>
        public static void CopyDirectoryContents(
            string sourceDirectory,
            string destinationDirectory,
            bool onlyCopyIfAlreadyExists,
            bool recursive,
            string[] ignoreFolderNames,
            string[] ignoreFileExtensions)
        {
            if (!Directory.Exists(destinationDirectory))
                Directory.CreateDirectory(destinationDirectory);

            foreach (var file in Directory.GetFiles(sourceDirectory))
            {
                var fileExt = file.ToLower();
                if (fileExt.EndsWith(".ds_store") || ignoreFileExtensions?.Any(p => p.Equals(fileExt, StringComparison.OrdinalIgnoreCase)) == true)
                    continue;

                var destinationFile = Path.Combine(destinationDirectory, Path.GetFileName(file));

                if (onlyCopyIfAlreadyExists && !File.Exists(destinationFile))
                    continue;

                File.Copy(file, destinationFile, true);
            }

            if (!recursive)
                return;

            foreach (var dir in Directory.GetDirectories(sourceDirectory))
            {
                if (ignoreFolderNames?.Any(i => i.Equals(Path.GetFileName(dir), StringComparison.InvariantCultureIgnoreCase)) == true)
                    continue;

                var destination = Path.Combine(destinationDirectory, Path.GetFileName(dir));
                Directory.CreateDirectory(destination);
                CopyDirectoryContents(dir, destination, onlyCopyIfAlreadyExists, recursive);
            }
        }

        /// <summary>
        /// Calculates the total size of all files, and the number of all files within the given directory.
        /// Supports long file paths.
        /// </summary>
        /// <param name="sourceDirectory">Source directory.</param>
        /// <param name="fileCount">Variable that will contain the file count.</param>
        /// <param name="fileByteCount">Variable that will contain the sum of all file sizes.</param>
        /// <returns>Indicates whether one or more files were not included in the totals due to an error</returns>
        public static bool CalculateChildren(string sourceDirectory, out long fileCount, out long fileByteCount)
        {
            fileCount = 0;
            fileByteCount = 0;
            bool allFilesIncluded = true;

            var childDirs = new List<string>();
            var data = OpenPath(PathHelper.UncFullPath(sourceDirectory, "*"), out IntPtr hFile);

            do
            {
                if ((data.dwFileAttributes & Win32.FILE_ATTRIBUTE_DIRECTORY) != Win32.FILE_ATTRIBUTE_DIRECTORY)
                {
                    fileCount++;
                    fileByteCount += data.GetSize();
                }
                else if (data.cFileName != "." && data.cFileName != "..")
                {
                    childDirs.Add(Path.Combine(sourceDirectory, data.cFileName));
                }

            } while (Win32.FindNextFileW(hFile, out data));

            Win32.FindClose(hFile);

            foreach (var childDir in childDirs)
            {
                try
                {
                    // don't short circuit
                    allFilesIncluded = CalculateChildren(childDir, out long childDirFileCount, out long childDirFileByteCount) && allFilesIncluded;
                    fileCount += childDirFileCount;
                    fileByteCount += childDirFileByteCount;
                }
                catch (Exception ex)
                {
                    allFilesIncluded = false;

                    Log.Exception(
                        LoggingLevel.INFO, 
                        ex, 
                        $"Unable to include {sourceDirectory} in {nameof(fileCount)} and {nameof(fileByteCount)} totals."
                    );
                }
            }

            return allFilesIncluded;
        }

        /// <summary>
        /// Get FIND_DATAW struct returned from FindFirstFileW.
        /// Important to call <see cref="OpenPathFinished(IntPtr)"/> when done.
        /// </summary>
        public static Win32.WIN32_FIND_DATAW OpenPath(string path, out IntPtr handle)
        {
            var data = new Win32.WIN32_FIND_DATAW();
            handle = Win32.FindFirstFileW(PathHelper.UncFullPath(path), out data);

            if (handle == new IntPtr(-1))
            {
                var error = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                var win32 = new Win32Exception(error, $"Unable to open {path} with native error code {error}.");

                if (error == 5) // access denied
                    throw new UnauthorizedAccessException($"Unable to open {path}", win32);

                throw win32;
            }

            return data;
        }

        /// <summary>
        /// Close the handle from a path opened with <see cref="OpenPathFinished(IntPtr)"/>.
        /// </summary>
        /// <param name="handle"></param>
        public static void OpenPathFinished(IntPtr handle)
        {
            if (handle == new IntPtr(-1))
                return;

            Win32.FindClose(handle);
        }

        /// <summary>
        /// Enumerate child folders for the given directory supporting long paths.
        /// </summary>
        public static IEnumerable<string> GetChildFolders(string sourceDirectory, bool includeReparsePoints = false)
        {
            return GetChildPaths(sourceDirectory, true, includeReparsePoints);
        }

        /// <summary>
        /// Enumerate child files for the given directory supporting long paths.
        /// </summary>
        public static IEnumerable<string> GetChildFiles(string sourceDirectory, bool includeReparsePoints = false)
        {
            return GetChildPaths(sourceDirectory, false, includeReparsePoints);
        }

        static IEnumerable<string> GetChildPaths(string sourceDirectory, bool dirsOnly, bool includeReparsePoints)
        {
            var data = OpenPath(PathHelper.UncFullPath(sourceDirectory, "*"), out IntPtr handle);
            do
            {
                if (!includeReparsePoints && (data.dwFileAttributes & Win32.FILE_ATTRIBUTE_REPARSE_POINT) == Win32.FILE_ATTRIBUTE_REPARSE_POINT)
                    continue;

                bool isDir = (data.dwFileAttributes & Win32.FILE_ATTRIBUTE_DIRECTORY) == Win32.FILE_ATTRIBUTE_DIRECTORY;
                
                // avoid listening current and parent folder
                if (isDir && (data.cFileName.EndsWith(".") || data.cFileName.EndsWith("..")))
                    continue;

                if (isDir && dirsOnly) // dir
                    yield return Path.Combine(sourceDirectory, data.cFileName);

                if (!isDir && !dirsOnly) // file
                    yield return Path.Combine(sourceDirectory, data.cFileName);

            } while (Win32.FindNextFileW(handle, out data));

            OpenPathFinished(handle);
        }

        public static long GetSize(this Win32.WIN32_FIND_DATAW data)
        {
            const long maxDword = 0xffffffff;
            return (data.nFileSizeHigh * (maxDword + 1)) + data.nFileSizeLow;
        }

        public static string GetExtension(this Win32.WIN32_FIND_DATAW data)
        {
            return Path.GetExtension(data.cFileName);
        }

        public static DateTime ToDateTime(this FILETIME time, DateTimeKind kind = DateTimeKind.Utc)
        {
            long highBits = time.dwHighDateTime;
            highBits <<= 32;

            if (kind == DateTimeKind.Local)
                return DateTime.FromFileTime(highBits + (uint)time.dwLowDateTime);
            
            return DateTime.FromFileTimeUtc(highBits + (uint)time.dwLowDateTime);
        }

        public static int ToUnixTimestamp(this FILETIME time)
        {
            var dt = ToDateTime(time);
            return dt.ToUnixTimestamp();
        }

        public static FileAttributes ToFileAttributes(this int attrFlags)
        {
            var attrs = (FileAttributes)0;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_ARCHIVE) != 0)
                attrs |= FileAttributes.Archive;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_COMPRESSED) != 0)
                attrs |= FileAttributes.Compressed;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_DEVICE) != 0)
                attrs |= FileAttributes.Device;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_DIRECTORY) != 0)
                attrs |= FileAttributes.Directory;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_ENCRYPTED) != 0)
                attrs |= FileAttributes.Encrypted;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_HIDDEN) != 0)
                attrs |= FileAttributes.Hidden;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_NORMAL) != 0)
                attrs |= FileAttributes.Normal;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_NOT_CONTENT_INDEXED) != 0)
                attrs |= FileAttributes.NotContentIndexed;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_OFFLINE) != 0)
                attrs |= FileAttributes.Offline;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_READONLY) != 0)
                attrs |= FileAttributes.ReadOnly;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_REPARSE_POINT) != 0)
                attrs |= FileAttributes.ReparsePoint;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_SPARSE_FILE) != 0)
                attrs |= FileAttributes.SparseFile;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_SYSTEM) != 0)
                attrs |= FileAttributes.System;

            if ((attrFlags & Win32.FILE_ATTRIBUTE_TEMPORARY) != 0)
                attrs |= FileAttributes.Temporary;

            return attrs;            
        }
    }
}