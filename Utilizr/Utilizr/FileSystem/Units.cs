﻿
namespace Utilizr.FileSystem
{
    public static class Units
    {
        public const long KiB = 1024;
        public const long MiB = KiB * 1024;
        public const long GiB = MiB * 1024;
        public const long TiB = GiB * 1024;
        public const long PiB = TiB * 1024;
        public const long EiB = PiB * 1024;

        public const long KB = 1000;
        public const long MB = KB * 1000;
        public const long GB = MB * 1000;
        public const long TB = GB * 1000;
        public const long PB = TB * 1000;
        public const long EB = PB * 1000;
    }
}
