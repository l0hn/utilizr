﻿using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using System;
using Utilizr.Logging;

namespace Utilizr.FileSystem.IncludeExclude
{
	public class FileTreeStatePersistence : IDisposable, ICloneable
	{
		private const string LogCat = "INCLUDEEXCLUDE";

		public FileTreeCheckRestrictionCollection CheckRestrictions { get; set; }
		private bool _changesMade;

		public bool changesMade
		{
			get { return _changesMade; }
			set { _changesMade = value; }
		}

		private Dictionary<string, bool> _includeFolders;
		private Dictionary<string, bool> _includeFiles;
		private Dictionary<string, bool> _excludeFolders;
		private Dictionary<string, bool> _excludeFiles;

		public List<string> IncludeDirs()
		{
			return _includeFolders.Keys.ToList();
		}

		public List<string> IncludeFiles()
		{
			return _includeFiles.Keys.ToList();
		}

		public List<string> ExcludeDirs()
		{
			return _excludeFolders.Keys.ToList();
		}

		public List<string> ExcludeFiles()
		{
			return _excludeFiles.Keys.ToList();
		}

		public void insertExcludeDir(string path)
		{
			_excludeFolders[SanitizePath(path)] = true;
		}

		public void insertIncludeDir(string path)
		{
			_includeFolders[SanitizePath(path)] = true;
		}

		public void insertExcludeFile(string path)
		{
			_excludeFiles[SanitizePath(path)] = true;
		}

		public void insertIncludeFile(string path)
		{
			_includeFiles[SanitizePath(path)] = true;
		}

		public FileTreeStatePersistence()
		{
			CheckRestrictions = new FileTreeCheckRestrictionCollection();
			CheckRestrictions.CheckRestrictionChanged += CheckRestrictions_CheckRestrictionChanged;
			_includeFiles = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
			_includeFolders = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
			_excludeFiles = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
			_excludeFolders = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
		}

		public void addAutoDefaultPaths()
		{
			setDirCheckState(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), true);
			setDirCheckState(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), true);
			setDirCheckState(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), true);
			//string envVar = SystemInfo.isXPOrLower() ? "%allusersprofile%" : "%public%";
			//statePersistence.setDirCheckState(Environment.ExpandEnvironmentVariables(envVar), true);

			//if (Settings.IsFreeAccount)
			//{
			setDirCheckState(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), true);

			string userProfile = Environment.ExpandEnvironmentVariables("%userprofile%");
			addlDirIfExists(userProfile, "Videos");
			addlDirIfExists(userProfile, "Downloads");
			//}
		}

		private void addlDirIfExists(string userProfilePath, string folder)
		{
			string filePath = Path.Combine(userProfilePath, folder);
			if (Directory.Exists(filePath))
			{
				setDirCheckState(filePath, true);
			}
		}

		public string debugString()
		{
			var dict = new Dictionary<string, object>();
			dict["include_files"] = IncludeFiles();
			dict["include_folders"] = IncludeDirs();
			dict["exclude_files"] = ExcludeFiles();
			dict["exclude_folders"] = ExcludeDirs();
			return JsonConvert.SerializeObject(dict, Formatting.Indented);
		}

		private void CheckRestrictions_CheckRestrictionChanged(object sender, EventArgs e)
		{
			preProcessCheckRestrictions();
		}


		public CheckState getFileCheckState(string file)
		{
			if (getFileState(file))
				return CheckState.CHECKED;

			return CheckState.UNCHECKED;
		}

		//public System.Windows.Forms.CheckState winFormsCheckStateEquivalent(CheckState state)
		//{
		//	switch (state)
		//	{
		//		case CheckState.CHECKED:
		//			return System.Windows.Forms.CheckState.Checked;
		//		case CheckState.INTERMEDIATE:
		//			return System.Windows.Forms.CheckState.Indeterminate;
		//		default:
		//			return System.Windows.Forms.CheckState.Unchecked;
		//	}
		//}

		public bool getFileState(string file)
		{
			file = SanitizePath(file);
			if (string.IsNullOrEmpty(file))
			{
				return false;
			}
			if (_includeFiles.ContainsKey(file))
			{
				return true;
			}
			if (_excludeFiles.ContainsKey(file))
			{
				return false;
			}
			string fileDir = getDirFromFilePath(file);
			if (_includeFolders.ContainsKey(fileDir))
			{
				return true;
			}
			if (_excludeFolders.ContainsKey(fileDir))
			{
				return false;
			}
			if (dirNearestParentInclusionState(fileDir) == NearestParentInclusionState.INCLUDED)
			{
				return true;
			}
			if (dirNearestParentInclusionState(fileDir) == NearestParentInclusionState.EXCLUDED)
			{
				return false;
			}
			return false;
		}

		public void setFileCheckState(string filePath, bool checkState)
		{
			filePath = SanitizePath(filePath);
			if (string.IsNullOrEmpty(filePath))
			{
				return;
			}
			foreach (IFileTreeCheckRestriction restriction in CheckRestrictions)
			{
				if (!restriction.isFileCheckAllowed(filePath))
				{
					Log.Info(LogCat, string.Format("{0} restricted filepath {1}", restriction.GetType(), filePath));
					checkState = false;
				}
			}
			_changesMade = true;
			_includeFiles.Remove(filePath);
			_excludeFiles.Remove(filePath);
			string fileDir = getDirFromFilePath(filePath);
			if (checkState)
			{
				if (_excludeFolders.ContainsKey(fileDir) ||
					(!_includeFolders.ContainsKey(fileDir) &&
					 dirNearestParentInclusionState(filePath) != NearestParentInclusionState.INCLUDED))
				{
					_includeFiles[filePath] = true;
				}
			}
			else
			{
				if (!_excludeFolders.ContainsKey(fileDir) &&
					dirNearestParentInclusionState(filePath) == NearestParentInclusionState.INCLUDED)
				{
					_excludeFiles[filePath] = true;
				}
			}
		}

		public CheckState getDirCheckState(string dir)
		{
			dir = SanitizePath(dir);
			try
			{
				if (string.IsNullOrEmpty(dir))
				{
					return CheckState.UNCHECKED;
				}

#if !MONO
				//windows check for drive letter
				if (dir.EndsWith(":"))
					dir += @"\";
#endif

				if (_includeFolders.ContainsKey(dir) && !dirContainsExcludedChild(dir))
				{
					return CheckState.CHECKED;
				}
				if (_includeFolders.ContainsKey(dir) && dirContainsExcludedChild(dir))
				{
					return CheckState.INTERMEDIATE;
				}
				if (!_excludeFolders.ContainsKey(dir) &&
					dirNearestParentInclusionState(dir) == NearestParentInclusionState.INCLUDED &&
					!dirContainsExcludedChild(dir))
				{
					return CheckState.CHECKED;
				}
				if (!_excludeFolders.ContainsKey(dir) &&
					dirNearestParentInclusionState(dir) == NearestParentInclusionState.INCLUDED &&
					dirContainsExcludedChild(dir))
				{
					return CheckState.INTERMEDIATE;
				}
				if (!_excludeFiles.ContainsKey(dir) && dirContainsIncludedChild(dir))
				{
					return CheckState.INTERMEDIATE;
				}
			}
			catch (Exception ex)
			{
				Log.Exception(LogCat, ex);
			}

			return CheckState.UNCHECKED;
		}

		public void setDirCheckState(string path, bool checkState)
		{
			path = SanitizePath(path);

			if (string.IsNullOrEmpty(path))
			{
				return;
			}
			foreach (IFileTreeCheckRestriction restriction in CheckRestrictions)
			{
				if (!restriction.isDirectoryAllowed(path))
				{
					Log.Info(LogCat, string.Format("{0} restricted dirPath {1}", restriction.GetType(), path));
					checkState = false;
				}
			}

#if !MONO
			//windows check for drive letter
			if (path.EndsWith(":"))
				path += @"\";
#endif

			_changesMade = true;
			if (checkState)
			{
				_excludeFolders.Remove(path);
				removeChildExclusions(path);
				if (dirNearestParentInclusionState(path) != NearestParentInclusionState.INCLUDED &&
					!_includeFolders.ContainsKey(path))
				{
					_includeFolders[path] = true;
				}
			}
			else
			{
				_includeFolders.Remove(path);
				removeChildInclusions(path);
				if (dirNearestParentInclusionState(path) == NearestParentInclusionState.INCLUDED &&
					!_excludeFolders.ContainsKey(path))
				{
					_excludeFolders[path] = true;
				}
			}
		}

		private void removeChildInclusions(string dir)
		{
			List<string> remItems = new List<string>();
			foreach (string incFile in _includeFiles.Keys)
			{
				if (System.IO.Path.GetDirectoryName(incFile).Contains(dir) &&
					incFile != dir)
				{
					remItems.Add(incFile);
				}
			}
			foreach (string remItem in remItems)
			{
				_includeFiles.Remove(remItem);
			}

			removeDirChildren(dir, _includeFolders);
		}

		private void removeChildExclusions(string dir)
		{
			List<string> remItems = new List<string>();
			foreach (string exFile in _excludeFiles.Keys)
			{
				if (System.IO.Path.GetDirectoryName(exFile).Contains(dir) /*&& Path.GetDirectoryName(exFile) != dir*/)
				{
					remItems.Add(exFile);
				}
			}
			foreach (string remItem in remItems)
			{
				_excludeFiles.Remove(remItem);
			}

			removeDirChildren(dir, _excludeFolders);
		}

		private NearestParentInclusionState dirNearestParentInclusionState(string dir)
		{
			string nearestExcluded = dirNearestExcludedParent(dir);
			string nearestIncluded = dirNearestIncludedParent(dir);

			if (string.IsNullOrEmpty(nearestExcluded) && string.IsNullOrEmpty(nearestIncluded))
			{
				return NearestParentInclusionState.NONE;
			}
			else if (nearestIncluded.Length > nearestExcluded.Length)
			{
				return NearestParentInclusionState.INCLUDED;
			}
			else if (nearestIncluded.Length < nearestExcluded.Length)
			{
				return NearestParentInclusionState.EXCLUDED;
			}
			return NearestParentInclusionState.NONE;
		}

		private string dirNearestIncludedParent(string dir)
		{
			string includedParent = "";
			string dirFromFilePath = getDirFromFilePath(dir);
			if (_includeFolders.ContainsKey(dirFromFilePath))
			{
				includedParent = dirFromFilePath;
				return includedParent;
			}

			if (!string.IsNullOrEmpty(dirFromFilePath))
			{
				includedParent = dirNearestIncludedParent(dirFromFilePath);
			}
			return includedParent;
		}

		private string dirNearestExcludedParent(string dir)
		{
			string excludedParent = "";
			string dirFromFilePath = getDirFromFilePath(dir);
			if (_excludeFolders.ContainsKey(dirFromFilePath))
			{
				excludedParent = dirFromFilePath;
				return excludedParent;
			}

			if (!string.IsNullOrEmpty(dirFromFilePath))
			{
				excludedParent = dirNearestExcludedParent(dirFromFilePath);
			}
			return excludedParent;
		}

		private bool dirContainsIncludedChild(string dir)
		{
			if (dirContainsIncludedDir(dir) || dirContainsIncludedFile(dir))
			{
				return true;
			}
			return false;
		}

		private void removeDirChildren(string parentDir, Dictionary<string, bool> dictToRemove)
		{
			List<string> remItems = new List<string>();
			foreach (string listDir in dictToRemove.Keys)
			{
				string[] incParts = listDir.Split(new char[] { Path.DirectorySeparatorChar });
				string[] dirParts = parentDir.Split(new char[] { Path.DirectorySeparatorChar });

				int partCount = dirParts.Length > incParts.Length ? incParts.Length : dirParts.Length;
				string incCombiner = "";
				string dirCombiner = "";
				for (int i = 0; i < partCount; i++)
				{

					incCombiner += incParts[i] + Path.DirectorySeparatorChar;
					dirCombiner += dirParts[i] + Path.DirectorySeparatorChar;

					if (dirParts[i] == Path.GetFileName(parentDir)
						&& dirCombiner == incCombiner)
					{
						remItems.Add(listDir);
					}
				}
				if (parentDir.Length == 3 && listDir.Contains(parentDir))
				{
					remItems.Add(listDir);
				}
			}

			foreach (string remItem in remItems)
			{
				dictToRemove.Remove(remItem);
			}
		}

		private bool dirContainsIncludedDir(string dir)
		{
			foreach (string incFolder in _includeFolders.Keys)
			{
				string[] incParts = incFolder.Split(new char[] { Path.DirectorySeparatorChar });
				string[] dirParts = dir.Split(new char[] { Path.DirectorySeparatorChar });
				int partCount = dirParts.Length > incParts.Length ? incParts.Length : dirParts.Length;
				string incCombiner = "";
				string dirCombiner = "";
				for (int i = 0; i < partCount; i++)
				{
					incCombiner += incParts[i] + Path.DirectorySeparatorChar;
					dirCombiner += dirParts[i] + Path.DirectorySeparatorChar;
					if (dirParts[i] == Path.GetFileName(dir)
						&& dirCombiner == incCombiner)
					{
						return true;
					}
				}
				if (dir.Length == 3 && incFolder.Contains(dir))
				{
					return true;
				}
			}
			return false;
		}

		private bool dirContainsIncludedFile(string checkingDir)
		{
			foreach (string includedFile in _includeFiles.Keys)
			{
				char[] separator = new char[] { Path.DirectorySeparatorChar };
				string includedFileDir = Path.GetDirectoryName(includedFile);
				string[] checkingDirParts = checkingDir.Trim(separator).Split(Path.DirectorySeparatorChar);
				string[] includedFileDirParts = includedFileDir.Trim(separator).Split(Path.DirectorySeparatorChar);

				if (checkingDirParts.Length > includedFileDirParts.Length)
					continue;

				bool foldersInPathMatch = true;
				for (int i = 0; i < checkingDirParts.Length; i++)
				{
					if (SanitizePath(checkingDirParts[i]) != SanitizePath(includedFileDirParts[i]))
					{
						foldersInPathMatch = false;
						break;
					}
				}

				if (!foldersInPathMatch)
					continue;

				return true;
			}
			return false;
		}

		private bool dirContainsExcludedChild(string dir)
		{
			if (dirContainsExcludedChildDir(dir) || dirContainsExcludedChildFile(dir))
			{
				return true;
			}
			return false;
		}

		private bool dirContainsExcludedChildDir(string dir)
		{
#if MONO
			//add trailing path separator for folders other than root (don't want '//')
			if (dir.Length > 1)
			{
				dir = string.Concat(dir, Path.DirectorySeparatorChar);
			}
#endif

			foreach (string exDir in _excludeFolders.Keys)
			{
				if (exDir.Contains(dir) && exDir != dir)
				{
					return true;
				}
			}
			return false;
		}

		private bool dirContainsExcludedChildFile(string dir)
		{
			foreach (string exFile in _excludeFiles.Keys)
			{
				if (System.IO.Path.GetDirectoryName(exFile).Contains(dir))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Gets the directory that contains a file or the parent directory of a directory
		/// </summary>
		/// <param name="filePath">path to a file or directory</param>
		/// <returns>path to directory, or empty string if root drive is supplied in filePath</returns>
		private string getDirFromFilePath(string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
			{
				return "";
			}

#if MONO
			if (filePath.Length <= 1)
			{
				return "";
			}
			int lastIndex = filePath.LastIndexOf(Path.DirectorySeparatorChar);
			if (lastIndex == 0)
			{
				//root folder
				return Path.DirectorySeparatorChar.ToString();
			}
			else if (lastIndex < 0)
			{
				//not found
				return "";
			}
#else
			if (filePath.Length <= 3)
			{
				return "";
			}
			int lastIndex = filePath.LastIndexOf(@"\");
			if (lastIndex < 1)
			{
				lastIndex = 0;
			}
#endif

			string retString = filePath.Substring(0, lastIndex);

#if !MONO
			//windows check for drive letter
			if (retString.EndsWith(@":"))
			{
				retString += @"\";
			}
#endif

			return retString;
		}

		public void Dispose()
		{
			this._excludeFiles = null;
			this._excludeFolders = null;
			this._includeFiles = null;
			this._includeFolders = null;
		}

		public void printDebugData()
		{
			//prints file and folder lists
			Debug.WriteLine(
				string.Format("CheckFolderCount: {0}, CheckFileCount: {1}, ExcludeFolders: {2}, ExcludeFiles: {3}",
					_includeFolders.Count,
					_includeFiles.Count,
					_excludeFolders.Count,
					_excludeFiles.Count));

			Debug.WriteLine("---------CHECKED FOLDERS----------");
			foreach (string str in _includeFolders.Keys)
			{
				Debug.WriteLine(str);
			}
			Debug.WriteLine("---------CHECKED FILES----------");
			foreach (string str in _includeFiles.Keys)
			{
				Debug.WriteLine(str);
			}
			Debug.WriteLine("---------EXCLUDED FOLDERS----------");
			foreach (string str in _excludeFolders.Keys)
			{
				Debug.WriteLine(str);
			}
			Debug.WriteLine("---------EXCLUDED FILES----------");
			foreach (string str in _excludeFiles.Keys)
			{
				Debug.WriteLine(str);
			}
		}

		/// <summary>
		/// processes all check restrictions on each included file/folder
		/// Will remove all entries that fail restriction check
		/// NOTE: you only need to run this once after initialization if using
		/// </summary>
		public void preProcessCheckRestrictions()
		{
			try
			{
				foreach (string includedDir in _includeFolders.Keys.Select(i => SanitizePath(i)))
				{
					if (!CheckRestrictions.isDirAllowed(includedDir))
					{
						setDirCheckState(includedDir, false);
					}
				}
				foreach (string includeFile in _includeFiles.Keys.Select(i => SanitizePath(i)))
				{
					setFileCheckState(includeFile, true);
				}
			}
			catch (System.Exception ex)
			{
				Log.Exception(LogCat, ex);
			}
		}

		void IDisposable.Dispose()
		{
			CheckRestrictions.Dispose();
		}

		/// <summary>
		/// clones the include/exclude file/folders. Useful for disposing current object
		/// </summary>
		/// <returns></returns>
		public object Clone()
		{
			FileTreeStatePersistence p = new FileTreeStatePersistence();
			p._includeFiles = _includeFiles;
			p._excludeFiles = _excludeFiles;
			p._includeFolders = _includeFolders;
			p._excludeFolders = _excludeFolders;
			return p;
		}

		private static string SanitizePath(string input)
		{
#if MONO
			return input;
#else
			return input?.ToLower();
#endif
		}

	}

	public enum CheckState
	{
		UNCHECKED = -1,
		CHECKED = 0,
		INTERMEDIATE = 1
	}

	public enum NearestParentInclusionState
	{
		INCLUDED = 1,
		EXCLUDED = 2,
		NONE = 3
	}
}
