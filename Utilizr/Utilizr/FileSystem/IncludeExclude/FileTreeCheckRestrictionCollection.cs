﻿using System;
using System.Collections.Generic;
using Utilizr.Logging;

namespace Utilizr.FileSystem.IncludeExclude
{

	public interface IFileTreeCheckRestriction : IDisposable
	{
		bool isFileCheckAllowed(string filePath);
		bool isDirectoryAllowed(string dirPath);
		event EventHandler RestrictionSettingsChanged;
	}

	public class FileTreeCheckRestrictionCollection : List<IFileTreeCheckRestriction>, IDisposable
	{
		private const string LogCat = "RESTRICTIONS";

		public event EventHandler CheckRestrictionChanged;

		public void Dispose()
		{
			foreach (IFileTreeCheckRestriction restriction in this)
			{
				restriction.Dispose();
				restriction.RestrictionSettingsChanged += restriction_RestrictionSettingsChanged;
			}
		}

		void restriction_RestrictionSettingsChanged(object sender, EventArgs e)
		{
			OnCheckRestrictionChanged();
		}

		protected virtual void OnCheckRestrictionChanged()
		{
			if (CheckRestrictionChanged != null)
			{
				CheckRestrictionChanged(this, new EventArgs());
			}
		}

		public bool isDirAllowed(string dir)
		{
			foreach (IFileTreeCheckRestriction restriction in this)
			{
				if (!restriction.isDirectoryAllowed(dir))
				{
					Log.Info(LogCat, string.Format("{0} restricted dir {1}", restriction.GetType(), dir));
					return false;
				}
			}
			return true;
		}

		public bool isFileAllowed(string file)
		{
			foreach (IFileTreeCheckRestriction restriction in this)
			{
				if (!restriction.isFileCheckAllowed(file))
				{
					Log.Info(LogCat, string.Format("{0} restricted file {1}", restriction.GetType(), file));
					return false;
				}
			}
			return true;
		}
	}
}