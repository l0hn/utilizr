﻿using System.Text;

namespace Utilizr.Conversion
{
    public static class Hex
    {
        public static string ToHexString(this byte[] digest)
        {
            StringBuilder strTemp = new StringBuilder(digest.Length * 2);
            foreach (byte b in digest)
            {
                strTemp.Append(b.ToString("X02"));
            }
            return strTemp.ToString();
        }
    }
}
