﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilizr.Conversion
{
    public static class ObjectToDictionary
    {
        public static Dictionary<String, object> Convert<T>(T value) where T : class
        {
            var fields = typeof(T).GetFields();
            var properties = typeof(T).GetProperties();

            var fieldsDict = fields.ToDictionary(x => x.Name, x => x.GetValue(value));
            var propertiesDict = properties.ToDictionary(x => x.Name, x => x.GetValue(value, null));

            return fieldsDict.Union(propertiesDict).ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
