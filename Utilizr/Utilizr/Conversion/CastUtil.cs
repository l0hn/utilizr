﻿using System;
using Utilizr.Logging;

namespace Utilizr.Conversion
{
    /// <summary>
    /// Helper methods to re-use common code for user-defined conversions
    /// </summary>
    public static class CastUtil
    {
        /// <summary>
        /// Creates an instance of <see cref="T1"/> setting all public properties specified in <see cref="T3"/> using the source object <see cref="T2"/>.
        /// </summary>
        /// <typeparam name="T1">Type of cast destination</typeparam>
        /// <typeparam name="T2">Type of cast source</typeparam>
        /// <typeparam name="T3">Type of interface that <see cref="(T1)"/> and <see cref="T2"/> must implement</typeparam>
        /// <param name="sourceValuesObj">Object whose values will be used when creating a new instance</param>
        /// <returns></returns>
        public static T1 InterfaceProperties<T1, T2, T3>(T2 sourceValuesObj)
            where T1 : T3
            where T2 : T3
        {
            var destinationType = typeof(T1);
            var destinationObj = Activator.CreateInstance<T1>();

            var iProperties = typeof(T3).GetProperties();
            foreach (var iProperty in iProperties)
            {
                try
                {
                    var propInfo = destinationType.GetProperty(iProperty.Name);
                    if (propInfo.SetMethod == null)
                    {
#if DEBUG
                        Log.Info(nameof(CastUtil), $"No set method for interface property '{propInfo.Name}'");
#endif
                        continue;
                    }

                    var sourceValue = iProperty.GetValue(sourceValuesObj);
                    propInfo.SetValue(destinationObj, sourceValue);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, $"Error when attempting to set {iProperty?.Name}");
#if DEBUG
                    throw;
#endif
                }
            }

            return destinationObj;
        }
    }
}