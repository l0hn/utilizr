﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Utilizr.Conversion
{
    public static class TypeExtensions
    {
        public static bool IsConvertibleFrom<T>(this Type t)
        {
            TypeConverter converter = t.GetConverter();
            if (converter != null && converter.CanConvertFrom(typeof(T)))
            {
                return true;
            }
            return false;
        }

        public static TypeConverter GetConverter(this Type t)
        {
            return TypeDescriptor.GetConverter(t);
        }

        public static T ConvertTo<T>(this object o)
        {
            TypeConverter typeConverter = GetConverter(typeof(T));
            return (T)typeConverter.ConvertFromString(o.ToString());
        }

        public static bool In<T>(this T t, params T[] values)
        {
            return values.Contains(t);
        }
    }
}
