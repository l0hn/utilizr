﻿namespace Utilizr.Conversion
{
    public static class StringParseExtensions
    {
        /// <summary>
        /// Attempt to parse as integer, returns 0 if unable to parse
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int TryParseInt(this string value)
        {
            int val = 0;
            int.TryParse(value, out val);
            return val;
        }
    }
}
