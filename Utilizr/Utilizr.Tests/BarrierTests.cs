﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using Utilizr.Async;
using Barrier = Utilizr.Async.Barrier;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("Barrier Tests")]
    class BarrierTests
    {
        private Barrier _sync;
        private bool _throw;

        private long _flewAway;
        private long _chopperFailures;

        [SetUp]
        public void Setup()
        {
            _flewAway = 0;
            _chopperFailures = 0;

            _sync?.Dispose();
            _sync = new Barrier(0, () =>
            {
                if (_throw)
                {
                    throw new Exception("Chopper failure");
                }
                Console.WriteLine("Chopper is taking off");
            });
        }


        [Test]
        [TestCase(10)]
        public void ThrowsExceptions(int iterations)
        {
            _throw = true;
            Run(iterations);
            Assert.AreEqual(iterations * 3, _chopperFailures);
            Assert.AreEqual(0, _flewAway);
        }

        [Test]
        [TestCase(10)]
        public void Completes(int iterations)
        {
            _throw = false;
            Run(iterations);
            Assert.AreEqual(iterations * 3, _flewAway);
            Assert.AreEqual(0, _chopperFailures);
        }

        [Test]
        public void RemoveParticipantThrows()
        {
            using (var _sync = new Barrier(3, () => throw new Exception("post phase")))
            {
                _sync.RemoveParticipant();

                var asyncResult = AsyncHelper.BeginExecute(() => _sync.SignalAndWait());

                Assert.Throws<Exception>(() => _sync.RemoveParticipant());
                Assert.Throws<Exception>(() => AsyncHelper.EndExecute(asyncResult));
            }
        }

        [Test]
        public void RemoveParticipant()
        {
            var postPhaseRan = false;
            using (var _sync = new Barrier(3, () => postPhaseRan = true))
            {
                _sync.RemoveParticipant();

                var asyncResult = AsyncHelper.BeginExecute(() => _sync.SignalAndWait());

                _sync.RemoveParticipant();
                AsyncHelper.EndExecute(asyncResult);
            }
            Assert.True(postPhaseRan);
        }

        void Run(int iterations)
        {
            var threads = new List<Thread>();

            Random random = new Random(Environment.TickCount);

            for (int i = 0; i < iterations; i++)
            {
                var arnie = new Thread(() =>
                {
                    GetToTheChopper("Arnie", TimeSpan.FromMilliseconds(random.Next(10, 1000)));
                });
                arnie.Start();

                var stalone = new Thread(() =>
                {
                    GetToTheChopper("Stalone", TimeSpan.FromMilliseconds(random.Next(10, 1000)));
                });
                stalone.Start();

                var lundgren = new Thread(() =>
                {
                    GetToTheChopper("Lundgren", TimeSpan.FromMilliseconds(random.Next(10, 1000)));
                });
                lundgren.Start();

                threads.AddRange(new[] { arnie, stalone, lundgren });
                Thread.Sleep(random.Next(100, 2000));
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
        }

        void GetToTheChopper(string name, TimeSpan timeToGetToChopper)
        {
            _sync.AddParticipant();
            Console.WriteLine($"[{name}] Shot some baddies");
            Thread.Sleep(timeToGetToChopper);
            Console.WriteLine($"[{name}] Got to the chopper");

            try
            {
                _sync.SignalAndWait();
            }
            catch (Exception ex)
            {
                Interlocked.Increment(ref _chopperFailures);
                Console.WriteLine($"[{name}] couldn't fly in the chopper");
                return;
            }

            Interlocked.Increment(ref _flewAway);
            Console.WriteLine($"[{name}] Flew way in the chopper");
        }
    }
}