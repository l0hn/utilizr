﻿using NUnit.Framework;
using System;
using System.Threading;
using Utilizr.Scheduling;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("Scheduling")]    
    public class ScheduleTests
    {
        [Test]
        public void ScheduleTest()
        {
            ManualResetEvent done = new ManualResetEvent(false);

            ScheduleManager manager = new ScheduleManager();
            manager.LoadSchedule();
            manager.ScheduledEventDue += (s) => done.Set();
            manager.Add(ScheduledEvent.CreateHourly("hourlyTest", DateTime.UtcNow.Minute + 1));
            manager.Add(ScheduledEvent.CreateDaily("dailyTest", DateTime.UtcNow.Hour, DateTime.UtcNow.Minute + 2));
            manager.Add(ScheduledEvent.CreateWeekly("weeklyTest", DateTime.UtcNow.Hour, DateTime.UtcNow.Minute + 10, DaysOfWeek.Tuesday | DaysOfWeek.Thursday));
            manager.SaveAll();
            done.WaitOne(1 * 60 * 1000, false);
        }

    }
}
