﻿using NUnit.Framework;
using System;
using System.IO;
using System.Text;
using Utilizr.Conversion;

namespace Utilizr.Tests
{
    public static class TestHelpers
    {
        const int BLOCK_SIZE = 4 << 10;

        public static void CreateFile(string path, int size)
        {
            using (FileStream stream = File.Open(path, FileMode.Create))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                var blocks = size/BLOCK_SIZE;
                if (blocks > 0)
                {
                    var block = Encoding.ASCII.GetBytes("".PadRight(BLOCK_SIZE, 'T'));
                    for (var i = 0; i < blocks; i++)
                        writer.Write(block);
                }

                for (var i = 0; i < size % BLOCK_SIZE; i++)
                    writer.Write('T');
            }
        }

        public static void CreateDirectories(string path, int breadth, int depth, Action<string> folderAction)
        {
            Directory.CreateDirectory(path);

            for (int i=0; i<breadth; i++)
            {
                if (depth > 0)
                    CreateDirectories(Path.Combine(path, i.ToString()), breadth, depth - 1, folderAction);
            }

            folderAction(path);
        }
    }

    [TestFixture]
    [Category("FileSystem")]
    public class FileSystemTests
    {
        public readonly string baseFolder = Path.Combine(Path.GetTempPath(), "BaseTestFolder" + DateTime.Now.ToUnixTimestamp());

        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 5)]
        [TestCase(5, 1)]
        [TestCase(3, 3)]
        public void RemoveEmptyTree(int breadth, int depth)
        {
            TestHelpers.CreateDirectories(baseFolder, breadth, depth, s => {});

            Assert.True(Directory.Exists(baseFolder));

            Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder);

            Assert.False(Directory.Exists(baseFolder));
        }

        [Test]
        [TestCase(0, 0, 5)]
        [TestCase(3, 3, 3)]
        [TestCase(1, 1, 10)]
        [TestCase(1, 5, 2)]
        [TestCase(5, 1, 2)]
        public void RemoveTree(int breadth, int depth, int filesPerFolder)
        {
            Assert.Greater(filesPerFolder, 0); //Just some validation

            TestHelpers.CreateDirectories(baseFolder, breadth, depth, path =>
            {
                for (var i=0; i<filesPerFolder; i++)
                {
                    var filePath = Path.Combine(path, i + ".file");
                    TestHelpers.CreateFile(filePath, 10);
                }
            });

            Assert.True(Directory.Exists(baseFolder));

            Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder);

            Assert.False(Directory.Exists(baseFolder));
        }

        [Test]
        public void TestIgnoreErrors()
        {
            if (!Utilizr.Info.Platform.IsWindows)
            {
                return;
            }
            FileStream fileHandle = null;
            string filePath = null;

            TestHelpers.CreateDirectories(baseFolder, 0, 0, path =>
            {
                filePath = Path.Combine(path, "LockedFile");
                TestHelpers.CreateFile(filePath, 100);
                fileHandle = File.Open(filePath, FileMode.Open);
            });

            Assert.True(Directory.Exists(baseFolder));

            Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder, true);

            Assert.True(Directory.Exists(baseFolder));

            Assert.Throws<IOException>(() => Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder));

            Assert.True(Directory.Exists(baseFolder));

            bool worked = false;
            Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder, onError: (path, error) =>
            {
                if (path == filePath && error is IOException)
                    worked = true;
            });

            Assert.True(worked);
            Assert.True(Directory.Exists(baseFolder));

            if (fileHandle != null)
                fileHandle.Dispose();

            Utilizr.FileSystem.FileSystem.RemoveTree(baseFolder);

            Assert.False(Directory.Exists(baseFolder));
        }
    }
}