﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Utilizr.Async;
using Utilizr.Queue;
using Utilizr.Info;

namespace Utilizr.Tests
{
    public class TestItem
    {
        public int Value { get; set; }

        public TestItem(int value)
        {
            Value = value;
        }

        public override bool Equals(object other)
        {
            var toCompare = other as TestItem;
            if (toCompare == null)
                return false;

            return Value == toCompare.Value;
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }

    abstract class CommonFileQueueTests
    {
        protected abstract IFileQueue<T> GetQueueObject<T>();
        protected abstract IFileQueue<T> GetQueueObject<T>(int bufferCapacity);

        [Test]
        public void SimpleQueueTest()
        {
            IFileQueue<TestItem> queue = GetQueueObject<TestItem>();

            Assert.True(queue.IsEmpty);

            List<TestItem> items = new List<TestItem> { new TestItem(1), new TestItem(2), new TestItem(3) };

            foreach (TestItem item in items)
            {
                queue.Enqueue(item);
            }

            Assert.AreEqual(items.Count, queue.Count);

            List<TestItem> returnedItems = new List<TestItem> { queue.Dequeue(), queue.Dequeue(), queue.Dequeue() };

            Assert.True(queue.IsEmpty);

            Assert.AreEqual(items.ToArray(), returnedItems.ToArray());
        }

        [OneTimeSetUp]
        public void FixtureSetup()
        {
            AppInfo.AppName = "Utilizr.Tests";
        }

        [Test]
        public void MediumQueueTest()
        {
            IFileQueue<TestItem> queue = GetQueueObject<TestItem>();

            int stackHeight = 5;

            for (int i = 0; i < stackHeight; i++)
                queue.Enqueue(new TestItem(i));

            Assert.AreEqual(stackHeight, queue.Count);

            for (int i = 0; i < 10; i++)
            {
                queue.Enqueue(new TestItem(i));
                queue.Dequeue();
                Assert.AreEqual(stackHeight, queue.Count);
            }

            for (int i = 0; i < stackHeight; i++)
                queue.Dequeue();

            Assert.True(queue.IsEmpty);
        }

        [Test]
        public void TestEmpty()
        {
            IFileQueue<TestItem> queue = GetQueueObject<TestItem>();

            int j;

            for (int i = 1; i < 6; i++)
            {
                TestItem item = new TestItem(110);

                for (j = 1; j <= i; j++)
                {
                    queue.Enqueue(item);

                    Assert.AreEqual(j, queue.Count);
                }

                for (j--; j > 0; j--)
                {
                    Assert.AreEqual(j, queue.Count);

                    queue.Dequeue();
                }

                Assert.Throws<EmptyQueueException>(() => queue.Dequeue(false));
            }

            Assert.Throws<EmptyQueueException>(() => queue.Dequeue(false));
            Assert.Throws<EmptyQueueException>(() => queue.Dequeue(timeout: 10));
        }

        [Test]
        public void TestTimeout()
        {
            IFileQueue<int> queue = GetQueueObject<int>();

            DateTime now = DateTime.Now;

            Assert.Throws<EmptyQueueException>(() => queue.Dequeue(timeout: 2000));

            Assert.True(DateTime.Now > now.AddSeconds(2));
        }

        [Test]
        public void TestExceedingBuffer([Values(3, 8, 200, 1017)] int bufferCapacity)
        {
            IFileQueue<int> queue = GetQueueObject<int>(bufferCapacity);

            int i;
            for (i = 1; i < bufferCapacity*2; i++)
            {
                queue.Enqueue(i);
                Assert.AreEqual(i, queue.Count);
            }

            for (i -= 2; i >= 0; i--)
            {
                queue.Dequeue();
                Assert.AreEqual(i, queue.Count);
            }

            Assert.Throws<EmptyQueueException>(() => queue.Dequeue(false));
        }

        [Test]        
        [TestCase(true, 0)]
        [TestCase(true, 10)]
        [TestCase(true, 1)]
        public void TestBlocks(bool block, int timeout)
        {
            IFileQueue<int> queue = GetQueueObject<int>();
            bool flag = false;

            IAsyncResult result = AsyncHelper.BeginExecute(() =>
                {
                    Sleeper.Sleep(100);
                    flag = true;
                    queue.Enqueue(1);
                });

            int item = queue.Dequeue();

            Assert.True(flag, "Flag still false, Dequeue didn't block!");

            AsyncHelper.EndExecute(result);

            Assert.True(result.IsCompleted, "Trigger thread didn't appear to return");
        }

        [Test]
        [TestCase(4, 4, 80000)]
        [TestCase(5, 10, 40000)]
        public void TestConcurrency([Values(2, 6)] int putters, [Values(2, 3, 8)] int getters, [Values(1000, 4000)] int itemsPerPutter)
        {
            TestItem[,] items = new TestItem[putters, itemsPerPutter];
            TestItem[] initialItems = new TestItem[putters * itemsPerPutter];
            TestItem[] resultingItems = new TestItem[putters * itemsPerPutter];
            IFileQueue<TestItem> queue = GetQueueObject<TestItem>();
            IAsyncResult[] putterResults = new IAsyncResult[putters];
            IAsyncResult[] getterResults = new IAsyncResult[getters];
            object LOCK = new object();
            bool[] puttersDone = new bool[putters];

            // Populate array with TestItems, also put items into a 2d array to make the enqueueing easier (1 row per putter)
            for (int i = 0; i < putters * itemsPerPutter; i++)
            {
                TestItem item = new TestItem(i);
                initialItems[i] = item;
                items[i / itemsPerPutter, i % itemsPerPutter] = item;
            }

            //Kick off threads to enqueue all the items into the queue
            for (int i = 0; i < putters; i++)
            {
                int index = i;
                putterResults[i] = AsyncHelper.BeginExecute(() =>
                    {
                        for (int j = 0; j < itemsPerPutter; j++)
                        {
                            queue.Enqueue(items[index, j]);
                        }
                        puttersDone[index] = true;
                    });
            }

            //Kick off threads to dequeue the items, and put them into a results array
            for (int i = 0; i < getters; i++)
            {
                getterResults[i] = AsyncHelper.BeginExecute(() =>
                    {
                        while (true)
                        {
                            try
                            {
                                TestItem item = queue.Dequeue(timeout: 2);
                                lock(LOCK)
                                {
                                    resultingItems[item.Value] = item;
                                }
                            }
                            catch (EmptyQueueException)
                            {
                                if (puttersDone.All(flag => flag == true))
                                    break;
                            }
                        }
                    });
            }

            foreach (IAsyncResult result in putterResults)
                AsyncHelper.EndExecute(result);

            foreach (IAsyncResult result in getterResults)
                AsyncHelper.EndExecute(result);

            //Verify that all items are present and are equal to what we had to start with
            Assert.AreEqual(initialItems, resultingItems);
            Assert.True(queue.IsEmpty);
        }

        [Test]
        [TestCase(1000)]
        [TestCase(100000)]
        public void TestQueueingLotsOfItems(int totalItems)
        {
            IFileQueue<int> queue = GetQueueObject<int>();

            for (int i = 1; i <= totalItems; i++)
            {
                queue.Enqueue(i);
                Assert.AreEqual(i, queue.Count);
            }

            for (int i = 1; i <= totalItems; i++)
            {
                int item = queue.Dequeue();
                Assert.AreEqual(i, item);
                Assert.AreEqual(totalItems - i, queue.Count);
            }
        }

        [Test]
        [TestCase(10)]
        [TestCase(15)]
        [TestCase(1789)]
        public void TestEnablingBuffer(int bufferCapacity)
        {
            IFileQueue<int> queue = GetQueueObject<int>();

            int i1 = 0;
            for (; i1 < 100; i1++)
            {
                queue.Enqueue(i1);
            }

            queue.BufferCapacity = bufferCapacity;

            int i2 = 0;
            for (; i2 < bufferCapacity * 2; i2++)
            {
                queue.Enqueue(i2);
            }

            queue.BufferCapacity /= 2;

            int i3 = 0;
            for (; i3 < bufferCapacity * 2; i3++)
            {
                queue.Enqueue(i3);
            }

            int expectedTotal = i1 + i2 + i3;

            Assert.AreEqual(expectedTotal, queue.Count);

            int actualTotal = 0;
            while (!queue.IsEmpty)
            {
                queue.Dequeue();
                actualTotal++;
            }

            Assert.AreEqual(expectedTotal, actualTotal);
        }
    }

    [TestFixture]
    [Category("Queues")]
    class FileQueueTests : CommonFileQueueTests
    {
        protected override IFileQueue<T> GetQueueObject<T>()
        {
            return new FileQueue<T>();
        }

        protected override IFileQueue<T> GetQueueObject<T>(int bufferCapacity)
        {
            return new FileQueue<T>(bufferCapacity);
        }
    }

    [TestFixture]
    [Category("Queues")]
    class PriorityFileQueueTests : CommonFileQueueTests
    {
        protected override IFileQueue<T> GetQueueObject<T>()
        {
            return new PriorityFileQueue<T>();
        }

        protected override IFileQueue<T> GetQueueObject<T>(int bufferCapacity)
        {
            return new PriorityFileQueue<T>(bufferCapacity);
        }

        [Test]
        public void TestOrdering()
        {
            PriorityFileQueue<string> priorityFileQueue = new PriorityFileQueue<string>();

            priorityFileQueue.Enqueue("so-so", 5);
            priorityFileQueue.Enqueue("not important", 1);
            priorityFileQueue.Enqueue("top priority", 10);

            Assert.AreEqual("top priority", priorityFileQueue.Dequeue());
            Assert.AreEqual("so-so", priorityFileQueue.Dequeue());
            Assert.AreEqual("not important", priorityFileQueue.Dequeue());
        }

        [Test]
        public void TestCount()
        {
            PriorityFileQueue<int> priorityFileQueue = new PriorityFileQueue<int>();

            Assert.True(priorityFileQueue.IsEmpty);
            Assert.AreEqual(0, priorityFileQueue.Count);

            int i;

            for (i = 1; i < 10; i++)
            {
                priorityFileQueue.Enqueue(i, i);
                Assert.AreEqual(i, priorityFileQueue.Count);
            }

            for (int j = i-1; j > 0; j--)
            {
                Assert.AreEqual(j, priorityFileQueue.Count);
                priorityFileQueue.Dequeue(false);
            }

            Assert.True(priorityFileQueue.IsEmpty);
            Assert.AreEqual(0, priorityFileQueue.Count);

        }

        [Test]
        public void TestDefaultPriority()
        {
            PriorityFileQueue<string> priorityFileQueue = new PriorityFileQueue<string>(defaultPriority: 3);

            priorityFileQueue.Enqueue("higher than default", 4);
            priorityFileQueue.Enqueue("enqueued as default");

            Assert.AreEqual("higher than default", priorityFileQueue.Dequeue());
            Assert.AreEqual("enqueued as default", priorityFileQueue.Dequeue());

            priorityFileQueue.Enqueue("lower than default", 2);
            priorityFileQueue.Enqueue("enqueued as default");

            Assert.AreEqual("enqueued as default", priorityFileQueue.Dequeue());
            Assert.AreEqual("lower than default", priorityFileQueue.Dequeue());

            priorityFileQueue.Enqueue("enqueued as default");
            priorityFileQueue.Enqueue("lower than default", -2);

            Assert.AreEqual("enqueued as default", priorityFileQueue.Dequeue());
            Assert.AreEqual("lower than default", priorityFileQueue.Dequeue());
        }

        [Test]
        public void TestChangingDefaultPriority()
        {
            PriorityFileQueue<int> priorityFileQueue = new PriorityFileQueue<int>(defaultPriority: 2);

            int[] expectedOrdering = new int[9];

            for (int i = 3; i < 6; i++)
            {
                priorityFileQueue.Enqueue(i);
                expectedOrdering[i] = i;
            }

            priorityFileQueue.defaultPriority = 1;

            for (int i = 6; i < 9; i++)
            {
                priorityFileQueue.Enqueue(i);
                expectedOrdering[i] = i;
            }

            priorityFileQueue.defaultPriority = 3;

            for (int i = 0; i < 3; i++)
            {
                priorityFileQueue.Enqueue(i);
                expectedOrdering[i] = i;
            }

            foreach (int i in expectedOrdering)
            {
                Assert.AreEqual(i, priorityFileQueue.Dequeue());
            }
        }
    }
}
