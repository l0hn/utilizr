﻿using NUnit.Framework;
using System.Diagnostics;
using System.IO;
using Utilizr.Hardware;
using System.Linq;
using Utilizr;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("HardwareInfo")]
    public class HardwareInfoTests
    {
        private HardwareInfo _hardwareInfo;

        [OneTimeSetUp]
        public void FixtureSetup()
        {
            _hardwareInfo = HardwareInfo.GetHardwareInfo();
        }

        [Test]
        public void TestCpuID()
        {
            string id = _hardwareInfo.GetProcessorID();
            Debug.WriteLine("processor id: " + id);
            Assert.That(id, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public void TestMotherboardID()
        {
            string id = _hardwareInfo.GetMotherboardSerialNumber();
            Debug.WriteLine("motherboard id: " + id);
            Assert.That(id, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public void TestVolumeSerial()
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives().Where(d => d.DriveType == DriveType.Fixed))
            {
                try
                {
                    string id = _hardwareInfo.GetVolumeSerial(drive.RootDirectory.FullName);
                    Debug.WriteLine(string.Format("volume serial ({0}): {1}", drive.RootDirectory.FullName, id));
                    Assert.That(id, Is.Not.Null.And.Not.Empty);
                }
                catch (HardwareInfoException)
                {
                }
            }
        }

        [Test]
        public void TestMacAddresses()
        {
            Assert.AreNotEqual(0, _hardwareInfo.GetMacAddresses().Length);
            foreach (string mac in _hardwareInfo.GetMacAddresses())
            {
                Debug.WriteLine("mac: " + mac);
                Assert.That(mac, Is.Not.Null.And.Not.Empty);
            }
        }

        [Test]
        public void TestUniqueID() {
            var expected = string.Format(
                               "{0}|{1}|{2}",
                               _hardwareInfo.GetMotherboardSerialNumber().ToLower().HashMD5(),
                               _hardwareInfo.GetRootHDDPhysicalSerial().ToLower().HashMD5(),
                               _hardwareInfo.GetProcessorID().ToLower().HashMD5()
                           ).ToLower();

            Assert.AreEqual(expected, UniqueID.GenerateUniqueHardwareID(), "Unique hardware id generated unexpected value");
        }
    }
}
