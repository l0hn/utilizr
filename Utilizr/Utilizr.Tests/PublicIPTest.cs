﻿using NUnit.Framework;
using Utilizr.Network;

namespace Utilizr.Tests
{
    [TestFixture]
    public class PublicIPTest
    {
        [Test]
        public void TestPublicIPGetter()
        {
            var ip = NetUtil.GetPublicIPFromOpenDNS();
            Assert.That(ip, Is.Not.Null.And.Not.Empty);
            Assert.AreEqual(4, ip.Split('.').Length);
        }
    }
}
