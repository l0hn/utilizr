﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Net;
using Utilizr.Rest;

namespace Utilizr.Tests
{
    //TODO: more and better tests
    [TestFixture]
    public class RestTest
    {
        [Test]
        public void TestRestClient()
        {
            var client = new BookRestClient();
            //test the hooks are processed
            bool hookProcessed = false;
            client.AddCustomRequestCompleteHook((ISBNLookup request, ISBNResponse response) =>
            {
                hookProcessed = true;
            });

            var bookResult = client.ExecuteRequest(new ISBNLookup() {ISBN = "1118951301"});
            Assert.AreEqual("books#volumes", bookResult.kind);
            Assert.AreEqual(1, bookResult.totalItems);
            Assert.AreEqual(1, bookResult.items.Count);
            Assert.AreEqual("Coding For Dummies", bookResult.items[0].volumeInfo.title);
            Assert.True(hookProcessed);
        }
    }

#region just a quick google books client for testing
    public class BookRestClient : RestClient
    {
        public BookRestClient() : base(new JsonRestSerializer())
        {
            ServiceUrl = "https://www.googleapis.com/books/v1";
        }

        public override void AddRequestHeaders<TResponse>(HttpWebRequest request, string postData, AbstractRequest<TResponse> requestObj)
        {
            request.ContentType = "text/json";
        }
    }

    public class ISBNLookup : AbstractRequest<ISBNResponse>
    {
        [JsonIgnore]
        public string ISBN { get; set; }

        public override string Endpoint
        {
            get { return $"volumes?q=isbn:{ISBN}"; }
        }

        public override RequestMethod Method
        {
            get
            {
                return RequestMethod.GET;
            }
        }
        protected override void ApplyRequestSpecificHeaders(HttpWebRequest request)
        {
            Console.WriteLine("TEST");
        }
    }
#endregion
}
