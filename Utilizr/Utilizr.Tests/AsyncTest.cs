﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using Utilizr.Async;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("Async Tests")]
    public class AsyncTest
    {
        private long _returnNumber = 100000;
        private ManualResetEvent _threadingDone = new ManualResetEvent(false);

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void ThreadTest(bool useThreadPool)
        {
            //method 1
            var iAsync = AsyncHelper<long>.BeginExecute(ReturnNumber, useThreadPool:useThreadPool);
            long result = iAsync.EndExecute();


            //method 2
            IAsyncResult asyncResult = AsyncHelper<long>.BeginExecute(ReturnNumber, useThreadPool:useThreadPool);
            result = AsyncHelper<long>.EndExecute(asyncResult);

            //check result
            Assert.AreEqual(_returnNumber, result);

            //with a callback
            var asyncWithCallback = AsyncHelper<long>.BeginExecute(ReturnNumber, Callback, useThreadPool:useThreadPool);
            _threadingDone.WaitOne();//this is only to stop the test from ending
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void MultiWaitCompleteTest(bool useThreadPool)
        {
            var results = new List<AsyncResult<long>>
            {
                AsyncHelper<long>.BeginExecute(ReturnNumber, useThreadPool:useThreadPool),
                AsyncHelper<long>.BeginExecute(ReturnNumber, useThreadPool:useThreadPool),
                AsyncHelper<long>.BeginExecute(ReturnNumber, useThreadPool:useThreadPool),
            };

            //test wait 
            AsyncHelper<long>.WaitComplete(results);

            foreach (var result in results)
            {
                //check resulting value directly
                Assert.AreEqual(_returnNumber, result.ResultObject);
            }
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void AsyncExceptionTest(bool useThreadPool)
        {
            //execute action that results in DivideByZeroException
            IAsyncResult result = AsyncHelper.BeginExecute(() =>
                {
                    int j;
                    for (int i = 10; i >= 0; i--)
                        j = 1 / i;
                },
                useThreadPool:useThreadPool
            );

            Assert.Throws<DivideByZeroException>(() => AsyncHelper.EndExecute(result));
        }

        public long ReturnNumber()
        {
            int i = 0;
            for (i = 0; i < _returnNumber; i++)
            {

            }
            return i;
        }

        public void Callback(IAsyncResult result)
        {
            long number = AsyncHelper<long>.EndExecute(result);
            _threadingDone.Set();
        }
    }
}
