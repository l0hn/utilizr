﻿using NUnit.Framework;
using System;
using Utilizr.Conversion;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("DateTime Extensions")]
    public class DateTimeExtensionTest
    {
        [Test]
        public void DateTimeExtensionsTest()
        {
            int ts = 1373994325;
            DateTime dt = ts.ToDateTime();
            int checkTs = dt.ToUnixTimestamp();
            Assert.AreEqual(ts, checkTs);

            DateTime newDate = new DateTime(2013, 7, 16, 17, 5, 25, DateTimeKind.Utc);
            int timestamp = newDate.ToUnixTimestamp();
            Assert.AreEqual(1373994325, timestamp);
        }

        [Test]
        public void DateTimeLocalTest()
        {
            DateTime someDate = new DateTime(2013, 8, 12, 17, 45, 20, DateTimeKind.Local);

            int timestamp = someDate.ToUnixTimestamp(DateTimeKind.Local);

            DateTime backAgain = timestamp.ToDateTime(DateTimeKind.Local);

            Assert.AreEqual(someDate, backAgain);
        }
    }
}
