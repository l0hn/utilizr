﻿#if !MONO && !NETCOREAPP
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utilizr.Windows;

namespace Utilizr.WPF.FontFix.Tests
{
    [TestFixture]
    public class Fonts
    {
        [Test]
        public void Test_RemoveInvalidCharacters()
        {
            string filename;
            string cleanName;

            filename = "<ASDASD.TTF";
            Assert.IsTrue(filename.IndexOfAny(new char[] { '<' }) >= 0);

            filename = $"*fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "fontname.ttf");

            filename = $"\\fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "fontname.ttf");

            filename = $"/fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "fontname.ttf");

            filename = $"<fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "fontname.ttf");

            //chars at start
            var invalidChars = @"*\/|<>?" + (char)13 + (char)10;

            filename = $"{invalidChars}fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "fontname.ttf");

            //filename with leading spaces
            filename = $" {invalidChars} fontname.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "  fontname.ttf");

            //name with an underscore
            filename = $"{invalidChars}font_name.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");

            //chars middle
            filename = $"font{invalidChars}_name.ttf";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");

            //chars at end
            filename = $"font_name.ttf{invalidChars}";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");

            //everywhere!!
            filename = $"f{invalidChars}o{invalidChars}n{invalidChars}t{invalidChars}_{invalidChars}n{invalidChars}a{invalidChars}m{invalidChars}e{invalidChars}.{invalidChars}t{invalidChars}t{invalidChars}f{invalidChars}";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");

            //Path.GetInvalidFileNameChars
            invalidChars = new string(Path.GetInvalidFileNameChars());
            filename = $"f{invalidChars}o{invalidChars}n{invalidChars}t{invalidChars}_{invalidChars}n{invalidChars}a{invalidChars}m{invalidChars}e{invalidChars}.{invalidChars}t{invalidChars}t{invalidChars}f{invalidChars}";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");

            //Path.GetInvalidPathChars
            invalidChars = new string(Path.GetInvalidPathChars());
            filename = $"f{invalidChars}o{invalidChars}n{invalidChars}t{invalidChars}_{invalidChars}n{invalidChars}a{invalidChars}m{invalidChars}e{invalidChars}.{invalidChars}t{invalidChars}t{invalidChars}f{invalidChars}";
            cleanName = RepairFonts.RemoveInvalidCharacters(filename);
            Assert.IsTrue(cleanName == "font_name.ttf");
        }
    }
}
#endif