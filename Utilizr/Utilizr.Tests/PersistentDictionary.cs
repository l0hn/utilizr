﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Utilizr.Collections;
using Utilizr.Info;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("Persistent Dictionary")]
    public class PersistentDictionaryTests
    {
        private TestProgress _progress;

        [OneTimeSetUp]
        public void FixtureSetup()
        {
            AppInfo.AppName = "Utilizr.Tests";

            _progress = new TestProgress();
            _progress.Show();
        }

        [OneTimeTearDown]
        public void FixtureTearDown()
        {
            if (_progress != null)
            {
                _progress.Dispose();
            }
        }

        [Test]
        public void TestPersistentDictionary()
        {
            _progress.testName = "Persistent Dictionary Test";

            int count = 1500;
            PersistentDictionary<string> stringDict = new PersistentDictionary<string>("stringTest", true);
            _progress.set(count, 0);

            for (int i = 0; i < count; i++)
            {
                stringDict[i + "-key"] = i + "-key";
                _progress.value++;
            }
            Assert.AreEqual(count, stringDict.Count);
            _progress.value = 0;
            foreach (KeyValuePair<string, string> item in stringDict)
            {
                Assert.NotNull(item);
                Assert.That(item.Key, Is.Not.Null.And.Not.Empty);
                Assert.That(item.Value, Is.Not.Null.And.Not.Empty);
                Assert.AreEqual(item.Key, item.Value);
                Assert.IsTrue(stringDict.ContainsKey(item.Key));
                _progress.value++;
            }
            stringDict.Clear();
            Assert.AreEqual(0, stringDict.Count);

            PersistentDictionary<int> intDict = new PersistentDictionary<int>("intTest", true);
            _progress.value = 0;
            for (int i = 0; i < count; i++)
            {
                intDict[i.ToString()] = i;
                _progress.value++;
            }
            Assert.AreEqual(count, intDict.Count);
            int intDictCount = 0;
            _progress.value = 0;
            foreach (var item in intDict)
            {
                intDictCount++;
                _progress.value++;
            }
            Assert.AreEqual(count, intDictCount);
        }

        [Test]
        public void TestDictionaryObjects()
        {
            _progress.testName = "Testing Dictionary Objects";

            var dict = new PersistentDictionary<TestValueItem>("customItems", true);
            int count = 1500;
            _progress.set(count, 0);
            for (int i = 0; i < count; i++)
            {
                dict[i.ToString()] = new TestValueItem()
                {
                    PropA = i,
                    PropB = "some text - " + i,
                    Date = DateTime.UtcNow
                };
                _progress.value++;
            }
            Assert.AreEqual(count, dict.Count);
            int enumerCount = 0;
            _progress.value = 0;
            foreach (var item in dict)
            {
                enumerCount++;
                Assert.AreEqual("some text - " + item.Value.PropA, item.Value.PropB);
                _progress.value++;
            }
            Assert.AreEqual(count, enumerCount);
        }
    }

    class TestValueItem
    {
        public int PropA { get; set; }
        public string PropB { get; set; }
        public DateTime Date { get; set; }
    }


}
