﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Utilizr.Database;
using Utilizr.Info;
using DateTime = System.DateTime;

namespace Utilizr.Tests
{
    class DbTestItem : Tuple<DbTestItem>
    {
        [PrimaryKey]
        public long id;
        [NotNull]
        [DefaultValue("default text")]
        public string text;
        [DefaultValue("1.2")]
        public float decimalValue;
        public DateTime dateTime;

        protected override string DatabaseFileName
        {
            get
            {
                return "testies.db";
            }
        }

        protected override string TableName
        {
            get
            {
                return "iffyjiffytablename";
            }
        }
    }


    [TestFixture]
    [Category("Data Mapping")]
    public class DatabaseTests
    {
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            AppInfo.AppName = "Utilizr.Tests";
        }

        [Test]
        public void DBCreateAndSave()
        {
            int testId = 1;
            string testText = "testing the text";
            DateTime testDateTime = DateTime.UtcNow;
            float testDecimalValue = 0.1f;

            var test = new DbTestItem()
            {
                decimalValue = testDecimalValue,
                id = testId,
                text = testText,
                dateTime = testDateTime,
            };

            test.Save();

            var loadTest = new DbTestItem()
            {
                id = testId
            };

            loadTest.Load();

            Assert.AreEqual(testText, loadTest.text);
            Assert.AreEqual(testDateTime, loadTest.dateTime);
            Assert.AreEqual(testDecimalValue, loadTest.decimalValue);
        }

        [Test]
        [TestCase(1000, 500)]
        public void DBTestLoadMany(int numberOfRecords, int queryAmount)
        {
            DbTestItem.DeleteMany();

            insertItems(numberOfRecords);

            List<DbTestItem> testList = DbTestItem.LoadMany().ToList();
            Assert.AreEqual(numberOfRecords, testList.Count);

            List<DbTestItem> testQuery = DbTestItem.LoadMany("WHERE ID < " + queryAmount).ToList();
            Assert.AreEqual(testQuery.Count, queryAmount);
        }

        [Test]
        public void DBTestDelete()
        {
            DbTestItem.DeleteMany();

            insertItems(100);

            Assert.AreEqual(100, DbTestItem.Count());

            foreach (var qItem in DbTestItem.LoadMany())
            {
                qItem.Delete();
            }

            int rowCount = DbTestItem.Count();
            Assert.AreEqual(0, rowCount);
        }

        [Test]
        public void DBEnumeratorTest()
        {
            DbTestItem.DeleteMany();

            insertItems(1000);
            foreach (var testItem in DbTestItem.LoadMany())
            {
                System.Console.WriteLine(testItem.id);
            }
        }

        private void insertItems(int count)
        {
            for (int i = 0; i < count; i++)
            {
                DbTestItem item = new DbTestItem()
                {
                    id = i,
                    decimalValue = i * 2.23f,
                    text = "some texties",
                    dateTime = DateTime.Now
                };
                item.Save();
            }
        }
    }
}
