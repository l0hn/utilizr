﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Utilizr.Tests
{
    public partial class TestProgress : Form
    {
        private long _max;
        public long max
        {
            get { return _max; }
            set { _max = value; updateDisplay(); }
        }

        private long _value;
        public long value
        {
            get { return _value; }
            set
            {
                _value = value;
                updateDisplay();
            }
        }

        private float percent
        {
            get
            {
                if (_value == 0)
                {
                    return 0;
                }
                return ((float)_value / (float)_max);
            }
        }

        private string _testName;
        public string testName
        {
            get { return _testName; }
            set 
            {
                if (InvokeRequired)
                {
                    try
                    {
                        Invoke(new MethodInvoker(delegate
                        {
                            testName = value;
                            Text = _testName;
                        }));
                    }
                    catch (Exception)
                    {
                    }
                    return;
                }            

                _testName = value;
                Text = _testName; 
            }
        }

        public TestProgress(string testName, long max)
        {
            InitializeComponent();
            _max = max;
            progressBar1.Maximum = 10000;
            progressBar1.Minimum = 0;
        }

        public TestProgress()
            : this("Test Progress", 100)
        {

        }

        public void set(long max, long value)
        {
            this.max = max;
            this.value = value;
        }

        public new void Show()
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                base.ShowDialog();
            });
        }

        public new void ShowDialog()
        {
            this.Show();
        }

        private void updateDisplay()
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke((MethodInvoker)updateDisplay);
                }
                catch (Exception)
                {
                }
                return;
            }
            float p = percent;
            percentLabel.Text = string.Format("{0:P2}", p);
            progressBar1.Value = (int)(p * progressBar1.Maximum);
        }
    }
}