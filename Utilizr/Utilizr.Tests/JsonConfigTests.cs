﻿using NUnit.Framework;
using System;
using System.IO;
using Utilizr.Info;

namespace Utilizr.Tests
{
    [TestFixture]
    [Category("Json Configs")]
    public class JsonConfigTests
    {
        [Test]
        public void TestWriteThenRead() 
        {
            var nowDate = DateTime.UtcNow;
            JCTestItem.Load();
            JCTestItem.Instance.TestProp1 = "some test string";
            JCTestItem.Instance.TestProp2 = nowDate;
            JCTestItem.Instance.TestProp3 = 1.11111;
            JCTestItem.Instance.TestProp4 = new SomeOtherClass()
            {
                SubProp1 = "some other string",
                SubProp2 = 123
            };
            //save the config to disk
            JCTestItem.EndSaveInstanceAsync(JCTestItem.SaveInstanceAsync(null));

            //make changes but dont save them
            JCTestItem.Instance.TestProp1 = "i've changed the string";
            JCTestItem.Instance.TestProp4.SubProp1 = "i've changed some other string";

            //reload and make sure original data is loaded from disk
            JCTestItem.ReloadInstance();
            Assert.AreEqual("some test string", JCTestItem.Instance.TestProp1);
            Assert.AreEqual("some other string", JCTestItem.Instance.TestProp4.SubProp1);
            Assert.AreEqual(nowDate, JCTestItem.Instance.TestProp2);
            Assert.AreEqual(1.11111, JCTestItem.Instance.TestProp3);

            //repeat changes but this time commit to disk
            JCTestItem.Instance.TestProp1 = "i've changed the string";
            JCTestItem.Instance.TestProp4.SubProp1 = "i've changed some other string";
            JCTestItem.EndSaveInstanceAsync(JCTestItem.SaveInstanceAsync(null));

            //make sure new values are loaded from disk
            JCTestItem.ReloadInstance();
            Assert.AreEqual("i've changed the string", JCTestItem.Instance.TestProp1);
            Assert.AreEqual("i've changed some other string", JCTestItem.Instance.TestProp4.SubProp1);
            Assert.AreEqual(nowDate, JCTestItem.Instance.TestProp2);
            Assert.AreEqual(1.11111, JCTestItem.Instance.TestProp3);
        }
    }

    class JCTestItem: Loadable<JCTestItem> 
    {
        public string TestProp1 { get; set; }
        public DateTime TestProp2 { get; set; }
        public double TestProp3 { get; set; }
        public SomeOtherClass TestProp4 { get; set; }



        #region implemented abstract members of Loadable

        protected override string getLoadPath()
        {
            return Path.Combine(AppInfo.DataDirectory, "jctestitem.jdat");
        }

        protected override string CustomDeserializeStep(string source)
        {
            return source;
        }

        protected override string CustomSerializeStep(string source)
        {
            return source;
        }

        #endregion
    }

    class SomeOtherClass {
        public string SubProp1 { get; set; }
        public int SubProp2 { get; set; }
    }
}

