﻿using NUnit.Framework;
using System;
using Utilizr.Info;
using Utilizr.Network;

namespace Utilizr.Tests
{
    [TestFixture(Category = "Favicons")]
    public class Favicons
    {
        [OneTimeSetUp]
        public void FixtureSetup()
        {
            AppInfo.AppName = "Utilizr.Tests";
        }

        [Test]
        [TestCase("google.com")]
        [TestCase("instagram.com")]
        [TestCase("http://www.instagram.com/someurl?someparam=something")]
        public void Get(string url)
        {
            var sizes = (FavIcoSize[]) Enum.GetValues(typeof(FavIcoSize));
            var ico = Favicon.GetFavicon(url, sizes);
            Assert.NotNull(ico);
        }
    }
}
