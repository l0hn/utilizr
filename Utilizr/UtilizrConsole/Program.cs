﻿using CLAP;
using GetText;
using GetText.Localization;
using System;
using System.Diagnostics;
using System.IO;
using Utilzr.WPF.Util;

namespace UtilizrConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                args = new string[] { "-help" };
            }
            try
            {
                Parser.Run<App>(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
    }


    public class App
    {
        [Verb(Description = "Create a POT file by recursively scanning a src directory for gettext methods")]
        public static void gen_pot(
            [Required][Description("the src root directory")] string source_dir,
            [Required][Description("the filename for the output POT file")] string output)
        {
            Utilizr.GetText.Helpers.PotGen.CreatePotFile(source_dir, output);
        }

        [Verb(Description = "generates source list ready to send to msginit")]
        public static void source_list(
            [Required][Description("the src root directory")] string source_dir,
            [Required][Description("the filename to write the file list to")] string output)
        {
            Utilizr.GetText.Helpers.SourceList.GenerateSourceList(source_dir, output);
        }

        [Verb(Description = "Decompress a compressed log file, or a folder containing many files")]
        public static void decompress(
            [Required][Description("The file or folder path to decompress")] string path,
            [Required][Description("The destination folder where the decompressed logs will be saved")] string destination,
            [Description("Whether an existing file would be overwritten")] bool overwrite = false
            )
        {
            Utilizr.Logging.LogC.Decompress(path, destination, overwrite);
        }

        [Verb(Description = "Iterates throughout all translations to ensure no exceptions will be thrown.")]        
        public static void validate_locales(
            [Required][Description("The locale folder which contains the *.mo files.")] string srcDir)
        {
            try
            {
                if (!Directory.Exists(srcDir))
                    throw new ArgumentException($"{nameof(srcDir)} - Folder '{srcDir}' does not exist on disk");

                foreach (var moFile in Directory.GetFiles(srcDir, "*.mo"))
                {
                    var ietfTag = Path.GetFileNameWithoutExtension(moFile);
                    var ctx = ValidationResourceContext.FromFile(moFile, ietfTag);

                    // TODO: This could be significantly improved, just something quick and dirty to list potentially dodgy translations, e.g. "{ 0 : NO }" as a placeholder.
                    // Might be able to use the rosyln compiler sdk to parse source files and figure out any string format placeholders count/types
                    // Might also be able to figure out singular / plural for better testing, too.
                    // None of that is trivial, however.
                    // Will always show string interpolation translations as errors, although not a good idea to use them inside translations anyway e.g. variable name changes

                    foreach (var entry in ctx.Entries())
                    {
                        try
                        {
                            string.Format(entry.Value, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
                        }
                        catch (Exception ex)
                        {
                            var nl = Environment.NewLine;
                            Console.WriteLine($"[{ietfTag}] - {ex.Message}{nl}English: {entry.Key}{nl}Translation: {entry.Value}{nl}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    Console.WriteLine("Catastrophic error:");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                    ex = ex.InnerException;
                }
            }
        }


        [Verb(Description =
            "Generates a UtilzrTheme.xaml file containing an entire UI theme ready to include in your project")]
        static void theme_gen(
            [Required] [Description("The theme to generate")]
            Theme theme,
            [Description("Output file name")]string out_file = "UtilizrTheme.xaml"
        )
        {
            ThemeMaker.GenerateThemeResourceDictionary(theme, out_file);
        }


        [Help]
        public static void help(string help)
        {
            Console.WriteLine(help);
        }

        [Global(Aliases = "d")]
        public static void debug()
        {
            Debugger.Launch();
        }
    }
}