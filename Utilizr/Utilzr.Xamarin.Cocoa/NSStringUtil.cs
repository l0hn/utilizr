﻿//using System;
//using CoreText;
//using Foundation;

//#if __IOS__
//using NSFont = UIKit.UIFont;
//using UIKit;
//using NSStringAttributes = Foundation.NSAttributedStringDocumentAttributes;
//#else
//using AppKit;

//#endif

//namespace Utilzr.Xamarin.Cocoa
//{
//	public static class NSStringUtil
//	{

//		public static NSAttributedString highlightedString(string fromString, string searchTerm, NSFont font)
//		{
            
//			var mutable = new NSMutableAttributedString(fromString);

//			if (string.IsNullOrEmpty(searchTerm))
//			{
//				return mutable;
//			}

//			int start = 0;
//			while (true)
//			{

//				int startOfT = fromString.ToLower().IndexOf(searchTerm.ToLower(), start);
//				if (startOfT < 0)
//				{
//					break;
//				}
//				else
//				{
//					mutable.AddAttributes(new CTStringAttributes()
//					{
//						Font = new CTFont(font.FontName, font.PointSize)
//					}, new NSRange(startOfT, searchTerm.Length));
//					start = startOfT + searchTerm.Length;
//				}

//			}
//			return mutable;
//		}
//		/// <summary>
//		/// Center and bold part of title
//		/// </summary>
//		/// <returns>The title string.</returns>
//		/// <param name="text">Text.</param>
//		/// <param name="font">Font.</param>
//		/// <param name="start">Start.</param>
//		/// <param name="end">End.</param>
//		public static NSAttributedString FormatTitleString(string text, NSFont font, int start, int end)
//		{
//			NSRange boldRange = new NSRange(start, end);
//			NSRange fullRange = new NSRange(0, text.Length);

//			//Bold text attributes
//			CTFont Font = new CTFont(font.FamilyName, font.PointSize);

//			var boldAttrs = new CTStringAttributes
//			{
//				Font = Font.WithSymbolicTraits(font.PointSize, CTFontSymbolicTraits.Bold, CTFontSymbolicTraits.Bold)
//			};

//			//Center text attributes
//			var centerAttrs = new NSStringAttributes
//			{
//				ParagraphStyle = new NSMutableParagraphStyle { Alignment = NSTextAlignment.Center }
//			};

//			NSMutableAttributedString test = new NSMutableAttributedString(text);
//			test.BeginEditing();
//			test.AddAttributes(centerAttrs, fullRange);
//			test.AddAttributes(boldAttrs, boldRange);
//			test.EndEditing();

//			return test;
//		}

//		public static NSAttributedString Align(this NSAttributedString text, NSTextAlignment align)
//		{
//			var mutable = text.MutableCopy() as NSMutableAttributedString;

//			//Center text attributes
//			var centerAttrs = new NSStringAttributes
//			{
//				ParagraphStyle = new NSMutableParagraphStyle { Alignment = NSTextAlignment.Center }
//			};

//			mutable.BeginEditing();
//			mutable.AddAttributes(centerAttrs, new NSRange(0, text.Length));
//			mutable.EndEditing();

//			return mutable;
//		}

//		public static NSAttributedString Color(this NSAttributedString str, NSColor color)
//		{
//			var m = str.MutableCopy() as NSMutableAttributedString;
//			//remove foreground & stroke (as stroke seems to interfere)
//			m.RemoveAttribute(NSStringAttributeKey.ForegroundColor, new NSRange(0, str.Length));
//			m.RemoveAttribute(NSStringAttributeKey.StrokeColor, new NSRange(0, str.Length));
//			m.AddAttributes(new NSStringAttributes { ForegroundColor = color }, new NSRange(0, str.Length));
//			return m;
//		}


//		public static NSAttributedString CleanUp(this NSAttributedString str)
//		{
//			var m = str.MutableCopy() as NSMutableAttributedString;
//			m.FixFontAttributeInRange(new NSRange(0, str.Length));
//			return m;
//		}

//		public static NSAttributedString ReplaceFont(this NSAttributedString str, NSFont font, bool overrideSize = true)
//		{
//			if (str == null)
//			{
//				return null;
//			}
//			var m = str.MutableCopy() as NSMutableAttributedString;
//			m.EnumerateAttribute(NSStringAttributeKey.Font, new NSRange(0, str.Length), NSAttributedStringEnumeration.None, (NSObject value, NSRange range, ref bool stop) =>
//			{
//				var f = value as NSFont;
//				if (f != null)
//				{

//					CTFont currentFont = new CTFont(f.FontName, f.PointSize);// new CTFont(font.fon, font.PointSize);

//					var size = f.PointSize;

//					if (overrideSize)
//					{
//						size = font.PointSize;
//					}

//					CTFont newFont = new CTFont(font.FamilyName, size);

//					//copy symbolic traits to new font...
//					newFont = newFont.WithSymbolicTraits(size, currentFont.SymbolicTraits, currentFont.SymbolicTraits);

//					//delete old attribute
//					m.RemoveAttribute(NSStringAttributeKey.Font, range);
//					var attrs = new CTStringAttributes
//					{
//						Font = newFont
//					};

//					//replace with new attribute
//					m.AddAttributes(attrs, range);
//				}
//			});
//			return m;
//		}

//		public static NSRange FullRange(this NSAttributedString str)
//		{
//			return new NSRange(0, str.Length);
//		}

//		public static NSAttributedString ToAttributed(this string unattributed)
//		{
//			return new NSAttributedString(unattributed);
//		}

//		public static NSAttributedString ToHtmlAttributedString(this string html)
//		{
//			NSDictionary _unused = null;
//			try
//			{
//				return new NSAttributedString(new NSString(html).Encode(NSStringEncoding.UTF8),
//									   new NSDictionary(
//												  //const bindings seem to be missing so use raw key vals
//												  new NSString("DocumentType"), NSDocumentType.HTML,
//												  new NSString("CharacterEncoding"), NSStringEncoding.UTF8), out _unused);
//			}
//			catch
//			{
//				//unexplained crashes seen in raygun, so use un formatted fallback value

//				html = html.Replace("<br/>", "\n");
//				var plain = HtmlUtil.ConvertHtml(html);
//				return new NSAttributedString(plain, new NSStringAttributes());


//				//return new NSAttributedString(plainFallback, new NSStringAttributes());
//			}
//		}

//		public static void InsertImage(this NSMutableAttributedString initial, NSImage img, int location = 0, bool insertSpaceBefore = false, bool insertSpaceAfter = true, int baselineOffset = -3)
//		{
//			NSTextAttachmentCell cell = new NSTextAttachmentCell(img);

//			//image.Image = NSImage.ImageNamed("in-app-buy-msg-star");
//			//image.Bounds = new CoreGraphics.CGRect(CGPoint.Empty, image.Image.Size);
//			//mutable.Append(NSAttributedString.FromAttachment( new NSTextAttachment() { AttachmentCell = cell } ));
//			//

//			var attachment = new NSTextAttachment() { AttachmentCell = cell };

//			var astr = NSAttributedString.FromAttachment(attachment).MutableCopy() as NSMutableAttributedString;
//			astr.AddAttributes(new NSStringAttributes { BaselineOffset = baselineOffset }, new NSRange(0, astr.Length));

//			//add icon at start
//			initial.Insert(astr, location);

//			if (insertSpaceBefore)
//			{
//				initial.Insert(new NSAttributedString(" "), location);
//			}
//			if (insertSpaceAfter)
//			{
//				//insert space between image & text
//				initial.Insert(new NSAttributedString(" "), location + 1);
//			}

//		}

//		public static NSMutableAttributedString InsertImage(this NSAttributedString initial, NSImage img, int location = 0, bool insertSpaceBefore = false, bool insertSpaceAfter = true, int baselineOffset = -3)
//		{
//			NSMutableAttributedString mutable = initial.MutableCopy() as NSMutableAttributedString;

//			mutable.InsertImage(img, location, insertSpaceBefore, insertSpaceAfter, baselineOffset);

//			return mutable;
//		}

//		/// <summary>
//		/// Wrap a string in html bold tags
//		/// </summary>
//		/// <returns>The bolded string.</returns>
//		/// <param name="input">Input.</param>
//		public static string HtmlBold(this string input)
//		{
//			return string.Format("<b>{0}</b>", input);
//		}

//		/// <summary>
//		/// Wrap a string in html font tags with specified color
//		/// </summary>
//		/// <returns>The bolded string.</returns>
//		/// <param name="input">Input.</param>
//		public static string HtmlColor(this string input, NSColor c)
//		{
//			return string.Format("<font color=\"{0}\">{1}</font>", c.ToHex(), input);
//		}

//	}
//}
