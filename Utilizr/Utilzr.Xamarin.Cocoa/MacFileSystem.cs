﻿using System;
using System.IO;
using System.Linq;
#if !__IOS__
using Mono.Unix;
#endif
using Utilizr;
using Utilizr.Logging;
using Utilizr.FileSystem;
namespace Utilzr.Xamarin.Cocoa
{
    /// <summary>
    /// Mac file system util. 
    /// </summary>
    public class MacFileSystem
    {
        /// <summary>
        /// Recursive directory copy helper. NOTE: this method will not copy any .ds_store files
        /// </summary>
        /// <param name="sourceDirectory">Source directory.</param>
        /// <param name="destinationDirectory">Destination directory.</param>
        /// <param name="onlyCopyIfAlreadyExists">If set to <c>true</c> only copy if already exists.</param>
        /// <param name="recursive">If set to <c>true</c> recursive.</param>
        /// <param name="ignoreFolderNames">Ignore folder names.</param>
        public static void CopyDirectoryContents(string sourceDirectory, string destinationDirectory, bool onlyCopyIfAlreadyExists = false, bool recursive = false, params string[] ignoreFolderNames)
        {
            if (!Directory.Exists(destinationDirectory))
                Directory.CreateDirectory(destinationDirectory);

            foreach (var file in Directory.GetFiles(sourceDirectory))
            {
                if (file.ToLower().EndsWith(".ds_store"))
                    continue;

                var destinationFile = Path.Combine(destinationDirectory, Path.GetFileName(file));

                if (onlyCopyIfAlreadyExists && !File.Exists(destinationFile))
                    continue;

                CopyFile(file, destinationFile, true);
            }

            if (!recursive)
                return;

            foreach (var dir in Directory.GetDirectories(sourceDirectory))
            {
                if (ignoreFolderNames.Any(i => i.Equals(Path.GetFileName(dir), StringComparison.InvariantCultureIgnoreCase)))
                    continue;

                var destination = Path.Combine(destinationDirectory, Path.GetFileName(dir));
                Directory.CreateDirectory(destination);
                CopyDirectoryContents(dir, destination, onlyCopyIfAlreadyExists, recursive);
            }
        }

        public static void CopyDirectoryContents(string sourceDirectory, string destinationDirectory, bool onlyCopyIfAlreadyExists = false, bool recursive = false, bool followSymlinks = false, params string[] ignoreFolderNames)
        {
            if (followSymlinks)
            {
                FileSystem.CopyDirectoryContents(sourceDirectory, destinationDirectory, onlyCopyIfAlreadyExists, recursive, ignoreFolderNames);
            }
            else
            {
                CopyDirectoryContents(sourceDirectory, destinationDirectory, onlyCopyIfAlreadyExists, recursive, ignoreFolderNames);
            }

        }

        public static void CopyFile(string source, string destination, bool overwrite = false)
        {
            var result = Shell.Exec("cp", "-R", $"\"{source}\"", $"\"{destination}\""); //specify -R to copy symbolic link rather than its target
            if (result.ExitCode != 0)
            {
                throw new IOException(string.Format("Bash cp error: {0}", result.ErrorOutput ?? result.Output));
            }
        }
    }
}
