﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
namespace Utilzr.Xamarin.Cocoa
{
    public class SharedProgressEstimator : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler PercentChanged;

        private double _percent;
        /// <summary>
        /// Current progress percentage between 0 and 1.
        /// </summary>
        public double Percent
        {
            get { return _percent; }
            set
            {
                _percent = value;
                OnPropertyChanged(nameof(Percent));
                OnPercentChanged();
            }
        }

        //public double PercentCalculationFrequencyMs
        //{
        //    get { return _timer.Interval; }
        //    set { _timer.Interval = value; }
        //}

        public bool IsPaused { get; private set; }

        protected int CurrentSegmentIndex { get; private set; }
        protected ReadOnlyCollection<SharedProgressEstimatorSegment> Segments => _segments.AsReadOnly();
        protected double TotalEstimatedTime => _segments.Sum(p => p.ExpectedMs);

        private DateTime _currentSegmentStarted;
        private DateTime _pauseStarted;
        private readonly List<SharedProgressEstimatorSegment> _segments;
        //private readonly object LOCK = new object();
        private static readonly Timer _timer;

        static SharedProgressEstimator()
        {
            _timer = new Timer();
            _timer.Interval = 500;
            _timer.Elapsed += (s, e) => SharedTick();
            _timer.Start();
        }

        private static List<SharedProgressEstimator> __estimators = new List<SharedProgressEstimator>();

        static void SharedTick()
        {
            foreach (var item in __estimators)
            {
                item.Tick();
            }
        }

        static void Register(SharedProgressEstimator e)
        {
            __estimators.Add(e);
        }

        static void Unregister(SharedProgressEstimator e)
        {
            __estimators.Remove(e);
        }

        bool IsRegistered()
        {
            return __estimators.Contains(this);
        }

        public SharedProgressEstimator()
        {
            _segments = new List<SharedProgressEstimatorSegment>();
         
        }

        public SharedProgressEstimator(SharedProgressEstimatorSegment segment)
            : this()
        {
            AddSegments(segment);
        }

        public SharedProgressEstimator(IEnumerable<SharedProgressEstimatorSegment> segments)
            : this()
        {
            AddSegments(segments.ToArray());
        }

        public SharedProgressEstimator(params SharedProgressEstimatorSegment[] segments)
            : this()
        {
            AddSegments(segments);
        }

        public void ClearSegments()
        {
            //lock (LOCK)
            //{
                if (IsRegistered())
                    throw new InvalidOperationException("Cannot clear segment collection whilst estimation is in progress");

                _segments.Clear();
            //}
        }

        public void AddSegments(params SharedProgressEstimatorSegment[] segments)
        {
            //lock (LOCK)
            //{
                if (IsRegistered())
                    throw new InvalidOperationException("Cannot add segments to collection whilst estimation is in progress");

                _segments.AddRange(segments);
            //}
        }

        public void AddSegments(IEnumerable<SharedProgressEstimatorSegment> segments)
        {
            //lock (LOCK)
            //{
                if (IsRegistered())
                    throw new InvalidOperationException("Cannot add segments to collection whilst estimation is in progress");

                _segments.AddRange(segments);
            //}
        }

        public void Start()
        {
            //lock (LOCK)
            //{
                if (IsRegistered())
                    return;

                CurrentSegmentIndex = 0;
                _currentSegmentStarted = DateTime.UtcNow;
                Percent = 0;
                 Register(this);
            //}
        }

        public void Pause()
        {
            // Since percentage is calculated from time elapsed, take note of
            // how long estimator is paused. We can then offset the start time
            // so we have the same percentage when resuming the estimator.
            //lock (LOCK)
            //{
                if (!IsRegistered())
                    return;

                _pauseStarted = DateTime.UtcNow;
                Unregister(this);
                IsPaused = true;
            //}
        }

        public void Resume()
        {
            //lock (LOCK)
            //{
                if (IsRegistered() || !IsPaused)
                    return;

                var pausedTime = DateTime.UtcNow - _pauseStarted;
                _currentSegmentStarted = _currentSegmentStarted.Add(pausedTime);
            Register(this);
                IsPaused = false;
            //}
        }

        public void SegmentDone()
        {
            CurrentSegmentIndex++;
            _currentSegmentStarted = DateTime.UtcNow;
        }

        /// <summary>
        /// Whether <see cref="Percent"/> should be set to '1.0'.
        /// </summary>
        /// <param name="updatePercentValue"></param>
        public void Finished(bool updatePercentValue = true)
        {
            //lock (LOCK)
            //{
            Unregister(this);
                IsPaused = false;

                if (updatePercentValue)
                    Percent = 1;
            //}
        }

        public bool IsRunning => IsRegistered() || IsPaused;

        void Tick()
        {
            double totalTime = TotalEstimatedTime; // cache
            double completedPercent = 0;

            // Calculate percentage for work already done.
            if (CurrentSegmentIndex > 0)
            {
                double doneSegmentsTime = _segments.Take(CurrentSegmentIndex)
                                                   .Sum(p => p.ExpectedMs);

                completedPercent = doneSegmentsTime / totalTime;
            }


            // Calculate the percentage for the current segment
            if (completedPercent < 1 && CurrentSegmentIndex < _segments.Count)
            {
                double takenMs = (DateTime.UtcNow - _currentSegmentStarted).TotalMilliseconds;
                double expectedMs = _segments[CurrentSegmentIndex].ExpectedMs;
                double percentWhenComplete = expectedMs / totalTime;

                // Use a function that increases from 0 to 1 where x increases from 0 to infinity.
                // This value can be used against the 'work complete' percentage of the segment.
                // Using y = x / (x + a) http://math.stackexchange.com/a/869157/78626
                // x = time taken
                // a = expected time
                // When x is equal to a, y = 0.5. Thus when reaching the estimated time (x == a),
                // only 50% of the segments total percentage will be included.
                // If a = expected / 4, when expected time = taken time, y = approx 0.8
                double y = takenMs / (takenMs + (expectedMs / 4));
                double segmentPercent = percentWhenComplete * y;
                completedPercent += segmentPercent;
            }

            double roundedDown = Math.Floor(completedPercent * 100);

            // Possible that Stop() called but Elapsed event raised afterwards or already running on invocation.
            // Only set if the timer is still enabled.
            //lock (LOCK)
            //{
                //if (IsRegistered())
                    Percent = roundedDown / 100;
            //}
        }

        protected virtual void OnPercentChanged()
        {
            PercentChanged?.Invoke(this, new EventArgs());
        }

        protected virtual void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class SharedProgressEstimatorSegment
    {
        public long ExpectedMs { get; set; }

        public SharedProgressEstimatorSegment()
        {

        }

        public SharedProgressEstimatorSegment(long expectedMs)
        {
            ExpectedMs = expectedMs;
        }
    }
}
