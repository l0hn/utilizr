﻿﻿using System;
using System.Text;
using Foundation;

#if __IOS__
using NSView = UIKit.UIView;
using UIKit;
#else
using Util;
using AppKit;

#endif

namespace Extensions
{
public static class ConstraintExtensions
	{

		public static ConstraintBuilder BuildConstraint(this NSView v)
		{
			return new ConstraintBuilder(v);
		}


	}


	public class ConstraintBuilder
	{
		protected NSView View;

		public ConstraintBuilder(NSView view)
		{
			this.View = view;
		}

		private ConstraintBuilder Parent;

		protected NSView CenterRef;
		protected float CenterOffset = 0;

		private VerticalConstraintBuilder Vertical;
		private HorizonalConstraintBuilder Horizontal;

		public HorizonalConstraintBuilder H()
		{
			var h = new HorizonalConstraintBuilder(View);

			if (Parent != null)
			{
				Parent.Horizontal = h;
				h.Parent = Parent;
			}
			else {
				Horizontal = h;
				Horizontal.Parent = this;
			}

			return h;
			//			if (Vertical != null) {
			//				Horizontal.Parent = Vertical.Parent;
			//			} else {
			//				Horizontal.Parent = this;
			//			}
			//			return Horizontal;
		}

		public void FillParentAndBuild()
		{
			V().FillParent().H().FillParent().Build();
		}

		public VerticalConstraintBuilder V()
		{
			var v = new VerticalConstraintBuilder(View);

			if (Parent != null)
			{
				Parent.Vertical = v;
				v.Parent = Parent;
			}
			else {
				Vertical = v;
				Vertical.Parent = this;
			}

			return v;


			//			Vertical = new VerticalConstraintBuilder (View);
			//			if (Horizontal != null) {
			//				Vertical.Parent = Horizontal.Parent;
			//			} else {
			//				Vertical.Parent = this;
			//			}
			//			return Vertical;
		}


		//		public BuilderEndpoint WidthEquals(NSView other) {
		//
		//		}
		//		progressContainer.ApplyHConstraint ("H:|-(4)-[v]-(4)-|", "v");

		public virtual void Build()
		{
			if (Parent != null)
			{
				if (Parent.Horizontal != null)
				{
					Parent.Horizontal.BuildInternal();
				}
				if (Parent.Vertical != null)
				{
					Parent.Vertical.BuildInternal();
				}
			}
		}

		protected virtual void BuildInternal()
		{

		}
	}

	public class HorizonalConstraintBuilder : ConstraintBuilder
	{
		public HorizonalConstraintBuilder(NSView view) : base(view)
		{
		}

		private float WidthConst;

		private NSView LeadingRef, TrailingRef, LeadingAlignRef, TrailingAlignRef;

		private float LeadingConst, TrailingConst;

		private bool SetLeading, SetTrailing;


		public HorizonalConstraintBuilder FillParent(float padding = 0)
		{
			return LeadingSpace(padding).TrailingSpace(padding);
		}

		public HorizonalConstraintBuilder LeadingSpace(float space = 0, NSView to = null)
		{
			LeadingRef = to;
			SetLeading = true;
			LeadingConst = space;
			return this;
		}

		public HorizonalConstraintBuilder Center(NSView inView = null, float offset = 0)
		{
			CenterRef = inView;

			if (CenterRef == null)
			{
				CenterRef = this.View.Superview;
			}
			CenterOffset = offset;
			return this;
		}


		public HorizonalConstraintBuilder TrailingSpace(float space = 0, NSView to = null)
		{
			TrailingRef = to;
			SetTrailing = true;
			TrailingConst = space;
			return this;
		}

		public HorizonalConstraintBuilder Width(float width)
		{
			WidthConst = width;
			return this;
		}


		protected override void BuildInternal()
		{
			if (CenterRef != null)
			{
				View.CenterInH(CenterRef, CenterOffset);
				//				return;
			}

			NSMutableDictionary<NSString, NSView> dict = new NSMutableDictionary<NSString, NSView>();

			dict.Add(new NSString("v"), View);

			StringBuilder sb = new StringBuilder();

			sb.Append("H:");

			if (SetLeading)
			{
				if (LeadingRef != null)
				{
					sb.Append("[l]-(" + LeadingConst + ")-");
					dict.Add(new NSString("l"), LeadingRef);
				}
				else {
					sb.Append("|-(" + LeadingConst + ")-");
				}
			}

			if (WidthConst > 0)
			{
				sb.Append("[v(" + WidthConst + ")]");
			}
			else {
				sb.Append("[v]");
			}

			if (SetTrailing)
			{
				if (TrailingRef != null)
				{
					sb.Append("-(" + TrailingConst + ")-[t]");
					dict.Add(new NSString("t"), TrailingRef);
				}
				else {
					sb.Append("-(" + TrailingConst + ")-|");
				}
			}

			//			ALog.Out ("VLF: " + sb.ToString ());
			View.TranslatesAutoresizingMaskIntoConstraints = false;
			var cs = NSLayoutConstraint.FromVisualFormat(sb.ToString(), NSLayoutFormatOptions.DirectionLeadingToTrailing, null, dict);
			cs.Activate();

		}
    }

    public class VerticalConstraintBuilder : ConstraintBuilder
	{
		public VerticalConstraintBuilder(NSView view) : base(view)
		{
		}

		private float HeightConst;

		private NSView TopRef, BottomRef;

		private NSLayoutRelation BottomRelation;

		private float TopConst, BottomConst;

		private bool SetTop, SetBottom;

		public VerticalConstraintBuilder TopSpace(float space = 0, NSView to = null)
		{
			TopRef = to;
			SetTop = true;
			TopConst = space;
			return this;
		}

		public VerticalConstraintBuilder Center(NSView inView = null, float offset = 0)
		{
			CenterRef = inView;
			if (CenterRef == null)
			{
				CenterRef = this.View.Superview;
			}
			CenterOffset = offset;
			return this;
		}

		public VerticalConstraintBuilder FillParent(float padding = 0)
		{
			return TopSpace(padding).BottomSpace(padding);
		}


		public VerticalConstraintBuilder BottomSpace(float space = 0, NSView to = null, NSLayoutRelation relation = NSLayoutRelation.Equal)
		{
			BottomRelation = relation;
			BottomRef = to;
			SetBottom = true;
			BottomConst = space;
			return this;
		}

		public VerticalConstraintBuilder Height(float height)
		{
			HeightConst = height;
			return this;
		}

		protected override void BuildInternal()
		{
			if (CenterRef != null)
			{
				View.CenterInV(CenterRef, CenterOffset);
			}

			NSMutableDictionary<NSString, NSView> dict = new NSMutableDictionary<NSString, NSView>();
			dict.Add(new NSString("v"), View);

			StringBuilder sb = new StringBuilder();

			sb.Append("V:");

			if (SetTop)
			{
				if (TopRef != null)
				{
					sb.Append("[l]-(" + TopConst + ")-");
					dict.Add(new NSString("l"), TopRef);
				}
				else {
					sb.Append("|-(" + TopConst + ")-");
				}
			}

			if (HeightConst > 0)
			{
				sb.Append("[v(" + HeightConst + ")]");
			}
			else {
				sb.Append("[v]");
			}

			if (SetBottom)
			{
				if (BottomRef != null)
				{
					switch (BottomRelation)
					{
						case NSLayoutRelation.Equal:
							sb.Append("-(" + BottomConst + ")-[t]");
							break;
						case NSLayoutRelation.LessThanOrEqual:
							sb.Append("-(<=" + BottomConst + ")-[t]");
							break;
						case NSLayoutRelation.GreaterThanOrEqual:
							sb.Append("-(>=" + BottomConst + ")-[t]");
							break;
					}
					dict.Add(new NSString("t"), BottomRef);
				}
				else {
					switch (BottomRelation)
					{
						case NSLayoutRelation.Equal:
							sb.Append("-(" + BottomConst + ")-|");
							break;
						case NSLayoutRelation.LessThanOrEqual:
							sb.Append("-(<=" + BottomConst + ")-|");
							break;
						case NSLayoutRelation.GreaterThanOrEqual:
							sb.Append("-(>=" + BottomConst + ")-|");
							break;
					}
				}
			}

			//			ALog.Out ("VLF: " + sb.ToString ());

			View.TranslatesAutoresizingMaskIntoConstraints = false;

			var cs = NSLayoutConstraint.FromVisualFormat(sb.ToString(), NSLayoutFormatOptions.DirectionLeadingToTrailing, null, dict);
			cs.Activate();
		}
	}
}

