﻿using System;
using CoreFoundation;

namespace Utilzr.Xamarin.Cocoa
{
    public static class GCDExtensions
    {
        public static void After(this DispatchQueue queue, TimeSpan t, Action a)
        {
            queue.DispatchAfter(new DispatchTime(DispatchTime.Now, t), a);
        }
    }
}
