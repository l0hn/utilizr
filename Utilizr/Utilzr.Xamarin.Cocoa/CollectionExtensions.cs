﻿using System;
using Foundation;
using System.Collections.Generic;

namespace Utilzr.Xamarin.Cocoa
{
    public static class CollectionExtensions
    {

        public static NSDictionary<K, V> ToNSDictionary<K,V>(this Dictionary<K, V> dictionary) where K : NSObject where V: NSObject
        {
            if (dictionary == null)
            {
                return null;
            }
            var mut = new NSMutableDictionary<K, V>();
            foreach (var item in dictionary)
            {
                mut.Add(item.Key, item.Value);
            }
            return mut as NSDictionary<K,V>;
        }


		public static NSDictionary<NSString, NSString> ToNSDictionary(this Dictionary<string, string> dictionary)
		{
			if (dictionary == null)
			{
				return null;
			}
			var mut = new NSMutableDictionary<NSString, NSString>();
			foreach (var item in dictionary)
			{
                mut.Add(new NSString(item.Key), new NSString(item.Value));
			}
            return new NSDictionary<NSString, NSString>(mut.Keys, mut.Values);
		}

    }
}
