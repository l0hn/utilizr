﻿using Foundation;

using System.Linq;
using CoreGraphics;
using System.Collections.Generic;

#if __IOS__
using NSView = UIKit.UIView;
using UIKit;
#else
using AppKit;
using Util;
#endif

namespace Extensions
{
	public static class ViewExtensions
	{


		public static void ApplyCenterAlignVertical(this NSView v, float offset = 0, NSView other = null)
		{
			if (other == null) other = v.Superview;
			v.Superview.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, other, NSLayoutAttribute.CenterY, 1, offset));
		}

		public static void ApplyCenterAlignHorizontal(this NSView v, float offset = 0, NSView other = null)
		{
			if (other == null) other = v.Superview;
			v.Superview.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, other, NSLayoutAttribute.CenterX, 1, offset));
		}


		public static void ApplyHConstraint(this NSView v, string vlf, string key)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			var cs = NSLayoutConstraint.FromVisualFormat(vlf, NSLayoutFormatOptions.DirectionLeadingToTrailing, null, new NSDictionary(key, v));
			cs.Activate();
		}

		public static void ApplyHConstraint(this NSView v, string vlf, string key, NSView other, string otherKey)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			var cs = NSLayoutConstraint.FromVisualFormat(vlf, NSLayoutFormatOptions.DirectionLeadingToTrailing, null, new NSDictionary(key, v, otherKey, other));
			cs.Activate();
		}

		public static void Activate(this NSLayoutConstraint[] constraints)
		{
#if __IOS__
            if (UIDevice.CurrentDevice.CheckSystemVersion (8,0))
#else
			if (VersionHelper.IsYosemiteOrHigher())
#endif
			{
				NSLayoutConstraint.ActivateConstraints(constraints);
			}
			else
			{
				foreach (var item in constraints)
				{
					//item.Relation
					var anc = item.CommonAncestorViewOrDefault();
					if (anc != null)
					{
						anc.AddConstraint(item);
					}
				}
			}
		}

		public static void Deactivate(this NSLayoutConstraint[] constraints)
		{
#if __IOS__
			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
#else
            if (VersionHelper.IsYosemiteOrHigher())
#endif
				{
				NSLayoutConstraint.DeactivateConstraints(constraints);
			}
			else
			{
				foreach (var item in constraints)
				{
					//item.Relation
					var anc = item.CommonAncestorViewOrDefault();
					if (anc != null)
					{
						anc.RemoveConstraint(item);
					}
				}
			}
		}

		public static void Activate(this NSLayoutConstraint constraint)
		{
            Activate(new NSLayoutConstraint[] { constraint });
        }

		public static void Deactivate(this NSLayoutConstraint constraint)
		{
            Deactivate(new NSLayoutConstraint[] { constraint });
		}

		private static NSView CommonAncestorViewOrDefault(this NSLayoutConstraint constraint)
		{
			NSView common = null;

			var firstItem = constraint.FirstItem as NSView;
			var secondItem = constraint.SecondItem as NSView;

			if (constraint.SecondItem == null || constraint.FirstItem == constraint.SecondItem)
			{
				common = firstItem;
			}
			else if (firstItem.Superview == secondItem.Superview)
			{
				common = firstItem.Superview;
			}
			else
			{
				var fSuperview = firstItem.SuperviewsForView();
				var sSuperview = secondItem.SuperviewsForView();

				foreach (var item in fSuperview)
				{
					if (sSuperview.Contains(item))
					{
						common = item;
						break;
					}
				}
			}

			return common;
		}

		private static NSView[] SuperviewsForView(this NSView view)
		{
			var superviews = new List<NSView>();

			var currentView = view;
			do
			{
				superviews.Add(currentView);
				currentView = currentView.Superview;
			} while (currentView != null);

			return superviews.ToArray();
		}

		public enum Edge
		{
			Left, Right, Top, Bottom, CenterX, CenterY
		}

		public static void CenterInH(this NSView v, NSView inside, float offset = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			inside.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, inside, NSLayoutAttribute.CenterX, 1, offset));
		}

        public static void CollapseVertically(this NSView v)
        {

            v.Hidden = true;
            var h = v.GetHeightConstraint();
            if (h == null)
            {
                h = NSLayoutConstraint.Create(v, NSLayoutAttribute.Height, NSLayoutRelation.Equal, 1, 0);
                v.AddConstraint(h);
            }
            else
            {
                h.Constant = 0;
            }

            var top = v.GetTopConstraint();
			if (top != null)
			{
				top.Constant = 0;
			}

            var bottom = v.GetBottomConstraint();
			if (bottom != null)
			{
				bottom.Constant = 0;
			}
        }

		public static void Align(this NSView v, NSView other, Edge edge, float offset = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;
			NSLayoutAttribute attr;
			switch (edge)
			{
				case Edge.Left:
					attr = NSLayoutAttribute.Left;
					break;
				case Edge.Right:
					attr = NSLayoutAttribute.Right;
					break;
				case Edge.Top:
					attr = NSLayoutAttribute.Top;
					break;
				case Edge.Bottom:
					attr = NSLayoutAttribute.Bottom;
					break;
				case Edge.CenterX:
					attr = NSLayoutAttribute.CenterX;
					break;
                case Edge.CenterY:
					attr = NSLayoutAttribute.CenterY;
					break;
				default:
					attr = NSLayoutAttribute.Left;
					break;
			}

			var constraint = NSLayoutConstraint.Create(other, attr, NSLayoutRelation.Equal, v, attr, 1, offset);
			v.Superview?.AddConstraint(constraint);

		}

		public static void AlignEdges(this NSView v, NSView other, float offset = 0)
		{

			Align(v, other, Edge.Bottom, offset);
			Align(v, other, Edge.Top, offset);
			Align(v, other, Edge.Left, offset);
			Align(v, other, Edge.Right, offset);

		}

		public static void CenterInV(this NSView v, NSView inside, float offset = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			inside.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, inside, NSLayoutAttribute.CenterY, 1, offset));
		}

		public static void ApplyWidthConstraint(this NSView v, float width, float multiplier = 1)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			v.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, multiplier, width));
		}


        public static void ApplyMaxWidthConstraint(this NSView v, float width, float multiplier = 1)
        {
            v.TranslatesAutoresizingMaskIntoConstraints = false;

            v.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.Width, NSLayoutRelation.LessThanOrEqual, null, NSLayoutAttribute.NoAttribute, multiplier, width));
        }

        public static NSLayoutConstraint ApplyHeightConstraint(this NSView v, float height, float multiplier = 1)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

            var constr = NSLayoutConstraint.Create(v, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, multiplier, height);

            v.AddConstraint(constr) ;
            return constr;
		}


        public static void ApplyProportionalHeightConstraint(this NSView v, NSView other, float multiplier)
        {
            v.TranslatesAutoresizingMaskIntoConstraints = false;

#if IOS
			var descendent = v.IsDescendantOfView(other);
#else
            var descendent = v.IsDescendantOf(other);
#endif

			if (descendent)
            {
                //swap order if v is a descendant of other
                var tmp = other;
                other = v;
                v = tmp;
            }

            var constraint = NSLayoutConstraint.Create(other, NSLayoutAttribute.Height, NSLayoutRelation.Equal, v, NSLayoutAttribute.Height, multiplier, 0);
            Activate(new[] { constraint });

        }


        public static NSLayoutConstraint GetHeightConstraint(this NSView v, bool ignoreAutoGenerated = true)
		{
            //need to filter by shouldBeArchived as system generated (un-archivable) constraint can also match the predicate
			return v.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.Height && (!ignoreAutoGenerated || a.ShouldBeArchived == true) );
		}

		public static NSLayoutConstraint GetLeadingConstraint(this NSView v)
		{
			return v.Superview?.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.Leading && a.FirstItem == v) ?? v.Superview.Constraints.FirstOrDefault(a => a.SecondAttribute == NSLayoutAttribute.Leading && a.SecondItem == v);
		}

		public static NSLayoutConstraint GetCenterYConstraint(this NSView v)
		{
			return v.Superview?.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.CenterY && a.FirstItem == v) ?? v.Superview.Constraints.FirstOrDefault(a => a.SecondAttribute == NSLayoutAttribute.CenterY && a.SecondItem == v);
		}

		public static NSLayoutConstraint GetTopConstraint(this NSView v)
		{
			return v.Superview?.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.Top && a.FirstItem == v) ?? v.Superview.Constraints.FirstOrDefault(a => a.SecondAttribute == NSLayoutAttribute.Top && a.SecondItem == v);
		}


		public static NSLayoutConstraint GetBottomConstraint(this NSView v)
		{
			return v.Superview?.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.Bottom && a.FirstItem == v) ?? v.Superview.Constraints.FirstOrDefault(a => a.SecondAttribute == NSLayoutAttribute.Bottom && a.SecondItem == v);
		}


		public static NSLayoutConstraint GetWidthConstraint(this NSView v)
		{
			return v.Constraints.FirstOrDefault(a => a.FirstAttribute == NSLayoutAttribute.Width);
		}


		public static NSLayoutConstraint ApplyWidthConstraint(this NSView v, NSView other, float multiplier = 1, float constant = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			var constraint = NSLayoutConstraint.Create(v, NSLayoutAttribute.Width, NSLayoutRelation.Equal, other, NSLayoutAttribute.Width, multiplier, constant);
			Activate(new[] { constraint });
            return constraint;
		}

		public static void ApplyLeadingToWidthConstraint(this NSView v, NSView other, float multiplier = 1, float constant = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

			other.AddConstraint(NSLayoutConstraint.Create(v, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, other, NSLayoutAttribute.Width, multiplier, constant));
		}

		public static NSLayoutConstraint ApplyHeightConstraint(this NSView v, NSView other, float multiplier = 1, float constant = 0)
		{
			v.TranslatesAutoresizingMaskIntoConstraints = false;

            var constraint = NSLayoutConstraint.Create(v, NSLayoutAttribute.Height, NSLayoutRelation.Equal, other, NSLayoutAttribute.Height, multiplier, constant);

            other.AddConstraint(constraint);

            return constraint;
		}

		public static void ApplyVConstraint(this NSView v, string vlf, string key)
		{
			if (!vlf.StartsWith("V:"))
			{
				vlf = "V:" + vlf;
			}
			v.TranslatesAutoresizingMaskIntoConstraints = false;
			var cs = NSLayoutConstraint.FromVisualFormat(vlf, NSLayoutFormatOptions.DirectionLeadingToTrailing, null, new NSDictionary(key, v));
			cs.Activate();
		}

		public static void ApplyVConstraint(this NSView v, string vlf, string key, NSView other, string otherKey)
		{
			if (!vlf.StartsWith("V:"))
			{
				vlf = "V:" + vlf;
			}
			v.TranslatesAutoresizingMaskIntoConstraints = false;
			var cs = NSLayoutConstraint.FromVisualFormat(vlf, NSLayoutFormatOptions.DirectionLeadingToTrailing, null, new NSDictionary(key, v, otherKey, other));
			cs.Activate();
		}

		public static void ApplyVConstraint(this NSView v, string vlf, NSDictionary keys)
		{
			if (!vlf.StartsWith("V:"))
			{
				vlf = "V:" + vlf;
			}
			v.TranslatesAutoresizingMaskIntoConstraints = false;
			var cs = NSLayoutConstraint.FromVisualFormat(vlf, NSLayoutFormatOptions.DirectionLeadingToTrailing, null, keys);
			cs.Activate();
		}

		public static void RemoveAllSubviews(this NSView v)
		{
			foreach (var sub in v.Subviews)
			{
				sub.RemoveFromSuperview();
			}
		}

		public static void FixAnchorPoint(this NSView view, CGPoint anchor)
		{
            if (view.Frame == CGRect.Empty) return; //don't adjust an empty frame

			var layer = view.Layer;
			var bounds = view.Bounds;

			CGPoint anchorPoint = anchor;
			CGPoint newPoint = new CGPoint(bounds.Size.Width * anchorPoint.X,
										   bounds.Size.Height * anchorPoint.Y);
			CGPoint oldPoint = new CGPoint(bounds.Size.Width * layer.AnchorPoint.X,
										   bounds.Size.Height * layer.AnchorPoint.Y);

			newPoint = layer.AffineTransform.TransformPoint(newPoint);
			oldPoint = layer.AffineTransform.TransformPoint(oldPoint);

			CGPoint position = layer.Position;

			position.X -= oldPoint.X;
			position.X += newPoint.X;

			position.Y -= oldPoint.Y;
			position.Y += newPoint.Y;

			layer.Position = position;
			layer.AnchorPoint = anchorPoint;
		}


	}
}

