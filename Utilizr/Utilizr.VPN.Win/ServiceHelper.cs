﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using Utilizr.Info;
using Utilizr.Logging;

namespace Utilizr.VPN.Win
{
    public static class ServiceUtil
    {
        public static void StartWindowsService(bool forceInstall = false)
        {
            //check installed
            if (!IsInstalled() || forceInstall)
            {
                var procPath = Path.Combine(AppInfo.AppDirectory, "OVPN.Service.exe");
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                var runas = !principal.IsInRole(WindowsBuiltInRole.Administrator);
                var installResult = Shell.Exec($"\"{procPath}\"", null, runas, "--install");
                Log.Info("SERVICE_INSTALL", $"service installer exited with code {installResult.ExitCode}");
            }

            var controller = new ServiceController("OVPNService");
            if (controller.Status != ServiceControllerStatus.Running)
            {
                controller.Start();
                controller.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
            }
        }

        public static void StopWindowsService()
        {
            try
            {
                if (!IsInstalled())
                {
                    return;
                }
                var controller = new ServiceController("OVPNService");
                controller.Stop();
                controller.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

        }

        static bool IsInstalled()
        {
            foreach (var serviceController in ServiceController.GetServices())
            {
                if (serviceController.ServiceName == "OVPNService")
                {
                    return true;
                }
            }
            return false;
        }
    }
}
