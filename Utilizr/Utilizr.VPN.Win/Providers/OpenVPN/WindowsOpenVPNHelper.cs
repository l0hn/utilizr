﻿using System;
using System.Collections.Generic;
using System.Text;
using GetText;
using Utilizr;
using Utilizr.Logging;
using Utilizr.OpenVPN;
using Utilizr.OpenVPN.ipc;
using Utilizr.VPN;
using Utilizr.IPC;


namespace Utilizr.VPN.Win
{
    public class WindowsOpenVPNHelper : AbstractOpenVPNHelper
    {        
        private ClientContainer<IWCFService> _wcfClientContainer;

        public WindowsOpenVPNHelper() : base()
        {
            _wcfClientContainer = new ClientContainer<IWCFService>(WCFService.ADDRESS);
        }

        private void SetupWCFClient()
        {
            ServiceUtil.StartWindowsService();

        }

        protected override void ConnectWorkWithCA(string host, string caFilePath, string mode, int ovpnPort, ConnectWorkCallback callback)
        {
            int port = -1;
            Exception exception = null;

            try
            {
                //don't start service in debug, launch it manually from VS with '--run' arg
#if !DEBUG
                SetupWCFClient();
#endif

                port = _wcfClientContainer.Client.RunOpenVPN(host, caFilePath, mode, pingTimeout: 60);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            callback?.Invoke(new CallbackHandlerArgs() {Port = port}, exception);
        }

        
      

        protected override void ConnectWorkWithConfig(string host, string configFile, string protocol, int ovpnPort, ConnectWorkCallback callback)
        {
            int port = -1;
            Exception exception = null;

            try
            {
                //don't start service in debug, launch it manually from VS with '--run' arg
#if !DEBUG
                SetupWCFClient();
#endif

                port = _wcfClientContainer.Client.RunOpenVPNWithConfigFile(host, configFile, protocol, port);
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            callback?.Invoke(new CallbackHandlerArgs() {Port = port}, exception);
        }


        protected override void KillAllOpenVPNConnectionsWork(KillConnectionCallback callback)
        {
            Exception exception = null;

            try
            {
                _wcfClientContainer.Client.StopOpenVPN();
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            callback?.Invoke(exception);
        }

        protected override void OnManagementClientFatal(object sender, MessageArgs args)
        {
            Log.Error("OPEN_VPN", args.Message);

            if (args.Message.ToLower().Contains("there are no tap-windows adapters on this system"))
            {
                OnTapDriverInstallationRequired();
            }
            else
            {
                OnFatalError(args);
            }
        }

        
    }
}
