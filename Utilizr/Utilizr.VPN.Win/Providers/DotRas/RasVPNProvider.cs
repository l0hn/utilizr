﻿using System;
using System.Collections.Generic;
using System.Text;
using DotRas;
using System.Threading;
using Utilizr.Logging;
using System.Linq;
using GetText;
using Utilizr.Async;
using System.Net;
using Utilizr.Info;

namespace Utilizr.VPN.Win.Providers
{

    public class RasVPNProvider : IVPNProvider
    {
        RasPhoneBook _book;
        RasDialer _dialer;
        RasDevice _device;
        RasConnectionWatcher _watcher;
        RasConnection _currentConnection;

        RasHandle _currentHandle;

        string _currentServer;
        private string _connectionName;
        //
        private string _phoneBookPath =
            Environment.ExpandEnvironmentVariables(
                @"%appdata%\Microsoft\Network\Connections\Pbk\rasphone.pbk");

        private AutoResetEvent _rasDialerDone;

        private UserPassHandler _userPassHandler;
        private L2TPSecretHandler _l2TpSecretHandler;

        public event ConnectionStateHandler Connecting;
        public event ConnectionStateHandler Connected;
        public event ConnectionStateHandler Disconnecting;
        public event ConnectionStateHandler Disconnected;
        public event ConnectionStateHandler ConnectError;
        //TODO implement bandwidth updates for rasprovider?
        public event EventHandler<BandwidthUsage> BandwidthUsageUpdated;
        public event EventHandler TapDriverInstallationRequired;

        public RasVPNProvider(string connectionName, L2TPSecretHandler lt2pSecretHandler)
        {
            RasLoader.HookAssemblyResolve();
            _l2TpSecretHandler = lt2pSecretHandler;
            _connectionName = connectionName;
        }

        public IEnumerable<ConnectionType> GetAvailableProtocols()
        {
            var available = new List<ConnectionType>();
            available.Add(ConnectionType.PPTP);
            available.Add(ConnectionType.L2TP_IPSEC);
            if (Platform.OSVersion >= Info.OperatingSystem.Win7)
            {
                available.Add(ConnectionType.IKEV2);
            }

            return available;
        }

        public void Initialize(UserPassHandler userPass)
        {
            _userPassHandler = userPass;

            _device = RasDevice.Create(_connectionName, RasDeviceType.Vpn);
            _book = new RasPhoneBook();
            //_book.EnableFileWatcher = true;
            Log.Info("VPN_CONTROLLER", "Opening phonebook {0}", _phoneBookPath);
            _book.Open(_phoneBookPath);
            _book.EnableFileWatcher = true;
            _rasDialerDone = new AutoResetEvent(false);
            _dialer = new RasDialer();
            _dialer.PhoneBookPath = _phoneBookPath;


            //check for active connections and set _vpnserver
            checkForActiveConnection();

            //explicit timout of 1 minute to dial
            _dialer.Timeout = 60 * 1000;

            _dialer.StateChanged += (sender, args) =>
            {
                Log.Objects(LoggingLevel.INFO, "DIALER", new object[] { args }, "State changed");
                if (args.ErrorCode != 0 || !args.ErrorMessage.IsNullOrEmpty())
                {
                    Log.Error("VPN_CONNECT", args.ErrorMessage ?? L._("An error occured during the connection attempt (Error: {0})", args.ErrorCode));
                    OnConnectionError(new RasDialException(args.ErrorMessage ?? L._("An error occured during the connection attempt (Error: {0})", args.ErrorCode)));
                    OnDisconnected(_currentServer);
                    _rasDialerDone.Set();
                }
            };
            _dialer.Error += (sender, args) =>
            {
                _rasDialerDone.Set();
                var ex = args.GetException();
                Log.Exception("VPN_CONNECT", ex);
                OnConnectionError(ex);
                OnDisconnected(_currentServer);
            };
            _dialer.DialCompleted += (sender, args) =>
            {
                if (args.TimedOut)
                {
                    Log.Exception("VPN_CONNECT", args.Error);
                    OnConnectionError(args.Error);
                    OnDisconnected(_currentServer);
                    return;
                }

                if (args.Error != null)
                {
                    return;
                }
                OnConnected();
                Sleeper.Sleep(200);
                _rasDialerDone.Set();
            };
        }

        protected virtual void OnConnectionError(Exception error, object userContext = null)
        {
            ConnectError?.Invoke(this, _currentServer, error, userContext);
        }

        protected virtual void OnDisconnected(string host, object userContext = null)
        {
            Disconnected?.Invoke(this, host, null, userContext);
        }

        protected virtual void OnConnected()
        {
            Connected?.Invoke(this, _currentServer, null);
        }

        protected virtual void OnConnecting(string host, object userContext = null)
        {
            Connecting?.Invoke(this, host, null, userContext);
        }

        protected virtual void OnDisconnecting()
        {
            Disconnecting?.Invoke(this, _currentServer, null);
        }

        List<RasConnection> getActiveConnections()
        {
            try
            {
                var connections = RasConnection.GetActiveConnections().Where(i => i.EntryName == _connectionName);
                return connections.ToList();

            }
            catch (Exception)
            {
                return new List<RasConnection>();
            }
        }

        public bool IsConnected
        {
            get
            {
                try
                {

                    return _currentConnection?.GetConnectionStatus().ConnectionState == RasConnectionState.Connected;
                }
                catch (Exception)
                {
                    _currentConnection = null;
                }
                return false;
            }
        }

        void checkForActiveConnection()
        {
            try
            {
                _currentConnection = getActiveConnections().FirstOrDefault();
                if (_currentConnection != null)
                {

                    _currentHandle = _currentConnection.Handle;

                    var entry = _book.Entries[_currentConnection.EntryName];
                    var hostName = entry.PhoneNumber;
                    _currentServer = hostName;
                    setupConnectionWatcher();
                    if (_currentConnection.GetConnectionStatus().ConnectionState == RasConnectionState.Connected)
                    {
                        OnConnected();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception("VPN_CONTROLLER", ex);
            }
        }

        public string CurrentServer => _currentServer;

        RasLinkStatistics SafeGetLinkStatistics()
        {
            if (_currentConnection == null)
            {
                return null;
            }
            try
            {
                return _currentConnection.GetConnectionStatistics();
            }
            catch (Exception ex)
            {
                Log.Exception("VPN_CONTROLLER", ex);
                _currentConnection = null;
            }
            return null;
        }

        public TimeSpan ConnectedDuration
        {
            get
            {
                var stats = SafeGetLinkStatistics();
                return stats?.ConnectionDuration ?? TimeSpan.Zero;
            }
        }

        private BandwidthUsage _usage = new BandwidthUsage();
        public BandwidthUsage Usage
        {
            get
            {

                var stats = SafeGetLinkStatistics();
                if (stats == null)
                {
                    return new BandwidthUsage();
                }
                _usage.Update (
                        stats.BytesTransmitted,
                        stats.BytesReceived
                    );
                return _usage;
            }
        }

        void clearJDIEntriesFromPhonebook()
        {
            Log.Info("VPN_CONTROLLER", "Hanging up existing connections");
            Disconnect();
            try
            {
                if (_book.Entries.Count > 0)
                {
                    for (int i = _book.Entries.Count - 1; i >= 0; i--)
                    {
                        if (_book.Entries[i].Name == _connectionName)
                        {
                            _book.Entries[i].Remove();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception("VPN_CONTROLLER", ex);
            }
        }

        void setupConnectionWatcher()
        {
            Log.Info("VPN_CONTROLLER", "Settings up connection watcher");
            if (_watcher != null)
            {
                _watcher.Dispose();
            }
            _watcher = new RasConnectionWatcher();
            _watcher.Handle = _currentHandle;
            _watcher.Connected += (sender, args) =>
            {
                if (args.Connection.EntryName != _connectionName)
                {
                    return;
                }
                _currentConnection = args.Connection;
            };
            var server = _currentServer;
            _watcher.Disconnected += (sender, args) =>
            {
                if (args.Connection.EntryName != _connectionName)
                {
                    return;
                }
                OnDisconnected(server);
            };

            _watcher.EnableRaisingEvents = true;
        }

        void dialAsync()
        {
            if (_dialer.IsBusy)
            {
                _dialer.DialAsyncCancel();
            }
            _currentConnection = null;
            if (_currentHandle != null)
            {
                _currentHandle.Dispose();
                _currentHandle = null;
            }
            _rasDialerDone.Reset();
            _currentHandle = _dialer.DialAsync();
            setupConnectionWatcher();
        }


        IAsyncResult createEntryAsync(string host, RasVpnStrategy connectionType, ConnectionType reportedConnectionType, bool dial = true, object userContext = null)
        {
            Log.Info("VPN_CONTROLLER", "Creating {0} ras entry for {1}", connectionType, host);
            return AsyncHelper.BeginExecute(
                () => createEntry(host, connectionType, reportedConnectionType, dial),
                (o) =>
                {
                    try
                    {
                        AsyncHelper.EndExecute(o);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception("VPN_CONTROLLER", ex);
                        OnConnectionError(ex);
                    }
                }
            );
        }

        void createEntry(string host, RasVpnStrategy connectionType, ConnectionType reportedConnectionType, bool dial = true, object userContext = null)
        {
            clearJDIEntriesFromPhonebook();

            var newEntry = _book.Entries.FirstOrDefault(i => i.Name == _connectionName);
            if (newEntry == null)
            {
                newEntry = RasEntry.CreateVpnEntry(_connectionName, host, connectionType, _device);
            }

            newEntry.VpnStrategy = connectionType;
            newEntry.Device = _device;
            newEntry.PhoneNumber = host;

            newEntry.Options.UsePreSharedKey = connectionType == RasVpnStrategy.L2tpOnly;

            newEntry.Options.UseLogOnCredentials = false;
            newEntry.RedialCount = 0;
            newEntry.RedialPause = 0;
            Log.Info("VPN_CONTROLLER", "Setting ipv6 ras option");

            //set ipv6 with reflection to prevent missing method on xp

            try
            {
                var ipv6Prop = typeof(RasNetworkProtocols).GetProperty("IPv6");
                ipv6Prop?.SetValue(newEntry.NetworkProtocols, false, null);
            }
            catch (Exception ex)
            {
                Log.Exception("VPN_CONTROLLER", ex);
            }


            newEntry.Options.RequireDataEncryption = true;
            newEntry.Options.SecureFileAndPrint = false;
            newEntry.NetworkProtocols.Ipx = false;
            newEntry.Options.SecureClientForMSNet = false;

            var userPass = _userPassHandler.Invoke(reportedConnectionType);

            var credentials = new NetworkCredential(userPass.Username, userPass.Password.ToUnsecureString());
            try
            {
                if (!_book.Entries.Contains(_connectionName))
                {
                    _book.Entries.Add(newEntry);
                }
            }
            catch (Exception ex)
            {
                Log.Exception("VPN_CONTROLLER", ex);
            }

            newEntry.Update();
            if (connectionType == RasVpnStrategy.L2tpOnly)
            {
                newEntry.UpdateCredentials(RasPreSharedKey.Client, _l2TpSecretHandler.Invoke());
            }


            newEntry.UpdateCredentials(credentials);
            newEntry.Update();
            _dialer.Credentials = credentials;
            _dialer.EntryName = newEntry.Name;
            if (dial)
            {
                OnConnecting(host, userContext);
                dialAsync();
                _rasDialerDone.WaitOne();
            }
        }



        public void Disconnect()
        {
            try
            {
                var con = RasConnection.GetActiveConnections().FirstOrDefault(i => i.EntryName == _connectionName);
                if (con != null && con.GetConnectionStatus().ConnectionState != RasConnectionState.Disconnected)
                {
                    OnDisconnecting();
                    con.HangUp();
                }
                return;
            }
            catch (Exception ex)
            {
                Log.Exception("VNP_CONTROLLER", ex);
                throw;
            }
        }

        public void Abort()
        {
            throw new NotImplementedException(); // todo
        }

        public void Dispose()
        {
            _dialer?.DialAsyncCancel();
            _book?.Dispose();
            _currentHandle?.Dispose();
            _dialer?.Dispose();
            _watcher?.Dispose();
        }

        public void Connect(IConnectionStartParams startParams)
        {
            _usage.Reset();
            _currentServer = startParams.Hostname;
            createEntry(startParams.Hostname, startParams.ConnectionType.ToRasVpnStrategy(), startParams.ConnectionType, true, startParams.Context);
        }
    }
}
