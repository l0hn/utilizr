﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using DotRas;

namespace Utilizr.VPN.Win
{
    public static class RasExtensions
    {
        public static RasVpnStrategy ToRasVpnStrategy(this ConnectionType connectionType)
        {
            switch (connectionType)
            {
                case ConnectionType.IKEV2:
                    return RasVpnStrategy.IkeV2Only;
                case ConnectionType.L2TP_IPSEC:
                    return RasVpnStrategy.L2tpOnly;
                case ConnectionType.PPTP:
                    return RasVpnStrategy.PptpOnly;
                case ConnectionType.SSTP:
                    return RasVpnStrategy.IkeV2Only;
            }
            return RasVpnStrategy.Default;
        }
    }
}
