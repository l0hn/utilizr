﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Utilizr.FileSystem;
using Utilizr.Info;
using OperatingSystem = Utilizr.Info.OperatingSystem;

namespace Utilizr.VPN.Win
{
    public static class RasLoader
    {
        public static void HookAssemblyResolve()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                if (args.Name.ToLower().Contains("dotras"))
                {
                    return LoadRasDLL();
                }
                return null;
            };
        }

        private static Assembly LoadRasDLL()
        {
            var rasPath = PathHelper.Combine(AppInfo.AppDirectory, "Providers", "DotRas", "Lib");
            var dllName = "DotRas.Win8.dll";
            switch (Utilizr.Info.Platform.OSVersion)
            {
                case OperatingSystem.XP:
                    dllName = "DotRas.XP.dll";
                    break;
                case OperatingSystem.Vista:
                    dllName = "DotRas.Win2K8.dll";
                    break;
                case OperatingSystem.Win7:
                    dllName = "DotRas.Win7.dll";
                    break;
            }

            var fullPath = Path.Combine(rasPath, dllName);
            Console.WriteLine($"Loading {fullPath}");
            var bytes = File.ReadAllBytes(fullPath);
            return Assembly.Load(bytes);
        }
    }
}
