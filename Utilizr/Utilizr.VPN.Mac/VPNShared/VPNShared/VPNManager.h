//
//  VPNManager.h
//  
//
//  Created by Paul Wiseman on 27/11/2015.
//
//

#import <SystemConfiguration/SystemConfiguration.h>
#import <Foundation/Foundation.h>
#import "VPNEnum.h"

@interface VPNManager : NSObject

// create and delete
+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType 
                          called:(NSString *)connectionName
                       withError:(NSError **)error;

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                withSharedSecret:(NSString *)sharedSecret
                          called:(NSString *)connectionName
                       withError:(NSError **)error;

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                    withOverride:(BOOL)override_
                          called:(NSString *)connectionName
                       withError:(NSError **)error;

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                    withOverride:(BOOL)override_
                withSharedSecret:(NSString *)sharedSecret
                          called:(NSString *)connectionName
                       withError:(NSError **)error;

+(BOOL)deleteConnectionWithName:(NSString*)connectionName withError:(NSError **)error;

// connect and disconnect
+(BOOL)connectServiceWithName:(NSString *)serviceName withCallback:(SCNetworkConnectionCallBack)callback withError:(NSError **)error;
+(BOOL)connectServiceWithName:(NSString *)serviceName withError:(NSError **)error;
+(BOOL)disconnectServiceWithName:(NSString *)serviceName withError:(NSError **)error;
+(BOOL)disconnectServiceWithName:(NSString *)serviceName withForceDisconnect:(BOOL)forceDisconnect withError:(NSError **)error;

// attach callback
+(BOOL)addCallback:(SCNetworkConnectionCallBack)callback ToConnectionWithName:(NSString *)connectionName withError:(NSError **)error;
+(BOOL)addCallback:(SCNetworkConnectionCallBack)callback ToConnectionWithName:(NSString *)connectionName withContext:(SCNetworkConnectionContext*)context withError:(NSError **)error;
//+ (void)removeCallbackForConnectionName:(NSString *)connectionName withCallback:(SCNetworkConnectionCallBack)callback withContext:(SCNetworkConnectionContext*)context ;

// get status
+ (NSString *)getConnectionHostnameForConnectionWithName:(NSString*)connectionName withError:(NSError **)error;
+(SCNetworkConnectionStatus) getConnectionStatusForConnectionWithName:(NSString*)connectionName withError:(NSError **)error;
+ (NSDate *) getConnectionStartTimeForConnectionWithName:(NSString*)connectionName withError:(NSError **)error;

// "private" methods
+(void)setError:(NSError **)error withCode:(int)code andReason:(NSString *)reason;
+ (SCNetworkServiceRef) createServiceRefWithServiceName:(NSString *)serviceName;
+ (SCNetworkServiceRef) createServiceRefWithServiceName:(NSString *)serviceName andPrefs:(SCPreferencesRef)prefs;

+(CFStringRef) createServiceIdWithServiceName:(NSString *)serviceName;

@end
