//
//  VPNKeychain.h
//  vpn_connect_test
//
//  Created by John Haines on 16/11/2015.
//  Copyright © 2015 jdi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VPNKeychain : NSObject
+ (int) createPasswordKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account andPassword:(NSString*)password;;
+ (int) createSharedSecretKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account withPassword:(NSString*)password;
+ (int) createXAuthKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account withPassword:(NSString*)password;

@end
