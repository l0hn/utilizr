//
//  VPNConfig.m
//  
//
//  Created by Paul Wiseman on 27/11/2015.
//
//

#import <SystemConfiguration/SystemConfiguration.h>
#import "VPNConfig.h"

@implementation VPNConfig

+(CFDictionaryRef) getConfigForConnectionType:(VPNConnectionType)connectionType
                                     withHost:(NSString *)host
                                 withUsername:(NSString *)username
                                 withPassword:(NSString *)password
                               withIdentifier:(NSString *)identifier
                             withSharedSecret:(NSString *)sharedSecret
                                withServiceId:(NSString *) serviceId{
    CFDictionaryRef config;
    switch (connectionType) {
        case kVPNConnectionTypePPTP: {
            CFStringRef keys[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
            CFStringRef vals[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
            CFIndex count = 0;
            
            keys[count] = kSCPropNetPPPCommRemoteAddress;
            vals[count++] = (__bridge CFStringRef)(host);
            
            keys[count] = kSCPropNetPPPAuthName;
            vals[count++] = (__bridge CFStringRef)(username);
            
            keys[count] = kSCPropNetPPPAuthPassword;
            vals[count++] = (__bridge CFStringRef)(serviceId);
            
            keys[count] = kSCPropNetPPPAuthPasswordEncryption;
            vals[count++] = kSCValNetPPPAuthPasswordEncryptionKeychain;
            
            keys[count] = kSCPropNetPPPAuthProtocol;
            vals[count++] = kSCValNetPPPAuthProtocolPAP;
            
            keys[count] = kSCPropNetPPPCCPEnabled;
            vals[count++] = CFSTR("1");
            
            keys[count] = kSCPropNetPPPCCPMPPE128Enabled;
            vals[count++] = CFSTR("1");
            
            keys[count] = kSCPropNetPPPCCPMPPE40Enabled;
            vals[count++] = CFSTR("1");
            
            config = CFDictionaryCreate(NULL, (const void **)&keys, (const void **)&vals, count, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
            break;
        }
        case kVPNConnectionTypeCiscoIPSEC: {
            CFStringRef keys[9] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
            CFStringRef vals[9] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
            CFIndex count = 0;
            
            keys[count]   = kSCPropNetIPSecAuthenticationMethod;
            vals[count++] = kSCValNetIPSecAuthenticationMethodSharedSecret;
            
            keys[count]   = kSCPropNetIPSecSharedSecret;
            vals[count++] = (__bridge CFStringRef)([NSString stringWithFormat:@"%@.SS", serviceId]);
            
            keys[count]   = kSCPropNetIPSecSharedSecretEncryption;
            vals[count++] = kSCValNetIPSecSharedSecretEncryptionKeychain;
            
            keys[count]   = kSCPropNetIPSecRemoteAddress;
            vals[count++] = (__bridge CFStringRef)(host);
            
            keys[count]   = kSCPropNetIPSecXAuthName;
            vals[count++] = (__bridge CFStringRef)(username);
            
            keys[count]   = kSCPropNetIPSecXAuthPassword;
            vals[count++] = (__bridge CFStringRef)([NSString stringWithFormat:@"%@.XAUTH", serviceId]);
            
            keys[count]   = kSCPropNetIPSecXAuthPasswordEncryption;
            vals[count++] = kSCValNetIPSecXAuthPasswordEncryptionKeychain;
            
            keys[count]   = kSCPropNetIPSecLocalIdentifier;
            vals[count++] =  CFSTR("");
            
            keys[count]    = kSCPropNetIPSecLocalIdentifierType;
            vals[count++]  = kSCValNetIPSecLocalIdentifierTypeKeyID;
            
            config = CFDictionaryCreate(NULL, (const void **)&keys, (const void **)&vals, count, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        }
        default:
            break;
    }
    return config;
}

+(CFDictionaryRef) getIPv4ConfigWithConnectionType:(VPNConnectionType)connectionType {
    CFStringRef keys[5] = { NULL, NULL, NULL, NULL, NULL };
    CFStringRef vals[5] = { NULL, NULL, NULL, NULL, NULL };
    CFIndex count = 0;
    
    keys[count] = kSCPropNetIPv4ConfigMethod;
    vals[count++] = kSCValNetIPv4ConfigMethodPPP;
    
    int one = 1;
    keys[count] = kSCPropNetOverridePrimary;
    
    // X-Code warns on this (CFString VS. CFNumber), but it should not matter, CFNumber is the correct type I think, as you can verify in the resulting /Library/Preferences/SystemConfiguration/preferences.plist file.
    // See also https://developer.apple.com/library/prerelease/ios/documentation/CoreFoundation/Conceptual/CFPropertyLists/Articles/Numbers.html
#pragma clang diagnostic ignored "-Wincompatible-pointer-types"
    vals[count++] = CFNumberCreate(NULL, kCFNumberIntType, &one);
    
    return CFDictionaryCreate(NULL, (const void **)&keys, (const void **)&vals, count, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
}

@end
