//
//  VPNHelperStatusProtocol.h
//  VPNHelper
//
//  Created by Paul Wiseman on 23/12/2015.
//  Copyright © 2015 jdi. All rights reserved.
//

#import "VPNEnum.h"

@protocol VPNConnectionStatusProtocol

- (void) statusChanged: (VPNConnectionStatus)status;

@end
