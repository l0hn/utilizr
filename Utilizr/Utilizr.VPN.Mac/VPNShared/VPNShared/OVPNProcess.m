//
//  OVPNProcess.m
//
//
//  Created by Darren Hale on 21/03/2016.
//
//

#import "OVPNProcess.h"

@interface OVPNProcess ()
@property(nonatomic, copy, readwrite) NSString * host;
@property(nonatomic, assign, readwrite) int availablePort;
@property(nonatomic, strong) NSMutableArray * args;
@property(nonatomic, strong) NSTask* task;
@property(nonatomic, strong, readwrite) NSError * terminationError;

@end

@implementation OVPNProcess

-(id)initWithHost:(NSString *)host
	 withUpScript:(NSString *)upScript
  withCertificate:(NSString *)certificateFile
         withMode:(NSString *)mode
  withPingTimeout:(int)timeout
withAvailablePort:(int)availablePort
     withOvpnFile:(NSString*)ovpnFile
        withError:(NSError **)error
		terminationHandler:(void (^)(NSInteger code))handler {

	self = [self initWithHost:host
				 withUpScript:upScript
				  openVpnFile:ovpnFile
						 args:@{
								@"--cipher": @"AES-256-CBC",
								@"--proto":mode,
								@"--ca": certificateFile,
								@"--ping-exit": [NSString stringWithFormat:@"%d", timeout],
								@"--ping": @"10"
								}
			withAvailablePort:availablePort
				   withPwFile:nil
					withError:error terminationHandler:handler];
    if(self) {
		
	}
    return self;
}

-(id)initWithHost:(NSString *)host
	 withUpScript:(NSString *)upScript
	   withConfig:(NSString *)configFile
withAvailablePort:(int)availablePort
	   withPwFile:(NSString * __nullable)passwordFile
	 withOvpnFile:(NSString *)ovpnFile
		withError:(NSError **)error
terminationHandler:(void (^)(NSInteger code))handler  {
	self = [self initWithHost:host
				 withUpScript:upScript
				  openVpnFile:ovpnFile
						 args:@{
								@"--config": [NSString stringWithFormat:@"%@", configFile],
								}
			withAvailablePort:availablePort
			withPwFile:passwordFile
					withError:error terminationHandler:handler];
	if (self) {

	}
	return self;
}

- (id)initWithHost:(NSString *)host
	  withUpScript:(NSString *)upScript
	   openVpnFile:(NSString *)binary
			  args:(NSDictionary *)customArgs
 withAvailablePort:(int)availablePort
		withPwFile:(NSString * __nullable)passwordFile
		 withError:(NSError **)error
terminationHandler:(void (^)(NSInteger code))handler {
	self = [super init];
	if (self) {
		self.host = host;
		self.availablePort = availablePort;
		
		NSMutableDictionary * args = [[NSMutableDictionary alloc] initWithDictionary:customArgs];
		
		// Args cannot contain any spaces. NSTask really doesn't like it.
		// Even if you surround with quotes!
		NSDictionary * mandatoryArgs = @{
									@"--client": @"",
									@"--remote": [self host],
									@"--nobind": @"",
									@"--dev": @"tun",
									@"--auth-nocache": @"",
									@"--script-security": @"3",
									@"--auth-user-pass": @"",
									@"--explicit-exit-notify": @"",
									@"--management-hold": @"",
									@"--management-signal": @"",
									@"--management-query-passwords": @"",
									@"--management-up-down": @"",
									@"--management-forget-disconnect": @"",
									@"--auth-retry": @"interact",
									@"--remap-usr1": @"SIGTERM" //if we don't do this, up/down scripts aren't run
									};
		
		NSDictionary * saneDefaults = @{
								    @"--port": @"1194",
								    @"--ping": @"10",
#if DEBUG
									@"--ping-exit": @"10",
#else
									@"--ping-exit": @"30",
#endif
									@"--resolv-retry": @"1", //try to resolve remote host for up to 30 secs (openvpn default is infinite...)
									@"--proto": @"udp",
//#if DEBUG
//									For debugging only
//									@"--log": @"~/Desktop/ovpn.log",
//#endif
									};
		
		for (NSString * key in [mandatoryArgs allKeys]) {
			[args setObject:mandatoryArgs[key] forKey:key];
		}
		
		for (NSString * key in [saneDefaults allKeys]) {
			if (![args objectForKey:key]) {
				[args setObject:saneDefaults[key] forKey:key];
			}
		}
		
		if ([upScript length]) {
			[args setObject:[NSString stringWithFormat:@"%@ up", upScript] forKey:@"--up"];
			[args setObject:[NSString stringWithFormat:@"%@ down", upScript]  forKey:@"--down"];
		}
		
		self.args = [NSMutableArray array];
		for (NSString * key in args) {
			if ([key length]) {
				[self.args addObject:key];
			}
			if ([args[key] length]) {
				[self.args addObject:args[key]];
			}
		}
		
		//openvpn complains if the following specified as "--management: localhost <port>" so split out in to 3 array items
		[self.args addObject:@"--management"];
		[self.args addObject:@"127.0.0.1"];
		[self.args addObject:[NSString stringWithFormat:@"%d", [self availablePort]]];
		if (passwordFile) {
			[self.args addObject:passwordFile];
		}

		_task = [[NSTask alloc] init];
		[_task setArguments:self.args];
		[_task setLaunchPath:binary];
		if (@available(macOS 10.7, *)) {
			if (handler) {
				[_task setTerminationHandler:^(NSTask * _Nonnull t) {
					NSLog(@"process terminated");
					handler([t terminationReason]);
				}];
			}
		}
	
		
		NSPipe* out = [NSPipe pipe];
		[_task setStandardOutput:out];
		NSFileHandle* fhOut = [out fileHandleForReading];
		[fhOut waitForDataInBackgroundAndNotify];

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedOpvnData:) name:NSFileHandleDataAvailableNotification object:fhOut];
		
		NSPipe* err = [NSPipe pipe];
		[_task setStandardError:err];
		NSFileHandle* fhErr = [err fileHandleForReading];
		[fhErr waitForDataInBackgroundAndNotify];

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedOpvnData:) name:NSFileHandleDataAvailableNotification object:fhErr];
		
		
		NSLog(@"About to launch, using arguments:");
		for(NSString* arg in self.args) {
			NSLog(@"%@", arg);
		}
		
		@try {
			
			[_task launch];
			
			NSLog(@"Launched %@. PID:%d", binary, [_task processIdentifier]);
			
			// Allow some time for openvpn to die if there's an error
			[NSThread sleepForTimeInterval:3.0f];
			
			if(![_task isRunning]) {
				[OVPNProcess setError:error withCode:1 andReason:@"NSTask started openvpn, but it is not running"];
			}
			
			if (@available(macOS 10.7, *)) {
			} else {
				__unsafe_unretained OVPNProcess * wkSelf = self;
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
					[wkSelf.task waitUntilExit];
					if (wkSelf.task) {
						if (handler) {
							handler([wkSelf.task terminationReason]);
						}
						NSLog(@"openvpn progress exited");
					}
				});
			}
			
		} @catch(NSException* ex) {
			NSLog(@"Exception thrown: %@", ex);
			NSString* reason = [NSString stringWithFormat:@"Exception: %@. Details: %@", [ex name], [ex reason]];
			[OVPNProcess setError:error withCode:1 andReason:reason];
		}
	} else {
		[OVPNProcess setError:error withCode:1 andReason:@"Failed to intialize OVPNProcess superclass"];
	}
	return self;
}

- (void)receivedOpvnData:(NSNotification *)notif {
    NSFileHandle *fh = [notif object];
    NSData *data = [fh availableData];
    if (data.length > 0) { // if data is found, re-register for more data (and print)
        [fh waitForDataInBackgroundAndNotify];
        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@" ,str);
		if ([str rangeOfString:@"failed password attempts"].location != NSNotFound) {
			self.terminationError = [NSError errorWithDomain:@"SS" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Management client not authenticated"}];
		}
    }
}

-(int)managementPort {
    if([self isRunning]) {
        return [self availablePort];
    }
    return -1;
}

-(BOOL)isRunning {
    if(_task == nil)
        return NO;

    return [_task isRunning];
}

- (BOOL)terminate {
	if(_task == nil)
		return NO;
	
	[_task terminate];
	return YES;
}

+(void)setError:(NSError **)error withCode:(int)code andReason:(NSString *)reason {
    NSLog(@"OVPNProcess error: %@", reason);
    if (error) {
        *error = [[NSError alloc] initWithDomain:@"OVPNProcess" code:code userInfo:@{ NSLocalizedDescriptionKey : reason }];
    }
}

@end
