//
//  VPNConfig.h
//  
//
//  Created by Paul Wiseman on 27/11/2015.
//
//

#import <Foundation/Foundation.h>
#import "VPNEnum.h"

@interface VPNConfig : NSObject
+(CFDictionaryRef) getConfigForConnectionType:(VPNConnectionType)connectionType
                                     withHost:(NSString *)host
                                 withUsername:(NSString *)username
                                 withPassword:(NSString *)password
                               withIdentifier:(NSString *)identifier
                             withSharedSecret:(NSString *)sharedSecret
                                withServiceId:(NSString *)serviceId;

+(CFDictionaryRef) getIPv4ConfigWithConnectionType:(VPNConnectionType)connectionType;
@end
