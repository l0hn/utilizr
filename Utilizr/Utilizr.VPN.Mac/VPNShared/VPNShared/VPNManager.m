//
//  VPNManager.m
//
//
//  Created by Paul Wiseman on 27/11/2015.
//
//

#import "VPNManager.h"
#import "VPNConfig.h"
#import "VPNKeychain.h"
#import "OVPNProcess.h"

#include <sys/socket.h>

#define SLEEP_SETTINGS_DELAY 2.0f

@implementation VPNManager

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                          called:(NSString *)connectionName
                       withError:(NSError **)error {

    // Convenience method for createConnection without the shared secret or callback argument
    return [VPNManager createConnectionWithHost:host
                                   withUsername:username
                                   withPassword:password
                                          using:connectionType
                                   withOverride:false
                               withSharedSecret:NULL
                                         called:connectionName
                                      withError:error];
}

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                    withOverride:(BOOL)override_
                          called:(NSString *)connectionName
                       withError:(NSError **)error {

    return [VPNManager createConnectionWithHost:host
                                   withUsername:username
                                   withPassword:password
                                          using:connectionType
                                   withOverride:override_
                               withSharedSecret:NULL
                                         called:connectionName
                                      withError:error];
}

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                withSharedSecret:(NSString *)sharedSecret
                          called:(NSString *)connectionName
                       withError:(NSError **)error {

    return [VPNManager createConnectionWithHost:host
                                   withUsername:username
                                   withPassword:password
                                          using:connectionType
                                   withOverride:false
                               withSharedSecret:sharedSecret
                                         called:connectionName
                                      withError:error];
}


+(BOOL) deleteConnectionWithName:(NSString*)connectionName withError:(NSError **)error {
    NSLog(@"deleteConnectionWithName %@", connectionName);

    CFStringRef connectionNameCF = nil;
    SCNetworkServiceRef serviceRef = nil;
    
    @try {
        
        connectionNameCF = (__bridge CFStringRef)(connectionName);
        SCPreferencesRef preferences = SCPreferencesCreateWithAuthorization(NULL, connectionNameCF, NULL, [VPNManager createAuth]);
        NSLog(@"deleteConnectionWithName prefs = %@", preferences);
        serviceRef = [VPNManager createServiceRefWithServiceName:connectionName andPrefs:preferences];
        
        if (serviceRef == nil) {
            [VPNManager setError:error withCode:-1 andReason:[@"No service exists with the name: " stringByAppendingString:connectionName]];
            return false;
        }
        
        if (SCPreferencesLock(preferences, true)) {
            NSLog(@"Locked prefs");
        } else {
            NSLog(@"Could not lock prefs");
        }
        
        if (!SCNetworkServiceRemove(serviceRef)) {
            [VPNManager setError:error
                        withCode:SCError()
                       andReason:[NSString stringWithFormat:@"Couldn't remove service for %@: %s (Code %i)", connectionName, SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        //commit all the changes to the system
        if (!SCPreferencesCommitChanges(preferences)) {
            [VPNManager setError:error
                        withCode:SCError()
                       andReason:[NSString stringWithFormat:@"Could not commit changes to system: %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        if (!SCPreferencesApplyChanges(preferences)) {
            [VPNManager setError:error
                        withCode:SCError()
                       andReason:[NSString stringWithFormat:@"Could not apply changes to service %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        //unlock the system preferences
        SCPreferencesUnlock(preferences);
        
        CFRelease(preferences);
        
        [NSThread sleepForTimeInterval:SLEEP_SETTINGS_DELAY];
    }
    @finally {
        if (serviceRef)
            CFRelease(serviceRef);
    }
    NSLog(@"finished deleteConnectionWithName %@", connectionName);

    return true;
}

+(BOOL) createConnectionWithHost:(NSString *)host
                    withUsername:(NSString *)username
                    withPassword:(NSString *)password
                           using:(VPNConnectionType)connectionType
                    withOverride:(BOOL)override_
                withSharedSecret:(NSString *)sharedSecret
                          called:(NSString *)connectionName
                       withError:(NSError **)error {

    NSLog(@"createConnectionWithHost %@", host);

    CFStringRef serviceIdCF = nil;
    CFStringRef connectionNameCF = nil;
    SCPreferencesRef prefs = nil;
    SCNetworkInterfaceRef interface = nil;
    CFDictionaryRef config = nil;
    SCNetworkSetRef networkSet = nil;
    SCNetworkProtocolRef protocol = nil;
    CFDictionaryRef ipv4Config = nil;
    
    @try {
        serviceIdCF = [VPNManager createServiceIdWithServiceName:connectionName];
        
        if (serviceIdCF) {
            if (override_) {
                // If connection exists and override is True, delete and recreate
                [VPNManager deleteConnectionWithName:connectionName withError:error];
                return [VPNManager createConnectionWithHost:host
                                               withUsername:username
                                               withPassword:password
                                                      using:connectionType
                                           withSharedSecret:sharedSecret
                                                     called:connectionName
                                                  withError:error];
            }
            else {
                [VPNManager setError:error
                            withCode:-1
                           andReason:[NSString stringWithFormat:@"Service already exists with the name: %@ (%@)", connectionName, (__bridge NSString *)(serviceIdCF)]];
            }
            
            return false;
        }
        
        connectionNameCF = (__bridge CFStringRef)(connectionName);
        
        prefs = SCPreferencesCreateWithAuthorization(NULL, connectionNameCF, NULL, [VPNManager createAuth]);
        
        //lock the system preferences
        if (SCPreferencesLock(prefs, true)) {
            NSLog(@"Locked prefs");
        } else {
            NSLog(@"Could not lock prefs");
        }
        
        interface = [VPNManager getInterface:connectionType];
        
        //create vpn service in memory
        SCNetworkServiceRef service = SCNetworkServiceCreate(prefs, interface);
        NSLog(@"%@", service);
        
        //give the service a name
        SCNetworkServiceSetName(service, connectionNameCF);
        
        //get the internal id (this is required to get passwords from the keychain
        NSString * serviceId = (__bridge NSString *)(SCNetworkServiceGetServiceID(service));
        
        //release the interfaceRefs as they are now useless
        CFRelease(interface);
        
        //reload the interface
        interface = SCNetworkServiceGetInterface(service);
        
        if ([VPNManager connectionTypeNeedsSharedSecret:connectionType] && sharedSecret == NULL) {
            [VPNManager setError:error withCode:-1 andReason:@"Shared secret not provided, needed for connection type"];
            return false;
        }
        
        config = [VPNConfig getConfigForConnectionType:connectionType
                                              withHost:host
                                          withUsername:username
                                          withPassword:password
                                        withIdentifier:connectionName
                                      withSharedSecret:sharedSecret
                                         withServiceId:serviceId];
        
        //apply the configuration to the interface
        //in this example a simple pptp
        SCNetworkInterfaceSetConfiguration(interface, config);
        
        //check for errors
        NSLog(@"%@", service);
        if (!SCNetworkServiceEstablishDefaultConfiguration(service)) {
            [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Could not establish default service configuration, %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        //fetch network services
        networkSet = SCNetworkSetCopyCurrent(prefs);
        if (!networkSet) {
            NSLog(@"Could not fetch current network set");
        }
        
        //add the service to the network set
        if (!SCNetworkSetAddService(networkSet, service)) {
            NSString* reason = [NSString stringWithFormat:@"Could not add VPN service %s (Code %i)", SCErrorString(SCError()), SCError()];
            if (SCError()==1005) {
                reason = [reason stringByAppendingString:@" (Service already exists)"];
            }
            [VPNManager setError:error withCode:SCError() andReason:reason];
            return false;
        }
        
        //fetch ipv4 protocol for service
        protocol = SCNetworkServiceCopyProtocol(service, kSCNetworkProtocolTypeIPv4);
        
        if (!protocol) {
            [VPNManager setError:error withCode:-1 andReason:@"Could not fetch ipv4 protocol of service"];
            return false;
        }
        
        //get ipv4 config
        ipv4Config = [VPNConfig getIPv4ConfigWithConnectionType:connectionType];
        
        if (!SCNetworkProtocolSetConfiguration(protocol, ipv4Config)) {
            [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Could not configure ipv4 protocol: %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        //commit all the changes to the system
        if (!SCPreferencesCommitChanges(prefs)) {
            [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Could not commit changes to system: %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
        //    add to keychain
        switch (connectionType) {
            case kVPNConnectionTypePPTP:
                [VPNKeychain createPasswordKeyChainItem:connectionName forService:serviceId withAccount:username andPassword:password];
                break;
            case kVPNConnectionTypeCiscoIPSEC:
                [VPNKeychain createSharedSecretKeyChainItem:connectionName forService:serviceId withAccount:username withPassword:sharedSecret];
                [VPNKeychain createXAuthKeyChainItem:connectionName forService:serviceId withAccount:username withPassword:password];
                break;
            default:
                break;
        }
        
        if (!SCNetworkServiceSetEnabled(service, true)) {
            NSLog(@"Could not enable service %s (Code %i)", SCErrorString(SCError()), SCError());
        }
        
        if (!SCPreferencesApplyChanges(prefs)) {
            NSLog(@"Could not apply changes to service %s (Code %i)", SCErrorString(SCError()), SCError());
        }
        
        //unlock the system preferences
        SCPreferencesUnlock(prefs);
        
        [NSThread sleepForTimeInterval:SLEEP_SETTINGS_DELAY];
    }
    @finally {
        if (serviceIdCF)
            CFRelease(serviceIdCF);
        //if (connectionNameCF)
        //    CFRelease(connectionNameCF);
        if (prefs)
            CFRelease(prefs);
        if (interface)
            CFRelease(interface);
        if (config)
            CFRelease(config);
        if (networkSet)
            CFRelease(networkSet);
        if (protocol)
            CFRelease(protocol);
        if (ipv4Config)
            CFRelease(ipv4Config);
    }
    NSLog(@"finished createConnectionWithHost %@", host);

    return true;
}

+(BOOL)connectServiceWithName:(NSString *)serviceName withError:(NSError **)error {
    return [VPNManager connectServiceWithName:serviceName withCallback:NULL withError:error];
}

+(BOOL)connectServiceWithName:(NSString *)serviceName withCallback:(SCNetworkConnectionCallBack)callback withError:(NSError **)error {
    NSLog(@"connectServiceWithName %@", serviceName);

    SCNetworkConnectionRef conn = nil;
    
    @try {
        CFStringRef serviceId = [VPNManager createServiceIdWithServiceName:serviceName];
        
        conn = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, callback, NULL);
        NSLog(@"%@", conn);
        
        CFRelease(serviceId);
        
        
        if (conn == nil)
        {
            [VPNManager setError:error withCode:-1 andReason:[NSString stringWithFormat:@"Connection with name %@ doesn't exist", serviceName]];
            return false;
        }
        
        SCNetworkConnectionStatus status = SCNetworkConnectionGetStatus(conn);
        
        NSLog(@"Status: %i", status);
        
        if (!SCNetworkConnectionStart(conn, NULL, true)) {
            [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Could not start connection %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
    }
    @finally {
        //        if (serviceId)
        //            CFRelease(serviceId);
        if (conn)
            CFRelease(conn);
    }
    
    return true;
}

+(BOOL)disconnectServiceWithName:(NSString *)serviceName withError:(NSError **)error {
    return [VPNManager disconnectServiceWithName:serviceName withForceDisconnect:FALSE withError:error];
}

+(BOOL)disconnectServiceWithName:(NSString *)serviceName withForceDisconnect:(BOOL) forceDisconnect withError:(NSError **)error {
    NSLog(@"disconnectServiceWithName %@", serviceName);

    CFStringRef serviceId = nil;
    SCNetworkConnectionRef conn = nil;
    
    @try {
        
        serviceId = [VPNManager createServiceIdWithServiceName:serviceName];
        
        conn = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, NULL, NULL);
        
        CFRelease(serviceId);
        
        NSLog(@"%@", conn);
        
        if (conn == nil)
        {
            [VPNManager setError:error withCode:-1 andReason:[NSString stringWithFormat:@"Connection with name %@ doesn't exist", serviceName]];
            return false;
        }
        
        SCNetworkConnectionStatus status = SCNetworkConnectionGetStatus(conn);
        
        NSLog(@"Status: %i", status);
        
        if (!SCNetworkConnectionStop(conn, forceDisconnect)) {
            [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Could not stop connection %s (Code %i)", SCErrorString(SCError()), SCError()]];
            return false;
        }
        
    }
    @finally {
        //        if (serviceId)
        //            CFRelease(serviceId);
        if (conn)
            CFRelease(conn);
    }
    
    return true;
}

+(BOOL) addCallback:(SCNetworkConnectionCallBack)callback ToConnectionWithName:(NSString *)connectionName withError:(NSError **)error {
    return [VPNManager addCallback:callback ToConnectionWithName:connectionName withContext:NULL withError:error];
}

+ (BOOL) addCallback:(SCNetworkConnectionCallBack)callback ToConnectionWithName:(NSString *)connectionName withContext:(SCNetworkConnectionContext*)context withError:(NSError **)error {
    
    BOOL result = true;
    
    NSLog(@"addCallback %@", connectionName);
    
    SCNetworkServiceRef serviceRef = [VPNManager createServiceRefWithServiceName:connectionName];
    CFStringRef serviceId = SCNetworkServiceGetServiceID(serviceRef);
    
    SCNetworkConnectionRef connectionRef = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, callback, context);
    
    if (!SCNetworkConnectionScheduleWithRunLoop(connectionRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode)) {
        [VPNManager setError:error withCode:SCError() andReason:[NSString stringWithFormat:@"Unable to schedule connection %s (Code %i)", SCErrorString(SCError()), SCError()]];
        result = false;
    }
    
    CFRelease(serviceRef);
    CFRelease(connectionRef);
    
    return result;
    
}
//
//+ (void)removeCallbackForConnectionName:(NSString *)connectionName withCallback:(SCNetworkConnectionCallBack)callback withContext:(SCNetworkConnectionContext*)context  {
//    SCNetworkServiceRef serviceRef = [VPNManager createServiceRefWithServiceName:connectionName];
//    CFStringRef serviceId = SCNetworkServiceGetServiceID(serviceRef);
//    CFRelease(serviceRef);
//    
//    SCNetworkConnectionRef connectionRef = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, callback, context);
//    
//    if (!SCNetworkConnectionUnscheduleFromRunLoop(connectionRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode)) {
//        NSLog([NSString stringWithFormat:@"Unable to unschedule connection %s (Code %i)", SCErrorString(SCError()), SCError()])
//    }
//    CFRelease(connectionRef);
//
//}

+ (NSString *)getConnectionHostnameForConnectionWithName:(NSString*)connectionName withError:(NSError **)error {
    SCNetworkServiceRef serviceRef = nil;
    SCNetworkInterfaceRef interfaceRef = nil;
    
    @try {
        serviceRef = [VPNManager createServiceRefWithServiceName:connectionName];
        
        interfaceRef = SCNetworkServiceGetInterface(serviceRef);
        
        CFDictionaryRef ref = SCNetworkInterfaceGetConfiguration(interfaceRef);
        NSDictionary * d = (__bridge NSDictionary *)ref;
        
        NSString * host = [d objectForKey:(__bridge NSString *)kSCPropNetPPPCommRemoteAddress];
        if (host == nil) {
            host = [d objectForKey:(__bridge NSString *)kSCPropNetIPSecRemoteAddress];
        }
        return host;
        
    }
    @finally {
        if (serviceRef)
            CFRelease(serviceRef);
        //        if (serviceId)
        //            CFRelease(serviceId);
        
    }
    
}

+(SCNetworkConnectionStatus) getConnectionStatusForConnectionWithName:(NSString*)connectionName withError:(NSError **)error {
    
    CFStringRef serviceId = [VPNManager createServiceIdWithServiceName:connectionName];
    NSLog(@"getConnectionStatusForConnectionWithName %@", serviceId);
    
    SCNetworkConnectionRef conn = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, NULL, NULL);
    
    CFRelease(serviceId);
    
    NSLog(@"getConnectionStatusForConnectionWithName %@", conn);
    
    if (conn == nil)
    {
        [VPNManager setError:error withCode:-1 andReason:[NSString stringWithFormat:@"Connection with name %@ doesn't exist", connectionName]];
        return false;
    }
    
    SCNetworkConnectionStatus stat =  SCNetworkConnectionGetStatus(conn);
    CFRelease(conn);
    return stat;
}

+ (NSDate *) getConnectionStartTimeForConnectionWithName:(NSString*)connectionName withError:(NSError **)error {
    
    NSDate * date = nil;
    
    CFStringRef serviceId = [VPNManager createServiceIdWithServiceName:connectionName];

    SCNetworkConnectionRef conn = SCNetworkConnectionCreateWithServiceID(kCFAllocatorDefault, serviceId, NULL, NULL);

    CFRelease(serviceId);

    CFDictionaryRef dict = SCNetworkConnectionCopyExtendedStatus(conn);
    
    NSDictionary * d = (__bridge NSDictionary *)dict;
//    for (id k in [d allKeys]) {
//        NSLog(@"%@ : %@", k, [d objectForKey:k]);
//    }
    
    NSNumber * raw = [[d objectForKey:@"PPP"] objectForKey:@"ConnectTime"];
    if (!raw) {
        raw = [[d objectForKey:@"IPSec"] objectForKey:@"ConnectTime"];
    }
    
    if (raw) {
        NSLog(@"raw %@", raw);
        NSLog(@"raw %zd", [raw integerValue]);
        if ([raw integerValue] > 0) {
            
            NSTimeInterval t = [[NSProcessInfo processInfo] systemUptime] - [raw integerValue];
            NSLog(@"t %f", t);

            date = [[NSDate date] dateByAddingTimeInterval:-t];
            
//            date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:t];
            NSLog(@"date %@", date);

        }
        
    }
    
    CFRelease(dict);
    CFRelease(conn);

    return date;
}

+(void)setError:(NSError **)error withCode:(int)code andReason:(NSString *)reason {
    NSLog(@"%@", reason);
    if (error) {
        *error = [[NSError alloc] initWithDomain:@"VPNManager" code:code userInfo:@{ NSLocalizedDescriptionKey : reason }];
    }
}


+ (SCNetworkServiceRef) createServiceRefWithServiceName:(NSString *)serviceName andPrefs:(SCPreferencesRef)prefs {
    SCNetworkServiceRef serviceRef = nil;
    NSLog(@"createServiceRefWithServiceName %@", serviceName);

    CFArrayRef allServices = SCNetworkServiceCopyAll(prefs);
    
    CFIndex count = CFArrayGetCount(allServices);
    
    for (CFIndex i = 0; i < count; i++) {
        SCNetworkServiceRef ref = (SCNetworkServiceRef)CFArrayGetValueAtIndex(allServices, i);
        NSString * name = (__bridge NSString *)SCNetworkServiceGetName(ref);
        
        if ([name  isEqual: serviceName]) {
            NSLog(@"found service ref: %@", ref);
            serviceRef = ref;
            CFRetain(serviceRef);
            break;
        }
    }
    
    CFRelease(allServices);
    
    return serviceRef;
}

+ (SCNetworkServiceRef) createServiceRefWithServiceName:(NSString *)serviceName {
    CFStringRef serviceNameCF = (__bridge CFStringRef)(serviceName);
    SCPreferencesRef preferences = SCPreferencesCreateWithAuthorization(NULL, serviceNameCF, NULL, [VPNManager createAuth]);
    SCNetworkServiceRef service = [VPNManager createServiceRefWithServiceName:serviceName andPrefs:preferences];
    CFRelease(preferences);
    return service;
}

+ (CFStringRef) createServiceIdWithServiceName:(NSString *)serviceName {
    
    SCNetworkServiceRef serviceRef = [VPNManager createServiceRefWithServiceName:serviceName];
    
    if (serviceRef == nil) {
        return nil;
    }
    NSLog(@"createServiceIdWithServiceName %@", serviceRef);
    
    CFStringRef serviceId = SCNetworkServiceGetServiceID(serviceRef);
    CFRetain(serviceId);
    CFRelease(serviceRef);
    return serviceId;
}

+(BOOL) connectionTypeNeedsSharedSecret:(VPNConnectionType)connectionType {
    return connectionType == kVPNConnectionTypeCiscoIPSEC;
}

+(SCNetworkInterfaceRef) getInterface:(VPNConnectionType)connectionType {
    
    SCNetworkInterfaceRef bottomInterface;
    SCNetworkInterfaceRef interface;
    
    switch (connectionType) {
        case kVPNConnectionTypePPTP: {
            // Think bottomInterface will leak - not sure how to clean it up with this in it's own method
            bottomInterface = SCNetworkInterfaceCreateWithInterface(kSCNetworkInterfaceIPv4, kSCNetworkInterfaceTypePPTP);
            interface = SCNetworkInterfaceCreateWithInterface(bottomInterface, kSCNetworkInterfaceTypePPP);
            break;
        }
        case kVPNConnectionTypeCiscoIPSEC: {
            interface = SCNetworkInterfaceCreateWithInterface (kSCNetworkInterfaceIPv4, kSCNetworkInterfaceTypeIPSec);
            break;
        }
        default:
            break;
    }
    return interface;
}

+(AuthorizationRef) createAuth {
    AuthorizationRef auth = NULL;
    
    OSStatus status;
    status = AuthorizationCreate(NULL, kAuthorizationEmptyEnvironment, [VPNManager flags], &auth);
    
    if (status == errAuthorizationSuccess) {
        //        NSLog(@"Auth success");
    } else {
        NSLog(@"Auth failed");
    }
    return auth;
}

+(AuthorizationFlags) flags {
    return kAuthorizationFlagDefaults    |
    kAuthorizationFlagExtendRights       |
    kAuthorizationFlagInteractionAllowed |
    kAuthorizationFlagPreAuthorize;
}

@end
