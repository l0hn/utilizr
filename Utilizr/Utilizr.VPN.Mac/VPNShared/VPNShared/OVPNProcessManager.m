//
//  OVPNProcessManager.m
//
//
//  Created by Darren Hale on 21/03/2016.
//
//

#import "OVPNProcessManager.h"

@implementation OVPNProcessManager

static OVPNProcess* _currentProcess;


+(int)runOpenVPNWithHost:(NSString *)host
			withUpScript:(NSString *)upScript
         withCertificate:(NSString *)certificateFile
                withMode:(NSString *)mode
         withPingTimeout:(int)timeout
       withAvailablePort:(int)availablePort
            withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler {

    if(_currentProcess != nil && [_currentProcess host] == host && [_currentProcess isRunning]) {
        return _currentProcess.managementPort;
    }

    [OVPNProcessManager stopAllOpenVPNProcessesWithError:error];
    if(*error != nil) {
        NSLog(@"Failed to ensure all previous processes are stopped. Error: %@", *error);
        return -1;
    }

    NSLog(@"Starting new process");
    _currentProcess = [[OVPNProcess alloc] initWithHost:host
										   withUpScript:upScript
                                        withCertificate:certificateFile
                                               withMode:mode
                                        withPingTimeout:timeout
                                      withAvailablePort:availablePort
                                           withOvpnFile:ovpnFile
                                              withError:error
									 terminationHandler:handler
						];
    return [_currentProcess managementPort];
}

+ (BOOL)terminateCurrentProcess {
	if (_currentProcess) {
		return [_currentProcess terminate];
	}
	return NO;
}

+ (NSError *)lastTerminationError {
	return [_currentProcess terminationError];
}

+(int)runOpenVPNWithHost:(NSString *)host
			withUpScript:(NSString *)upScript
			  withConfig:(NSString *)configFile
	   withAvailablePort:(int)availablePort
			withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler {
	return [self runOpenVPNWithHost:host withUpScript:upScript withConfig:configFile withAvailablePort:availablePort withPwFile:nil withOvpnFile:ovpnFile withError:error terminationHandler:handler];
}

+(int)runOpenVPNWithHost:(NSString *)host
			withUpScript:(NSString *)upScript
			  withConfig:(NSString *)configFile
	   withAvailablePort:(int)availablePort
			  withPwFile:(NSString * __nullable)passwordFile
			withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler {
	
	if(_currentProcess != nil && [_currentProcess host] == host && [_currentProcess isRunning]) {
		return _currentProcess.managementPort;
	}
	
	[OVPNProcessManager stopAllOpenVPNProcessesWithError:error];
	if(*error != nil) {
		NSLog(@"Failed to ensure all previous processes are stopped. Error: %@", *error);
		return -1;
	}
	
	NSLog(@"Starting new process");
	_currentProcess = [[OVPNProcess alloc] initWithHost:host
										   withUpScript:upScript
											 withConfig:configFile
									  withAvailablePort:availablePort
											 withPwFile:passwordFile
										   withOvpnFile:ovpnFile
											  withError:error
									 terminationHandler:handler];
	NSLog(@"Created new process");
	
	return [_currentProcess managementPort];
}


+(void)stopAllOpenVPNProcessesWithError:(NSError **)error  {

    NSTask * task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/sh"];

	//use SIGTERM / signal 15 to give 'down' scripts a chance to run
	if (@available(macOS 10.8,*)) {
		[task setArguments:@[@"-c", @"pkill -SIGTERM openvpn"]];
	} else {
		//pkill not available on 10.6/10.7
//		[task setArguments:@[@"-c", @"kill -15 $(ps ax | grep openvpn | grep -v grep | awk '{ print $1 }')"]];
		[task setArguments:@[@"-c", @"pid=$(ps ax | grep openvpn | grep -v grep | awk '{ print $1 }') && [[ ! -z \"$pid\" ]] && kill -15 $pid"]]; //get pid & if not empty, execute SIGTERM kill
	}
	
    NSPipe* out = [NSPipe pipe];
    [task setStandardOutput:out];

    NSPipe* err = [NSPipe pipe];
    [task setStandardError:err];

    @try {
        [task launch];
        [task waitUntilExit];
		
		NSCharacterSet* set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
		
		NSFileHandle* fhOut = [out fileHandleForReading];
		NSString* outString = [[NSString alloc] initWithData:[fhOut availableData] encoding:NSUTF8StringEncoding];
		if([[outString stringByTrimmingCharactersInSet:set] length] > 0 ) {
			NSLog(@"Stnout for killing previous openvpn process: %@", outString);
		}
		
		NSFileHandle* fhErr = [err fileHandleForReading];
		NSString* errString = [[NSString alloc] initWithData:[fhErr availableData] encoding:NSUTF8StringEncoding];
		if([[errString stringByTrimmingCharactersInSet:set] length] > 0 ) {
			NSLog(@"Stnerr for killing previous openvpn process: %@", errString);
		}
		
		
		if([[errString stringByTrimmingCharactersInSet:set] length] > 0) {
			[OVPNProcessManager setError:error withCode:1 andReason:[NSString stringWithFormat:@"Failed to stop previous openvpn process. Standard Error: %@,", errString]];
		}
        
    } @catch(NSException* ex) {
        [OVPNProcessManager setError:error withCode:1 andReason:[NSString stringWithFormat:@"Failed to stop previous openvpn process. Exception: %@ Reason: %@", [ex name], [ex reason]]];
    }
}

+(void)setError:(NSError **)error withCode:(int)code andReason:(NSString *)reason {
    NSLog(@"OVPNManager error: %@", reason);
    if (error) {
        *error = [[NSError alloc] initWithDomain:@"OVPNProcessManager" code:code userInfo:@{ NSLocalizedDescriptionKey : reason }];
    }
}

@end
