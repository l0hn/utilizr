//
//  OVPNProcessManager.h
//  
//
//  Created by Darren Hale on 21/03/2016.
//
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "OVPNProcessManager.h"
#import "OVPNProcess.h"

@interface OVPNProcessManager : NSObject

+(int)runOpenVPNWithHost:(NSString *)host
	  withUpScript:(NSString *)upScript
         withCertificate:(NSString *)certificateFile
                withMode:(NSString *)mode
         withPingTimeout:(int)timeout
       withAvailablePort:(int)availablePort
            withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler;

+(int)runOpenVPNWithHost:(NSString *)host
			withUpScript:(NSString *)upScript
			  withConfig:(NSString *)configFile
	   withAvailablePort:(int)availablePort
			withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler;

+(int)runOpenVPNWithHost:(NSString *)host
			withUpScript:(NSString *)upScript
			  withConfig:(NSString *)configFile
	   withAvailablePort:(int)availablePort
			  withPwFile:(NSString * __nullable)passwordFile
			withOvpnFile:(NSString *)ovpnFile
			   withError:(NSError **)error
	  terminationHandler:(void (^)(NSInteger code))handler;

+(void)stopAllOpenVPNProcessesWithError:(NSError **)error;
+(BOOL)terminateCurrentProcess;
+ (NSError *)lastTerminationError;

@end
