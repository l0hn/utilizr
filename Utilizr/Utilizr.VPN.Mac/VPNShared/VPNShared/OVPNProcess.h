//
//  OVPNProcess.h
//  
//
//  Created by Darren Hale on 21/03/2016.
//
//

#import <Foundation/Foundation.h>

@interface OVPNProcess : NSObject

@property(nonatomic, copy, readonly) NSString* host;
@property(nonatomic, assign, readonly) int availablePort;
@property(nonatomic, readonly) int managementPort;
@property(nonatomic, strong, readonly) NSError * terminationError;


-(id)initWithHost:(NSString *)host
	 withUpScript:(NSString *)upScript
  withCertificate:(NSString *)certificateFile
         withMode:(NSString *)mode
  withPingTimeout:(int)timeout
withAvailablePort:(int)availablePort
     withOvpnFile:(NSString*)ovpnFile
		withError:(NSError **)error
terminationHandler:(void (^)(NSInteger code))handler;

-(id)initWithHost:(NSString *)host
	 withUpScript:(NSString *)upScript
	   withConfig:(NSString *)configFile
withAvailablePort:(int)availablePort
	   withPwFile:(NSString * __nullable)passwordFile
	 withOvpnFile:(NSString*)ovpnFile
		withError:(NSError **)error
terminationHandler:(void (^)(NSInteger code))handler;

-(BOOL)isRunning;

-(BOOL)terminate;

@end
