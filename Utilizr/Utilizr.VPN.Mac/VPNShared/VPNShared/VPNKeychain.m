//
//  VPNKeychain.m
//  vpn_connect_test
//
//  Created by John Haines on 16/11/2015.
//  Copyright © 2015 jdi. All rights reserved.
//

#import "VPNKeychain.h"

// These are the applications which are going to get access to new Keychain items.
// How do we know them? Just create a VPN service manualy and run the following command:
//   security dump-keychain -a /Library/Keychains/System.keychain
// Among the results, you will find your VPN service and you can see the paths that have access to it
static const char * trustedAppPaths[] = {
    "/System/Library/Frameworks/SystemConfiguration.framework/Versions/A/Helpers/SCHelper",
    "/System/Library/PreferencePanes/Network.prefPane/Contents/XPCServices/com.apple.preference.network.remoteservice.xpc",
    "/System/Library/CoreServices/SystemUIServer.app",
    "/usr/sbin/pppd",
    "/usr/sbin/racoon",
    "/usr/libexec/configd",
};

// This class contains all code we need to handle System Keychain Items
// Exit status codes: 60-79
@implementation VPNKeychain

/******************
 * PUBLIC METHODS *
 ******************/

// This will create a PPP Password Keychain Item
+ (int) createPasswordKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account andPassword:(NSString*)password {
    return [self createItem:label withService:service account:account description:@"VPN Password" andPassword:password];
}

// This will create an IPSec Shared Secret Keychain Item
+ (int) createSharedSecretKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account withPassword:(NSString*)password {
    service = [NSString stringWithFormat:@"%@.SS", service];
    return [self createItem:label withService:service account:@"" description:@"IPSec Shared Secret" andPassword:password];
}

// This will create an Cisco IPSec XAuth Keychain Item
+ (int) createXAuthKeyChainItem:(NSString*)label forService:(NSString*)service withAccount:(NSString*)account withPassword:(NSString*)password {
    service = [NSString stringWithFormat:@"%@.XAUTH", service];
    return [self createItem:label withService:service account:account description:@"IPSec XAuth Password" andPassword:password];
}

/********************
 * INTERNAL METHODS *
 ********************/

// A generic method to create Keychain Items holding Network service passwords
+ (int) createItem:(NSString*)label withService:(NSString*)service account:(NSString*)account description:(NSString*)description andPassword:(NSString*)password {
    
    SecKeychainItemRef item = nil;
    SecAccessRef access = nil;
    
    @try {
        // This variable will hold all sorts of operation status responses
        OSStatus status;
        
        // Converting the NSStrings to char* variables which we will need later
        const char *labelUTF8 = [label UTF8String];
        const char *serviceUTF8 = [service UTF8String];
        const char *accountUTF8 = [account UTF8String];
        const char *descriptionUTF8 = [description UTF8String];
        const char *passwordUTF8 = [password UTF8String];
        
        // This variable is soon to hold the System Keychain
        SecKeychainRef keychain = NULL;
        
        status = SecKeychainCopyDomainDefault(kSecPreferencesDomainSystem, &keychain);
        if (status == errSecSuccess) {
            NSLog(@"Succeeded opening System Keychain");
        } else {
            NSLog(@"Could not obtain System Keychain: %@", SecCopyErrorMessageString(status, NULL));
            return 60;
        }
        
        NSLog(@"Unlocking System Keychain");
        status = SecKeychainUnlock(keychain, 0, NULL, FALSE);
        if (status == errSecSuccess) {
            NSLog(@"Succeeded unlocking System Keychain");
        } else {
            NSLog(@"Could not unlock System Keychain: %@", SecCopyErrorMessageString(status, NULL));
            return 61;
        }
        
        SecKeychainStatus keychainStatus;
        SecKeychainGetStatus(keychain, &keychainStatus);
        
        if((keychainStatus & kSecUnlockStateStatus) == 0) {
            NSLog(@"Keychain is locked");
        }
        if((keychainStatus & kSecReadPermStatus) == 0) {
            NSLog(@"Keychain is not readable");
        }
        if((keychainStatus & kSecWritePermStatus) == 0) {
            NSLog(@"Keychain is not writable");
        }

        
        status = SecAccessCreate((__bridge CFStringRef)service, (__bridge CFArrayRef)[VPNKeychain trustedApps], &access);
        
        if(status == noErr) {
            NSLog(@"Created empty Keychain access object");
        } else {
            NSLog(@"Could not unlock System Keychain: %@", SecCopyErrorMessageString(status, NULL));
            return 62;
        }
        
        // Putting together the configuration options
        SecKeychainAttribute attrs[] = {
            {kSecLabelItemAttr, (int)strlen(labelUTF8), (char *)labelUTF8},
            {kSecAccountItemAttr, (int)strlen(accountUTF8), (char *)accountUTF8},
            {kSecServiceItemAttr, (int)strlen(serviceUTF8), (char *)serviceUTF8},
            {kSecDescriptionItemAttr, (int)strlen(descriptionUTF8), (char *)descriptionUTF8}
        };
        
        SecKeychainAttributeList attributes = {sizeof(attrs) / sizeof(attrs[0]), attrs};
        
        status = SecKeychainItemCreateFromContent(kSecGenericPasswordItemClass, &attributes, (int)strlen(passwordUTF8), passwordUTF8, keychain, access, &item);
        
        if(status == noErr) {
            NSLog(@"Successfully created Keychain Item");
        } else {
            NSLog(@"Creating Keychain item failed: %@", SecCopyErrorMessageString(status, NULL));
            
            UInt32 pwdLength = 0;
            void *pwd = NULL;
            
            status = SecKeychainFindGenericPassword(NULL, (int)(strlen(serviceUTF8)), serviceUTF8, 0, NULL, &pwdLength, &pwd, &item);
            if (status == noErr) {
                NSLog(@"Got existing item");
                status = SecKeychainItemModifyContent(item, &attributes, 0, NULL);
                if (status == noErr) {
                    NSLog(@"Successfully updated existing keychain item");
                } else {
                    NSLog(@"Failed to update existing keychain item");
                    return 61;
                }
            } else {
                NSLog(@"Getting existing keychain item failed: %@", SecCopyErrorMessageString(status, NULL));
                return 61;
            }
            
        }
    }
    @finally {
        if (item)
            CFRelease(item);
        if (access)
            CFRelease(access);
    }
    return 0;
}

+ (NSArray*) trustedApps {
    NSMutableArray *apps = [NSMutableArray array];
    SecTrustedApplicationRef app;
    OSStatus err;
    
    for (int i = 0; i < (sizeof(trustedAppPaths) / sizeof(*trustedAppPaths)); i++) {
        NSLog(@"trusted app: %s", trustedAppPaths[i]);

        err = SecTrustedApplicationCreateFromPath(trustedAppPaths[i], &app);
        if (err == errSecSuccess) {
            [apps addObject:(__bridge id)app];

            //DDLogDebug(@"SecTrustedApplicationCreateFromPath succeeded: %@", SecCopyErrorMessageString(err, NULL));
        } else {
            NSLog(@"SecTrustedApplicationCreateFromPath failed: %@", SecCopyErrorMessageString(err, NULL));
        }
        
        
    }
    
    NSLog(@"trusted app count: %zd", [apps count]);

    
    return apps;
}
@end
