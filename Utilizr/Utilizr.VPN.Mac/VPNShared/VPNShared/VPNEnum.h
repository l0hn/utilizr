//
//  VPNEnum.h
//  VPNManager
//
//  Created by Paul Wiseman on 27/11/2015.
//  Copyright (c) 2015 Pseudio. All rights reserved.
//

#ifndef VPNManager_VPNEnum_h
#define VPNManager_VPNEnum_h

typedef enum {
    kVPNConnectionTypePPTP,
    kVPNConnectionTypeCiscoIPSEC,
    kVPNConnectionTypeOpenVPN,
} VPNConnectionType;

typedef enum {
    kVPNConnectionStatusRegisteredCallback = -3,
    kVPNConnectionStatusError           = -2,
    kVPNConnectionStatusInvalidService  = -1,
    kVPNConnectionStatusDisconnected    = 0,
    kVPNConnectionStatusConnecting      = 1,
    kVPNConnectionStatusConnected       = 2,
    kVPNConnectionStatusDisconnecting   = 3
} VPNConnectionStatus;  

#endif
