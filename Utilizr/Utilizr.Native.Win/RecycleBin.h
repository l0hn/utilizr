#pragma once

#include <Windows.h>
#include <atlsafe.h>
#include <iostream>
#include <vector>
#include <stdlib.h>

#define export __declspec(dllexport)

struct BinItem
{
    std::string shortName;
    std::string longName;
};

std::vector<BinItem> GetRecycleBinItemNames();

extern "C"
{
    __declspec(dllexport)HRESULT NativeGetRecycleBinItemNames(SAFEARRAY** ppsa, SAFEARRAY** ppsb)
    {
        auto items = GetRecycleBinItemNames();

        try
        {
            auto count = items.size();
            CComSafeArray<BSTR> sa(count);

            CComSafeArray<BSTR> sb(count);

            for (LONG i = 0; i < count; i++)
            {
                CComBSTR bstra = items.at(i).shortName.c_str();
                HRESULT hr = sa.SetAt(i, bstra.Detach(), FALSE);
                if (FAILED(hr)) return hr;

                CComBSTR bstrb = items.at(i).longName.c_str();
                hr = sb.SetAt(i, bstrb.Detach(), FALSE);
                if (FAILED(hr)) return hr;
            }

            *ppsa = sa.Detach();
            *ppsb = sb.Detach();
        }
        catch (const CAtlException& e)
        {
            return e;
        }

        return S_OK;
    }
}