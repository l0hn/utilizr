#include "pch.h"
#include "RecycleBin.h"

#include <iostream>
#include <vector>
#include <shlobj.h>

#include "wtsapi32.h"
#pragma comment(lib, "WtsApi32.lib")

std::string GetDisplayName(IShellItem* psi, SIGDN sigdn);

std::vector<BinItem> GetRecycleBinItemNames()
{
    std::vector<BinItem> output;

    HRESULT hr = CoInitialize(NULL);
    if (SUCCEEDED(hr))
    {
        IShellItem* psiRecycleBin;
        hr = SHGetKnownFolderItem(FOLDERID_RecycleBinFolder, KF_FLAG_DEFAULT, NULL, IID_PPV_ARGS(&psiRecycleBin));

        if (SUCCEEDED(hr))
        {
            IEnumShellItems* pesi;
            hr = psiRecycleBin->BindToHandler(NULL, BHID_EnumItems, IID_PPV_ARGS(&pesi));
            if (hr == S_OK)
            {
                IShellItem* psi;

                auto buffer = std::string();

                while (pesi->Next(1, &psi, NULL) == S_OK)
                {
                    IShellItem2* psi2;
                    if (SUCCEEDED(psi->QueryInterface(IID_PPV_ARGS(&psi2))))
                    {
                        BinItem item;

                        item.shortName = GetDisplayName(psi2, SIGDN_PARENTRELATIVE);
                        item.longName = GetDisplayName(psi2, SIGDN_NORMALDISPLAY);

                        psi2->Release();

                        output.push_back(item);
                    }
                    psi->Release();
                }
            }
            psiRecycleBin->Release();
        }
        CoUninitialize();
    }

    return output;
}

std::string GetDisplayName(IShellItem* psi, const SIGDN sigdn)
{
    LPWSTR pszName;
    HRESULT hr = psi->GetDisplayName(sigdn, &pszName);
    if (SUCCEEDED(hr))
    {
        auto len = wcslen(pszName) + 1; // Add 1 for null terminator
        auto buffer = new char[len];

        size_t rv;
        wcstombs_s(&rv, buffer, len, pszName, _TRUNCATE);

        auto ssout = std::string(buffer);

        delete[] buffer;
        CoTaskMemFree(pszName);

        return ssout;
    }

    return std::string();
}