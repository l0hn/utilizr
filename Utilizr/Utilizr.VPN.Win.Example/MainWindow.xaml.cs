﻿using Microsoft.Win32;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Utilizr.Info;
using Utilizr.VPN.Providers;
using Utilizr.VPN.Win.Providers;
using Utilzr.WPF;

namespace Utilizr.VPN.Win.Example
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      //  private WindowsVPNControllerOld Controller;
        private VPNController Controller;
        //string certpath = System.IO.Path.Combine(AppInfo.AppDirectory, "ca.crt");

        private string OpenVPNCert;

        public MainWindow()
        {
            InitializeComponent();

            //Controller = new WindowsVPNControllerOld("Example", new UserPassHandler(() =>
            //{
            //    return new UserPass(user, pass.ToSecureString());
            //}), null, certpath);

            var providerConfig =
                OpenVpnProviderConfig.FromCertificateHandler(() => Path.Combine(AppInfo.AppDirectory, "get-cert.crt"));

            //Controller = new VPNController(
            //    () => new UserPass(user, pass.ToSecureString()),
            //    true,
            //    new OpenVPNProvider(providerConfig /*OpenVPNCert*/, new WindowsOpenVPNHelper()),
            //    new RasVPNProvider("ExampleVPN", () => ""));

            Controller.Connected += (sender, host, error, context) =>
            {
                Application.Current.Dispatcher.SafeInvoke(() =>
                {
                    Status.Text = "Connected";
                    Connectbutton.Content = "Disconnect";
                    Connectbutton.IsEnabled = true;
                });
              
            };
            Controller.Connecting += (sender, host, error, context) =>
            {
                Application.Current.Dispatcher.SafeInvoke(() =>
                {
                    Status.Text = "Connecting";
                    Connectbutton.IsEnabled = false;
                });
            
            };

            Controller.Disconnecting += (sender, host, error, context) =>
            {
                Application.Current.Dispatcher.SafeInvoke(() =>
                {
                    Status.Text = "Disconnecting";
                    Connectbutton.IsEnabled = false;
                });
        
            };

            Controller.Disconnected += (sender, host, error, context) =>
            {
   
                Application.Current.Dispatcher.SafeInvoke(() =>
                {
                    Status.Text = "Disconnected";
                    Connectbutton.Content = "Connect";
                    Connectbutton.IsEnabled = true;
                });
            };

            Controller.ConnectError += Controller_ConnectError;
            
            Protocol.ItemsSource = Controller.GetAvailableProtocols();
            Protocol.SelectionChanged += Protocol_SelectionChanged;
            Protocol.SelectedIndex = 0;
        }

        private void Controller_ConnectError(object sender, string host, System.Exception error, object userContext = null)
        {
            MessageBox.Show(error.Message);
        }

        private void Protocol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var proto = Controller.GetAvailableProtocols().ElementAt(Protocol.SelectedIndex);

            VpncertGrid.Visibility = proto == ConnectionType.OPEN_VPN ? Visibility.Visible : Visibility.Hidden;
            VpncertTitle.Visibility = proto == ConnectionType.OPEN_VPN ? Visibility.Visible : Visibility.Hidden; ;
        }

        private string user, pass;

        private void CertButton_Click(object sender, RoutedEventArgs e)
        {

            var dlg = new OpenFileDialog();
            dlg.Multiselect = false;

            var result = dlg.ShowDialog();
            if (result == true)
            {
                Vpncert.Text = dlg.FileName;
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Controller.IsConnected) {
                Controller.DisconnectAsync();
                return;
            }

            var index = Protocol.SelectedIndex;
            if (index >= 0)
            {
                user = username.Text;
                pass = password.Password;
                var proto = Controller.GetAvailableProtocols().ElementAt(index);
                    
                if (proto == ConnectionType.OPEN_VPN) {
                    if (!File.Exists(Vpncert.Text))
                    {
                        MessageBox.Show("Please choose a certificate file");
                        return;
                    }
                    OpenVPNCert = Vpncert.Text;
                }


                //var ar = Controller.ConnectAsync(Host.Text, proto);
               // AsyncHelper.EndExecute(ar);
            } else
            {
                MessageBox.Show("Please select a protocol");
            }

        }
    }
}
