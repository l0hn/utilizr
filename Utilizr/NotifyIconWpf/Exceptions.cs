﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hardcodet.Wpf.TaskbarNotification
{
    public class TrayNotFoundException : Exception
    {
        public TrayNotFoundException(string message) : base(message) { }
    }
}
