﻿using System;

using Foundation;
using AppKit;
using UI;
using Utilizr.Validation;

namespace Utilizr.MacKit.Demo
{
	public partial class MainWindowController : NSWindowController
	{
		public MainWindowController (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public MainWindowController (NSCoder coder) : base (coder)
		{
		}

		public MainWindowController () : base ("MainWindow")
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();

			var btn = new AppButton ("Title Button");
			btn.Frame = new CoreGraphics.CGRect ((View.Frame.Width - btn.Frame.Width) / 2, View.Frame.Height - 100, btn.Frame.Width, btn.Frame.Height);
			View.AddSubview (btn);
			btn.Activated += (sender, e) => {
				Console.WriteLine ("btn clicked");
			};

			var iconBtn = new AppButton ("Icon Button", NSImage.ImageNamed("button_arrow"));
			iconBtn.Frame = new CoreGraphics.CGRect ((View.Frame.Width - iconBtn.Frame.Width) / 2,View.Frame.Height - 170, iconBtn.Frame.Width, iconBtn.Frame.Height);
			View.AddSubview (iconBtn);
			iconBtn.Activated += (sender, e) => {
				Console.WriteLine ("iconBtn clicked");
			
			};

			var animBtn = new AppButton ("Animating Button", NSImage.ImageNamed("button_arrow"));
			animBtn.Frame = new CoreGraphics.CGRect ((View.Frame.Width - animBtn.Frame.Width) / 2,View.Frame.Height - 240, animBtn.Frame.Width, animBtn.Frame.Height);
			View.AddSubview (animBtn);
			animBtn.Activated += (sender, e) => {
				Console.WriteLine ("iconBtn clicked");
				//if (animBtn.IsAnimatingIcon) {
				//	animBtn.StopAnimatingIcon();
				//	animBtn.Image = NSImage.ImageNamed("button_arrow");
				//} else {
				//	animBtn.Image = NSImage.ImageNamed("button_loading");
				//	animBtn.StartAnimatingIcon(false);
				//}
			};

			var passField = new AppTextField ("PASSWORD", true);
			passField.Frame = new CoreGraphics.CGRect ((View.Frame.Width - passField.Frame.Width) / 2,View.Frame.Height - 380, passField.Frame.Width, passField.Frame.Height);
			View.AddSubview (passField);

			var validator = Validater.CreateEmailValidater ();
			var field = new AppTextField ("NAME");
			field.InputValidater = validator;

			field.Frame = new CoreGraphics.CGRect ((View.Frame.Width - field.Frame.Width) / 2,View.Frame.Height - 310, field.Frame.Width, field.Frame.Height);
			field.Activated += (sender, e) => {
//				passField.BecomeFirstResponder();
				validator.Validate(field.Text);					
			};
			View.AddSubview (field);

			XibField.PlaceholderText = "SOMETHING";

			XibField2.PlaceholderText = "SECURE";
			XibField2.IsSecure = true;
			XibField2.InputValidater = Validater.CreatePasswordValidater (5);
			XibField2.ValidateOnAction = true;
			XibField2.ValidateOnEndEditing = true;

			var toggle = new AppToggleSwitch ();

			toggle.Frame = new CoreGraphics.CGRect ((View.Frame.Width - toggle.Frame.Width) / 2,View.Frame.Height - 420, toggle.Frame.Width, toggle.Frame.Height);
			toggle.Toggled += (sender, e) => {
				Console.WriteLine(e.ToString());
			};
			View.AddSubview (toggle);

            AppPageIndicator.IndicatorCount = 5;
            AppPageIndicator.SelectionChanged += (s, e) => IndicatorIndex.StringValue = $"SelectedIndex: {e.NewSelectedIndex}";
		}

		public new MainWindow Window {
			get { return (MainWindow)base.Window; }
		}
	}
}
