// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;
using AppKit;


namespace Utilizr.MacKit.Demo
{
	[Register ("MainWindowController")]
	partial class MainWindowController
	{
		[Outlet]
		UI.AppPageIndicator AppPageIndicator { get; set; }

		[Outlet]
		UI.AppTextField IndicatorIndex { get; set; }

		[Outlet]
		NSView View { get; set; }

		[Outlet]
		UI.AppTextField XibField { get; set; }

		[Outlet]
		UI.AppTextField XibField2 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AppPageIndicator != null) {
				AppPageIndicator.Dispose ();
				AppPageIndicator = null;
			}

			if (IndicatorIndex != null) {
				IndicatorIndex.Dispose ();
				IndicatorIndex = null;
			}

			if (XibField != null) {
				XibField.Dispose ();
				XibField = null;
			}

			if (XibField2 != null) {
				XibField2.Dispose ();
				XibField2 = null;
			}

			if (View != null) {
				View.Dispose ();
				View = null;
			}
		}
	}
}
