﻿using System;
using System.Collections.Generic;
using System.Text;

namespace In_AppPurchaseListingCompressor
{

    class Program
    {
        static string[] lines;
        static CountryCodesMatcher matcher;

        static string testText = "Albania $74.39\nAlgeria $61.99\nAngola $61.99\nAnguilla $61.99\nAntigua and Barbuda $61.99\nArgentina $61.99\nArmenia $61.99\nAustralia $97.99\nAustria 65,99 €\nAzerbaijan $61.99\nBahamas $61.99\nBahrain $61.99\nBarbados $61.99\nBelarus $61.99\nBelgium 65,99 €\nBelize $61.99\nBenin $61.99\nBermuda $61.99\nBhutan $61.99\nBolivia $61.99\nBotswana $61.99\nBrazil R$ 234,90\nBrunei Darussalam $61.99\nBulgaria 127,99 лв\nBurkina Faso $61.99\nCambodia $61.99\nCanada $83.99\nCape Verde $61.99\nCayman Islands $61.99\nChad $61.99\nChile $42.900\nChina ¥438.00\nColombia $199.900,00\nCongo, Republic of $61.99\nCosta Rica $61.99\nCroatia 504,99 kn\nCyprus 65,99 €\nCzech Republic 1 690,00 Kč\nDenmark 509,00 kr\nDominica $61.99\nDominican Republic $61.99\nEcuador $61.99\nEgypt EGP1,249.99\nEl Salvador $61.99\nEstonia 65,99 €\nFiji $61.99\nFinland 65,99 €\nFrance 65,99 €\nGambia $61.99\nGermany 65,99 €\nGhana $61.99\nGreece 65,99 €\nGrenada $61.99\nGuatemala $61.99\nGuinea-Bissau $61.99\nGuyana $61.99\nHonduras $61.99\nHong Kong HK$ 488.00\nHungary 22 290 Ft\nIceland $76.87\nIndia Rs 5,100\nIndonesia Rp 899ribu\nIreland 65,99 €\nIsrael NIS 229.90\nItaly 65,99 €\nJamaica $61.99\nJapan ¥6,800\nJordan $61.99\nKazakhstan 23,990.00₸\nKenya $61.99\nKorea, Republic of ￦76,000\nKuwait $61.99\nKyrgyzstan $61.99\nLao People's Democratic Republic $61.99\nLatvia 65,99 €\nLebanon $61.99\nLiberia $61.99\nLithuania 65,99 €\nLuxembourg 65,99 €\nMacau $61.99\nMacedonia, The Former Yugoslav Republic of $61.99\nMadagascar $61.99\nMalawi $61.99\nMalaysia RM254.90\nMali $61.99\nMalta 65,99 €\nMauritania $61.99\nMauritius $61.99\nMexico $1,209.00\nMicronesia, Federated States of $61.99\nMoldova, Republic of $61.99\nMongolia $61.99\nMontserrat $61.99\nMozambique $61.99\nNamibia $61.99\nNepal $61.99\nNetherlands 65,99 €\nNew Zealand $106.99\nNicaragua $61.99\nNiger $61.99\nNigeria ₦ 22,900.00\nNorway 679,00 kr\nOman $61.99\nPakistan Rs 8,700.00\nPalau $61.99\nPanama $61.99\nPapua New Guinea $61.99\nParaguay $61.99\nPeru S/ 209.90\nPhilippines ₱ 3,290.00\nPoland 289,99 zł\nPortugal 65,99 €\nQatar 224.99 QAR\nRomania 304,99 lei\nRussia 5 090 р.\nSaint Lucia $61.99\nSão Tomé and Príncipe $61.99\nSaudi Arabia SR 244.99\nSenegal $61.99\nSeychelles $61.99\nSierra Leone $61.99\nSingapore S$ 84.98\nSlovakia 65,99 €\nSlovenia 65,99 €\nSolomon Islands $61.99\nSouth Africa R1,049.99\nSpain 65,99 €\nSri Lanka $61.99\nSt. Kitts and Nevis $61.99\nSt. Vincent and The Grenadines $61.99\nSuriname $61.99\nSwaziland $61.99\nSweden 695,00 kr\nSwitzerland CHF 66.00\nTaiwan NT$ 1,990\nTajikistan $61.99\nTanzania, United Republic of 142,900.00 TZS\nThailand ฿2,000.00\nTrinidad and Tobago $61.99\nTunisia $61.99\nTurkey 389,99 TL\nTurkmenistan $61.99\nTurks and Caicos $61.99\nUganda $61.99\nUkraine $61.99\nUnited Arab Emirates AED 239.99\nUnited Kingdom £58.99\nUnited States $61.99\nUruguay $61.99\nUzbekistan $61.99\nVenezuela $61.99\nVietnam 1.449.000đ\nVirgin Islands, British $61.99\nYemen $61.99\nZimbabwe $61.99";

        static void Main(string[] args)
        {
            args = new string[] { testText };
            matcher = new CountryCodesMatcher();
            if (args?.Length <= 0 )
            {
                Console.WriteLine("No Input Provided"); 
            }
            string rawtext = args[0];
            if (string.IsNullOrEmpty(rawtext))
                Console.WriteLine("No Input Provided");
            lines = rawtext.Split(new[] { "\r\n", "\r", "\n" },StringSplitOptions.None);
            if(lines.Length <=0)
                Console.WriteLine("No Lines of text found");

            foreach (var line in lines)
            {
                if (line.Length <= 0) continue;
                var l = line?.Trim();
                var country = GetCountry(l);
                //Console.WriteLine($"Extracted Country: {country}");
                var res = matcher.MatchAndReplace(country);
                var code = res.Item2;
                if (string.IsNullOrEmpty(code))
                    Console.WriteLine($"{country} : {l.Replace(country, code + " ")}");
                else
                    Console.WriteLine($"{l.Replace(country, code + " ")}");

                //Console.WriteLine($"{l} : {l.Replace(country, code+" ")}");
            }
        }

        static string GetCountry(string line)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < line.Length; i++)
            {
                if (char.IsSymbol(line[i]) || char.IsDigit(line[i]))
                    break;
                sb.Append(line[i]);
            }
            return sb.ToString();
        }
    }
}
