﻿using System;
namespace In_AppPurchaseListingCompressor
{
    public class CountryCodesMatcher
    {
        public CountryCodesMatcher()
        {
        }

        public Tuple<bool, string> MatchAndReplace(string _address_country_id)
        {
            if (_address_country_id.Containss("Andorra"))
                return new Tuple<bool, string>(true, "AD");
            if (_address_country_id.Containss("Emirates"))
                return new Tuple<bool, string>(true, "AE");
            if (_address_country_id.Containss("Afghanistan"))
                return new Tuple<bool, string>(true, "AF");
            if (_address_country_id.Containss("Antigua"))
                return new Tuple<bool, string>(true, "AG");
            if (_address_country_id.Containss("Anguilla"))
                return new Tuple<bool, string>(true, "AI");
            if (_address_country_id.Containss("Albania"))
                return new Tuple<bool, string>(true, "AL");
            if (_address_country_id.Containss("Armenia"))
                return new Tuple<bool, string>(true, "AM");
            if (_address_country_id.Containss("Angola"))
                return new Tuple<bool, string>(true, "AO");
            if (_address_country_id.Containss("Antarctica"))
                return new Tuple<bool, string>(true, "AQ");
            if (_address_country_id.Containss("Argentina"))
                return new Tuple<bool, string>(true, "AR");
            if (_address_country_id.Containss("Samoa"))
                return new Tuple<bool, string>(true, "AS");
            if (_address_country_id.Containss("Austria"))
                return new Tuple<bool, string>(true, "AT");
            if (_address_country_id.Containss("Australia"))
                return new Tuple<bool, string>(true, "AU");
            if (_address_country_id.Containss("Aruba"))
                return new Tuple<bool, string>(true, "AW");
            if (_address_country_id.Containss("Åland"))
                return new Tuple<bool, string>(true, "AX");
            if (_address_country_id.Containss("Azerbaijan"))
                return new Tuple<bool, string>(true, "AZ");
            if (_address_country_id.Containss("Bosnia"))
                return new Tuple<bool, string>(true, "BA");
            if (_address_country_id.Containss("Barbados"))
                return new Tuple<bool, string>(true, "BB");
            if (_address_country_id.Containss("Bangladesh"))
                return new Tuple<bool, string>(true, "BD");
            if (_address_country_id.Containss("Belgium"))
                return new Tuple<bool, string>(true, "BE");
            if (_address_country_id.Containss("Burkina"))
                return new Tuple<bool, string>(true, "BF");
            if (_address_country_id.Containss("Bulgaria"))
                return new Tuple<bool, string>(true, "BG");
            if (_address_country_id.Containss("Bahrain"))
                return new Tuple<bool, string>(true, "BH");
            if (_address_country_id.Containss("Burundi"))
                return new Tuple<bool, string>(true, "BI");
            if (_address_country_id.Containss("Benin"))
                return new Tuple<bool, string>(true, "BJ");
            if (_address_country_id.Containss("Barthélemy"))
                return new Tuple<bool, string>(true, "BL");
            if (_address_country_id.Containss("Bermuda"))
                return new Tuple<bool, string>(true, "BM");
            if (_address_country_id.Containss("Brunei"))
                return new Tuple<bool, string>(true, "BN");
            if (_address_country_id.Containss("Bolivia"))
                return new Tuple<bool, string>(true, "BO");
            if (_address_country_id.Containss("Bonaire"))
                return new Tuple<bool, string>(true, "BQ");
            if (_address_country_id.Containss("Brazil"))
                return new Tuple<bool, string>(true, "BR");
            if (_address_country_id.Containss("Bahamas"))
                return new Tuple<bool, string>(true, "BS");
            if (_address_country_id.Containss("Bhutan"))
                return new Tuple<bool, string>(true, "BT");
            if (_address_country_id.Containss("Bouvet"))
                return new Tuple<bool, string>(true, "BV");
            if (_address_country_id.Containss("Botswana"))
                return new Tuple<bool, string>(true, "BW");
            if (_address_country_id.Containss("Belarus"))
                return new Tuple<bool, string>(true, "BY");
            if (_address_country_id.Containss("Belize"))
                return new Tuple<bool, string>(true, "BZ");
            if (_address_country_id.Containss("Canada"))
                return new Tuple<bool, string>(true, "CA");
            if (_address_country_id.Containss("Cocos (Keeling) Islands"))
                return new Tuple<bool, string>(true, "CC");
            if (_address_country_id.Containss("Congo, the Democratic Republic of the"))
                return new Tuple<bool, string>(true, "CD");
            if (_address_country_id.Containss("Central African Republic"))
                return new Tuple<bool, string>(true, "CF");
            if (_address_country_id.Containss("Congo"))
                return new Tuple<bool, string>(true, "CG");
            if (_address_country_id.Containss("Switzerland"))
                return new Tuple<bool, string>(true, "CH");
            if (_address_country_id.Containss("Ivoire"))
                return new Tuple<bool, string>(true, "CI");
            if (_address_country_id.Containss("Cook Islands"))
                return new Tuple<bool, string>(true, "CK");
            if (_address_country_id.Containss("Chile"))
                return new Tuple<bool, string>(true, "CL");
            if (_address_country_id.Containss("Cameroon"))
                return new Tuple<bool, string>(true, "CM");
            if (_address_country_id.Containss("China"))
                return new Tuple<bool, string>(true, "CN");
            if (_address_country_id.Containss("Colombia"))
                return new Tuple<bool, string>(true, "CO");
            if (_address_country_id.Containss("Costa Rica"))
                return new Tuple<bool, string>(true, "CR");
            if (_address_country_id.Containss("Cuba"))
                return new Tuple<bool, string>(true, "CU");
            if (_address_country_id.Containss("Verde")) return new Tuple<bool, string>(true, "CV");
            if (_address_country_id.Containss("Curaçao")) return new Tuple<bool, string>(true, "CW");
            if (_address_country_id.Containss("Christmas")) return new Tuple<bool, string>(true, "CX");
            if (_address_country_id.Containss("Cyprus")) return new Tuple<bool, string>(true, "CY");
            if (_address_country_id.Containss("Czech")) return new Tuple<bool, string>(true, "CZ");
            if (_address_country_id.Containss("Germany")) return new Tuple<bool, string>(true, "DE");
            if (_address_country_id.Containss("Djibouti")) return new Tuple<bool, string>(true, "DJ");
            if (_address_country_id.Containss("Denmark")) return new Tuple<bool, string>(true, "DK");
            if (_address_country_id.Containss("Dominica")) return new Tuple<bool, string>(true, "DM");
            if (_address_country_id.Containss("Dominican")) return new Tuple<bool, string>(true, "DO");
            if (_address_country_id.Containss("Algeria")) return new Tuple<bool, string>(true, "DZ");
            if (_address_country_id.Containss("Ecuador")) return new Tuple<bool, string>(true, "EC");
            if (_address_country_id.Containss("Estonia")) return new Tuple<bool, string>(true, "EE");
            if (_address_country_id.Containss("Egypt")) return new Tuple<bool, string>(true, "EG");
            if (_address_country_id.Containss("Western")) return new Tuple<bool, string>(true, "EH");
            if (_address_country_id.Containss("Eritrea")) return new Tuple<bool, string>(true, "ER");
            if (_address_country_id.Containss("Spain")) return new Tuple<bool, string>(true, "ES");
            if (_address_country_id.Containss("Ethiopia")) return new Tuple<bool, string>(true, "ET");
            if (_address_country_id.Containss("Finland")) return new Tuple<bool, string>(true, "FI");
            if (_address_country_id.Containss("Fiji")) return new Tuple<bool, string>(true, "FJ");
            if (_address_country_id.Containss("Falkland")) return new Tuple<bool, string>(true, "FK");
            if (_address_country_id.Containss("Micronesia")) return new Tuple<bool, string>(true, "FM");
            if (_address_country_id.Containss("Faroe")) return new Tuple<bool, string>(true, "FO");
            if (_address_country_id.Containss("France")) return new Tuple<bool, string>(true, "FR");
            if (_address_country_id.Containss("Gabon")) return new Tuple<bool, string>(true, "GA");
            if (_address_country_id.Containss("United Kingdom") || _address_country_id.Containss("Britain"))
                return new Tuple<bool, string>(true, "GB");
            if (_address_country_id.Containss("Grenada")) return new Tuple<bool, string>(true, "GD");
            if (_address_country_id.Containss("Georgia")) return new Tuple<bool, string>(true, "GE");
            if (_address_country_id.Containss("Guiana")) return new Tuple<bool, string>(true, "GF");
            if (_address_country_id.Containss("Guernsey")) return new Tuple<bool, string>(true, "GG");
            if (_address_country_id.Containss("Ghana")) return new Tuple<bool, string>(true, "GH");
            if (_address_country_id.Containss("Gibraltar")) return new Tuple<bool, string>(true, "GI");
            if (_address_country_id.Containss("Greenland")) return new Tuple<bool, string>(true, "GL");
            if (_address_country_id.Containss("Gambia")) return new Tuple<bool, string>(true, "GM");
            if (_address_country_id.Containss("Guinea")) return new Tuple<bool, string>(true, "GN");
            if (_address_country_id.Containss("Guadeloupe")) return new Tuple<bool, string>(true, "GP");
            if (_address_country_id.Containss("Equatorial%Guinea")) return new Tuple<bool, string>(true, "GQ");
            if (_address_country_id.Containss("Greece")) return new Tuple<bool, string>(true, "GR");
            if (_address_country_id.Containss("Georgia%Sandwich")) return new Tuple<bool, string>(true, "GS");
            if (_address_country_id.Containss("Guatemala")) return new Tuple<bool, string>(true, "GT");
            if (_address_country_id.Containss("Guam")) return new Tuple<bool, string>(true, "GU");
            if (_address_country_id.Containss("Guinea-Bissau")) return new Tuple<bool, string>(true, "GW");
            if (_address_country_id.Containss("Guyana")) return new Tuple<bool, string>(true, "GY");
            if (_address_country_id.Containss("Hong Kong")) return new Tuple<bool, string>(true, "HK");
            if (_address_country_id.Containss("McDonald")) return new Tuple<bool, string>(true, "HM");
            if (_address_country_id.Containss("Honduras")) return new Tuple<bool, string>(true, "HN");
            if (_address_country_id.Containss("Croatia")) return new Tuple<bool, string>(true, "HR");
            if (_address_country_id.Containss("Haiti")) return new Tuple<bool, string>(true, "HT");
            if (_address_country_id.Containss("Hungary")) return new Tuple<bool, string>(true, "HU");
            if (_address_country_id.Containss("Indonesia")) return new Tuple<bool, string>(true, "ID");
            if (_address_country_id.Containss("Ireland")) return new Tuple<bool, string>(true, "IE");
            if (_address_country_id.Containss("Israel")) return new Tuple<bool, string>(true, "IL");
            if (_address_country_id.Containss("Isle of Man")) return new Tuple<bool, string>(true, "IM");
            if (_address_country_id.Containss("India")) return new Tuple<bool, string>(true, "IN");
            if (_address_country_id.Containss("British%Territory")) return new Tuple<bool, string>(true, "IO");
            if (_address_country_id.Containss("Iraq")) return new Tuple<bool, string>(true, "IQ");
            if (_address_country_id.Containss("Iran")) return new Tuple<bool, string>(true, "IR");
            if (_address_country_id.Containss("Iceland")) return new Tuple<bool, string>(true, "IS");
            if (_address_country_id.Containss("Italy")) return new Tuple<bool, string>(true, "IT");
            if (_address_country_id.Containss("Jersey")) return new Tuple<bool, string>(true, "JE");
            if (_address_country_id.Containss("Jamaica")) return new Tuple<bool, string>(true, "JM");
            if (_address_country_id.Containss("Jordan")) return new Tuple<bool, string>(true, "JO");
            if (_address_country_id.Containss("Japan")) return new Tuple<bool, string>(true, "JP");
            if (_address_country_id.Containss("Kenya")) return new Tuple<bool, string>(true, "KE");
            if (_address_country_id.Containss("Kyrgyzstan")) return new Tuple<bool, string>(true, "KG");
            if (_address_country_id.Containss("Cambodia")) return new Tuple<bool, string>(true, "KH");
            if (_address_country_id.Containss("Kiribati")) return new Tuple<bool, string>(true, "KI");
            if (_address_country_id.Containss("Comoros")) return new Tuple<bool, string>(true, "KM");
            if (_address_country_id.Containss("Saint Kitts and Nevis")) return new Tuple<bool, string>(true, "KN");
            if (_address_country_id.Containss("St. Kitts and Nevis")) return new Tuple<bool, string>(true, "KN");
            if (_address_country_id.Containss("Korea")) return new Tuple<bool, string>(true, "KR");
            if (_address_country_id.Containss("Kuwait")) return new Tuple<bool, string>(true, "KW");
            if (_address_country_id.Containss("Cayman")) return new Tuple<bool, string>(true, "KY");
            if (_address_country_id.Containss("Kazakhstan")) return new Tuple<bool, string>(true, "KZ");
            if (_address_country_id.Containss("Lao People's Democratic Republic")) return new Tuple<bool, string>(true, "LA");
            if (_address_country_id.Containss("Lebanon")) return new Tuple<bool, string>(true, "LB");
            if (_address_country_id.Containss("Saint Lucia")) return new Tuple<bool, string>(true, "LC");
            if (_address_country_id.Containss("Liechtenstein")) return new Tuple<bool, string>(true, "LI");
            if (_address_country_id.Containss("Sri Lanka")) return new Tuple<bool, string>(true, "LK");
            if (_address_country_id.Containss("Liberia")) return new Tuple<bool, string>(true, "LR");
            if (_address_country_id.Containss("Lesotho")) return new Tuple<bool, string>(true, "LS");
            if (_address_country_id.Containss("Lithuania")) return new Tuple<bool, string>(true, "LT");
            if (_address_country_id.Containss("Luxembourg")) return new Tuple<bool, string>(true, "LU");
            if (_address_country_id.Containss("Latvia")) return new Tuple<bool, string>(true, "LV");
            if (_address_country_id.Containss("Libya")) return new Tuple<bool, string>(true, "LY");
            if (_address_country_id.Containss("Morocco")) return new Tuple<bool, string>(true, "MA");
            if (_address_country_id.Containss("Monaco")) return new Tuple<bool, string>(true, "MC");
            if (_address_country_id.Containss("Moldova")) return new Tuple<bool, string>(true, "MD");
            if (_address_country_id.Containss("Montenegro")) return new Tuple<bool, string>(true, "ME");
            if (_address_country_id.Containss("Martin")) return new Tuple<bool, string>(true, "MF");
            if (_address_country_id.Containss("Madagascar")) return new Tuple<bool, string>(true, "MG");
            if (_address_country_id.Containss("Marshall")) return new Tuple<bool, string>(true, "MH");
            if (_address_country_id.Containss("Macedonia")) return new Tuple<bool, string>(true, "MK");
            if (_address_country_id.Containss("Mali")) return new Tuple<bool, string>(true, "ML");
            if (_address_country_id.Containss("Myanmar")) return new Tuple<bool, string>(true, "MM");
            if (_address_country_id.Containss("Mongolia")) return new Tuple<bool, string>(true, "MN");
            if (_address_country_id.Containss("Macao") || _address_country_id.Containss("Macau")) return new Tuple<bool, string>(true, "MO");
            if (_address_country_id.Containss("Mariana")) return new Tuple<bool, string>(true, "MP");
            if (_address_country_id.Containss("Martinique")) return new Tuple<bool, string>(true, "MQ");
            if (_address_country_id.Containss("Mauritania")) return new Tuple<bool, string>(true, "MR");
            if (_address_country_id.Containss("Montserrat")) return new Tuple<bool, string>(true, "MS");
            if (_address_country_id.Containss("Malta")) return new Tuple<bool, string>(true, "MT");
            if (_address_country_id.Containss("Mauritius")) return new Tuple<bool, string>(true, "MU");
            if (_address_country_id.Containss("Maldives")) return new Tuple<bool, string>(true, "MV");
            if (_address_country_id.Containss("Malawi")) return new Tuple<bool, string>(true, "MW");
            if (_address_country_id.Containss("Mexico")) return new Tuple<bool, string>(true, "MX");
            if (_address_country_id.Containss("Malaysia")) return new Tuple<bool, string>(true, "MY");
            if (_address_country_id.Containss("Mozambique")) return new Tuple<bool, string>(true, "MZ");
            if (_address_country_id.Containss("Namibia")) return new Tuple<bool, string>(true, "NA");
            if (_address_country_id.Containss("Caledonia")) return new Tuple<bool, string>(true, "NC");
            if (_address_country_id.Containss("Niger")) return new Tuple<bool, string>(true, "NE");
            if (_address_country_id.Containss("Norfolk")) return new Tuple<bool, string>(true, "NF");
            if (_address_country_id.Containss("Nigeria")) return new Tuple<bool, string>(true, "NG");
            if (_address_country_id.Containss("Nicaragua")) return new Tuple<bool, string>(true, "NI");
            if (_address_country_id.Containss("Netherlands")) return new Tuple<bool, string>(true, "NL");
            if (_address_country_id.Containss("Norway")) return new Tuple<bool, string>(true, "NO");
            if (_address_country_id.Containss("Nepal")) return new Tuple<bool, string>(true, "NP");
            if (_address_country_id.Containss("Nauru")) return new Tuple<bool, string>(true, "NR");
            if (_address_country_id.Containss("Niue")) return new Tuple<bool, string>(true, "NU");
            if (_address_country_id.Containss("Zealand")) return new Tuple<bool, string>(true, "NZ");
            if (_address_country_id.Containss("Oman")) return new Tuple<bool, string>(true, "OM");
            if (_address_country_id.Containss("Panama")) return new Tuple<bool, string>(true, "PA");
            if (_address_country_id.Containss("Peru")) return new Tuple<bool, string>(true, "PE");
            if (_address_country_id.Containss("Polynesia")) return new Tuple<bool, string>(true, "PF");
            if (_address_country_id.Containss("Papua")) return new Tuple<bool, string>(true, "PG");
            if (_address_country_id.Containss("Philippines")) return new Tuple<bool, string>(true, "PH");
            if (_address_country_id.Containss("Pakistan")) return new Tuple<bool, string>(true, "PK");
            if (_address_country_id.Containss("Poland")) return new Tuple<bool, string>(true, "PL");
            if (_address_country_id.Containss("Pierre")) return new Tuple<bool, string>(true, "PM");
            if (_address_country_id.Containss("Pitcairn")) return new Tuple<bool, string>(true, "PN");
            if (_address_country_id.Containss("Puerto")) return new Tuple<bool, string>(true, "PR");
            if (_address_country_id.Containss("Palestine")) return new Tuple<bool, string>(true, "PS");
            if (_address_country_id.Containss("Portugal")) return new Tuple<bool, string>(true, "PT");
            if (_address_country_id.Containss("Palau")) return new Tuple<bool, string>(true, "PW");
            if (_address_country_id.Containss("Paraguay")) return new Tuple<bool, string>(true, "PY");
            if (_address_country_id.Containss("Qatar")) return new Tuple<bool, string>(true, "QA");
            if (_address_country_id.Containss("Réunion")) return new Tuple<bool, string>(true, "RE");
            if (_address_country_id.Containss("Romania")) return new Tuple<bool, string>(true, "RO");
            if (_address_country_id.Containss("Serbia")) return new Tuple<bool, string>(true, "RS");
            if (_address_country_id.Containss("Russia")) return new Tuple<bool, string>(true, "RU");
            if (_address_country_id.Containss("Rwanda")) return new Tuple<bool, string>(true, "RW");
            if (_address_country_id.Containss("Saudi")) return new Tuple<bool, string>(true, "SA");
            if (_address_country_id.Containss("Solomon")) return new Tuple<bool, string>(true, "SB");
            if (_address_country_id.Containss("Seychelles")) return new Tuple<bool, string>(true, "SC");
            if (_address_country_id.Containss("Sudan")) return new Tuple<bool, string>(true, "SD");
            if (_address_country_id.Containss("Sweden")) return new Tuple<bool, string>(true, "SE");
            if (_address_country_id.Containss("Singapore")) return new Tuple<bool, string>(true, "SG");
            if (_address_country_id.Containss("Helena")) return new Tuple<bool, string>(true, "SH");
            if (_address_country_id.Containss("Slovenia")) return new Tuple<bool, string>(true, "SI");
            if (_address_country_id.Containss("Svalbard")) return new Tuple<bool, string>(true, "SJ");
            if (_address_country_id.Containss("Slovakia")) return new Tuple<bool, string>(true, "SK");
            if (_address_country_id.Containss("Sierra")) return new Tuple<bool, string>(true, "SL");
            if (_address_country_id.Containss("Marino")) return new Tuple<bool, string>(true, "SM");
            if (_address_country_id.Containss("Senegal")) return new Tuple<bool, string>(true, "SN");
            if (_address_country_id.Containss("Somalia")) return new Tuple<bool, string>(true, "SO");
            if (_address_country_id.Containss("Suriname")) return new Tuple<bool, string>(true, "SR");
            if (_address_country_id.Containss("Sudan")) return new Tuple<bool, string>(true, "SS");
            if (_address_country_id.Containss("São Tomé and Príncipe")) return new Tuple<bool, string>(true, "ST");
            if (_address_country_id.Containss("Principe")) return new Tuple<bool, string>(true, "ST");
            if (_address_country_id.Containss("Salvador")) return new Tuple<bool, string>(true, "SV");
            if (_address_country_id.Containss("Sint Maarten")) return new Tuple<bool, string>(true, "SX");
            if (_address_country_id.Containss("Syrian")) return new Tuple<bool, string>(true, "SY");
            if (_address_country_id.Containss("Swaziland")) return new Tuple<bool, string>(true, "SZ");
            if (_address_country_id.Containss("Turks")) return new Tuple<bool, string>(true, "TC");
            if (_address_country_id.Containss("Chad")) return new Tuple<bool, string>(true, "TD");
            if (_address_country_id.Containss("French%Territories")) return new Tuple<bool, string>(true, "TF");
            if (_address_country_id.Containss("Togo")) return new Tuple<bool, string>(true, "TG");
            if (_address_country_id.Containss("Thailand")) return new Tuple<bool, string>(true, "TH");
            if (_address_country_id.Containss("Tajikistan")) return new Tuple<bool, string>(true, "TJ");
            if (_address_country_id.Containss("Tokelau")) return new Tuple<bool, string>(true, "TK");
            if (_address_country_id.Containss("Timor")) return new Tuple<bool, string>(true, "TL");
            if (_address_country_id.Containss("Turkmenistan")) return new Tuple<bool, string>(true, "TM");
            if (_address_country_id.Containss("Tunisia")) return new Tuple<bool, string>(true, "TN");
            if (_address_country_id.Containss("Tonga")) return new Tuple<bool, string>(true, "TO");
            if (_address_country_id.Containss("Turkey")) return new Tuple<bool, string>(true, "TR");
            if (_address_country_id.Containss("Trinidad")) return new Tuple<bool, string>(true, "TT");
            if (_address_country_id.Containss("Tuvalu")) return new Tuple<bool, string>(true, "TV");
            if (_address_country_id.Containss("Taiwan")) return new Tuple<bool, string>(true, "TW");
            if (_address_country_id.Containss("Tanzania")) return new Tuple<bool, string>(true, "TZ");
            if (_address_country_id.Containss("Ukraine")) return new Tuple<bool, string>(true, "UA");
            if (_address_country_id.Containss("Uganda")) return new Tuple<bool, string>(true, "UG");
            if (_address_country_id.Containss("U%S%Islands")) return new Tuple<bool, string>(true, "UM");
            if (_address_country_id.Containss("United States")) return new Tuple<bool, string>(true, "US");
            if (_address_country_id.Containss("Uruguay")) return new Tuple<bool, string>(true, "UY");
            if (_address_country_id.Containss("Uzbekistan")) return new Tuple<bool, string>(true, "UZ");
            if (_address_country_id.Containss("Holy")) return new Tuple<bool, string>(true, "VA");
            if (_address_country_id.Containss("Vincent")) return new Tuple<bool, string>(true, "VC");
            if (_address_country_id.Containss("Venezuela")) return new Tuple<bool, string>(true, "VE");
            if (_address_country_id.Containss("Virgin Islands, British")) return new Tuple<bool, string>(true, "VG");
            if (_address_country_id.Containss("Virgin%Islands%U%S")) return new Tuple<bool, string>(true, "VI");
            if (_address_country_id.Containss("VietNam")) return new Tuple<bool, string>(true, "VN");
            if (_address_country_id.Containss("Vanuatu")) return new Tuple<bool, string>(true, "VU");
            if (_address_country_id.Containss("Wallis%Futuna")) return new Tuple<bool, string>(true, "WF");
            if (_address_country_id.Containss("Samoa")) return new Tuple<bool, string>(true, "WS");
            if (_address_country_id.Containss("Yemen")) return new Tuple<bool, string>(true, "YE");
            if (_address_country_id.Containss("Mayotte")) return new Tuple<bool, string>(true, "YT");
            if (_address_country_id.Containss("Africa")) return new Tuple<bool, string>(true, "ZA");
            if (_address_country_id.Containss("Zambia"))
                return new Tuple<bool, string>(true, "ZM");
            if (_address_country_id.Containss("Zimbabwe"))
                return new Tuple<bool, string>(true, "ZW");
            return new Tuple<bool, string>(false, null);
        }
    }

    public static class Extension
    {
        public static bool Containss(this string s, string toCompare) => s.Contains(toCompare, StringComparison.InvariantCultureIgnoreCase);
    }
}
