﻿using System;
using System.Collections.Generic;
using Utilizr.Validation;

namespace Utilzr.WPF.Demo
{
    public class MainWindowViewModel: ParentViewModel
    {
        public Validater DemoNameValidater { get; set; }
        public Validater DemoEmailValidater { get; set; }
        public Validater DemoPasswordValidater { get; set; }

        private bool _showLightBox;

        public bool ShowLightBox
        {
            get { return _showLightBox; }
            set
            {
                _showLightBox = value;
                OnPropertyChanged(nameof(ShowLightBox));
            }
        }

        public MainWindowViewModel()
        {
            DemoNameValidater = Validater.CreateNameValidater(3);
            DemoEmailValidater = Validater.CreateEmailValidater();
            DemoPasswordValidater = Validater.CreatePasswordValidater(6);
        }
    }
}
