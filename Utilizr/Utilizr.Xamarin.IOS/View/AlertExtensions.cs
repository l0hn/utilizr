﻿
using System;
using GetText;
using UIKit;
using Utilizr.Logging;
namespace UI.Util
{
    public static class AlertExtensions
    {

        public static void Show(this UIAlertController alert, bool animated = true, Action completion = null) 
        {
            
            var window = UIApplication.SharedApplication?.Delegate?.GetWindow();
            if (window == null)
            {
                window = UIApplication.SharedApplication.KeyWindow;
            }

            if (window == null)
            {
                Log.Warning("Trying to show alert from extension without keyWindow");
                return;
            }
          
            window.ActiveViewController().PresentViewController(alert, animated, completion);
        }

        public static UIAlertController AddCancelItem(this UIAlertController controller, Action<UIAlertAction> action = null)
        {
            controller.AddAction(UIAlertAction.Create(L._("Cancel"), UIAlertActionStyle.Default, (obj) => action?.Invoke(obj)));
            return controller;
        }

        public static UIAlertController AddOKItem(this UIAlertController controller, Action<UIAlertAction> action = null)
		{
			controller.AddAction(UIAlertAction.Create(L._("OK"), UIAlertActionStyle.Default, (obj) => action?.Invoke(obj)));
            return controller;
		}

        public static UIViewController ActiveViewController(this UIWindow window)
        {
            return window.RootViewController.PresentedViewController ?? window.RootViewController;
        }
    }
}
