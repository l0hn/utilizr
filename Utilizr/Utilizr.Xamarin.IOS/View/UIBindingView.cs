﻿using System;
using UIKit;
using SecureSuite.Native.iOS.ViewModel;
using System.Collections.Generic;
using static SecureSuite.Native.iOS.ViewModel.BaseViewModel;

namespace Utilzr.Xamarin.IOS.View
{
    public class UIBindingView<T> : UIView where T : BaseViewModel
    {
        public UIBindingView(T viewModel)
		{
			_viewModel = viewModel;
			CallInit();
		}

		protected T _viewModel { get; private set; }

		public UIBindingView(Foundation.NSCoder coder) : base(coder)
        {
		}

		public UIBindingView(Foundation.NSObjectFlag t) : base(t)
        {
		}

		public UIBindingView(IntPtr handle) : base(handle)
        {
		}

        public UIBindingView()
        {
            CallInit();
        }

		public UIBindingView(CoreGraphics.CGRect frameRect) : base(frameRect)
        {
			CallInit();
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
			CallInit();
		}


		protected void Bind<S>(string propertyName, S view, Action<S> setter, bool initialSet = true) where S : UIView
		{
			var binding = _viewModel.Bind(propertyName, view, setter);
			_Bindings.Add(binding);
			if (initialSet)
			{
				setter?.Invoke(view);
			}
		}

		private bool _IsInit = false;
		private void CallInit()
		{
			if (_IsInit) return;
			_IsInit = true;

			Initialize();
		}

		protected virtual void Initialize()
		{

		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_Bindings != null)
			{
				foreach (var item in _Bindings)
				{
					_viewModel.Unbind(item);
				}
				_Bindings.Clear();
				_Bindings = null;
			}
		}

		private List<Binding> _Bindings = new List<Binding>();
	}
}

