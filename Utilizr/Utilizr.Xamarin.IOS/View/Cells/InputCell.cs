﻿using System;
using UIKit;
using UI.View;
using Utilizr.Validation;
using Extensions;
using GetText;
using ObjCRuntime;
using Foundation;
using MobileCoreServices;
using Utilzr.Xamarin.IOS.View.Cells;

namespace UI.View
{
    public class InputCell : UITableViewCell, IInputCell
    {
        public InputCell(Foundation.NSCoder coder) : base(coder)
        {
        }

        public InputCell(Foundation.NSObjectFlag t) : base(t)
        {
        }

        public InputCell(IntPtr handle) : base(handle)
        {
        }

        public event EventHandler<string> Changed;

        private UILabel TitleLabel { get; set; }
        private UITextField InputField { get; set; }

        public bool IsSecure
        {
            get { return InputField.SecureTextEntry; }
            set { InputField.SecureTextEntry = value; }
        }

        public string Title
        {
            get { return TitleLabel.Text; }
            set { TitleLabel.Text = value; }
        }

        public string Value
        {
            get { return InputField.Text; }
            set { InputField.Text = value; }
        }

        public Validater Validater { get; set; }

        public UITextAutocorrectionType AutocorrectionType
        {
            get { return InputField.AutocorrectionType; }
            set { InputField.AutocorrectionType = value; }
        }
		public UITextAutocapitalizationType AutocapitalizationType
		{
            get { return InputField.AutocapitalizationType; }
            set { InputField.AutocapitalizationType = value; }
		}

		public InputCell()
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;

            TitleLabel = new UILabel();
            ContentView.AddSubview(TitleLabel);

            TitleLabel.BuildConstraint().H().LeadingSpace(10).V().Center().Build();

            InputField = new UITextField();
            InputField.BorderStyle = UITextBorderStyle.None;
            InputField.TextAlignment = UITextAlignment.Right;
            ContentView.AddSubview(InputField);

            InputField.EditingChanged += (sender, e) =>
            {
                Changed?.Invoke(this, InputField.Text);
            };

            InputField.BuildConstraint().H().LeadingSpace(10, TitleLabel).Width(160).TrailingSpace(10).V().Center().Build();

        }

        public event EventHandler<InputCell> Copied;

        public ValidationResult Validate()
        {
            if (Validater == null) return new ValidationResult() { IsValid = true };
            return Validater.Validate(InputField.Text);
        }

        public bool Enabled
        {
            get
            {
                return InputField.Enabled;
            }
            set
            {
                InputField.Enabled = value;
            }
        }

        public UITableViewCell Cell => this;

        public override bool BecomeFirstResponder()
		{
            
            return InputField.BecomeFirstResponder();
		}

        public override bool ResignFirstResponder()
        {
            return InputField.ResignFirstResponder();
        }
    }
}
