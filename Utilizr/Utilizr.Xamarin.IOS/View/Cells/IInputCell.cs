﻿using System;
using UIKit;
using Utilizr.Validation;

namespace Utilzr.Xamarin.IOS.View.Cells
{
    public interface IInputCell
    {
		bool Enabled { get; set; }
		string Value { get; set; }
		UITableViewCell Cell { get; }
        ValidationResult Validate();

        event EventHandler<string> Changed;
    }
}
