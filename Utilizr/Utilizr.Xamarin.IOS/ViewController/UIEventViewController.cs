﻿using System;
using UIKit;
namespace Utilzr.Xamarin.IOS.ViewController
{
    public class UIEventViewController : UIViewController, IEventViewController
    {

        public UIEventViewController()
        {
        }

        public UIEventViewController(Foundation.NSCoder coder) : base(coder)
        {
        }

        public UIEventViewController(Foundation.NSObjectFlag t) : base(t)
        {
        }

        public UIEventViewController(IntPtr handle) : base(handle)
        {
        }

        public UIEventViewController(string nibName, Foundation.NSBundle bundle) : base(nibName, bundle)
        {
        }

        public event EventHandler<bool> WillAppear;
        public event EventHandler<bool> WillDisappear;
        public event EventHandler<bool> DidAppear;
        public event EventHandler<bool> DidDisappear;


        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            WillAppear?.Invoke(this, animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            WillDisappear?.Invoke(this, animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            DidAppear?.Invoke(this, animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            DidDisappear?.Invoke(this, animated);
        }

    }
}
