﻿using System;
using System.Collections.Generic;
using SecureSuite.Native.iOS.ViewModel;
using UIKit;
using static SecureSuite.Native.iOS.ViewModel.BaseViewModel;
using Utilizr.Logging;
using Foundation;
using Utilzr.Xamarin.Cocoa;

namespace Utilzr.Xamarin.IOS.ViewController
{
    public class UIBindingViewController<T> : UIEventViewController where T : BaseViewModel
    {


		private const string LogCat = "UIBindingViewController";
        public UIBindingViewController(T viewModel)
		{
			SetViewModel(viewModel);
		}

        public UIBindingViewController(T viewModel, string nibName, Foundation.NSBundle bundle = null) : base(nibName, bundle)
        {
            SetViewModel(viewModel);
		}

        public void SetViewModel(T viewModel)
        {
            if (_viewModel != null)
            {
				_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
			}
            if (viewModel != null)
            {
                _viewModel = viewModel;
                _viewModel.PropertyChanged += OnViewModelPropertyChanged;
            }
        }

		protected T _viewModel { get; private set; }

		public UIBindingViewController(Foundation.NSCoder coder) : base(coder)
        {
		}

		public UIBindingViewController(Foundation.NSObjectFlag t) : base(t)
        {
		}

		public UIBindingViewController(IntPtr handle) : base(handle)
        {
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			CallInit();

		}

        protected virtual void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

		protected void Bind<S>(string propertyName, S view, Action<S> setter, bool initialSet = true) where S : UIView
		{
			var binding = _viewModel.Bind(propertyName, view, setter);
			_Bindings.Add(binding);
			if (initialSet)
			{
				setter?.Invoke(view);
			}
		}

		protected void Bind(string propertyName, UILabel label, bool initialSet = true)
		{
            Bind(propertyName, label, (obj) =>
            {
                var prop = _viewModel.GetType().GetProperty(propertyName);
                if (prop.PropertyType == typeof(NSAttributedString))
                {
                    var attributed = prop.GetValue(_viewModel, null) as NSAttributedString;
                    obj.AttributedText = attributed.ReplaceFont(obj.Font, true);
				}
                else
                {
					obj.Text = prop.GetValue(_viewModel, null) as string ?? string.Empty;
				}
				obj.SizeToFit();
            }, initialSet);
		}

		protected void Bind(string propertyName, UIImageView imageView, bool initialSet = true)
		{
			Bind(propertyName, imageView, (obj) => obj.Image = _viewModel.GetType().GetProperty(propertyName).GetValue(_viewModel, null) as UIImage, initialSet);
		}


		private bool _IsInit = false;
		private void CallInit()
		{
            if (_viewModel == null)
            {
                Log.Info("Attempted to init viewcontroller without view model");
                return;
            }

			if (_IsInit) return;
			_IsInit = true;

			Initialize();
		}

		protected virtual void Initialize()
		{

		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_Bindings != null)
			{
				foreach (var item in _Bindings)
				{
					_viewModel.Unbind(item);
				}
				_Bindings.Clear();
				_Bindings = null;
			}
            SetViewModel(null);
		}


		private List<Binding> _Bindings = new List<Binding>();
    }

    public class UIBindingTableViewController<T> : UIEventTableViewController where T : BaseViewModel
	{


		private const string LogCat = "UIBindingViewController";
		public UIBindingTableViewController(T viewModel)
		{
            SetViewModel(viewModel);
		}

		public void SetViewModel(T viewModel)
		{
			if (_viewModel != null)
			{
				_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
			}
			if (viewModel != null)
			{
				_viewModel = viewModel;
				_viewModel.PropertyChanged += OnViewModelPropertyChanged;
			}
		}

		protected T _viewModel { get; private set; }

		public UIBindingTableViewController(Foundation.NSCoder coder) : base(coder)
		{
		}

		public UIBindingTableViewController(Foundation.NSObjectFlag t) : base(t)
		{
		}

		public UIBindingTableViewController(IntPtr handle) : base(handle)
		{
		}

        public UIBindingTableViewController(UITableViewStyle withStyle) : base(withStyle)
        {
        }


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			CallInit();

		}


		protected void Bind<S>(string propertyName, S view, Action<S> setter, bool initialSet = true) where S : UIView
		{
			var binding = _viewModel.Bind(propertyName, view, setter);
			_Bindings.Add(binding);
			if (initialSet)
			{
				setter?.Invoke(view);
			}
		}

		protected void Bind(string propertyName, UILabel label, bool initialSet = true)
		{
			Bind(propertyName, label, (obj) => obj.Text = _viewModel.GetType().GetProperty(propertyName).GetValue(_viewModel, null) as string ?? string.Empty, initialSet);
		}

		protected void Bind(string propertyName, UIImageView imageView, bool initialSet = true)
		{
			Bind(propertyName, imageView, (obj) => obj.Image = _viewModel.GetType().GetProperty(propertyName).GetValue(_viewModel, null) as UIImage, initialSet);
		}

		protected virtual void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{

		}


		private bool _IsInit = false;
		private void CallInit()
		{
			if (_viewModel == null)
			{
				Log.Info("Attempted to init viewcontroller without view model");
				return;
			}

			if (_IsInit) return;
			_IsInit = true;

			Initialize();
		}

		protected virtual void Initialize()
		{

		}

   

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_Bindings != null)
			{
				foreach (var item in _Bindings)
				{
					_viewModel.Unbind(item);
				}
				_Bindings.Clear();
				_Bindings = null;
			}
			SetViewModel(null);
		}


		private List<Binding> _Bindings = new List<Binding>();
	}
}
