﻿using System;
using UIKit;
namespace Utilzr.Xamarin.IOS.ViewController
{
    public class UIEventTableViewController : UITableViewController, IEventViewController
    {
       public UIEventTableViewController()
        {
        }

        public UIEventTableViewController(Foundation.NSCoder coder) : base(coder)
        {
        }

        public UIEventTableViewController(Foundation.NSObjectFlag t) : base(t)
        {
        }

        public UIEventTableViewController(IntPtr handle) : base(handle)
        {
        }

        public UIEventTableViewController(string nibName, Foundation.NSBundle bundle) : base(nibName, bundle)
        {
        }

        public UIEventTableViewController(UITableViewStyle withStyle) : base(withStyle)
        {
        }

		public event EventHandler<bool> WillAppear;
		public event EventHandler<bool> WillDisappear;
		public event EventHandler<bool> DidAppear;
		public event EventHandler<bool> DidDisappear;


		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			WillAppear?.Invoke(this, animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			WillDisappear?.Invoke(this, animated);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			DidAppear?.Invoke(this, animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			DidDisappear?.Invoke(this, animated);
		}

	}
}
