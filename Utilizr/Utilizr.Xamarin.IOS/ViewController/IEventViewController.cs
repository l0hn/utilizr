﻿using System;
namespace Utilzr.Xamarin.IOS.ViewController
{
    public interface IEventViewController
    {
        event EventHandler<bool> WillAppear;
        event EventHandler<bool> WillDisappear;
        event EventHandler<bool> DidAppear;
        event EventHandler<bool> DidDisappear;
    }
}
