﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Foundation;
using UIKit;
using Utilizr;

namespace SecureSuite.Native.iOS.ViewModel
{
    public class BaseViewModel : NSObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public RateLimiter UiRateLimiter = new RateLimiter(100);

        public void InvokeUIRateLimited(Action action, bool drop = true)
        {
            if (drop)
            {
                UiRateLimiter.ExecuteActionDrop(() =>
                {
                    InvokeOnMainThread(action);
                });
            }
            else
            {
                InvokeOnMainThread(action);
            }
        }

        private Dictionary<string, BindingContainer> Bindings = new Dictionary<string, BindingContainer>();

        public Binding Bind<T>(string propertyName, T view, Action<T> setter) where T : UIView
        {
            BindingContainer container = null;

            if (!Bindings.TryGetValue(propertyName, out container))
            {
                container = new BindingContainer(propertyName);
                Bindings[propertyName] = container;
            }

            var binding = new Binding((t) => setter?.Invoke(view), view) { PropertyName = propertyName };
            container.Callbacks.Add(binding);
            return binding;

        }

        public void Unbind(Binding binding)
        {
            BindingContainer container = null;

            if (Bindings.TryGetValue(binding.PropertyName, out container))
            {
                container.Callbacks.Remove(binding);
            }
        }

        public void Unbind(UIView view)
        {
            foreach (var item in Bindings)
            {
                item.Value.Callbacks.RemoveAll(a => a.View == view);
            }
        }

        private class BindingContainer
        {
            public BindingContainer(string propertyName)
            {
                PropertyName = propertyName;
            }
            string PropertyName;
            public readonly List<Binding> Callbacks = new List<Binding>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Bindings?.Clear();
        }

        public class Binding
        {
            public string PropertyName;
            public Action<UIView> Callback;
            public UIView View;
            public Binding(Action<UIView> callback, UIView view)
            {
                Callback = callback;
                View = view;
            }

            public void SendCallback()
            {
                Callback?.Invoke(View);
            }

            public override bool Equals(object obj)
            {
                // If parameter is null return false.
                if (obj == null)
                {
                    return false;
                }

                // If parameter cannot be cast to Point return false.
                Binding p = obj as Binding;
                if ((System.Object)p == null)
                {
                    return false;
                }

                // Return true if the fields match:
                return PropertyName == p.PropertyName && View == p.View;
            }
        }

        private string _logCat;
        protected string LogCat => string.IsNullOrEmpty(_logCat) ? (_logCat = GetType().Name) : _logCat;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            InvokeOnMainThread(() =>
            {
                foreach (var propertyName in propertyNames)
                {
                    FireBindings(propertyName);
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }
            });
        }
        public virtual void OnPropertyChanged(string propertyName)
        {
            InvokeOnMainThread(() =>
            {
                FireBindings(propertyName);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            });
        }

        private void FireBindings(string propertyName)
        {
            BindingContainer ct = null;
            if (Bindings.TryGetValue(propertyName, out ct))
            {
                foreach (var item in ct.Callbacks)
                {
                    item.SendCallback();
                }
            }
        }
    }
}
