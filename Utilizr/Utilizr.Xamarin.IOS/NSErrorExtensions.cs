﻿using System;
using Foundation;
using System.Linq;

namespace Extensions
{
    public static class NSErrorExtensions
    {
        public static Exception ToException(this NSError error)
        {
            if(error == null)
                return null;

            string flatUserInfo = string.Empty;
            if(error.UserInfo != null && error.UserInfo.Count > 0)
            {
                var entries = error.UserInfo.Select(p => string.Format("{0}={1}", p.Key, p.Value));
                flatUserInfo = string.Join(",", entries);
            }

            string exMessage = string.Format(
                "Domain: {0}, Code: {1}, Description: {2}, Reason: {3} RecoveryOptions: {4}, RecoverySuggestion: {5}, UserInfo: {6}",
               error.Domain,
               error.Code,
               error.LocalizedDescription,
               error.LocalizedFailureReason,
               error.LocalizedRecoveryOptions,
               error.LocalizedRecoverySuggestion,
               flatUserInfo
           );

            return new Exception(exMessage);
        }
    }
}

