﻿using System;
#if __IOS__
using NSColor = UIKit.UIColor;
#else
using AppKit;
#endif

namespace Cocoa.Style
{
    public static class ColorExtensions
    {
        public static string ToHex(this NSColor c)
        {
#if __IOS__
            return Color.FromNSColor(c).ToHex();
#else
           return string.Format("#{0:X2}{1:X2}{2:X2}", (int)(c.RedComponent * 0xFF), (int)(c.GreenComponent * 0xFF),
                                    (int)(c.BlueComponent * 0xFF));
#endif

        }

        public static string ToHex(this Color c)
        {
#if __IOS__
            var hex = string.Format("#{0:X2}{1:X2}{2:X2}", (int)(c.R * 0xFF), (int)(c.G * 0xFF),
                                    (int)(c.B * 0xFF));
#else
            var hex = string.Format("#{0:X2}{1:X2}{2:X2}", (int)(c.RedComponent * 0xFF), (int)(c.GreenComponent * 0xFF),
                                    (int)(c.BlueComponent * 0xFF));
#endif

            return hex;
        }
    }
}
