﻿using System;
using GetText;
using UIKit;
namespace SecureSuite.Native.iOS.UI.Util
{
    public static class UIBarButtonExtensions
    {

        public static UIBarButtonItem CreateCancelItem(Action clicked = null)
        {
            return new UIBarButtonItem(L._("Cancel"), UIBarButtonItemStyle.Plain, (sender, e) => clicked?.Invoke());
        }

        public static UIBarButtonItem CreateDoneItem(Action clicked = null)
        {
            return new UIBarButtonItem(L._("Done"), UIBarButtonItemStyle.Plain, (sender, e) => clicked?.Invoke());
        }
    }
}
