﻿using System;
using System.Drawing;
using CoreGraphics;
using UIKit;

namespace Utilzr.Xamarin.IOS.Util
{
    public static class UIImageExtensions
    {
        public static UIImage TintImage(this UIImage image, UIColor color)
        {
            UIImage output;
            UIGraphics.BeginImageContextWithOptions(image.Size, false, 0);
            //original.Draw(new CGRect(0, 0, size.Width, size.Height));

            using (CGContext context = UIGraphics.GetCurrentContext())
            {

                context.TranslateCTM(0, image.Size.Height);
                context.ScaleCTM(1.0f, -1.0f);

                var rect = new CGRect(0, 0, image.Size.Width, image.Size.Height);

                // draw image, (to get transparancy mask)
                context.SetBlendMode(CGBlendMode.Normal);
                context.DrawImage(rect, image.CGImage);

                // draw the color using the sourcein blend mode so its only draw on the non-transparent pixels
                context.SetBlendMode(CGBlendMode.SourceAtop);
                context.SetFillColor(color.CGColor);
                context.FillRect(rect);

                output = UIGraphics.GetImageFromCurrentImageContext();
            }

            UIGraphics.EndImageContext();
            return output;
        }

    }
}
